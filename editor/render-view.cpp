#include <engine/utility/logging.hpp>
#include "render-view.hpp"

#if !defined(_WIN32) && !defined(__APPLE__)
#include <qpa/qplatformnativeinterface.h>
#endif

#include <QPlatformSurfaceEvent>
#include <QGuiApplication>
#include <QWindow>
#include <QScreen>

namespace editor {

class surf_filter : public QObject {
public:
	render_view *view;
	int          timer_id = 0;

	inline surf_filter(render_view *view) noexcept : view(view) {}

	bool eventFilter(QObject *obj, QEvent *event) noexcept override
	{
		bool ret = QObject::eventFilter(obj, event);

		if (event->type() == QEvent::PlatformSurface) {
			QPlatformSurfaceEvent *surf_event = (QPlatformSurfaceEvent *)event;
			if (view && surf_event->surfaceEventType() == QPlatformSurfaceEvent::SurfaceAboutToBeDestroyed)
				view->destroy_display();
		} else if (event->type() == QEvent::Hide) {
			view->destroy_display();
		}

		return ret;
	}
};

render_view::render_view(QWidget *parent, Qt::WindowFlags flags) noexcept : QWidget(parent, flags)
{
	setAttribute(Qt::WA_PaintOnScreen);
	setAttribute(Qt::WA_StaticContents);
	setAttribute(Qt::WA_NoSystemBackground);
	setAttribute(Qt::WA_OpaquePaintEvent);
	setAttribute(Qt::WA_DontCreateNativeAncestors);
	setAttribute(Qt::WA_NativeWindow);

	auto size_changed = [this](QScreen *) {
		if (isVisible() && display) {
			QSize size = this->size();
			display->resize((uint32_t)size.width(), (uint32_t)size.height());
		}
	};

	connect(windowHandle(), &QWindow::screenChanged, size_changed);

	filter.reset(new surf_filter(this));
	windowHandle()->installEventFilter(filter.get());
}

render_view::~render_view() noexcept {}

void render_view::init() noexcept {}

bool render_view::create_display(bool now) noexcept
{
	if (display)
		return true;
	if (!now && !windowHandle()->isExposed())
		return false;

	crpg::graphics::display::info info = {};
#if defined(_WIN32)
	info.hwnd = (void *)winId();
#elif defined(__APPLE__)
#error "bla"
#else
	QPlatformNativeInterface *native = QGuiApplication::platformNativeInterface();

	const char *session_type = getenv("XDG_SESSION_TYPE");
	if (session_type && strcmp(session_type, "x11") == 0) {
		info.x11_display = (void *)native->nativeResourceForIntegration("display");
		info.x11_id      = (uint32_t)windowHandle()->winId();

	} else if (session_type && strcmp(session_type, "wayland") == 0) {
		info.wayland_display = (void *)native->nativeResourceForIntegration("display");
		info.wayland_surf    = (void *)native->nativeResourceForWindow("surface", windowHandle());
	}
#endif
	info.format = crpg::graphics::texture_format::rgba;

	QSize size = this->size();
	display    = crpg::graphics::display::create(info, (uint32_t)size.width(), (uint32_t)size.height());
	return !!display;
}

void render_view::destroy_display() noexcept
{
	{
		auto test = std::move(display);
	}
	failed = false;
}

bool render_view::load() noexcept
{
	if (failed)
		return false;
	if (!display && !create_display()) {
		failed = true;
		return false;
	}

	display->load();

	if (!initialized) {
		init();
		initialized = true;
	}

	return true;
}

void render_view::swap() noexcept
{
	if (display)
		display->swap();
}

void render_view::resizeEvent(QResizeEvent *event)
{
	if (display) {
		QSize size = this->size();
		display->resize((uint32_t)size.width(), (uint32_t)size.height());
	}

	QWidget::resizeEvent(event);
	emit resized();
}

void render_view::paintEvent(QPaintEvent *event)
{
	QWidget::paintEvent(event);
	emit painted();
}

QPaintEngine *render_view::paintEngine() const
{
	return nullptr;
}

/* ----------------------------------------------- */
} // namespace editor
