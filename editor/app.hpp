#pragma once

#include <QApplication>
#include <QJsonDocument>

namespace editor {

class app : public QApplication {
	Q_OBJECT

	QJsonDocument settings_;

public:
	app(int &argc, char **argv) noexcept;
	~app() noexcept;

	static inline app           *get() noexcept { return static_cast<app *>(qApp); }
	static inline QJsonDocument &settings() noexcept { return static_cast<app *>(qApp)->settings_; }
};
} // namespace editor
