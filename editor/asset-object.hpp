#pragma once

#include "asset-sprite.hpp"

#include <QStringList>
#include <QVariant>

#include <vector>

namespace crpg {
class serializer;
}

namespace editor {

struct object_property {
	QString                    name;
	crpg::object_property_type type = crpg::object_property_type::boolean;
	std::string                default_val;
	std::string                config_info;

	friend crpg::serializer &operator<<(crpg::serializer &s, const object_property &p) noexcept;
	friend crpg::serializer &operator>>(crpg::serializer &s, object_property &p) noexcept;
};

enum class object_type : uint32_t {
	box,
	point,
	sprite,
	boundary,
};

class object_asset : public asset {
	friend class asset_model;
	friend class asset;

	inline object_asset(const QString       &name__,
	                    const QString       &file_name_,
	                    const QIcon         &icon_,
	                    asset               *parent_,
	                    crpg::asset_type     type_,
	                    crpg::asset_category category_,
	                    crpg::type::asset_id id__ = 0) noexcept
	        : asset(name__, file_name_, icon_, parent_, type_, category_, id__)
	{
	}

	void save(const QString &path) noexcept override;
	void load() noexcept override;

	sprite_ref sprite_;

public:
	object_type                  obj_type = object_type::box;
	std::vector<object_property> properties;

	inline const sprite_ref &sprite() noexcept { return sprite_; }
	void                     set_sprite(sprite_ref &sprite) noexcept;
	void                     update_sprite() noexcept;

	inline int             cx() const noexcept { return sprite_ ? sprite_->frame_cx : 0; }
	inline int             cy() const noexcept { return sprite_ ? sprite_->frame_cy : 0; }
	inline int             tile_width() const noexcept { return sprite_ ? sprite_->tile_width : 1; }
	inline crpg::math::box box() const noexcept { return sprite_ ? sprite_->box() : crpg::math::box::zero(); }

	crpg::graphics::texture_ref       texture() noexcept;
	crpg::graphics::vertex_buffer_ref vb() noexcept;

	void open() noexcept override;

	void save_state(crpg::serializer &s) noexcept override;
	void load_state(crpg::serializer &s) noexcept override;

	void save_xref_properties(crpg::serializer &s) noexcept;
	void load_xref_properties(crpg::serializer &s) noexcept;
	void clear_row_in_xrefs(int row) noexcept;
	void insert_row_in_xrefs(int row) noexcept;
	void remove_row_in_xrefs(int row) noexcept;

	void remove_xrefs() noexcept override;
	void remove_sprite() noexcept;
};

using object_ref = asset_ref<object_asset>;

} // namespace editor
