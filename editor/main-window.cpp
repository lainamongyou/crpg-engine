#include <QFileDialog>
#include <QMessageBox>
#include <QCloseEvent>

#include <engine/utility/fserializer.hpp>
#include <engine/utility/logging.hpp>

#include "object-properties-widget.hpp"
#include "map-layers-widget.hpp"
#include "tile-selector.hpp"
#include "main-window.hpp"
#include "main-view.hpp"

#include "asset-browser.hpp"

#include "ui_main-window.h"

#include <DockManager.h>
#include <DockSplitter.h>
#include <DockAreaWidget.h>

using namespace ads;

namespace editor {

static main_window *current_ = nullptr;

main_window::main_window() : QMainWindow(nullptr), ui(new Ui_main_window), rm(crpg::resource_manager::create())
{
	current_ = this;
	setAttribute(Qt::WA_DeleteOnClose);
	setFocusPolicy(Qt::StrongFocus);

	using namespace crpg::graphics;
	renderer = renderer::create(nullptr, true);
	if (!renderer)
		throw crpg::strprintf("renderer::create failed");

	ui->setupUi(this);

	CDockManager::setConfigFlag(CDockManager::OpaqueSplitterResize, false);
	CDockManager::setConfigFlag(CDockManager::FocusHighlighting, true);

	dockman = new CDockManager(this);

	QWidget     *empty_area     = new QWidget();
	CDockWidget *central_widget = new CDockWidget("central_widget");
	central_widget->setWidget(empty_area);
	central_widget->setFeature(CDockWidget::NoTab, true);
	central_area = dockman->setCentralWidget(central_widget);

	ads::CDockWidget   *dock_map_layers = new CDockWidget(tr("Map Layers"), this);
	editor::map_layers *map_layers      = new editor::map_layers(dock_map_layers);
	dock_map_layers->setWidget(map_layers);
	dock_map_layers->setMinimumSizeHintMode(CDockWidget::MinimumSizeHintFromContent);
	map_layers->resize(150, 250);
	map_layers->setMinimumSize(150, 150);
	ads::CDockAreaWidget *left_area = dockman->addDockWidget(DockWidgetArea::LeftDockWidgetArea, dock_map_layers);

	auto splitter = ads::internal::findParent<ads::CDockSplitter *>(left_area);

	ads::CDockWidget                 *dock_obj_properties = new CDockWidget(tr("Object Properties"), this);
	editor::object_properties_widget *obj_properties = new editor::object_properties_widget(dock_obj_properties);
	dock_obj_properties->setWidget(obj_properties);
	dock_obj_properties->setMinimumSizeHintMode(CDockWidget::MinimumSizeHintFromContent);
	obj_properties->resize(150, 250);
	obj_properties->setMinimumSize(150, 150);
	dockman->addDockWidget(DockWidgetArea::BottomDockWidgetArea, dock_obj_properties, left_area);

	ads::CDockWidget     *dock_asset_browser = new CDockWidget(tr("Asset Browser"), this);
	asset_browser_widget *asset_browser      = new asset_browser_widget(dock_asset_browser);
	dock_asset_browser->setWidget(asset_browser);
	dock_asset_browser->setMinimumSizeHintMode(CDockWidget::MinimumSizeHintFromContent);
	asset_browser->resize(250, 250);
	asset_browser->setMinimumSize(150, 150);
	ads::CDockAreaWidget *right_area =
	        dockman->addDockWidget(DockWidgetArea::RightDockWidgetArea, dock_asset_browser);

	editor::tile_selector *tile_selector = new editor::tile_selector(tr("Tile Selector"), this);
	tile_selector->setMinimumSizeHintMode(CDockWidget::MinimumSizeHintFromContent);
	tile_selector->resize(250, 250);
	tile_selector->setMinimumSize(150, 150);
	dockman->addDockWidget(DockWidgetArea::BottomDockWidgetArea, tile_selector, right_area);

	splitter->setSizes({400, 0, 400});

	auto addDockToViewMenu = [&](CDockWidget *dock, bool showing = true) {
		QAction *action = ui->view_menu->addAction(dock->windowTitle());
		action->setCheckable(true);
		action->setChecked(showing);

		connect(dock, &CDockWidget::viewToggled, action, &QAction::setChecked);
		connect(action, &QAction::toggled, [dock](bool open) {
			dock->blockSignals(true);
			dock->toggleView(open);
			dock->blockSignals(false);
		});
	};

	addDockToViewMenu(dock_asset_browser);
	addDockToViewMenu(dock_map_layers);
	addDockToViewMenu(dock_obj_properties);
	addDockToViewMenu(tile_selector);
}

main_window::~main_window() noexcept
{
	current_ = nullptr;
}

main_window *main_window::get() noexcept
{
	return current_;
}

void main_window::add_document(ads::CDockWidget *dock) noexcept
{
	dockman->addDockWidgetTabToArea(dock, central_area);
}

main_window::confirm main_window::confirm_save() noexcept
{
	QMessageBox::StandardButton button;
	button = QMessageBox::question(this,
	                               tr("Unsaved Changed"),
	                               tr("There are unsaved changes.  Save Changes?"),
	                               QMessageBox::No | QMessageBox::Yes | QMessageBox::Cancel);

	if (button == QMessageBox::Yes)
		return confirm::yes;
	else if (button == QMessageBox::No)
		return confirm::no;
	else
		return confirm::cancel;
}

void main_window::closeEvent(QCloseEvent *event)
{
	if (modified_index != 0) {
		switch (confirm_save()) {
		case confirm::no:
			break;
		case confirm::yes:
			if (save())
				break;
		case confirm::cancel:
			event->ignore();
			return;
		}
	}

	QMainWindow::closeEvent(event);
}

void main_window::add_action_internal(const QString &name,
                                      undo_redo_cb   undo,
                                      undo_redo_cb   redo,
                                      undo_redo_cb   save_undo,
                                      undo_redo_cb   save_redo,
                                      bool           repeatable) noexcept
{
	undo_stack.add_action(name, undo, redo, save_undo, save_redo, repeatable);
	ui->redo->setText(tr("&Redo"));
	ui->undo->setText(tr("&Undo %1").arg(name));
	ui->undo->setEnabled(true);
	ui->redo->setEnabled(false);

	++modified_index;
}

void main_window::add_action_internal(const QString     &name,
                                      undo_redo_cb       undo,
                                      undo_redo_cb       redo,
                                      const std::string &undo_data,
                                      const std::string &redo_data,
                                      bool               repeatable) noexcept
{
	undo_stack.add_action(name, undo, redo, undo_data, redo_data, repeatable);
	ui->redo->setText(tr("&Redo"));
	ui->undo->setText(tr("&Undo %1").arg(name));
	ui->undo->setEnabled(true);
	ui->redo->setEnabled(false);

	++modified_index;
}

void main_window::on_exit_triggered() noexcept
{
	close();
}

void main_window::on_undo_triggered() noexcept
{
	QString undo, redo;
	undo_stack.undo(undo, redo);

	ui->undo->setText(undo.isEmpty() ? tr("&Undo") : tr("&Undo %1").arg(undo));
	ui->redo->setText(tr("&Redo %1").arg(redo));
	ui->redo->setEnabled(true);

	if (undo.isEmpty())
		ui->undo->setEnabled(false);

	--modified_index = 0;
}

void main_window::on_redo_triggered() noexcept
{
	QString undo, redo;
	undo_stack.redo(undo, redo);

	ui->undo->setText(tr("&Undo %1").arg(undo));
	ui->redo->setText(redo.isEmpty() ? tr("&Redo") : tr("&Redo %1").arg(redo));
	ui->undo->setEnabled(true);

	if (redo.isEmpty())
		ui->redo->setEnabled(false);

	++modified_index;
}

bool main_window::save() noexcept
{
	asset_manager::save();
	modified_index = 0;
	return true;
}

void main_window::on_save_triggered() noexcept
{
	save();
}

void main_window::show_message_internal(const QString &msg) noexcept
{
	ui->statusbar->showMessage(msg);
}

void main_window::clear_message_internal() noexcept
{
	ui->statusbar->clearMessage();
}

/* ---------------------------------------------- */
} // namespace editor
