#pragma once

#include "asset.hpp"

#include <engine/graphics/graphics.hpp>

namespace editor {

class tileset_asset : public asset {
	friend class asset_model;
	friend class asset;

	inline tileset_asset(const QString       &name__,
	                     const QString       &file_name_,
	                     const QIcon         &icon_,
	                     asset               *parent_,
	                     crpg::asset_type     type_,
	                     crpg::asset_category category_,
	                     crpg::type::asset_id id__ = 0) noexcept
	        : asset(name__, file_name_, icon_, parent_, type_, category_, id__)
	{
	}

	crpg::graphics::texture_ref load_tex() noexcept;

	void save(const QString &path) noexcept override;
	void load() noexcept override;

	void remove_xrefs() noexcept override;

	~tileset_asset() noexcept;

public:
	crpg::graphics::weak_texture_ref weak_texture;
	std::string                      ext;
	std::string                      data;
	uint32_t                         cx         = 0;
	uint32_t                         cy         = 0;
	uint32_t                         tile_width = 16; /* XXX */

	void open() noexcept override;
	void select() noexcept override;
	void load_state(crpg::serializer &s) noexcept override;
	void save_state(crpg::serializer &s) noexcept override;
	void update() noexcept override;

	crpg::graphics::texture_ref texture() noexcept;
};

using tileset_ref = asset_ref<tileset_asset>;

} // namespace editor
