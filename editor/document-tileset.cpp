#include <engine/utility/file-helpers.hpp>
#include <engine/utility/serializer.hpp>

#include "ui_document-tileset.h"
#include "document-tileset.hpp"
#include "asset-browser.hpp"
#include "asset-tileset.hpp"
#include "main-window.hpp"

#include <QGuiApplication>
#include <QDragEnterEvent>
#include <QDragLeaveEvent>
#include <QDragMoveEvent>
#include <QMessageBox>
#include <QDropEvent>
#include <QMimeData>
#include <QFileInfo>

using namespace crpg;

namespace editor {

document_tileset::document_tileset(QWidget *parent, tileset_asset *asset_) noexcept
        : QWidget(parent), ui(new Ui_document_tileset), asset(asset_)
{
	setAcceptDrops(true);

	ui->setupUi(this);
	ui->view->set_asset(asset);
}

document_tileset::~document_tileset() noexcept {}

void document_tileset::update_texture() noexcept
{
	ui->view->update_texture();
}

static const char *supported_image_file_exts[] = {"png", "mask"};

bool supported_image_file_type(const std::string &ext) noexcept
{
	for (const char *supported_ext : supported_image_file_exts) {
		if (ext == supported_ext) {
			return true;
		}
	}
	return false;
}

extern std::string image_to_mask(const QString &path) noexcept;

void document_tileset::dropEvent(QDropEvent *event)
{
	const QMimeData *md = event->mimeData();
	if (!md->hasUrls())
		return;

	QList<QUrl> urls = md->urls();

	QUrl      url  = urls[0];
	QString   file = url.toLocalFile();
	QFileInfo fi(file);

	if (!fi.exists())
		return;

	std::string ext = fi.suffix().toLower().toUtf8().constData();
	std::string data;

	Qt::KeyboardModifiers mods = QGuiApplication::keyboardModifiers();
	if (mods & Qt::AltModifier) {
		auto button = QMessageBox::question(main_window::get(),
		                                    tr("Convert to mask?"),
		                                    tr("Holding the Alt key converts the texture to a mask. "
		                                       "Convert to mask?"));
		if (button == QMessageBox::Yes) {
			data = image_to_mask(file);
			ext  = "mask";
		}
	}

	if (!supported_image_file_type(ext))
		return;

	if (asset->data.size()) {
		auto button = QMessageBox::question(main_window::get(),
		                                    tr("Overwrite texture?"),
		                                    tr("Are you sure you want to overwrite "
		                                       "the current tileset texture?"));
		if (button == QMessageBox::No)
			return;
	}

	if (data.empty())
		data = load_memory_from_file(file.toUtf8().constData());

	graphics::texture_ref tex = graphics::texture::create_from_memory(ext.c_str(), data);
	if (!tex) {
		QMessageBox::warning(
		        main_window::get(), tr("Invalid image file"), tr("Image file invalid or unsupported"));
		return;
	}

	std::vector<int> possible_tile_sizes;
	uint32_t         cx       = (uint32_t)tex->cx();
	uint32_t         cy       = (uint32_t)tex->cy();
	uint32_t         max_size = cy > cy ? (cx / 2) : (cy / 2);

	for (uint32_t w = 8; w < max_size; w += 8) {
		if (cx % w == 0 && cy % w == 0) {
			possible_tile_sizes.push_back(w);
		}
	}

	if (!possible_tile_sizes.size()) {
		QMessageBox::warning(main_window::get(),
		                     tr("Invalid texture size"),
		                     tr("No valid tile sizes found for the texture"));
		return;
	}

	/* XXX: In the future it'd be nice to base it upon whether the texture
	 * is actually in use */
	if (asset->data.size() && (cx != asset->cx || cy != asset->cy)) {
		QMessageBox::warning(main_window::get(),
		                     tr("Invalid image size"),
		                     tr("Cannot replace tileset image with an image size "
		                        "that is different than the previous image size"));
		return;
	}

	auto undo_redo = [](crpg::serializer &s) {
		type::asset_id id;
		s >> id;

		tileset_asset *asset = static_cast<tileset_asset *>(asset_manager::get_asset(id));
		asset->load_state(s);
		asset->select();
		asset->update();
	};
	auto save_undo = [&](crpg::serializer &s) {
		s << this->asset->id();
		this->asset->save_state(s);
	};
	auto save_redo = [&](crpg::serializer &s) {
		this->asset->data = data;
		this->asset->ext  = ext;
		this->asset->cx   = tex->cx();
		this->asset->cy   = tex->cy();

		s << this->asset->id();
		this->asset->save_state(s);
	};

	main_window::add_undo_action(
	        tr("change texture of tileset '%1'").arg(asset->name()), undo_redo, undo_redo, save_undo, save_redo);

	tex.reset();
	asset->update();
}

void document_tileset::dragEnterEvent(QDragEnterEvent *event)
{
	if (event->source() != nullptr) {
		event->setDropAction(Qt::IgnoreAction);
		return;
	}

	event->acceptProposedAction();
}

void document_tileset::dragLeaveEvent(QDragLeaveEvent *event)
{
	event->accept();
}

void document_tileset::dragMoveEvent(QDragMoveEvent *event)
{
	event->acceptProposedAction();
}

} // namespace editor
