#pragma once

#include <vector>
#include <memory>
#include <string>

#include <QAbstractItemModel>
#include <QTreeView>

#include <engine/types.hpp>

#include "asset.hpp"

namespace crpg {
class serializer;
}

namespace editor {

/* ========================================================================= */

class asset_model : public QAbstractItemModel {
	Q_OBJECT

	friend class asset_browser_widget;
	friend class asset;

	QIcon icon_category_maps;
	QIcon icon_category_objects;
	QIcon icon_category_sprites;
	QIcon icon_category_tilesets;
	QIcon icon_category_sounds;
	QIcon icon_category_music;

	QIcon icon_folder;
	QIcon icon_file;

	void create_new_project() noexcept;

	static QString get_first_available_name(asset *item, const QString &name) noexcept;

	QModelIndex create(const QModelIndex   &parent_idx,
	                   crpg::type::asset_id id,
	                   const QString       &name,
	                   bool                 folder) noexcept;
	void        add_new(const QString &default_name, bool folder) noexcept;

	void move_asset(const QModelIndex &src_parent_idx,
	                int                src_row,
	                const QModelIndex &dst_parent_idx,
	                int                dst_row) noexcept;

	void remove_id(crpg::type::asset_id id) noexcept;
	void rename_id(crpg::type::asset_id id, const QString &name) noexcept;

	void on_create(const QModelIndex &index) noexcept;
	void on_rename(const QModelIndex &index, const QString &prev_name) noexcept;
	void on_remove(const QModelIndex &index) noexcept;

	struct asset_save_data {
		crpg::type::asset_id id;
		crpg::type::asset_id parent_id;
		QString              name;
		std::string          data;
		bool                 folder;
	};

	friend crpg::serializer &operator<<(crpg::serializer &s, const asset_save_data &asd) noexcept;
	friend crpg::serializer &operator>>(crpg::serializer &s, asset_save_data &asd) noexcept;

	static void get_assets_recursive(asset *item, std::vector<asset_save_data> &assets) noexcept;

protected:
	std::unique_ptr<asset>                  root;
	std::map<crpg::type::asset_id, asset *> items;

	QVariant        data(const QModelIndex &index, int role) const override;
	bool            setData(const QModelIndex &index, const QVariant &val, int role) override;
	Qt::ItemFlags   flags(const QModelIndex &index) const override;
	Qt::DropActions supportedDropActions() const override;
	QModelIndex     index(int row, int column, const QModelIndex &parent) const override;
	QModelIndex     parent(const QModelIndex &index) const override;
	int             rowCount(const QModelIndex &parent) const override;
	int             columnCount(const QModelIndex &parent) const override;

	void  init() noexcept;
	QIcon get_icon_internal(crpg::asset_type type, crpg::asset_category category) noexcept;

public:
	asset_model(QObject *parent) noexcept;

public slots:
	void add_folder() noexcept;
	void add_tileset() noexcept;
	void add_sprite() noexcept;
	void add_object() noexcept;
	void add_map() noexcept;
	void add_sound() noexcept;
	void add_music() noexcept;
	void remove_item() noexcept;
	void rename_item() noexcept;
};

/* ========================================================================= */

class asset_manager : public asset_model {
	static asset_manager *current;

	asset *selected_asset_internal() const noexcept;

	void save_folder(asset *a) noexcept;

public:
	asset_manager(QObject *parent) noexcept;
	~asset_manager() noexcept;

	static inline asset *get_asset(crpg::type::asset_id id) noexcept { return get()->items[id]; }
	static inline asset *selected_asset() noexcept { return get()->selected_asset_internal(); }

	static void save() noexcept;

	static inline asset_manager *get() noexcept { return current; }

	static inline QIcon get_icon(crpg::asset_type type, crpg::asset_category category) noexcept
	{
		return get()->get_icon_internal(type, category);
	}
};

/* ========================================================================= */

class asset_browser_widget : public QTreeView {
	Q_OBJECT

	friend class asset_model;

	static asset_browser_widget *current;

	QString last_action_name;

	bool edit(const QModelIndex &index, QAbstractItemView::EditTrigger trigger, QEvent *event) override;
	void closeEditor(QWidget *editor, QAbstractItemDelegate::EndEditHint hint) override;
	void dropEvent(QDropEvent *event) override;

	std::function<void()> action_after_edit;

public:
	asset_browser_widget(QWidget *parent = nullptr) noexcept;
	~asset_browser_widget() noexcept;

	static inline asset_browser_widget *get() noexcept { return current; }
};

} // namespace editor
