#pragma once

#include "asset.hpp"

#include <engine/graphics/graphics.hpp>
#include <engine/math/vec2.hpp>
#include <engine/math/box.hpp>

#include <QImage>

namespace editor {

struct frame_info {
	uint32_t         start_vertex;
	uint32_t         duration_ms;
	crpg::math::vec2 min;
	crpg::math::vec2 max;
};

enum class sequence_type : uint32_t {
	still,
	forward,
	reverse,
	cycle,
};

struct sequence {
	sequence_type type;
	uint32_t      start;
	uint32_t      end;
};

class sprite_asset : public asset {
	friend class asset_model;
	friend class asset;

	inline sprite_asset(const QString       &name__,
	                    const QString       &file_name_,
	                    const QIcon         &icon_,
	                    asset               *parent_,
	                    crpg::asset_type     type_,
	                    crpg::asset_category category_,
	                    crpg::type::asset_id id__ = 0) noexcept
	        : asset(name__, file_name_, icon_, parent_, type_, category_, id__)
	{
	}

	void recalc_box() noexcept;

	crpg::graphics::texture_ref       load_tex() noexcept;
	crpg::graphics::vertex_buffer_ref load_vb() noexcept;

	crpg::orientation orientation_ = crpg::orientation::top_left;
	crpg::math::vec2  offset_      = crpg::math::vec2::zero();
	crpg::math::vec2  origin_      = crpg::math::vec2::zero(); /* calculated from the other two */

	/* adjusted for tile width */
	crpg::math::box box_ = crpg::math::box::zero();

	void save(const QString &path) noexcept override;
	void load() noexcept override;

public:
	crpg::graphics::weak_texture_ref       weak_texture;
	crpg::graphics::weak_vertex_buffer_ref weak_vb;
	std::vector<frame_info>                frames;
	std::map<std::string, sequence>        sequences;
	std::string                            ext;
	std::string                            data;
	uint32_t                               sheet_cx   = 0;
	uint32_t                               sheet_cy   = 0;
	uint32_t                               frame_cx   = 0;
	uint32_t                               frame_cy   = 0;
	uint32_t                               tile_width = 16; /* XXX */

	void set_orientation_offset(crpg::orientation orientation, const crpg::math::vec2 &offset) noexcept;

	inline crpg::orientation orientation() const noexcept { return orientation_; }
	inline crpg::math::vec2  offset() const noexcept { return offset_; }
	inline crpg::math::vec2  origin() const noexcept { return origin_; }
	inline crpg::math::box   box() const noexcept { return box_; }

	void open() noexcept override;
	void load_state(crpg::serializer &s) noexcept override;
	void save_state(crpg::serializer &s) noexcept override;
	void update() noexcept;

	void remove_xrefs() noexcept override;

	crpg::graphics::texture_ref       texture() noexcept;
	crpg::graphics::vertex_buffer_ref vb() noexcept;
};

using sprite_ref = asset_ref<sprite_asset>;

} // namespace editor
