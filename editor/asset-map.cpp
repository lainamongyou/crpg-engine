#include <engine/utility/fserializer.hpp>
#include <engine/utility/logging.hpp>
#include <engine/math/math-extra.hpp>
#include <engine/utility/dict.hpp>
#include <engine/types.hpp>

#include "asset-map.hpp"
#include "main-window.hpp"
#include "document-map.hpp"
#include "asset-sprite.hpp"
#include "asset-object.hpp"
#include "asset-tileset.hpp"
#include "asset-browser.hpp"
#include "object-properties-widget.hpp"

#include <DockWidget.h>

using namespace crpg;
using namespace ads;

namespace editor {

static map_layer *create_layer_type(crpg::map_layer_type type) noexcept
{
	switch (type) {
	case crpg::map_layer_type::folder:
		return new folder_layer;
	case crpg::map_layer_type::tileset:
		return new tileset_layer;
	case crpg::map_layer_type::sprite:
		return new sprite_layer;
	case crpg::map_layer_type::object:
		return new object_layer;
	case crpg::map_layer_type::collision:
		return new collision_layer;
	case crpg::map_layer_type::custom:
		return new map_layer;
	}

	return nullptr;
}

/* ================================================================================== */

void map_layer::save_state(crpg::serializer &) noexcept {}
void map_layer::load_state(crpg::serializer &) noexcept {}

void map_layer::save(crpg::serializer &s) noexcept
{
	crpg::type::layer_id parent_id = parent ? parent->id : 0;

	crpg::dict dict;
	dict.set("map_id", map_id);
	dict.set("type", type);
	dict.set("parent_id", parent_id);
	dict.set("name", name);
	dict.set("id", id);
	dict.set("blend", blend);
	dict.set("opacity", opacity);
	dict.set("visible", visible);
	s << dict;

	save_state(s);
}

void map_layer::load(crpg::serializer &s) noexcept
{
	load_state(s);
}

map_layer *map_layer::load_new(crpg::serializer &s) noexcept
{
	crpg::map_layer_type type;
	crpg::type::layer_id parent_id;
	crpg::type::asset_id map_id;

	crpg::dict dict(s);
	dict.get("map_id", map_id);
	dict.get("type", type);
	dict.get("parent_id", parent_id);

	map_asset *map   = static_cast<map_asset *>(asset_manager::get_asset(map_id));
	map_layer *layer = create_layer_type(type);
	if (!layer)
		return nullptr;

	layer->type   = type;
	layer->map_id = map_id;
	if (parent_id)
		layer->parent = static_cast<folder_layer *>(map->layers[parent_id].get());

	dict.get("name", layer->name);
	dict.get("id", layer->id);
	dict.get("blend", layer->blend);
	dict.get("opacity", layer->opacity);
	dict.get("visible", layer->visible);
	map->layers.emplace(layer->id, layer);

	layer->load(s);
	return layer;
}

/* ================================================================================== */

void folder_layer::save(crpg::serializer &s) noexcept
{
	map_layer::save(s);

	s << children.size();

	for (auto *layer : children)
		layer->save(s);
}

void folder_layer::load(crpg::serializer &s) noexcept
{
	map_layer::load(s);

	size_t size;
	s >> size;

	map_asset    *map = static_cast<map_asset *>(asset_manager::get_asset(map_id));
	document_map *doc = map->is_open() ? static_cast<document_map *>(map->document()) : nullptr;

	for (size_t i = 0; i < size; i++) {
		map_layer *new_layer = map_layer::load_new(s);
		children.push_back(new_layer);
		if (doc)
			doc->add_renderable_layer(new_layer);
	}

	if (doc)
		doc->update();
}

/* ================================================================================== */

crpg::serializer &operator<<(crpg::serializer &s, const tileset_data &tileset) noexcept
{
	crpg::dict dict;
	dict.set("id", tileset.id);
	dict.set("asset", tileset.asset);
	dict.set("refs", tileset.refs);
	return s << dict;
}

crpg::serializer &operator>>(crpg::serializer &s, tileset_data &tileset) noexcept
{
	crpg::dict dict(s);
	dict.get("id", tileset.id);
	dict.get("asset", tileset.asset);
	dict.get("refs", tileset.refs);
	return s;
}

void tileset_layer::save_state(crpg::serializer &s) noexcept
{
	crpg::dict dict;
	dict.set("tilesets", tilesets);
	dict.set("segments", segments);
	dict.set("tileset_id_counter", tileset_id_counter);
	s << dict;
}

void tileset_layer::load_state(crpg::serializer &s) noexcept
{
	crpg::dict dict(s);
	dict.get("tilesets", tilesets);
	dict.get("segments", segments);
	dict.get("tileset_id_counter", tileset_id_counter);
}

void tileset_layer::remove_tileset_from_segments(crpg::type::tileset_id id) noexcept
{
	std::vector<crpg::type::segment_id> segs_to_remove;

	for (auto &[seg_id, seg] : segments) {
		for (size_t y = 0; y < crpg::segment::width; y++) {
			for (size_t x = 0; x < crpg::segment::width; x++) {
				crpg::segment::tile &tile = seg.tiles[y][x];

				if (tile.tileset_id == id) {
					tile = {};
					seg.refs--;
				}
			}
		}

		if (!seg.refs)
			segs_to_remove.push_back(seg_id);
	}

	for (auto seg_id : segs_to_remove)
		segments.erase(seg_id);
}

void tileset_layer::remove_tileset(map_asset *map, tileset_asset *asset) noexcept
{
	for (auto &[id, tileset] : tilesets) {
		if (tileset.asset == asset) {
			remove_tileset_from_segments(id);
			tilesets.erase(id);
			break;
		}
	}

	if (map->is_open()) {
		document_map             *doc = static_cast<document_map *>(map->document());
		renderable_tileset_layer *rlayer =
		        static_cast<renderable_tileset_layer *>(doc->renderable_layers[id].get());

		doc->build_tileset_layer(this, rlayer);
	}
}

/* ================================================================================== */

crpg::serializer &operator<<(crpg::serializer &s, const sprite_data &sprite) noexcept
{
	crpg::dict dict;
	dict.set("id", sprite.id);
	dict.set("asset", sprite.asset);
	dict.set("pos", sprite.pos);
	dict.set("segments", sprite.segments);
	return s << dict;
}

crpg::serializer &operator>>(crpg::serializer &s, sprite_data &sprite) noexcept
{
	crpg::dict dict(s);
	dict.get("id", sprite.id);
	dict.get("asset", sprite.asset);
	dict.get("pos", sprite.pos);
	dict.get("segments", sprite.segments);
	return s;
}

crpg::serializer &operator<<(crpg::serializer &s, const sprite_segment &seg) noexcept
{
	crpg::dict dict;
	dict.set("sprites", seg.sprites);
	return s << dict;
}

crpg::serializer &operator>>(crpg::serializer &s, sprite_segment &seg) noexcept
{
	crpg::dict dict(s);
	dict.get("sprites", seg.sprites);
	return s;
}

void sprite_layer::save_state(crpg::serializer &s) noexcept
{
	crpg::dict dict;
	dict.set("sprites", sprites);
	dict.set("segments", segments);
	dict.set("sprite_id_counter", sprite_id_counter);
	s << dict;
}

void sprite_layer::load_state(crpg::serializer &s) noexcept
{
	crpg::dict dict(s);
	dict.get("sprites", sprites);
	dict.get("segments", segments);
	dict.get("sprite_id_counter", sprite_id_counter);
}

void sprite_layer::add_sprite_to_segments(sprite_data &sprite) noexcept
{
	math::box box = sprite.asset->box() + sprite.pos;

	map_tools::seg_pos tl_pos = get_map_segment_position(box.min);
	map_tools::seg_pos br_pos = get_map_segment_position(box.max);

	for (int16_t y = tl_pos.y; y <= br_pos.y; y++) {
		for (int16_t x = tl_pos.x; x <= br_pos.x; x++) {
			map_tools::seg_pos pos(x, y);

			sprite_segment &segment = segments[pos.hash];
			segment.sprites.push_back(sprite.id);
			sprite.segments.push_back(pos.hash);
		}
	}
}

void sprite_layer::remove_sprite_from_segments(sprite_data &sprite) noexcept
{
	for (type::segment_id hash : sprite.segments) {
		sprite_segment &seg = segments[hash];
		auto            it  = std::find(seg.sprites.begin(), seg.sprites.end(), sprite.id);
		seg.sprites.erase(it);
		if (!seg.sprites.size())
			segments.erase(hash);
	}

	sprite.segments.clear();
}

void sprite_layer::update_sprite(sprite_asset *asset) noexcept
{
	for (auto &[_, sprite] : sprites) {
		if (sprite.asset == asset) {
			remove_sprite_from_segments(sprite);
			add_sprite_to_segments(sprite);
		}
	}
}

void sprite_layer::remove_sprite(map_asset *map, sprite_asset *asset) noexcept
{
	std::vector<crpg::type::sprite_id> ids;

	for (auto &[id, sprite] : sprites) {
		if (sprite.asset == asset) {
			remove_sprite_from_segments(sprite);
			ids.push_back(id);
		}
	}

	for (auto id : ids)
		sprites.erase(id);

	if (map->is_open()) {
		document_map            *doc = static_cast<document_map *>(map->document());
		renderable_sprite_layer *rlayer =
		        static_cast<renderable_sprite_layer *>(doc->renderable_layers[id].get());

		doc->build_sprite_layer(this, rlayer);
	}
}

/* ================================================================================== */

crpg::serializer &operator<<(crpg::serializer &s, const object_data &object) noexcept
{
	crpg::dict dict;
	dict.set("id", object.id);
	dict.set("asset", object.asset);
	dict.set("pos", object.pos);
	dict.set("custom_size", object.custom_size);
	dict.set("properties", object.properties);
	dict.set("links", object.links);
	dict.set("segments", object.segments);
	return s << dict;
}

crpg::serializer &operator>>(crpg::serializer &s, object_data &object) noexcept
{
	crpg::dict dict(s);
	dict.get("id", object.id);
	dict.get("asset", object.asset);
	dict.get("pos", object.pos);
	dict.get("custom_size", object.custom_size);
	dict.get("properties", object.properties);
	dict.get("links", object.links);
	dict.get("segments", object.segments);
	object.asset->load_if_not_loaded();
	return s;
}

crpg::serializer &operator<<(crpg::serializer &s, const object_segment &seg) noexcept
{
	crpg::dict dict;
	dict.set("objects", seg.objects);
	return s << dict;
}

crpg::serializer &operator>>(crpg::serializer &s, object_segment &seg) noexcept
{
	crpg::dict dict(s);
	dict.get("objects", seg.objects);
	return s;
}

crpg::math::box object_data::box() const noexcept
{
	if (asset->obj_type == object_type::sprite && asset->sprite()) {
		crpg::math::box b = asset->sprite()->box();
		return b + pos;
	} else if (asset->obj_type == object_type::point) {
		return crpg::math::box(math::vec2(-0.4f, -0.4f), math::vec2(0.4f, 0.4f)) + pos;
	} else {
		crpg::math::box b(crpg::math::vec2::zero(), custom_size);
		return b + pos;
	}
}

void object_layer::save_state(crpg::serializer &s) noexcept
{
	crpg::dict dict;
	dict.set("objects", objects);
	dict.set("segments", segments);
	dict.set("object_id_counter", object_id_counter);
	s << dict;
}

void object_layer::load_state(crpg::serializer &s) noexcept
{
	crpg::dict dict(s);
	dict.get("objects", objects);
	dict.get("segments", segments);
	dict.get("object_id_counter", object_id_counter);
}

void object_layer::add_object_to_segments(object_data &object) noexcept
{
	math::box box = object.box();

	map_tools::seg_pos tl_pos = get_map_segment_position(box.min);
	map_tools::seg_pos br_pos = get_map_segment_position(box.max);

	for (int16_t y = tl_pos.y; y <= br_pos.y; y++) {
		for (int16_t x = tl_pos.x; x <= br_pos.x; x++) {
			map_tools::seg_pos pos(x, y);

			object_segment &segment = segments[pos.hash];
			segment.objects.push_back(object.id);
			object.segments.push_back(pos.hash);
		}
	}
}

void object_layer::remove_object_from_segments(object_data &object) noexcept
{
	for (type::segment_id hash : object.segments) {
		object_segment &seg = segments[hash];
		auto            it  = std::find(seg.objects.begin(), seg.objects.end(), object.id);
		seg.objects.erase(it);
		if (!seg.objects.size())
			segments.erase(hash);
	}

	object.segments.clear();
}

void object_layer::update_object_sprite(object_asset *asset) noexcept
{
	for (auto &[_, object] : objects) {
		if (object.asset == asset) {
			remove_object_from_segments(object);
			add_object_to_segments(object);

			map_asset *map = static_cast<map_asset *>(object.asset.owner());
			if (map->is_open()) {
				document_map            *doc = static_cast<document_map *>(map->document());
				renderable_sprite_layer *rlayer =
				        static_cast<renderable_sprite_layer *>(doc->renderable_layers[id].get());

				renderable_sprite &rsprite = rlayer->sprites[object.id];
				doc->build_object(rsprite, object);
			}
		}
	}
}

void object_layer::save_object_properties(object_asset *asset, crpg::serializer &s) noexcept
{
	for (auto &[_, object] : objects) {
		if (object.asset == asset) {
			s << object.properties;
		}
	}
}

void object_layer::load_object_properties(object_asset *asset, crpg::serializer &s) noexcept
{
	for (auto &[_, object] : objects) {
		if (object.asset == asset) {
			s >> object.properties;
		}
	}
}

void object_layer::clear_object_property(object_asset *asset, int row) noexcept
{
	for (auto &[_, object] : objects) {
		if (object.asset == asset) {
			object_properties_widget::remove_object(object);
			object.properties[row].clear();
		}
	}
}

void object_layer::remove_object_property(object_asset *asset, int row) noexcept
{
	for (auto &[_, object] : objects) {
		if (object.asset == asset) {
			object_properties_widget::remove_object(object);
			object.properties.erase(object.properties.begin() + row);
		}
	}
}

void object_layer::insert_object_property(object_asset *asset, int row) noexcept
{
	for (auto &[_, object] : objects) {
		if (object.asset == asset) {
			object.properties.insert(object.properties.begin() + row, std::string());
		}
	}
}

void object_layer::remove_object(map_asset *map, object_asset *asset) noexcept
{
	std::vector<crpg::type::object_id> ids;

	for (auto &[id, object] : objects) {
		if (object.asset == asset) {
			object_properties_widget::remove_object(object);
			remove_object_from_segments(object);
			ids.push_back(id);
		}
	}

	for (auto id : ids)
		objects.erase(id);

	if (map->is_open()) {
		document_map            *doc = static_cast<document_map *>(map->document());
		renderable_sprite_layer *rlayer =
		        static_cast<renderable_sprite_layer *>(doc->renderable_layers[id].get());

		doc->build_object_layer(this, rlayer);
	}
}

/* ================================================================================== */

void collision_layer::save_collision_data(crpg::serializer &s) noexcept
{
	std::unordered_map<type::tileset_id, type::tileset_id> remapped_ids;
	type::tileset_id                                       idx = 0;

	s << tilesets.size();

	remapped_ids[0] = 0;

	for (auto &[id, tileset] : tilesets) {
		tileset_asset *asset = tileset.asset;
		remapped_ids[id]     = ++idx;

		s << asset->id();
	}

	s << segments.size();

	for (auto &[id, seg] : segments) {
		collision_segment cseg;
		cseg.x = seg.x;
		cseg.y = seg.y;

		for (size_t y = 0; y < segment::width; y++) {
			for (size_t x = 0; x < segment::width; x++) {
				auto &tile  = seg.tiles[y][x];
				auto &ctile = cseg.tiles[y][x];

				ctile.tileset_id = remapped_ids[tile.tileset_id];
				ctile.val        = tile.vals[0];
			}
		}

		s << id << cseg;
	}
}

/* ================================================================================== */

map_asset::map_asset(const QString       &name__,
                     const QString       &file_name_,
                     const QIcon         &icon_,
                     asset               *parent_,
                     crpg::asset_type     type_,
                     crpg::asset_category category_,
                     crpg::type::asset_id id__) noexcept
        : asset(name__, file_name_, icon_, parent_, type_, category_, id__)
{
	root         = new folder_layer;
	root->id     = layer_id_counter++;
	root->map_id = id();
	root->type   = crpg::map_layer_type::folder;
	layers.emplace(root->id, root);
}

void map_asset::open() noexcept
{
	load_if_not_loaded();

	if (dock) {
		dock->raise();
		return;
	}

	main_window *window = main_window::get();
	dock                = new CDockWidget(name_, window);

	dock->setProperty("asset_category", (int)category);
	dock->setProperty("asset_id", (qulonglong)id());
	dock->setFeature(ads::CDockWidget::DockWidgetDeleteOnClose, true);
	dock->setWidget(new document_map(dock, this));
	window->add_document(dock);
}

void map_asset::save(const QString &path) noexcept
try {
	std::string        file = path.toStdString() + "/asset.data";
	crpg::ofserializer s(file);
	if (s.fail())
		throw crpg::strprintf("Failed to open '%s'", file.c_str());

	root->save(s);
	s << layer_id_counter;

	/* ---------------------------------------------- */
	/* pre-calculated engine data                     */

	file = path.toStdString() + "/engine.data";
	s.close();
	s.open(file);
	if (s.fail())
		throw crpg::strprintf("Failed to open '%s'", file.c_str());

	size_t num_collision_layers = 0;
	for (auto &pair : layers) {
		auto *layer = pair.second.get();
		if (layer->type == crpg::map_layer_type::collision)
			num_collision_layers++;
	}

	s << num_collision_layers;
	for (auto &pair : layers) {
		auto *layer = pair.second.get();
		if (layer->type == crpg::map_layer_type::collision) {
			s << layer->id;
			static_cast<collision_layer *>(layer)->save_collision_data(s);
		}
	}

} catch (const std::string &str) {
	crpg::error("%s: %s", __FUNCTION__, str.c_str());
}

void map_asset::load() noexcept
try {
	std::string        file = path().toStdString() + "/asset.data";
	crpg::ifserializer s(file);
	if (s.fail())
		throw crpg::strprintf("Failed to open '%s'", file.c_str());

	layers.clear();

	root = static_cast<folder_layer *>(map_layer::load_new(s));
	s >> layer_id_counter;

} catch (const std::string &str) {
	crpg::error("%s: %s", __FUNCTION__, str.c_str());
}

void map_asset::save_state(crpg::serializer &s) noexcept
{
	s << layers.size();

	for (auto &[id, layer] : layers) {
		s << id;
		layer->save_state(s);
	}

	asset::save_state(s);
}

void map_asset::load_state(crpg::serializer &s) noexcept
{
	size_t size;
	s >> size;

	for (size_t i = 0; i < size; i++) {
		type::layer_id id;
		s >> id;

		layers[id]->load_state(s);
	}

	asset::load_state(s);
}

bool map_asset::has_layer(const QString &name) const noexcept
{
	for (auto &pair : layers) {
		map_layer *layer = pair.second.get();
		if (layer->name.compare(name, Qt::CaseInsensitive) == 0) {
			return true;
		}
	}

	return false;
}

void map_asset::update_object_sprite(object_asset *asset) noexcept
{
	for (auto &[_, layer] : layers) {
		auto object_layer = dynamic_cast<editor::object_layer *>(layer.get());
		if (object_layer)
			object_layer->update_object_sprite(asset);
	}
}

void map_asset::update_sprite(sprite_asset *asset) noexcept
{
	for (auto &[_, layer] : layers) {
		auto sprite_layer = dynamic_cast<editor::sprite_layer *>(layer.get());
		if (sprite_layer)
			sprite_layer->update_sprite(asset);
	}
}

void map_asset::remove_tileset(tileset_asset *asset) noexcept
{
	for (auto &[_, layer] : layers) {
		auto *tileset_layer = dynamic_cast<editor::tileset_layer *>(layer.get());
		if (tileset_layer) {
			tileset_layer->remove_tileset(this, asset);
			continue;
		}

		auto *collision_layer = dynamic_cast<editor::collision_layer *>(layer.get());
		if (collision_layer) {
			collision_layer->remove_tileset(this, asset);
		}
	}
}

void map_asset::save_object_properties(object_asset *asset, crpg::serializer &s) noexcept
{
	for (auto &[_, layer] : layers) {
		auto *object_layer = dynamic_cast<editor::object_layer *>(layer.get());
		if (object_layer) {
			object_layer->save_object_properties(asset, s);
		}
	}
}

void map_asset::load_object_properties(object_asset *asset, crpg::serializer &s) noexcept
{
	for (auto &[_, layer] : layers) {
		auto *object_layer = dynamic_cast<editor::object_layer *>(layer.get());
		if (object_layer) {
			object_layer->load_object_properties(asset, s);
		}
	}
}

void map_asset::clear_object_property(object_asset *asset, int row) noexcept
{
	for (auto &[_, layer] : layers) {
		auto *object_layer = dynamic_cast<editor::object_layer *>(layer.get());
		if (object_layer) {
			object_layer->clear_object_property(asset, row);
		}
	}
}

void map_asset::remove_object_property(object_asset *asset, int row) noexcept
{
	for (auto &[_, layer] : layers) {
		auto *object_layer = dynamic_cast<editor::object_layer *>(layer.get());
		if (object_layer) {
			object_layer->remove_object_property(asset, row);
		}
	}
}

void map_asset::insert_object_property(object_asset *asset, int row) noexcept
{
	for (auto &[_, layer] : layers) {
		auto *object_layer = dynamic_cast<editor::object_layer *>(layer.get());
		if (object_layer) {
			object_layer->insert_object_property(asset, row);
		}
	}
}

void map_asset::remove_object(object_asset *asset) noexcept
{
	for (auto &[_, layer] : layers) {
		auto *object_layer = dynamic_cast<editor::object_layer *>(layer.get());
		if (object_layer) {
			object_layer->remove_object(this, asset);
		}
	}
}

void map_asset::remove_sprite(sprite_asset *asset) noexcept
{
	for (auto &[_, layer] : layers) {
		auto *sprite_layer = dynamic_cast<editor::sprite_layer *>(layer.get());
		if (sprite_layer) {
			sprite_layer->remove_sprite(this, asset);
		}
	}
}
} // namespace editor
