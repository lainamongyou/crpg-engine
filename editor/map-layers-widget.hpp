#pragma once

#include <QTreeView>
#include <QPointer>

#include <engine/types.hpp>

class QLabel;
class QCheckBox;
class QLineEdit;
class QHBoxLayout;
class Ui_map_layers_dock;

namespace ads {
class CDockWidget;
}

namespace editor {

struct renderable_map_layer;
struct folder_layer;
struct map_layer;
class document_map;
class map_layers;
class map_asset;

class map_layers_widget_item : public QWidget {
	Q_OBJECT

	friend class map_layers_model;
	friend class map_layers_widget;

	map_layer   *layer;
	QHBoxLayout *box_layout;
	QLabel      *icon_label;
	QLabel      *label;
	QCheckBox   *vis;

	QLineEdit            *editor;
	std::function<void()> action_after_edit;

	void on_toggle_visible(bool visible) noexcept;

	void edit(std::function<void()> func) noexcept;

	bool eventFilter(QObject *obj, QEvent *event) override;

private slots:
	void close_editor(bool save) noexcept;

public:
	map_layers_widget_item(QWidget *parent, map_layer *layer) noexcept;
};

class map_layers_model : public QAbstractItemModel {
	Q_OBJECT

	friend class map_layers_widget_item;
	friend class map_layers_widget;
	friend class map_layers;
	friend class document_map;

	static map_layers_model *current;

	map_layers   *container = nullptr;
	document_map *doc       = nullptr;
	map_asset    *asset     = nullptr;

	void update_layer_widgets(folder_layer *parent) noexcept;
	void dock_focus_changed(ads::CDockWidget *prev, ads::CDockWidget *current) noexcept;
	void clear_document() noexcept;

	/* internal actions, these do not generate undo data */
	QModelIndex create_layer(
	        const QString                   &name,
	        int                              type,
	        int                              row,
	        crpg::type::layer_id             id,
	        const QModelIndex               &parent_idx,
	        folder_layer                    *parent,
	        map_layer                      *&layer,
	        renderable_map_layer           *&rlayer,
	        const QString                   &blend_name,
	        std::function<void(map_layer *)> create_cb = std::function<void(map_layer *)>()) noexcept;
	void add_layer(QModelIndex parent_idx, int type) noexcept;
	void remove_children(folder_layer *parent) noexcept;
	void remove_layer(const QModelIndex &parent_idx, int row) noexcept;
	void rename_layer(map_layer *layer, const QString &name) noexcept;
	void set_layer_visible(map_layer *layer, bool visible) noexcept;
	void set_layer_blend(map_layer *layer, const QString &blend_name) noexcept;
	void set_layer_opacity(map_layer *layer, float opacity) noexcept;

	/* user actions, can generate undo data */
	void on_create(const QModelIndex &index) noexcept;
	void on_remove(const QModelIndex &index) noexcept;
	void on_rename(const QModelIndex &index, const QString &prev_name) noexcept;
	void on_select(const QModelIndex &index) noexcept;
	void on_blend_change(const QModelIndex &index, const QString &blend_name) noexcept;
	void on_opacity_change(const QModelIndex &index, float opacity) noexcept;

public:
	map_layers_model(QObject *parent) noexcept;
	~map_layers_model();

	QVariant        data(const QModelIndex &index, int role) const override;
	bool            setData(const QModelIndex &index, const QVariant &val, int role) override;
	Qt::ItemFlags   flags(const QModelIndex &index) const override;
	Qt::DropActions supportedDropActions() const override;
	QModelIndex     index(int row, int column, const QModelIndex &parent) const override;
	QModelIndex     parent(const QModelIndex &index) const override;
	int             rowCount(const QModelIndex &parent) const override;
	int             columnCount(const QModelIndex &parent) const override;

	static map_layers_model *get() noexcept { return current; }

	QModelIndex get_layer_index(map_layer *layer) const noexcept;
};

class map_layers_widget : public QTreeView {
	Q_OBJECT

	friend class map_layers_model;
	friend class map_layers_widget_item;

	static map_layers_widget *current;

	void selectionChanged(const QItemSelection &selected, const QItemSelection &deselected) override;
	bool edit(const QModelIndex &index, QAbstractItemView::EditTrigger trigger, QEvent *event) override;
	void dropEvent(QDropEvent *event) override;

	void move_layer(const QModelIndex &src_parent_idx,
	                int                src_row,
	                const QModelIndex &dst_parent_idx,
	                int                dst_row) noexcept;

public:
	map_layers_widget(QWidget *parent) noexcept;
	~map_layers_widget();

	static inline map_layers_widget *get() noexcept { return current; }
};

class map_layers : public QWidget {
	Q_OBJECT

	friend class map_layers_model;

	std::unique_ptr<Ui_map_layers_dock> ui;

public:
	map_layers(QWidget *parent) noexcept;
	~map_layers();
};

} // namespace editor
