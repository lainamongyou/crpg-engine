#pragma once

#include <QString>

namespace editor {

bool    valid_filename(const QString &name) noexcept;
QString get_safe_filename(const QString &path, const QString &name, const QString &ignore_name = QString()) noexcept;

} // namespace editor
