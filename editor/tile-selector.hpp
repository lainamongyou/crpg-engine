#pragma once

#include "tileset-view.hpp"

#include <DockWidget.h>

namespace editor {

class tileset_asset;

class tile_selector : public ads::CDockWidget {
	Q_OBJECT

	static tile_selector *current;
	tileset_view         *view;

public:
	tile_selector(const QString &name, QWidget *parent) noexcept;
	~tile_selector();

	void           set_asset(tileset_asset *asset) noexcept;
	tileset_asset *asset() const noexcept;

	inline bool              selected() const noexcept { return view->selected(); }
	inline tileset_selection selection() const noexcept { return view->selection(); }

	static inline tile_selector *get() noexcept { return current; }
};

} // namespace editor
