#include <engine/utility/fserializer.hpp>
#include <engine/utility/logging.hpp>
#include <engine/utility/dict.hpp>

#include "document-sprite.hpp"
#include "asset-browser.hpp"
#include "asset-sprite.hpp"
#include "asset-object.hpp"
#include "main-window.hpp"
#include "asset-map.hpp"

#include <DockWidget.h>

using namespace crpg;
using namespace ads;

namespace editor {

void sprite_asset::open() noexcept
{
	load_if_not_loaded();

	if (dock) {
		dock->raise();
		return;
	}

	main_window *window = main_window::get();
	dock                = new CDockWidget(name_, window);

	dock->setProperty("asset_category", (int)category);
	dock->setProperty("asset_id", (qulonglong)id());
	dock->setFeature(ads::CDockWidget::DockWidgetDeleteOnClose, true);
	dock->setWidget(new document_sprite(dock, this));
	window->add_document(dock);

	if (weak_texture.expired())
		update();
	else
		QMetaObject::invokeMethod(dock->widget(), "update_sprite");
}

void sprite_asset::save(const QString &path) noexcept
try {
	/* -------------------------------------- */
	/* engine data                            */

	std::string        file = path.toStdString() + "/asset.data";
	crpg::ofserializer s(file);
	if (s.fail())
		throw crpg::strprintf("Failed to open '%s'", file.c_str());

	crpg::dict dict;
	dict.set("orientation", orientation_);
	dict.set("offset", offset_);
	dict.set("origin", origin_);
	dict.set("box", box_);
	dict.set("frames", frames);
	dict.set("sequences", sequences);
	dict.set("ext", ext);
	dict.set("sheet_cx", sheet_cx);
	dict.set("sheet_cy", sheet_cy);
	dict.set("frame_cx", frame_cx);
	dict.set("frame_cy", frame_cy);
	dict.set("tile_width", tile_width);
	s << dict;

	/* -------------------------------------- */
	/* image file                             */

	file         = path.toStdString() + "/image." + ext;
	bool success = save_memory_to_file(file, data);
	if (!success)
		throw crpg::strprintf("Failed to save '%s'", file.c_str());

} catch (const std::string &str) {
	crpg::error("%s: %s", __FUNCTION__, str.c_str());
}

void sprite_asset::load() noexcept
try {
	/* -------------------------------------- */
	/* engine data                            */

	std::string        file = path().toStdString() + "/asset.data";
	crpg::ifserializer s(file);
	if (s.fail())
		throw crpg::strprintf("Failed to open '%s'", file.c_str());

	crpg::dict dict(s);
	dict.get("orientation", orientation_);
	dict.get("offset", offset_);
	dict.get("origin", origin_);
	dict.get("box", box_);
	dict.get("frames", frames);
	dict.get("sequences", sequences);
	dict.get("ext", ext);
	dict.get("sheet_cx", sheet_cx);
	dict.get("sheet_cy", sheet_cy);
	dict.get("frame_cx", frame_cx);
	dict.get("frame_cy", frame_cy);
	dict.get("tile_width", tile_width);

	/* -------------------------------------- */
	/* image file                             */

	file = path().toStdString() + "/image." + ext;
	data = load_memory_from_file(file);
	if (data.empty())
		throw crpg::strprintf("Failed to load '%s'", file.c_str());

} catch (const std::string &str) {
	crpg::error("%s: %s", __FUNCTION__, str.c_str());
}

crpg::graphics::texture_ref sprite_asset::load_tex() noexcept
{
	load_if_not_loaded();

	graphics::texture_ref texture;
	if (data.size() && !ext.empty())
		texture = graphics::texture::create_from_memory(ext.c_str(), data);

	graphics::texture_ref current;
	if (weak_texture.expired()) {
		weak_texture = texture;
		current      = texture;
	} else {
		current = weak_texture.lock();
		current->swap(texture);
	}
	return current;
}

crpg::graphics::vertex_buffer_ref sprite_asset::load_vb() noexcept
{
	load_if_not_loaded();

	graphics::vertex_buffer_ref vb;
	if (data.size() && !ext.empty()) {
		math::vec2 p1 = -origin_;
		math::vec2 p2((float)frame_cx, (float)frame_cy);
		float      f_tile_width = (float)tile_width;

		p2 += p1;
		p1 /= f_tile_width;
		p2 /= f_tile_width;

		graphics::vertex_buffer::data vbd;

		if (frames.size()) {
			for (frame_info &frame : frames) {
				vbd.points.emplace_back(p1.x, p1.y);
				vbd.points.emplace_back(p1.x, p2.y);
				vbd.points.emplace_back(p2.x, p1.y);
				vbd.points.emplace_back(p2.x, p2.y);

				vbd.coords.emplace_back(frame.min.x, frame.min.y);
				vbd.coords.emplace_back(frame.min.x, frame.max.y);
				vbd.coords.emplace_back(frame.max.x, frame.min.y);
				vbd.coords.emplace_back(frame.max.x, frame.max.y);
			}
		} else {
			vbd.points.emplace_back(p1.x, p1.y);
			vbd.points.emplace_back(p1.x, p2.y);
			vbd.points.emplace_back(p2.x, p1.y);
			vbd.points.emplace_back(p2.x, p2.y);
			vbd.coords.emplace_back(0.0f, 0.0f);
			vbd.coords.emplace_back(0.0f, 1.0f);
			vbd.coords.emplace_back(1.0f, 0.0f);
			vbd.coords.emplace_back(1.0f, 1.0f);
		}

		vb = graphics::vertex_buffer::create(std::move(vbd));
	}

	graphics::vertex_buffer_ref current;
	if (weak_vb.expired()) {
		weak_vb = vb;
		current = vb;
	} else {
		current = weak_vb.lock();
		current->swap(vb);
	}
	return current;
}

void sprite_asset::load_state(crpg::serializer &s) noexcept
{
	s >> frames >> sequences >> ext >> data >> sheet_cx >> sheet_cy >> frame_cx >> frame_cy >> tile_width >>
	        orientation_ >> offset_ >> origin_ >> box_;
	asset::load_state(s);
}

void sprite_asset::save_state(crpg::serializer &s) noexcept
{
	s << frames << sequences << ext << data << sheet_cx << sheet_cy << frame_cx << frame_cy << tile_width
	  << orientation_ << offset_ << origin_ << box_;
	asset::save_state(s);
}

void sprite_asset::recalc_box() noexcept
{
	float f_tile_width = (float)tile_width;

	box_.min.set_zero();
	box_.max.set((float)frame_cx, (float)frame_cy);
	box_ -= origin_;
	box_.min /= f_tile_width;
	box_.max /= f_tile_width;
}

void sprite_asset::update() noexcept
{
	recalc_box();

	graphics::texture_ref       texture;
	graphics::vertex_buffer_ref vb;
	if (data.size()) {
		texture = load_tex();
		vb      = load_vb();
	} else {
		weak_texture.reset();
		weak_vb.reset();
	}

	/* update dock */
	if (dock)
		QMetaObject::invokeMethod(dock->widget(), "update_sprite");

	/* update xrefs */
	for (auto &[id, _] : xrefs()) {
		editor::asset *asset  = asset_manager::get_asset(id);
		object_asset  *object = dynamic_cast<object_asset *>(asset);
		if (object) {
			object->load_if_not_loaded();
			object->update_sprite();
			continue;
		}

		map_asset *map = dynamic_cast<map_asset *>(asset);
		if (map) {
			map->load_if_not_loaded();
			map->update_sprite(this);
			continue;
		}
	}
}

crpg::graphics::texture_ref sprite_asset::texture() noexcept
{
	load_if_not_loaded();

	graphics::texture_ref texture = weak_texture.lock();
	if (texture)
		return texture;

	return load_tex();
}

crpg::graphics::vertex_buffer_ref sprite_asset::vb() noexcept
{
	load_if_not_loaded();

	graphics::vertex_buffer_ref vb = weak_vb.lock();
	if (vb)
		return vb;

	return load_vb();
}

void sprite_asset::set_orientation_offset(crpg::orientation orientation, const crpg::math::vec2 &offset) noexcept
{
	int o = (int)orientation;
	origin_.set_zero();

	orientation_ = orientation;
	offset_      = offset;

	if (o & orientation_center_x)
		origin_.x += floorf((float)frame_cx * 0.5f);
	else if (o & orientation_right)
		origin_.x += (float)frame_cx;

	if (o & orientation_center_y)
		origin_.y += floorf((float)frame_cy * 0.5f);
	else if (o & orientation_bottom)
		origin_.y += (float)frame_cy;

	origin_ += offset;

	update();
}

void sprite_asset::remove_xrefs() noexcept
{
	std::vector<crpg::type::asset_id> xref_ids;
	xref_ids.reserve(xrefs().size());

	for (auto &[id, _] : xrefs())
		xref_ids.push_back(id);

	for (auto id : xref_ids) {
		editor::asset *asset = asset_manager::get_asset(id);

		map_asset *map = dynamic_cast<map_asset *>(asset);
		if (map) {
			map->remove_sprite(this);
			map->redraw_if_open();
			continue;
		}

		object_asset *object = dynamic_cast<object_asset *>(asset);
		if (object) {
			object->remove_sprite();
			object->redraw_if_open();
		}
	}
}

} // namespace editor
