#pragma once

#include <QMainWindow>
#include <QPointer>

#include <engine/graphics/graphics.hpp>
#include <engine/resources.hpp>

#include "undo-stack.hpp"

class Ui_main_window;

namespace ads {
class CDockWidget;
class CDockManager;
class CDockAreaWidget;
} // namespace ads

namespace editor {

class main_view;
class asset_browser_widget;

class main_window : public QMainWindow {
	Q_OBJECT

	std::unique_ptr<Ui_main_window> ui;

	crpg::graphics::renderer_ref            renderer;
	std::unique_ptr<crpg::resource_manager> rm;

	editor::undo_stack undo_stack;
	int                modified_index = 0;

	QString cur_filename;

	bool save() noexcept;

	enum class confirm { yes, no, cancel };
	confirm confirm_save() noexcept;

	void closeEvent(QCloseEvent *event) override;

	ads::CDockManager    *dockman;
	ads::CDockAreaWidget *central_area;
	main_view            *view;

	void add_action_internal(const QString &name,
	                         undo_redo_cb   undo,
	                         undo_redo_cb   redo,
	                         undo_redo_cb   save_undo,
	                         undo_redo_cb   save_redo,
	                         bool           repeatable) noexcept;
	void add_action_internal(const QString     &name,
	                         undo_redo_cb       undo,
	                         undo_redo_cb       redo,
	                         const std::string &undo_data,
	                         const std::string &redo_data,
	                         bool               repeatable) noexcept;

	void show_message_internal(const QString &msg) noexcept;
	void clear_message_internal() noexcept;

public:
	main_window();
	~main_window();

	static inline void add_undo_action(const QString &name,
	                                   undo_redo_cb   undo,
	                                   undo_redo_cb   redo,
	                                   undo_redo_cb   save_undo,
	                                   undo_redo_cb   save_redo,
	                                   bool           repeatable = false) noexcept
	{
		get()->add_action_internal(name, undo, redo, save_undo, save_redo, repeatable);
	}

	static inline void add_undo_action(const QString     &name,
	                                   undo_redo_cb       undo,
	                                   undo_redo_cb       redo,
	                                   const std::string &undo_data,
	                                   const std::string &redo_data,
	                                   bool               repeatable = false) noexcept
	{
		get()->add_action_internal(name, undo, redo, undo_data, redo_data, repeatable);
	}

	static inline void show_message(const QString &msg) noexcept { get()->show_message_internal(msg); }
	static inline void clear_message() noexcept { get()->clear_message_internal(); }

	static main_window *get() noexcept;

	inline ads::CDockManager    *get_dock_manager() const noexcept { return dockman; }
	inline ads::CDockAreaWidget *get_central_area() const noexcept { return central_area; }

	void add_document(ads::CDockWidget *dock) noexcept;

public slots:
	void on_exit_triggered() noexcept;
	void on_undo_triggered() noexcept;
	void on_redo_triggered() noexcept;
	void on_save_triggered() noexcept;
};
} // namespace editor
