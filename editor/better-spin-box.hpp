#pragma once

#include <QDoubleSpinBox>
#include <QSpinBox>

namespace editor {

class better_spin_box : public QSpinBox {
	Q_OBJECT

	void mouseReleaseEvent(QMouseEvent *event) override;

	int last_val = 0;

signals:
	void editing_actually_finished();

public:
	better_spin_box(QWidget *parent) noexcept;

	/* subtle function override for Qt's UI generation */
	void setMinimum(int min) noexcept;
};

class better_double_spin_box : public QDoubleSpinBox {
	Q_OBJECT

	void mouseReleaseEvent(QMouseEvent *event) override;

	double last_val = 0.0;

signals:
	void editing_actually_finished();

public:
	better_double_spin_box(QWidget *parent) noexcept;

	/* subtle function override for Qt's UI generation */
	void setMinimum(double min) noexcept;
};

} // namespace editor
