#include <engine/utility/fserializer.hpp>
#include <engine/utility/logging.hpp>
#include <engine/utility/dict.hpp>
#include <engine/math/vec2.hpp>
#include <engine/math/box.hpp>

#include "asset.hpp"
#include "asset-map.hpp"
#include "asset-sprite.hpp"
#include "asset-object.hpp"
#include "asset-tileset.hpp"
#include "asset-browser.hpp"

#include <cinttypes>

#include <QDir>

#include <DockWidget.h>

using namespace crpg;

namespace editor {

static type::asset_id id_counter = 0;

asset::asset(const QString &name__,
             const QString &file_name_,
             const QIcon   &icon_,
             asset         *parent_,
             asset_type     type_,
             asset_category category_,
             type::asset_id id__) noexcept
        : name_(name__), file_name(file_name_), icon(icon_), type(type_), category(category_), parent(parent_)
{
	if (id__ != 0) {
		if (id__ > id_counter)
			id_counter = id__;
		id_ = id__;
	} else {
		id_ = ++id_counter;
	}

	auto *am = asset_manager::get();

	if (am->items.find(id_) == am->items.end()) {
		am->items[id_] = this;
	} else {
		error("item id %" PRIu64 " tried to overwrite existing asset, pls fix", id_);
	}
}

asset::~asset() noexcept
{
	delete dock;

	auto *am = asset_manager::get();
	if (am)
		am->items.erase(id_);
}

void asset::set_base_counter() noexcept
{
	id_counter = 100;
}

int asset::get_child_idx(const asset *item) const noexcept
{
	for (size_t idx = 0; idx < children.size(); idx++) {
		if (children[idx].get() == item)
			return (int)idx;
	}

	error("%s: %s", __FUNCTION__, "this should never happen");
	return -1;
}

bool asset::has_child(const QString &child_name) const noexcept
{
	for (auto &child : children) {
		if (child->name_.compare(child_name, Qt::CaseInsensitive) == 0) {
			return true;
		}
	}
	return false;
}

int asset::get_sorted_child_idx(const QString &child_name, bool folder) const noexcept
{
	size_t new_idx = 0;
	for (; new_idx < children.size(); new_idx++) {
		asset *child = children[new_idx].get();

		if (folder && child->type == asset_type::file)
			break;
		if (!folder && child->type != asset_type::file)
			continue;
		if (child->name_.localeAwareCompare(child_name) > 0)
			break;
	}

	return (int)new_idx;
}

QString asset::full_name() const noexcept
{
	if (!parent)
		return name_;
	else
		return parent->name() + "/" + name_;
}

QString asset::path() const noexcept
{
	if (!parent)
		return file_name;
	else
		return parent->path() + "/" + file_name;
}

QVariant asset::data(int role) const noexcept
{
	if (role == Qt::DisplayRole || role == Qt::EditRole)
		return name_;
	else if (role == Qt::DecorationRole)
		return icon;
	return QVariant();
}

QModelIndex asset::idx() const noexcept
{
	asset_manager *am = asset_manager::get();
	if (!parent)
		return am->createIndex(0, 0, this);

	int idx_ = parent->get_child_idx(this);
	return am->createIndex(idx_, 0, this);
}

void asset::remove_xrefs() noexcept
{
	for (auto &child : children)
		child->remove_xrefs();
}

void asset::open() noexcept {}

void asset::select() noexcept
{
	asset_manager::get()->items[id_] = this;
	asset_browser_widget::get()->setCurrentIndex(idx());
}

void asset::load_state(crpg::serializer &s) noexcept
{
	crpg::dict dict(s);
	dict.get("file_name", file_name);
	dict.get("xrefs", xrefs_);
}

void asset::save_state(crpg::serializer &s) noexcept
{
	crpg::dict dict;
	dict.set("file_name", file_name);
	dict.set("xrefs", xrefs_);
	s << dict;
}

void asset::save_folder(const QString &path) noexcept
try {
	std::string        file = path.toStdString() + "/info.data";
	crpg::ofserializer s(file);
	if (s.fail())
		throw crpg::strprintf("Failed to open %s", file.c_str());

	crpg::dict dict;
	dict.set("id", id_);
	dict.set("name", name_);
	dict.set("type", type);
	dict.set("category", category);
	dict.set("xrefs", xrefs_);
	s << dict;

} catch (const std::string &str) {
	error("%s: %s", __FUNCTION__, str.c_str());
}

void asset::save(const QString &path) noexcept
{
	QDir dir(path);

	QStringList old_folders = dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
	QStringList new_folders;

	if (type == asset_type::root)
		save_folder(path);

	/* --------------------------------------------------------- */
	/* process new folders                                       */

	for (auto &child_ptr : children) {
		auto *child = child_ptr.get();

		if (child->rename_idx != child->last_rename_idx) {
			child->last_rename_idx = child->rename_idx;
		}

		new_folders.push_back(child->file_name);
		if (child->loaded) {
			dir.mkdir(child->file_name);

			QString child_path = path + QStringLiteral("/") + child->file_name;
			child->save_folder(child_path);
			child->save(child_path);
		}
	}

	/* --------------------------------------------------------- */
	/* delete unused folders                                     */

	for (auto &folder : old_folders) {
		if (!new_folders.contains(folder)) {
			QString child_path = path + QStringLiteral("/") + folder;
			QDir(child_path).removeRecursively();
		}
	}
}

void asset::load_children(const QString &path) noexcept
{
	QDir        dir(path);
	QStringList folders = dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot);

	children.reserve(folders.size());

	for (auto &folder : folders) {
		QString child_path = path + QStringLiteral("/") + folder;
		asset  *child      = asset::load_asset(child_path, this);
		if (child)
			children.emplace_back(child);
	}
}

asset *asset::load_asset(const QString &path, asset *parent) noexcept
{
	QString            dir_name = parent ? QDir(path).dirName() : path;
	std::string        file     = path.toStdString() + "/info.data";
	crpg::ifserializer s(file);
	if (s.fail())
		return nullptr;

	type::asset_id id;
	QString        name;
	asset_type     type;
	asset_category category;

	crpg::dict dict(s);
	dict.get("id", id);
	dict.get("name", name);
	dict.get("type", type);
	dict.get("category", category);

	asset *new_asset = nullptr;

	if (type == asset_type::file) {
		switch (category) {
		case asset_category::map:
			new_asset = new map_asset(
			        name, dir_name, asset_manager::get_icon(type, category), parent, type, category, id);
			break;
		case asset_category::object:
			new_asset = new object_asset(
			        name, dir_name, asset_manager::get_icon(type, category), parent, type, category, id);
			break;
		case asset_category::sprite:
			new_asset = new sprite_asset(
			        name, dir_name, asset_manager::get_icon(type, category), parent, type, category, id);
			break;
		case asset_category::tileset:
			new_asset = new tileset_asset(
			        name, dir_name, asset_manager::get_icon(type, category), parent, type, category, id);
			break;
		}
		new_asset->loaded = false;
	} else {
		new_asset =
		        new asset(name, dir_name, asset_manager::get_icon(type, category), parent, type, category, id);
		if (type != asset_type::file)
			new_asset->load_children(path);
	}

	dict.get("xrefs", new_asset->xrefs_);
	return new_asset;
}

void asset::load() noexcept {}
void asset::update() noexcept {}

void asset::load_children() noexcept
{
	for (auto &child : children)
		child->load_recursively();
}

bool asset::is_open() const noexcept
{
	return !!dock;
}

QWidget *asset::document() noexcept
{
	if (!dock)
		open();
	return dock->widget();
}

void asset::redraw_if_open() noexcept
{
	if (dock)
		dock->widget()->update();
}

long asset::asset_loading_refs = 0;

void asset::load_if_not_loaded() noexcept
{
	if (!loaded) {
		asset_loading_refs++;
		load();
		loaded = true;
		asset_loading_refs--;
	}
}

void asset::inc_xref(asset *owner, asset *xref) noexcept
{
	if (asset::asset_loading_refs)
		return;

	owner->xrefs_[xref->id_]++;
	xref->xrefs_[owner->id_]++;
}

void asset::dec_xref(asset *owner, asset *xref) noexcept
{
	if (asset::asset_loading_refs)
		return;

	if (!--owner->xrefs_[xref->id_]) {
		owner->xrefs_.erase(xref->id_);
		xref->xrefs_.erase(owner->id_);
	} else {
		--xref->xrefs_[owner->id_];
	}
}

xref_base &xref_base::set(editor::asset *new_owner, editor::asset *new_asset) noexcept
{
	if (owner_ == new_owner && asset == new_asset)
		return *this;

	if (!asset::asset_loading_refs) {
		if (owner_ && asset)
			editor::asset::dec_xref(owner_, asset);
		if (new_owner && new_asset)
			editor::asset::inc_xref(new_owner, new_asset);
	}

	owner_ = new_owner;
	asset  = new_asset;
	return *this;
}

crpg::serializer &operator<<(crpg::serializer &s, const xref_base &xref) noexcept
{
	if (!xref.asset || !xref.owner_)
		return s << 0ULL << 0ULL;
	return s << xref.owner_->id() << xref.asset->id();
}

crpg::serializer &operator>>(crpg::serializer &s, xref_base &xref) noexcept
{
	crpg::type::asset_id owner_id;
	crpg::type::asset_id asset_id;

	s >> owner_id >> asset_id;

	editor::asset *owner = asset_manager::get_asset(owner_id);
	editor::asset *asset = asset_manager::get_asset(asset_id);

	xref.set(owner, asset);
	return s;
}

} // namespace editor

/* ======================================================================== */

crpg::serializer &operator<<(crpg::serializer &s, const QString &str) noexcept
{
	return s << str.toStdString();
}

crpg::serializer &operator>>(crpg::serializer &s, QString &str) noexcept
{
	if (s.out()) {
		s << str.toStdString();
	} else {
		std::string std_str;
		s >> std_str;
		str = QString::fromStdString(std_str);
	}

	return s;
}

crpg::serializer &operator<<(crpg::serializer &s, const QStringList &sl) noexcept
{
	QByteArray  qba;
	QDataStream qds(&qba, QIODeviceBase::WriteOnly);
	qds << sl;
	return s << qba;
}

crpg::serializer &operator>>(crpg::serializer &s, QStringList &sl) noexcept
{
	if (s.out())
		return s << sl;

	QByteArray qba;
	s >> qba;
	QDataStream qds(qba);
	qds >> sl;
	return s;
}

crpg::serializer &operator<<(crpg::serializer &s, const QVariant &var) noexcept
{
	QByteArray  qba;
	QDataStream qds(&qba, QIODeviceBase::WriteOnly);
	qds << var;
	return s << qba;
}

crpg::serializer &operator>>(crpg::serializer &s, QVariant &var) noexcept
{
	if (s.out())
		return s << var;

	QByteArray qba;
	s >> qba;
	QDataStream qds(qba);
	qds >> var;
	return s;
}

crpg::serializer &operator<<(crpg::serializer &s, const QVariantList &vl) noexcept
{
	QByteArray  qba;
	QDataStream qds(&qba, QIODeviceBase::WriteOnly);
	qds << vl;
	return s << qba;
}

crpg::serializer &operator>>(crpg::serializer &s, QVariantList &vl) noexcept
{
	if (s.out())
		return s << vl;

	QByteArray qba;
	s >> qba;
	QDataStream qds(qba);
	qds >> vl;
	return s;
}

crpg::serializer &operator<<(crpg::serializer &s, const QByteArray &qba) noexcept
{
	s << qba.size();
	return s.serialize((void *)qba.constData(), qba.size());
}

crpg::serializer &operator>>(crpg::serializer &s, QByteArray &qba) noexcept
{
	if (s.out())
		return s << qba;

	qsizetype size;
	s >> size;
	qba.resize(size);
	return s.serialize(qba.data(), size);
}
