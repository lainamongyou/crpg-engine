#include <engine/utility/fserializer.hpp>
#include <engine/utility/logging.hpp>
#include <engine/utility/dict.hpp>

#include "document-tileset.hpp"
#include "asset-browser.hpp"
#include "asset-tileset.hpp"
#include "tile-selector.hpp"
#include "main-window.hpp"
#include "asset-map.hpp"

#include <DockWidget.h>

using namespace crpg;
using namespace ads;

namespace editor {

tileset_asset::~tileset_asset() noexcept
{
	tile_selector *selector = tile_selector::get();
	if (selector && selector->asset() == this) {
		selector->set_asset(nullptr);
		QMetaObject::invokeMethod(selector->widget(), "update_texture");
	}
}

void tileset_asset::open() noexcept
{
	if (dock) {
		dock->raise();
		return;
	}

	main_window *window = main_window::get();
	dock                = new CDockWidget(name_, window);

	dock->setProperty("asset_category", (int)category);
	dock->setProperty("asset_id", (qulonglong)id());
	dock->setFeature(ads::CDockWidget::DockWidgetDeleteOnClose, true);
	dock->setWidget(new document_tileset(dock, this));
	window->add_document(dock);

	if (weak_texture.expired())
		update();
	else
		QMetaObject::invokeMethod(dock->widget(), "update_texture");
}

void tileset_asset::save(const QString &path) noexcept
try {
	/* -------------------------------------- */
	/* engine data                            */

	std::string        file = path.toStdString() + "/asset.data";
	crpg::ofserializer s(file);
	if (s.fail())
		throw crpg::strprintf("Failed to open '%s'", file.c_str());

	crpg::dict dict;
	dict.set("ext", ext);
	dict.set("cx", cx);
	dict.set("cy", cy);
	dict.set("tile_width", tile_width);
	s << dict;

	/* -------------------------------------- */
	/* image file                             */

	file         = path.toStdString() + "/image." + ext;
	bool success = save_memory_to_file(file, data);
	if (!success)
		throw crpg::strprintf("Failed to save '%s'", file.c_str());

} catch (const std::string &str) {
	crpg::error("%s: %s", __FUNCTION__, str.c_str());
}

void tileset_asset::load() noexcept
try {
	/* -------------------------------------- */
	/* engine data                            */

	std::string        file = path().toStdString() + "/asset.data";
	crpg::ifserializer s(file);
	if (s.fail())
		throw crpg::strprintf("Failed to open '%s'", file.c_str());

	crpg::dict dict(s);
	dict.get("ext", ext);
	dict.get("cx", cx);
	dict.get("cy", cy);
	dict.get("tile_width", tile_width);

	/* -------------------------------------- */
	/* image file                             */

	file = path().toStdString() + "/image." + ext;
	data = load_memory_from_file(file);
	if (data.empty())
		throw crpg::strprintf("Failed to load '%s'", file.c_str());

} catch (const std::string &str) {
	crpg::error("%s: %s", __FUNCTION__, str.c_str());
}

void tileset_asset::select() noexcept
{
	asset::select();

	tile_selector *selector = tile_selector::get();
	if (selector->asset() == this)
		return;

	selector->set_asset(this);

	if (weak_texture.expired())
		update();
	else
		QMetaObject::invokeMethod(selector->widget(), "update_texture");
}

void tileset_asset::load_state(crpg::serializer &s) noexcept
{
	s >> ext >> data >> cx >> cy >> tile_width;
	asset::load_state(s);
}

void tileset_asset::save_state(crpg::serializer &s) noexcept
{
	s << ext << data << cx << cy << tile_width;
	asset::save_state(s);
}

crpg::graphics::texture_ref tileset_asset::load_tex() noexcept
{
	load_if_not_loaded();

	graphics::texture_ref texture;
	if (data.size() && !ext.empty())
		texture = graphics::texture::create_from_memory(ext.c_str(), data);

	graphics::texture_ref current;
	if (weak_texture.expired()) {
		weak_texture = texture;
		current      = texture;
	} else {
		current = weak_texture.lock();
		current->swap(texture);
	}
	return current;
}

void tileset_asset::update() noexcept
{
	graphics::texture_ref texture;
	if (data.size()) {
		texture = load_tex();
	} else {
		weak_texture.reset();
	}

	/* update dock */
	if (dock)
		QMetaObject::invokeMethod(dock->widget(), "update_texture");

	/* update tile selector */
	tile_selector *selector = tile_selector::get();
	if (selector->asset() == this)
		QMetaObject::invokeMethod(selector->widget(), "update_texture");
}

crpg::graphics::texture_ref tileset_asset::texture() noexcept
{
	load_if_not_loaded();

	graphics::texture_ref texture = weak_texture.lock();
	if (texture)
		return texture;

	return load_tex();
}

void tileset_asset::remove_xrefs() noexcept
{
	std::vector<crpg::type::asset_id> xref_ids;
	xref_ids.reserve(xrefs().size());

	for (auto &[id, _] : xrefs())
		xref_ids.push_back(id);

	for (auto id : xref_ids) {
		map_asset *map = dynamic_cast<map_asset *>(asset_manager::get_asset(id));
		if (map) {
			map->remove_tileset(this);
			map->redraw_if_open();
		}
	}
}

} // namespace editor
