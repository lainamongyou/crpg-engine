#pragma once

#include <QAbstractTableModel>
#include <QWidget>
#include <QIcon>

#include <memory>

class QTableView;
class Ui_document_object;

namespace crpg {
class serializer;
}

namespace editor {

class variables_model;
class variable_widget;
class object_asset;
struct object_property;

class document_object : public QWidget {
	Q_OBJECT

	friend class object_asset;
	friend class variables_model;

	std::unique_ptr<Ui_document_object> ui;
	variables_model                    *model;

	object_asset *asset;
	void          update_sprite() noexcept;
	void          update_type() noexcept;
	void          set_type() noexcept;

private slots:
	void reset_data() noexcept;

public:
	document_object(QWidget *parent, object_asset *asset) noexcept;
	~document_object();

	variable_widget *get_variable_widget(const QString &name) noexcept;

public slots:
	void on_set_sprite_clicked() noexcept;
};

class variables_model : public QAbstractTableModel {
	friend class variable_widget;
	friend class document_object;

	Q_OBJECT

	QTableView   *view;
	object_asset *asset;

	QIcon trash_icon;

	int get_row_by_property(object_property *property) noexcept;

	void load_row(crpg::serializer &s, int row, bool update_widgets = true) noexcept;
	void save_row(crpg::serializer &s, int row) noexcept;
	void remove_row(int row) noexcept;

	void on_remove_row(int row) noexcept;
	void on_idx_clicked(const QModelIndex &idx) noexcept;

	inline object_property &get_property(int row) noexcept;
	inline object_property &get_property(int row) const noexcept;
	inline bool             valid_property(int row) const noexcept;

	static void undo_redo_remove_row(crpg::serializer &s) noexcept;
	static void undo_redo_insert_row(crpg::serializer &s) noexcept;
	static void undo_redo_variable_column(crpg::serializer &s, int column) noexcept;
	static void update_variable_column_data(document_object *doc, int column, int row) noexcept;

public:
	variables_model(QTableView *view, object_asset *asset_) noexcept;

	int           rowCount(const QModelIndex &parent) const override;
	int           columnCount(const QModelIndex &parent) const override;
	QModelIndex   index(int row, int column, const QModelIndex &parent) const override;
	QVariant      data(const QModelIndex &index, int role) const override;
	QVariant      headerData(int section, Qt::Orientation orientation, int role) const override;
	Qt::ItemFlags flags(const QModelIndex &index) const override;
	bool          setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;

	void update_type_widgets(int row) noexcept;
	void update_row_widgets(int row) noexcept;
	void reset_widgets() noexcept;
};

} // namespace editor
