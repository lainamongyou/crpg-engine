#include <engine/utility/fserializer.hpp>
#include <engine/utility/logging.hpp>
#include <engine/utility/dict.hpp>

#include "document-object.hpp"
#include "asset-browser.hpp"
#include "asset-object.hpp"
#include "document-map.hpp"
#include "main-window.hpp"
#include "asset-map.hpp"

#include <DockWidget.h>

using namespace crpg;
using namespace ads;
using namespace std;

namespace editor {

crpg::serializer &operator<<(crpg::serializer &s, const object_property &p) noexcept
{
	crpg::dict dict;
	dict.set("name", p.name);
	dict.set("type", p.type);
	dict.set("default_val", p.default_val);
	dict.set("config_info", p.config_info);
	return s << dict;
}

crpg::serializer &operator>>(crpg::serializer &s, object_property &p) noexcept
{
	crpg::dict dict(s);
	dict.get("name", p.name);
	dict.get("type", p.type);
	dict.get("default_val", p.default_val);
	dict.get("config_info", p.config_info);
	return s;
}

void object_asset::update_sprite() noexcept
{
	if (is_open()) {
		document_object *doc = static_cast<document_object *>(document());
		doc->update_sprite();
	}

	for (auto &[id, _] : xrefs()) {
		map_asset *map = dynamic_cast<map_asset *>(asset_manager::get_asset(id));
		if (!map)
			continue;

		map->load_if_not_loaded();
		map->update_object_sprite(this);
	}
}

void object_asset::set_sprite(sprite_ref &sprite) noexcept
{
	sprite_ = std::move(sprite);
	update_sprite();
}

void object_asset::open() noexcept
{
	load_if_not_loaded();

	if (dock) {
		dock->raise();
		return;
	}

	main_window *window = main_window::get();
	dock                = new CDockWidget(name_, window);

	dock->setProperty("asset_category", (int)category);
	dock->setProperty("asset_id", (qulonglong)id());
	dock->setFeature(ads::CDockWidget::DockWidgetDeleteOnClose, true);
	dock->setWidget(new document_object(dock, this));
	window->add_document(dock);
}

void object_asset::save(const QString &path) noexcept
try {
	std::string        file = path.toStdString() + "/asset.data";
	crpg::ofserializer s(file);
	if (s.fail())
		throw crpg::strprintf("Failed to open '%s'", file.c_str());

	crpg::dict dict;
	dict.set("type", obj_type);
	dict.set("sprite", sprite_);
	dict.set("properties", properties);
	s << dict;

} catch (const std::string &str) {
	crpg::error("%s: %s", __FUNCTION__, str.c_str());
}

void object_asset::load() noexcept
try {
	std::string        file = path().toStdString() + "/asset.data";
	crpg::ifserializer s(file);
	if (s.fail())
		throw crpg::strprintf("Failed to open '%s'", file.c_str());

	crpg::dict dict(s);
	dict.get("type", obj_type);
	dict.get("sprite", sprite_);
	dict.get("properties", properties);

} catch (const std::string &str) {
	crpg::error("%s: %s", __FUNCTION__, str.c_str());
}

void object_asset::save_state(crpg::serializer &s) noexcept
{
	crpg::dict dict;
	dict.set("type", obj_type);
	dict.set("sprite", sprite_);
	dict.set("properties", properties);
	s << dict;
	asset::save_state(s);
}

void object_asset::load_state(crpg::serializer &s) noexcept
{
	crpg::dict dict(s);
	dict.get("type", obj_type);
	dict.get("sprite", sprite_);
	dict.get("properties", properties);
	asset::load_state(s);
}

crpg::graphics::texture_ref object_asset::texture() noexcept
{
	load_if_not_loaded();
	return sprite_ ? sprite_->texture() : crpg::graphics::texture_ref();
}

crpg::graphics::vertex_buffer_ref object_asset::vb() noexcept
{
	load_if_not_loaded();
	return sprite_ ? sprite_->vb() : crpg::graphics::vertex_buffer_ref();
}

void object_asset::save_xref_properties(crpg::serializer &s) noexcept
{
	for (auto &[id, _] : xrefs()) {
		map_asset *map = dynamic_cast<map_asset *>(asset_manager::get_asset(id));
		if (map)
			map->save_object_properties(this, s);
	}
}

void object_asset::load_xref_properties(crpg::serializer &s) noexcept
{
	for (auto &[id, _] : xrefs()) {
		map_asset *map = dynamic_cast<map_asset *>(asset_manager::get_asset(id));
		if (map)
			map->load_object_properties(this, s);
	}
}

void object_asset::clear_row_in_xrefs(int row) noexcept
{
	for (auto &[id, _] : xrefs()) {
		map_asset *map = dynamic_cast<map_asset *>(asset_manager::get_asset(id));
		if (map)
			map->clear_object_property(this, row);
	}
}

void object_asset::insert_row_in_xrefs(int row) noexcept
{
	for (auto &[id, _] : xrefs()) {
		map_asset *map = dynamic_cast<map_asset *>(asset_manager::get_asset(id));
		if (map)
			map->insert_object_property(this, row);
	}
}

void object_asset::remove_row_in_xrefs(int row) noexcept
{
	for (auto &[id, _] : xrefs()) {
		map_asset *map = dynamic_cast<map_asset *>(asset_manager::get_asset(id));
		if (map)
			map->remove_object_property(this, row);
	}
}

void object_asset::remove_xrefs() noexcept
{
	std::vector<crpg::type::asset_id> xref_ids;
	xref_ids.reserve(xrefs().size());

	for (auto &[id, _] : xrefs())
		xref_ids.push_back(id);

	for (auto id : xref_ids) {
		map_asset *map = dynamic_cast<map_asset *>(asset_manager::get_asset(id));
		if (map) {
			map->remove_object(this);
			map->redraw_if_open();
		}
	}
}

void object_asset::remove_sprite() noexcept
{
	sprite_ref blank_ref;
	set_sprite(blank_ref);
}

} // namespace editor
