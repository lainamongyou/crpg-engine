#include <engine/utility/variant.hpp>

#include "document-object.hpp"
#include "asset-browser.hpp"
#include "asset-object.hpp"
#include "asset-sprite.hpp"
#include "main-window.hpp"

#include "ui_document-object.h"

#include <QStyledItemDelegate>
#include <QSignalBlocker>
#include <QDoubleSpinBox>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QFormLayout>
#include <QComboBox>
#include <QCheckBox>
#include <QSpinBox>
#include <QLabel>

#include <functional>

namespace editor {

/* clang-format off */
static const char *type_name_invalid     = QT_TRANSLATE_NOOP("editor::variables_model", "Invalid");
static const char *type_name_boolean     = QT_TRANSLATE_NOOP("editor::variables_model", "Boolean");
static const char *type_name_integer     = QT_TRANSLATE_NOOP("editor::variables_model", "Integer");
static const char *type_name_real        = QT_TRANSLATE_NOOP("editor::variables_model", "Floating Point");
static const char *type_name_string      = QT_TRANSLATE_NOOP("editor::variables_model", "String");
static const char *type_name_enumeration = QT_TRANSLATE_NOOP("editor::variables_model", "Enumeration");
static const char *type_name_asset       = QT_TRANSLATE_NOOP("editor::variables_model", "Asset");
/* clang-format on */

#define tr_type(type) QCoreApplication::translate("editor::variables_model", type_name_##type)

static crpg::object_property_type name_to_type(const QString &name) noexcept
{
#define return_type(type)                                                                                              \
	if (name == #type)                                                                                             \
	return crpg::object_property_type::type

	return_type(boolean);
	return_type(integer);
	return_type(real);
	return_type(string);
	return_type(enumeration);
	return_type(asset);
#undef return_type

	return crpg::object_property_type::invalid;
}

static QString type_to_name(crpg::object_property_type type) noexcept
{
#define case_type_name(type)                                                                                           \
	case crpg::object_property_type::type:                                                                         \
		return #type

	switch (type) {
		case_type_name(invalid);
		case_type_name(boolean);
		case_type_name(integer);
		case_type_name(real);
		case_type_name(string);
		case_type_name(enumeration);
		case_type_name(asset);
	}
#undef case_type_name

	return QString();
}

static QString type_to_display_name(crpg::object_property_type type) noexcept
{
#define case_type_display_name(type)                                                                                   \
	case crpg::object_property_type::type:                                                                         \
		return QCoreApplication::translate("editor::variables_model", type_name_##type)

	switch (type) {
		case_type_display_name(invalid);
		case_type_display_name(boolean);
		case_type_display_name(integer);
		case_type_display_name(real);
		case_type_display_name(string);
		case_type_display_name(enumeration);
		case_type_display_name(asset);
	}
#undef case_type_display_name

	return QString();
}

enum class variable_column : int {
	name,
	type,
	config,
	remove,

	count,
};

/* ========================================================================= */

class variable_widget : public QWidget {
protected:
	inline variable_widget(QWidget *parent, object_asset *asset_, int row_) noexcept
	        : QWidget(parent), asset(asset_), row(row_)
	{
	}

	void save_undo_redo_data(object_property &before) noexcept
	{
		auto undo_redo = std::bind(variables_model::undo_redo_variable_column,
		                           std::placeholders::_1,
		                           (int)variable_column::config);

		auto save_undo = [&](crpg::serializer &s) { s << asset->id() << row << before; };
		auto save_redo = [&](crpg::serializer &s) { s << asset->id() << row << get_property(); };

		QString text = tr("change variable '%1' of object asset '%2'").arg(get_property().name, asset->name());
		main_window::add_undo_action(text, undo_redo, undo_redo, save_undo, save_redo, true);
	}

	inline object_property &get_property() noexcept { return asset->properties[row]; }

public:
	object_asset *asset;
	int           row;

	virtual void update_value() = 0;
};

class boolean_var_widget : public variable_widget {
public:
	QCheckBox *default_val;

	boolean_var_widget(QWidget *parent, object_asset *asset_, int row_) noexcept
	        : variable_widget(parent, asset_, row_)
	{
		default_val = new QCheckBox(tr("Default Value"), this);
		default_val->setChecked(crpg::variant::get<bool>(get_property().default_val));

		QVBoxLayout *layout = new QVBoxLayout;
		layout->addWidget(default_val);
		setLayout(layout);

		connect(default_val, &QAbstractButton::clicked, [this](bool checked) {
			object_property before     = get_property();
			get_property().default_val = checked;
			save_undo_redo_data(before);
		});
	}

	void update_value() noexcept override
	{
		default_val->setChecked(crpg::variant::get<bool>(get_property().default_val));
	}
};

class integer_var_widget : public variable_widget {
public:
	QSpinBox *int_min;
	QSpinBox *int_max;
	QSpinBox *int_def;

	integer_var_widget(QWidget *parent, object_asset *asset_, int row_) noexcept
	        : variable_widget(parent, asset_, row_)
	{
		QLabel *label_min = new QLabel(tr("Minimum"), this);
		QLabel *label_max = new QLabel(tr("Maximum"), this);
		QLabel *label_def = new QLabel(tr("Default"), this);

		int_min = new QSpinBox(this);
		int_max = new QSpinBox(this);
		int_def = new QSpinBox(this);

		int def;
		int min;
		int max;

		if (get_property().config_info.size()) {
			crpg::istringserializer s(get_property().config_info);
			s >> min >> max;
			def = crpg::variant::get<int>(get_property().default_val);
		} else {
			min = 0;
			max = 1;
			def = 0;

			crpg::ostringserializer s(get_property().config_info);
			s << min << max;
			get_property().default_val = crpg::variant::create(def);
		}

		int_min->setRange(-0x7FFFFFFF, 0x7FFFFFFF);
		int_max->setRange(-0x7FFFFFFF, 0x7FFFFFFF);
		int_def->setRange(min, max);
		int_min->setValue(min);
		int_max->setValue(max);
		int_def->setValue(def);

		label_min->setBuddy(int_min);
		label_max->setBuddy(int_max);
		label_def->setBuddy(int_def);

		QFormLayout *layout = new QFormLayout;
		layout->setLabelAlignment(Qt::AlignRight);
		layout->addRow(label_min, int_min);
		layout->addRow(label_max, int_max);
		layout->addRow(label_def, int_def);
		setLayout(layout);

		connect(int_min, &QSpinBox::valueChanged, [this](int min) {
			int max = int_max->value();
			if (min >= max) {
				int_max->blockSignals(true);
				int_max->setValue(min + 1);
				int_max->blockSignals(false);
			}
		});
		connect(int_max, &QSpinBox::valueChanged, [this](int max) {
			int min = int_min->value();
			if (max <= min) {
				int_min->blockSignals(true);
				int_min->setValue(max - 1);
				int_min->blockSignals(false);
			}
		});

		auto check_range = [this]() {
			int min = int_min->value();
			int max = int_max->value();

			int_def->blockSignals(true);
			int_def->setRange(min, max);
			int_def->blockSignals(false);
		};

		auto changed = [this]() {
			int min = int_min->value();
			int max = int_max->value();
			int def = int_def->value();

			object_property &p      = get_property();
			object_property  before = p;

			p.config_info.clear();
			crpg::ostringserializer s(p.config_info);
			s << min << max;

			p.default_val = crpg::variant::create<int>(def);

			save_undo_redo_data(before);
		};

		connect(int_min, &QSpinBox::valueChanged, check_range);
		connect(int_max, &QSpinBox::valueChanged, check_range);
		connect(int_min, &QSpinBox::valueChanged, changed);
		connect(int_max, &QSpinBox::valueChanged, changed);
		connect(int_def, &QSpinBox::valueChanged, changed);
	}

	void update_value() noexcept override
	{
		int min;
		int max;
		int def;

		crpg::istringserializer s(get_property().config_info);
		s >> min >> max;
		def = crpg::variant::get<int>(get_property().default_val);

		int_min->blockSignals(true);
		int_max->blockSignals(true);
		int_def->blockSignals(true);

		int_min->setValue(min);
		int_max->setValue(max);
		int_def->setRange(min, max);
		int_def->setValue(def);

		int_min->blockSignals(false);
		int_max->blockSignals(false);
		int_def->blockSignals(false);
	}
};

class real_var_widget : public variable_widget {
	QDoubleSpinBox *real_min;
	QDoubleSpinBox *real_max;
	QDoubleSpinBox *real_def;

public:
	real_var_widget(QWidget *parent, object_asset *asset_, int row_) noexcept
	        : variable_widget(parent, asset_, row_)
	{
		QLabel *label_min = new QLabel(tr("Minimum"), this);
		QLabel *label_max = new QLabel(tr("Maximum"), this);
		QLabel *label_def = new QLabel(tr("Default"), this);

		real_min = new QDoubleSpinBox(this);
		real_max = new QDoubleSpinBox(this);
		real_def = new QDoubleSpinBox(this);

		real_min->setSingleStep(0.001);
		real_max->setSingleStep(0.001);
		real_def->setSingleStep(0.001);

		float def;
		float min;
		float max;

		if (get_property().config_info.size()) {
			crpg::istringserializer s(get_property().config_info);
			s >> min >> max;
			def = crpg::variant::get<float>(get_property().default_val);
		} else {
			min = 0.0f;
			max = 1.0f;
			def = 0.0f;

			crpg::ostringserializer s(get_property().config_info);
			s << min << max;
			get_property().default_val = crpg::variant::create(def);
		}

		real_min->setRange(-100000.0, 100000.0);
		real_max->setRange(-100000.0, 100000.0);
		real_def->setRange(min, max);
		real_min->setValue(min);
		real_max->setValue(max);
		real_def->setValue(def);

		label_min->setBuddy(real_min);
		label_max->setBuddy(real_max);
		label_def->setBuddy(real_def);

		QFormLayout *layout = new QFormLayout;
		layout->setLabelAlignment(Qt::AlignRight);
		layout->addRow(label_min, real_min);
		layout->addRow(label_max, real_max);
		layout->addRow(label_def, real_def);
		setLayout(layout);

		connect(real_min, &QDoubleSpinBox::valueChanged, [this](double min) {
			double max = real_max->value();
			if (min >= max) {
				real_max->blockSignals(true);
				real_max->setValue(min + 1.0);
				real_max->blockSignals(false);
			}
		});
		connect(real_max, &QDoubleSpinBox::valueChanged, [this](double max) {
			double min = real_min->value();
			if (max <= min) {
				real_min->blockSignals(true);
				real_min->setValue(max - 1.0);
				real_min->blockSignals(false);
			}
		});

		auto set_range = [this]() {
			double min = real_min->value();
			double max = real_max->value();

			real_def->blockSignals(true);
			real_def->setRange(min, max);
			real_def->blockSignals(false);
		};

		auto change_value = [this]() {
			double min = real_min->value();
			double max = real_max->value();
			double val = real_def->value();

			object_property &p      = get_property();
			object_property  before = p;

			p.config_info.clear();
			crpg::ostringserializer s(p.config_info);
			s << (float)min << (float)max;

			p.default_val = crpg::variant::create((float)val);

			save_undo_redo_data(before);
		};

		connect(real_min, &QDoubleSpinBox::valueChanged, set_range);
		connect(real_max, &QDoubleSpinBox::valueChanged, set_range);
		connect(real_min, &QDoubleSpinBox::valueChanged, change_value);
		connect(real_max, &QDoubleSpinBox::valueChanged, change_value);
		connect(real_def, &QDoubleSpinBox::valueChanged, change_value);
	}

	void update_value() noexcept override
	{
		float min;
		float max;
		float def;

		crpg::istringserializer s(get_property().config_info);
		s >> min >> max;
		def = crpg::variant::get<float>(get_property().default_val);

		real_min->blockSignals(true);
		real_max->blockSignals(true);
		real_def->blockSignals(true);

		real_min->setValue(min);
		real_max->setValue(max);
		real_def->setRange(min, max);
		real_def->setValue(def);

		real_min->blockSignals(false);
		real_max->blockSignals(false);
		real_def->blockSignals(false);
	}
};

class enumeration_var_widget : public variable_widget {
	QComboBox *available;
	QComboBox *default_val;

public:
	enumeration_var_widget(QWidget *parent, object_asset *asset_, int row_) noexcept
	        : variable_widget(parent, asset_, row_)
	{
		QLabel *available_label = new QLabel(tr("Values"), this);
		QLabel *default_label   = new QLabel(tr("Default"), this);

		available = new QComboBox(this);
		available->setEditable(true);

		default_val = new QComboBox(this);

		available_label->setBuddy(available);
		default_label->setBuddy(default_val);

		QPushButton *add = new QPushButton(tr("Add"), this);
		QPushButton *del = new QPushButton(tr("Remove"), this);

		QHBoxLayout *button_layout = new QHBoxLayout;
		button_layout->addWidget(add);
		button_layout->addWidget(del);

		QFormLayout *layout = new QFormLayout;
		layout->setLabelAlignment(Qt::AlignRight);
		layout->addRow(available_label, available);
		layout->addRow(nullptr, button_layout);
		layout->addRow(default_label, default_val);
		setLayout(layout);

		if (get_property().config_info.size()) {
			QStringList             list;
			crpg::istringserializer s(get_property().config_info);
			s >> list;

			for (const QString &str : list) {
				available->addItem(str);
				default_val->addItem(str);
			}

			int idx = default_val->findText(crpg::variant::get<QString>(get_property().default_val));
			default_val->setCurrentIndex(idx);
		} else {
			QStringList             list;
			crpg::ostringserializer s(get_property().config_info);
			s << list;

			get_property().default_val = crpg::variant::create(QString());
		}

		auto on_add = [this]() {
			QString text = available->currentText().trimmed();
			if (text.isEmpty())
				return;

			object_property before = get_property();

			QStringList list;
			{
				crpg::istringserializer s(get_property().config_info);
				s >> list;
			}

			if (list.contains(text))
				return;

			list.push_back(text);

			get_property().config_info.clear();
			crpg::ostringserializer s(get_property().config_info);
			s << list;

			available->addItem(text);
			default_val->addItem(text);

			save_undo_redo_data(before);
		};

		auto on_remove = [this]() {
			QString text = available->currentText().trimmed();
			if (text.isEmpty())
				return;

			object_property before = get_property();

			QStringList list;
			{
				crpg::istringserializer s(get_property().config_info);
				s >> list;
			}

			if (!list.contains(text))
				return;

			int idx = available->findText(text);
			list.removeAt(idx);
			available->removeItem(idx);
			default_val->removeItem(idx);

			get_property().config_info.clear();
			crpg::ostringserializer s(get_property().config_info);
			s << list;

			save_undo_redo_data(before);
		};

		connect(add, &QAbstractButton::clicked, on_add);
		connect(del, &QAbstractButton::clicked, on_remove);
		connect(default_val, &QComboBox::currentIndexChanged, [this]() {
			object_property before     = get_property();
			get_property().default_val = crpg::variant::create(default_val->currentText());
			save_undo_redo_data(before);
		});
	}

	void update_value() noexcept override
	{
		QString                 def = crpg::variant::get<QString>(get_property().default_val);
		QStringList             list;
		crpg::istringserializer s(get_property().config_info);
		s >> list;

		available->blockSignals(true);
		default_val->blockSignals(true);

		available->clear();
		default_val->clear();

		for (const QString &str : list) {
			available->addItem(str);
			default_val->addItem(str);
		}

		available->setCurrentText(QString());
		int idx = default_val->findText(def);
		default_val->setCurrentIndex(idx);

		available->blockSignals(false);
		default_val->blockSignals(false);
	}
};

/* ========================================================================= */

variables_model::variables_model(QTableView *view_, object_asset *asset_) noexcept
        : QAbstractTableModel(), view(view_), asset(asset_), trash_icon(":/other/icons/trash.svg")
{
}

inline object_property &variables_model::get_property(int row) noexcept
{
	return asset->properties[row];
}

inline object_property &variables_model::get_property(int row) const noexcept
{
	return asset->properties[row];
}

inline bool variables_model::valid_property(int row) const noexcept
{
	return row >= 0 && row < asset->properties.size();
}

int variables_model::rowCount(const QModelIndex &) const
{
	return (int)asset->properties.size() + 1;
}

int variables_model::columnCount(const QModelIndex &) const
{
	return (int)variable_column::count;
}

Qt::ItemFlags variables_model::flags(const QModelIndex &index) const
{
	Qt::ItemFlags flags = QAbstractTableModel::flags(index);
	if (!index.isValid())
		return flags;

	if (index.column() == (int)variable_column::name)
		return Qt::ItemIsEnabled | Qt::ItemNeverHasChildren | Qt::ItemIsEditable;

	return flags | Qt::ItemIsEnabled | Qt::ItemNeverHasChildren;
}

QModelIndex variables_model::index(int row, int column, const QModelIndex &parent) const
{
	if (!hasIndex(row, column, parent))
		return QModelIndex();
	return createIndex(row, column);
}

QVariant variables_model::data(const QModelIndex &index, int role) const
{
	if (!index.isValid() || (role != Qt::EditRole && role != Qt::DisplayRole))
		return QVariant();

	if (!valid_property(index.row())) {
		return (index.column() == (int)variable_column::name) ? QString() : QVariant();
	}

	object_property &p = get_property(index.row());
	switch (index.column()) {
	case (int)variable_column::name:
		return p.name;
	case (int)variable_column::type:
		return role == Qt::EditRole ? type_to_name(p.type) : type_to_display_name(p.type);
	case (int)variable_column::config:;
	}

	return QVariant();
}

void variables_model::undo_redo_variable_column(crpg::serializer &s, int column) noexcept
{
	crpg::type::asset_id asset_id;
	int                  row;

	s >> asset_id >> row;

	object_asset *asset = static_cast<object_asset *>(asset_manager::get_asset(asset_id));
	asset->open();

	document_object *doc = static_cast<document_object *>(asset->document());
	object_property &p   = doc->model->get_property(row);

	s >> p;

	update_variable_column_data(doc, column, row);
}

void variables_model::update_variable_column_data(document_object *doc, int column, int row) noexcept
{
	object_property &p = doc->model->get_property(row);

	auto       *model = doc->model;
	QModelIndex index = model->index(row, column, QModelIndex());

	if (column == (int)variable_column::name) {
		emit model->dataChanged(index, index);

	} else if (column == (int)variable_column::type) {
		QComboBox *combo = static_cast<QComboBox *>(doc->ui->variables->indexWidget(index));

		combo->blockSignals(true);
		int idx = combo->findData(type_to_name(p.type));
		combo->setCurrentIndex(idx);
		combo->blockSignals(false);

		model->update_type_widgets(row);

	} else if (column == (int)variable_column::config) {
		variable_widget *widget = static_cast<variable_widget *>(doc->ui->variables->indexWidget(index));
		if (widget)
			widget->update_value();
	}
}

bool variables_model::setData(const QModelIndex &index, const QVariant &value, int role)
{
	if (!index.isValid() || role != Qt::EditRole)
		return false;

	if (!valid_property(index.row())) {
		if (index.column() != (int)variable_column::name)
			return false;
		QString name = value.toString().trimmed();
		if (name.isEmpty())
			return false;

		for (auto &p : asset->properties) {
			if (p.name == name) {
				return false;
			}
		}

		int new_row = index.row() + 1;
		beginInsertRows(QModelIndex(), new_row, new_row);

		object_property new_property;
		new_property.name = name;
		asset->properties.push_back(std::move(new_property));

		endInsertRows();

		update_row_widgets(index.row());
		emit dataChanged(index, index);

		auto save_undo = [&](crpg::serializer &s) {
			s << asset->id() << index.row();
			/* TODO: handle xrefs. did I forget to comment this? */
		};
		auto save_redo = [&](crpg::serializer &s) {
			s << asset->id() << index.row() << asset->properties.back();
			/* TODO: handle xrefs */
		};

		QString text = tr("add variable '%1' to object asset '%2'").arg(name, asset->name());
		main_window::add_undo_action(text, undo_redo_remove_row, undo_redo_insert_row, save_undo, save_redo);
		return true;
	}

	object_property &p      = get_property(index.row());
	object_property  before = p;
	int              column = index.column();
	QString          action_name;

	if (index.column() == (int)variable_column::name) {
		QString name = value.toString().trimmed();
		if (name.isEmpty())
			return false;
		if (name == p.name)
			return false;

		action_name = tr("rename variable '%1' to '%2' in object asset '%3'").arg(p.name, name, asset->name());
		p.name      = name;

		emit dataChanged(index, index);

		auto undo_redo = std::bind(undo_redo_variable_column, std::placeholders::_1, column);
		auto save_undo = [&](crpg::serializer &s) { s << asset->id() << index.row() << before; };
		auto save_redo = [&](crpg::serializer &s) { s << asset->id() << index.row() << p; };

		main_window::add_undo_action(action_name, undo_redo, undo_redo, save_undo, save_redo);

	} else if (index.column() == (int)variable_column::type) {
		QString name = value.toString();
		action_name  = tr("change type of variable '%1' to '%2' in object asset '%3'")
		                      .arg(p.name, name, asset->name());
		p.type = name_to_type(name);
		p.default_val.clear();
		p.config_info.clear();
		update_type_widgets(index.row());

		auto undo_redo = [column](crpg::serializer &s) {
			crpg::type::asset_id asset_id;
			int                  row;

			s >> asset_id >> row;

			object_asset *asset = static_cast<object_asset *>(asset_manager::get_asset(asset_id));
			asset->open();

			document_object *doc = static_cast<document_object *>(asset->document());
			object_property &p   = doc->model->get_property(row);

			s >> p;

			asset->load_xref_properties(s);

			auto *model = doc->model;
			model->update_variable_column_data(doc, column, row);
		};
		auto save_undo = [&](crpg::serializer &s) {
			s << asset->id() << index.row() << before;
			asset->save_xref_properties(s);
		};
		auto save_redo = [&](crpg::serializer &s) {
			s << asset->id() << index.row() << p;
			asset->clear_row_in_xrefs(index.row());
			asset->save_xref_properties(s);
		};

		main_window::add_undo_action(action_name, undo_redo, undo_redo, save_undo, save_redo);
	}
	return true;
}

QVariant variables_model::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (role != Qt::DisplayRole || orientation != Qt::Orientation::Horizontal)
		return QVariant();

	switch (section) {
	case (int)variable_column::name:
		return tr("Name");
	case (int)variable_column::type:
		return tr("Type");
	case (int)variable_column::config:
		return tr("Configuration");
	}

	return QVariant();
}

void variables_model::load_row(crpg::serializer &s, int row, bool update_widgets) noexcept
{
	object_property p;
	s >> p;

	beginInsertRows(QModelIndex(), row, row);
	asset->properties.emplace(asset->properties.begin() + row, std::move(p));
	asset->insert_row_in_xrefs(row);
	endInsertRows();

	if (update_widgets)
		update_row_widgets(row);
}

void variables_model::save_row(crpg::serializer &s, int row) noexcept
{
	s << asset->properties[row];
}

void variables_model::undo_redo_remove_row(crpg::serializer &s) noexcept
{
	crpg::type::asset_id asset_id;
	int                  row;

	s >> asset_id >> row;

	object_asset *asset = static_cast<object_asset *>(asset_manager::get_asset(asset_id));
	asset->open();

	document_object *doc = static_cast<document_object *>(asset->document());
	doc->model->remove_row(row);
}

void variables_model::undo_redo_insert_row(crpg::serializer &s) noexcept
{
	crpg::type::asset_id asset_id;
	int                  row;

	s >> asset_id >> row;

	object_asset *asset = static_cast<object_asset *>(asset_manager::get_asset(asset_id));
	asset->open();

	document_object *doc = static_cast<document_object *>(asset->document());
	doc->model->load_row(s, row);
}

void variables_model::remove_row(int row) noexcept
{
	beginRemoveRows(QModelIndex(), row, row);
	asset->properties.erase(asset->properties.begin() + row);
	asset->remove_row_in_xrefs(row);
	endRemoveRows();
}

void variables_model::on_remove_row(int row) noexcept
{
	auto undo = [](crpg::serializer &s) {
		crpg::type::asset_id asset_id;
		int                  row;

		s >> asset_id >> row;

		object_asset *asset = static_cast<object_asset *>(asset_manager::get_asset(asset_id));
		asset->open();

		document_object *doc = static_cast<document_object *>(asset->document());
		doc->model->load_row(s, row, false);
		doc->model->reset_widgets();

		asset->load_xref_properties(s);
	};

	auto save_undo = [&](crpg::serializer &s) {
		s << asset->id() << row << asset->properties[row];
		asset->save_xref_properties(s);
	};
	auto save_redo = [&](crpg::serializer &s) {
		s << asset->id() << row;
		remove_row(row);
		reset_widgets();
	};

	QString name = asset->properties[row].name;

	QString text = tr("remove variable '%1' from object asset '%2'").arg(name, asset->name());
	main_window::add_undo_action(text, undo, undo_redo_remove_row, save_undo, save_redo);
}

void variables_model::update_type_widgets(int row) noexcept
{
	QModelIndex idx = index(row, (int)variable_column::config, QModelIndex());

	switch (get_property(row).type) {
	case crpg::object_property_type::invalid:
		view->setIndexWidget(idx, nullptr);
		break;
	case crpg::object_property_type::boolean:
		view->setIndexWidget(idx, new boolean_var_widget(view, asset, row));
		break;
	case crpg::object_property_type::integer:
		view->setIndexWidget(idx, new integer_var_widget(view, asset, row));
		break;
	case crpg::object_property_type::real:
		view->setIndexWidget(idx, new real_var_widget(view, asset, row));
		break;
	case crpg::object_property_type::string:
		view->setIndexWidget(idx, nullptr);
		break;
	case crpg::object_property_type::enumeration:
		view->setIndexWidget(idx, new enumeration_var_widget(view, asset, row));
		break;
	case crpg::object_property_type::asset:
		view->setIndexWidget(idx, nullptr);
		break;
	}

	view->resizeRowToContents(row);
}

void variables_model::update_row_widgets(int row) noexcept
{
	QComboBox *types = new QComboBox(view);

#define add_item(type) types->addItem(tr_type(type), #type)

	add_item(boolean);
	add_item(integer);
	add_item(real);
	add_item(string);
	add_item(enumeration);
	add_item(asset);
#undef add_item

	int type_idx = types->findData(type_to_name(get_property(row).type));
	types->setCurrentIndex(type_idx);

	QModelIndex midx = createIndex(row, (int)variable_column::type);
	view->setIndexWidget(midx, types);
	update_type_widgets(row);

	connect(types, &QComboBox::currentIndexChanged, [this, types, row]() {
		QModelIndex midx = index(row, (int)variable_column::type, QModelIndex());
		setData(midx, types->currentData());
	});

	QPushButton *remove = new QPushButton(view);
	remove->setMinimumSize(20, 20);
	remove->setFlat(true);
	remove->setIcon(trash_icon);

	connect(remove, &QAbstractButton::clicked, [this, row]() { on_remove_row(row); });

	midx = createIndex(row, (int)variable_column::remove);
	view->setIndexWidget(midx, remove);
}

void variables_model::reset_widgets() noexcept
{
	for (size_t i = 0; i < asset->properties.size(); i++)
		update_row_widgets((int)i);
}

void variables_model::on_idx_clicked(const QModelIndex &idx) noexcept
{
	auto *view = qobject_cast<QTableView *>(sender());
	if (!view)
		return;

	if (idx.row() == (int)asset->properties.size())
		view->edit(createIndex(idx.row(), (int)variable_column::name));
}

/* ========================================================================= */

document_object::document_object(QWidget *parent, object_asset *asset_) noexcept
        : QWidget(parent), ui(new Ui_document_object), asset(asset_)
{
	ui->setupUi(this);

	model = new variables_model(ui->variables, asset);

	ui->variables->setModel(model);
	ui->variables->verticalHeader()->setVisible(false);
	ui->variables->horizontalHeader()->setMinimumSectionSize(23);
	ui->variables->horizontalHeader()->setDefaultSectionSize(23);
	ui->variables->horizontalHeader()->setSectionResizeMode((int)variable_column::name,
	                                                        QHeaderView::ResizeMode::Stretch);
	ui->variables->horizontalHeader()->setSectionResizeMode((int)variable_column::type,
	                                                        QHeaderView::ResizeMode::Stretch);
	ui->variables->horizontalHeader()->setSectionResizeMode((int)variable_column::config,
	                                                        QHeaderView::ResizeMode::Stretch);
	ui->variables->horizontalHeader()->setSectionResizeMode((int)variable_column::remove,
	                                                        QHeaderView::ResizeMode::Fixed);
	ui->variables->setEditTriggers(QAbstractItemView::EditKeyPressed |
	                               QAbstractItemView::EditTrigger::DoubleClicked);

	connect(ui->type_box, &QAbstractButton::clicked, this, &document_object::set_type);
	connect(ui->type_point, &QAbstractButton::clicked, this, &document_object::set_type);
	connect(ui->type_sprite, &QAbstractButton::clicked, this, &document_object::set_type);
	connect(ui->type_boundary, &QAbstractButton::clicked, this, &document_object::set_type);

	connect(ui->variables, &QAbstractItemView::clicked, model, &variables_model::on_idx_clicked);

	model->reset_widgets();
	update_type();
	update_sprite();
}

document_object::~document_object() noexcept {}

void document_object::update_sprite() noexcept
{
	sprite_asset *sprite = asset->sprite();

	ui->view->asset = sprite;
	if (sprite) {
		ui->view->texture = sprite->texture();
		ui->view->vb      = sprite->vb();
	} else {
		ui->view->texture.reset();
		ui->view->vb.reset();
	}
	ui->view->update();
}

void document_object::update_type() noexcept
{
	QSignalBlocker s1(ui->type_box);
	QSignalBlocker s2(ui->type_point);
	QSignalBlocker s3(ui->type_sprite);
	QSignalBlocker s4(ui->type_boundary);

	if (asset->obj_type == object_type::box) {
		if (!ui->type_box->isChecked()) {
			ui->type_box->setChecked(true);
			ui->type_point->setChecked(false);
			ui->type_sprite->setChecked(false);
			ui->type_boundary->setChecked(false);
		}
	} else if (asset->obj_type == object_type::point) {
		if (!ui->type_point->isChecked()) {
			ui->type_box->setChecked(false);
			ui->type_point->setChecked(true);
			ui->type_sprite->setChecked(false);
			ui->type_boundary->setChecked(false);
		}
	} else if (asset->obj_type == object_type::sprite) {
		if (!ui->type_sprite->isChecked()) {
			ui->type_box->setChecked(false);
			ui->type_point->setChecked(false);
			ui->type_sprite->setChecked(true);
			ui->type_boundary->setChecked(false);
		}
	} else if (asset->obj_type == object_type::boundary) {
		if (!ui->type_sprite->isChecked()) {
			ui->type_box->setChecked(false);
			ui->type_point->setChecked(false);
			ui->type_sprite->setChecked(false);
			ui->type_boundary->setChecked(true);
		}
	}

	ui->sprite_stuff->setVisible(asset->obj_type == object_type::sprite);
}

void document_object::set_type() noexcept
{
	object_type type;

	if (ui->type_box->isChecked())
		type = object_type::box;
	else if (ui->type_point->isChecked())
		type = object_type::point;
	else if (ui->type_sprite->isChecked())
		type = object_type::sprite;
	else
		type = object_type::boundary;

	if (asset->obj_type == type)
		return;

	auto undo_redo = [](crpg::serializer &s) {
		crpg::type::asset_id id;
		object_type          type;

		s >> id >> type;

		object_asset *asset = static_cast<object_asset *>(asset_manager::get_asset(id));
		asset->open();
		asset->obj_type = type;

		document_object *doc = static_cast<document_object *>(asset->document());
		doc->update_type();
		asset->update_sprite();
	};

	auto save_undo = [&](crpg::serializer &s) { s << asset->id() << asset->obj_type; };
	auto save_redo = [&](crpg::serializer &s) {
		s << asset->id() << type;

		asset->obj_type = type;
		update_type();
		asset->update_sprite();
	};

	QString text = tr("change type of object asset '%1'").arg(asset->name());
	main_window::add_undo_action(text, undo_redo, undo_redo, save_undo, save_redo);
}

void document_object::reset_data() noexcept
{
	update_sprite();
}

void document_object::on_set_sprite_clicked() noexcept
{
	sprite_asset *new_sprite = static_cast<sprite_asset *>(asset_manager::selected_asset());
	if (!new_sprite || !new_sprite->is_file(crpg::asset_category::sprite))
		return;

	auto undo_redo = [](crpg::serializer &s) {
		crpg::type::asset_id id;
		sprite_ref           sprite;

		s >> id >> sprite;

		object_asset *asset = static_cast<object_asset *>(asset_manager::get_asset(id));
		asset->open();
		asset->set_sprite(sprite);
	};

	auto save_undo = [&](crpg::serializer &s) { s << asset->id() << asset->sprite(); };
	auto save_redo = [&](crpg::serializer &s) {
		sprite_ref ref(asset, new_sprite);
		asset->set_sprite(ref);
		s << asset->id() << asset->sprite();
	};

	QString text;
	if (new_sprite) {
		text = tr("change sprite of object asset '%1' to '%2'").arg(asset->name(), new_sprite->name());
	} else {
		text = tr("clear sprite of object asset '%1'").arg(asset->name());
	}

	main_window::add_undo_action(text, undo_redo, undo_redo, save_undo, save_redo);
}

variable_widget *document_object::get_variable_widget(const QString &name) noexcept
{
	for (size_t row = 0; row < asset->properties.size(); row++) {
		auto &property = asset->properties[row];
		if (property.name == name) {
			QModelIndex idx = model->index((int)row, (int)variable_column::config, QModelIndex());
			return static_cast<variable_widget *>(ui->variables->indexWidget(idx));
		}
	}

	assert(false);
	return nullptr;
}

/* ========================================================================= */

} // namespace editor
