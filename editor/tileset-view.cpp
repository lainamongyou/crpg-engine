#include "asset-tileset.hpp"
#include "tileset-view.hpp"

#include <QMouseEvent>

using namespace crpg;
using namespace crpg::graphics;

namespace editor {

void tileset_view::init() noexcept
{
	main_view::init();
}

void tileset_view::clear() noexcept
{
	center_cam = false;
	zoom       = 400.0f;
	texture    = texture_ref();
	viewable_area.set_zero();
}

void tileset_view::mousePressEvent(QMouseEvent *event)
{
	main_view::mousePressEvent(event);

	if (type == tileset_view_type::view_only)
		return;

	if (event->button() == Qt::LeftButton) {
		selected_ = false;

		/* no selection if out of bounds */
		if (cursor_pos.x < viewable_area.min.x || cursor_pos.y < viewable_area.min.y ||
		    cursor_pos.x > viewable_area.max.x || cursor_pos.y > viewable_area.max.y)
			return;

		first_tile.x   = (uint8_t)(cursor_pos.x / tile_width);
		first_tile.y   = (uint8_t)(cursor_pos.y / tile_width);
		last_tile      = first_tile;
		selection_.min = first_tile;
		selection_.max = first_tile;

		mouse_down = true;
		selected_  = true;
		QWidget::update();
	}

	if (!mouse_down && event->button() == Qt::RightButton) {
		selected_  = false;
		selection_ = tileset_selection::invalid();
	}
}

void tileset_view::mouseReleaseEvent(QMouseEvent *event)
{
	main_view::mouseReleaseEvent(event);

	if (type == tileset_view_type::view_only)
		return;

	if (event->button() == Qt::LeftButton) {
		mouse_down = false;
	}
}

void tileset_view::mouseMoveEvent(QMouseEvent *event)
{
	main_view::mouseMoveEvent(event);

	if (type != tileset_view_type::multi_selection)
		return;
	if (!mouse_down)
		return;

	math::vec2 pos;
	pos = math::vec2::max(cursor_pos, viewable_area.min);
	pos = math::vec2::min(pos, viewable_area.max);

	last_tile.x = (uint8_t)(pos.x / tile_width);
	last_tile.y = (uint8_t)(pos.y / tile_width);

	selection_.min.x = first_tile.x < last_tile.x ? first_tile.x : last_tile.x;
	selection_.min.y = first_tile.y < last_tile.y ? first_tile.y : last_tile.y;
	selection_.max.x = first_tile.x > last_tile.x ? first_tile.x : last_tile.x;
	selection_.max.y = first_tile.y > last_tile.y ? first_tile.y : last_tile.y;
}

void tileset_view::update_texture() noexcept
{
	graphics::texture_ref new_texture;
	if (asset_)
		new_texture = asset_->texture();

	cam_pos.set_zero();
	zoom = default_zoom;

	texture = new_texture;
	if (!texture) {
		clear();
		update();
		return;
	}

	int cx = texture->cx();
	int cy = texture->cy();

	zoom       = cx > cy ? (float)cx : (float)cy;
	tile_width = (float)asset_->tile_width;

	viewable_area.min.set(0.0f, 0.0f);
	viewable_area.max.set((float)cx, (float)cy);

	update();
}

void tileset_view::render_selection_box(const float thickness) noexcept
{
	const float thickness_d2 = thickness * 0.5f;
	int16_t     max_x        = selection_.max.x + 1;
	int16_t     max_y        = selection_.max.y + 1;
	int16_t     inc_x        = max_x - selection_.min.x;
	int16_t     inc_y        = max_y - selection_.min.y;
	float       line_cx      = (float)(max_x - selection_.min.x) * tile_width + thickness;
	float       line_cy      = (float)(max_y - selection_.min.y) * tile_width + thickness;
	float       f_x;
	float       f_y;

	f_x = (float)selection_.min.x * tile_width - thickness_d2;
	for (uint8_t y = selection_.min.y; y <= max_y; y += inc_y) {
		f_y = (float)y * tile_width - thickness_d2;

		matrix_guard g;
		matrix_scale(line_cx, thickness);
		matrix_translate(f_x, f_y);
		draw(graphics::primitive::tristrip);
	}

	f_y = (float)selection_.min.y * tile_width - thickness_d2;
	for (uint8_t x = selection_.min.x; x <= max_x; x += inc_x) {
		f_x = (float)x * tile_width - thickness_d2;

		matrix_guard g;
		matrix_scale(thickness, line_cy);
		matrix_translate(f_x, f_y);
		draw(graphics::primitive::tristrip);
	}
}

void tileset_view::render() noexcept
{
	render_transparency_background();
	if (!texture)
		return;

	vs_unlit->load();
	ps_unlit->load();

	draw(texture);

	/* ----------------------------- */

	vs_solid->load();
	ps_solid->load();

	vs_solid->set_world(math::matrix::identity());
	ps_solid->set("color", math::vec4(1.0f, 1.0f, 1.0f, 0.75f));

	float cx         = (float)texture->cx() + math::epsilon;
	float cy         = (float)texture->cy() + math::epsilon;
	float tile_width = (float)asset_->tile_width;

	/* ----------------------------- */

	vb_line_horizontal->load();
	for (float y = 0.0f; y <= cy; y += tile_width) {
		matrix_guard g;
		matrix_translate(0.0f, y);
		matrix_scale(cx, 1.0f);

		draw(graphics::primitive::lines);
	}

	/* ----------------------------- */

	vb_line_vertical->load();
	for (float x = 0.0f; x <= cx; x += tile_width) {
		matrix_guard g;
		matrix_translate(x, 0.0f);
		matrix_scale(1.0f, cy);

		draw(graphics::primitive::lines);
	}

	/* ----------------------------- */

	if (selected_) {
		vb_sprite->load();
		ps_solid->set("color", math::vec4(0.0f, 0.0f, 0.0f, 1.0f));
		render_selection_box(1.5f);
		ps_solid->set("color", math::vec4(1.0f, 1.0f, 1.0f, 1.0f));
		render_selection_box(0.8f);
	}
}

} // namespace editor
