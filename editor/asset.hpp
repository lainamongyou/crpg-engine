#pragma once

#include <QPointer>
#include <QObject>
#include <QIcon>

#include <vector>
#include <string>
#include <map>

#include <engine/types.hpp>

class QWidget;
class QVariant;
class QByteArray;

namespace crpg {
class serializer;
}

namespace ads {
class CDockWidget;
}

namespace editor {

class asset {
	friend class asset_browser_widget;
	friend class asset_model;
	friend class asset_manager;
	friend class xref_base;

	crpg::type::asset_id                 id_;
	std::map<crpg::type::asset_id, long> xrefs_;

	static long asset_loading_refs;

	int last_rename_idx = 0;
	int rename_idx      = 0;

	static void set_base_counter() noexcept;

	void          save_folder(const QString &path) noexcept;
	void          load_children(const QString &path) noexcept;
	static asset *load_asset(const QString &path, asset *parent) noexcept;

	virtual void save(const QString &path) noexcept;
	virtual void load() noexcept;

	virtual void remove_xrefs() noexcept;

	static void inc_xref(asset *owner, asset *xref) noexcept;
	static void dec_xref(asset *owner, asset *xref) noexcept;

	void        load_children() noexcept;
	inline void load_recursively() noexcept
	{
		if (type == crpg::asset_type::folder)
			load_children();
		else
			load_if_not_loaded();
	}

protected:
	QString              name_;
	QString              file_name;
	QIcon                icon;
	crpg::asset_type     type;
	crpg::asset_category category;
	bool                 loaded   = true;
	bool                 expanded = true;

	asset                              *parent;
	std::vector<std::unique_ptr<asset>> children;

	QPointer<ads::CDockWidget> dock;

	int      get_child_idx(const asset *item) const noexcept;
	bool     has_child(const QString &child_name) const noexcept;
	int      get_sorted_child_idx(const QString &child_name, bool folder) const noexcept;
	QVariant data(int role) const noexcept;

	asset(const QString       &name,
	      const QString       &file_name,
	      const QIcon         &icon,
	      asset               *parent,
	      crpg::asset_type     type,
	      crpg::asset_category category,
	      crpg::type::asset_id id = 0) noexcept;

public:
	virtual ~asset();

	inline const QString       &name() const noexcept { return name_; }
	QString                     full_name() const noexcept;
	QString                     path() const noexcept;
	inline crpg::type::asset_id id() const noexcept { return id_; }
	QModelIndex                 idx() const noexcept;

	const std::map<crpg::type::asset_id, long> &xrefs() const noexcept { return xrefs_; }

	inline bool is_type(crpg::asset_type type_) const noexcept { return type == type_; }
	inline bool is_file(crpg::asset_category category_) const noexcept
	{
		return type == crpg::asset_type::file && category == category_;
	}

	bool is_open() const noexcept;

	virtual void open() noexcept;
	virtual void select() noexcept;
	virtual void load_state(crpg::serializer &s) noexcept;
	virtual void save_state(crpg::serializer &s) noexcept;
	virtual void update() noexcept;

	QWidget *document() noexcept;

	void redraw_if_open() noexcept;

	void load_if_not_loaded() noexcept;
};

class xref_base {
protected:
	editor::asset *owner_ = nullptr;
	editor::asset *asset  = nullptr;

	inline void clear() noexcept
	{
		if (owner_ && asset)
			editor::asset::dec_xref(owner_, asset);
	}

public:
	inline xref_base() = default;
	explicit inline xref_base(editor::asset *owner, editor::asset *asset) noexcept : owner_(owner), asset(asset)
	{
		editor::asset::inc_xref(owner, asset);
	}
	inline xref_base(const xref_base &xref) noexcept { set(xref.owner_, xref.asset); }
	inline xref_base(xref_base &&xref) noexcept
	{
		clear();

		owner_      = xref.owner_;
		asset       = xref.asset;
		xref.owner_ = nullptr;
	}

	inline ~xref_base() noexcept { clear(); }

	xref_base &set(editor::asset *owner, editor::asset *asset) noexcept;

	inline editor::asset *get() const noexcept { return asset; }
	inline editor::asset *owner() const noexcept { return owner_; }

	inline xref_base &operator=(const xref_base &xref) noexcept { return set(xref.owner_, xref.asset); }
	inline xref_base &operator=(xref_base &&xref) noexcept
	{
		clear();
		owner_      = xref.owner_;
		asset       = xref.asset;
		xref.owner_ = nullptr;
		return *this;
	}

	inline operator bool() const noexcept { return !!asset; }

	friend crpg::serializer &operator<<(crpg::serializer &s, const xref_base &xref) noexcept;
	friend crpg::serializer &operator>>(crpg::serializer &s, xref_base &xref) noexcept;
};

template<class T> class asset_ref : public xref_base {
public:
	inline asset_ref() = default;
	inline asset_ref(editor::asset *owner, T *asset) noexcept : xref_base(owner, asset) {}

	inline    operator T *() const noexcept { return static_cast<T *>(asset); }
	inline T *operator->() const noexcept { return static_cast<T *>(asset); }

	friend inline crpg::serializer &operator<<(crpg::serializer &s, const asset_ref<T> &ref) noexcept
	{
		return s << reinterpret_cast<const xref_base &>(ref);
	}

	friend inline crpg::serializer &operator>>(crpg::serializer &s, asset_ref<T> &ref) noexcept
	{
		return s >> reinterpret_cast<xref_base &>(ref);
	}
};

} // namespace editor

crpg::serializer &operator<<(crpg::serializer &s, const QString &str) noexcept;
crpg::serializer &operator>>(crpg::serializer &s, QString &str) noexcept;
crpg::serializer &operator<<(crpg::serializer &s, const QStringList &var) noexcept;
crpg::serializer &operator>>(crpg::serializer &s, QStringList &var) noexcept;
crpg::serializer &operator<<(crpg::serializer &s, const QVariant &var) noexcept;
crpg::serializer &operator>>(crpg::serializer &s, QVariant &var) noexcept;
crpg::serializer &operator<<(crpg::serializer &s, const QVariantList &var) noexcept;
crpg::serializer &operator>>(crpg::serializer &s, QVariantList &var) noexcept;
crpg::serializer &operator<<(crpg::serializer &s, const QByteArray &qba) noexcept;
crpg::serializer &operator>>(crpg::serializer &s, QByteArray &qba) noexcept;
