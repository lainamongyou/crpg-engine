#pragma once

#include <engine/graphics/graphics.hpp>
#include <engine/math/box.hpp>
#include <engine/map-tools.hpp>

#include "asset.hpp"

namespace crpg {
class serializer;
}

namespace editor {

struct folder_layer;
class tileset_asset;
class sprite_asset;
class object_asset;
class map_asset;

using tileset_ref = asset_ref<tileset_asset>;
using sprite_ref  = asset_ref<sprite_asset>;
using object_ref  = asset_ref<object_asset>;

/* ------------------------ */

struct map_layer {
	QString               name;
	crpg::map_layer_type  type    = crpg::map_layer_type::invalid;
	crpg::map_layer_blend blend   = crpg::map_layer_blend::alpha;
	folder_layer         *parent  = nullptr;
	crpg::type::asset_id  map_id  = 0;
	crpg::type::layer_id  id      = 0;
	float                 opacity = 1.0f;
	bool                  visible = true;

	inline int get_row() noexcept;

	virtual ~map_layer() = default;

	virtual void      save(crpg::serializer &s) noexcept;
	virtual void      load(crpg::serializer &s) noexcept;
	static map_layer *load_new(crpg::serializer &s) noexcept;
	virtual void      save_state(crpg::serializer &s) noexcept;
	virtual void      load_state(crpg::serializer &s) noexcept;
};

/* ------------------------ */

struct folder_layer : map_layer {
	std::vector<map_layer *> children;

	void save(crpg::serializer &s) noexcept override;
	void load(crpg::serializer &s) noexcept override;
};

inline int map_layer::get_row() noexcept
{
	if (!parent)
		return 0;

	for (size_t i = 0; i < parent->children.size(); i++) {
		map_layer *layer = parent->children[i];
		if (layer == this)
			return (int)i;
	}

	assert(false);
	return 0;
}

/* ------------------------ */

struct tileset_data {
	tileset_ref            asset;
	crpg::type::tileset_id id;
	uint32_t               refs = 0;

	friend crpg::serializer &operator<<(crpg::serializer &s, const tileset_data &tileset) noexcept;
	friend crpg::serializer &operator>>(crpg::serializer &s, tileset_data &tileset) noexcept;
};

struct tileset_layer : map_layer {
	std::map<crpg::type::tileset_id, tileset_data>  tilesets;
	std::map<crpg::type::segment_id, crpg::segment> segments;
	crpg::type::tileset_id                          tileset_id_counter = 1;

	void save_state(crpg::serializer &s) noexcept override;
	void load_state(crpg::serializer &s) noexcept override;

	void remove_tileset_from_segments(crpg::type::tileset_id id) noexcept;

	void remove_tileset(map_asset *map, tileset_asset *asset) noexcept;
};

/* ------------------------ */

struct sprite_data {
	sprite_ref            asset;
	crpg::type::sprite_id id;
	crpg::math::vec2      pos;

	std::vector<crpg::type::segment_id> segments;

	friend crpg::serializer &operator<<(crpg::serializer &s, const sprite_data &sprite) noexcept;
	friend crpg::serializer &operator>>(crpg::serializer &s, sprite_data &sprite) noexcept;
};

struct sprite_segment {
	std::vector<crpg::type::sprite_id> sprites;

	friend crpg::serializer &operator<<(crpg::serializer &s, const sprite_segment &seg) noexcept;
	friend crpg::serializer &operator>>(crpg::serializer &s, sprite_segment &seg) noexcept;
};

struct sprite_layer : map_layer {
	std::map<crpg::type::sprite_id, sprite_data>     sprites;
	std::map<crpg::type::segment_id, sprite_segment> segments;
	crpg::type::sprite_id                            sprite_id_counter = 1;

	void save_state(crpg::serializer &s) noexcept override;
	void load_state(crpg::serializer &s) noexcept override;

	void add_sprite_to_segments(sprite_data &sprite) noexcept;
	void remove_sprite_from_segments(sprite_data &sprite) noexcept;

	void update_sprite(sprite_asset *asset) noexcept;
	void remove_sprite(map_asset *map, sprite_asset *asset) noexcept;
};

/* ------------------------ */

struct object_data {
	object_ref               asset;
	crpg::type::object_id    id;
	crpg::math::vec2         pos;
	crpg::math::vec2         custom_size = crpg::math::vec2::zero();
	std::vector<std::string> properties;

	std::vector<crpg::type::object_id> links;

	std::vector<crpg::type::segment_id> segments;

	crpg::math::box box() const noexcept;

	friend crpg::serializer &operator<<(crpg::serializer &s, const object_data &object) noexcept;
	friend crpg::serializer &operator>>(crpg::serializer &s, object_data &object) noexcept;
};

struct object_segment {
	std::vector<crpg::type::object_id> objects;

	friend crpg::serializer &operator<<(crpg::serializer &s, const object_segment &seg) noexcept;
	friend crpg::serializer &operator>>(crpg::serializer &s, object_segment &seg) noexcept;
};

struct object_layer : map_layer {
	std::map<crpg::type::object_id, object_data>     objects;
	std::map<crpg::type::segment_id, object_segment> segments;
	crpg::type::object_id                            object_id_counter = 1;

	void save_state(crpg::serializer &s) noexcept override;
	void load_state(crpg::serializer &s) noexcept override;

	void save_object_properties(object_asset *asset, crpg::serializer &s) noexcept;
	void load_object_properties(object_asset *asset, crpg::serializer &s) noexcept;

	void add_object_to_segments(object_data &object) noexcept;
	void remove_object_from_segments(object_data &object) noexcept;

	void update_object_sprite(object_asset *asset) noexcept;
	void clear_object_property(object_asset *asset, int row) noexcept;
	void remove_object_property(object_asset *asset, int row) noexcept;
	void insert_object_property(object_asset *asset, int row) noexcept;
	void remove_object(map_asset *map, object_asset *asset) noexcept;
};

/* ------------------------ */

struct collision_layer : tileset_layer {
	void save_collision_data(crpg::serializer &s) noexcept;
};

/* ------------------------ */

class map_asset : public asset {
	friend class asset_model;
	friend class asset;

	map_asset(const QString       &name__,
	          const QString       &file_name_,
	          const QIcon         &icon_,
	          asset               *parent_,
	          crpg::asset_type     type_,
	          crpg::asset_category category_,
	          crpg::type::asset_id id__ = 0) noexcept;

	void save(const QString &path) noexcept override;
	void load() noexcept override;

public:
	folder_layer                                              *root;
	std::map<crpg::type::layer_id, std::unique_ptr<map_layer>> layers;
	crpg::type::layer_id                                       layer_id_counter = 1;

	bool has_layer(const QString &name) const noexcept;
	void open() noexcept override;

	void save_state(crpg::serializer &s) noexcept override;
	void load_state(crpg::serializer &s) noexcept override;

	void save_object_properties(object_asset *asset, crpg::serializer &s) noexcept;
	void load_object_properties(object_asset *asset, crpg::serializer &s) noexcept;

	void update_object_sprite(object_asset *asset) noexcept;
	void update_sprite(sprite_asset *asset) noexcept;

	void clear_object_property(object_asset *asset, int row) noexcept;
	void remove_object_property(object_asset *asset, int row) noexcept;
	void insert_object_property(object_asset *asset, int row) noexcept;

	void remove_tileset(tileset_asset *asset) noexcept;
	void remove_object(object_asset *asset) noexcept;
	void remove_sprite(sprite_asset *asset) noexcept;
};

using map_ref = asset_ref<map_asset>;

} // namespace editor
