#include <engine/utility/file-helpers.hpp>
#include <engine/utility/serializer.hpp>

#include "document-sprite.hpp"
#include "asset-browser.hpp"
#include "asset-sprite.hpp"
#include "main-window.hpp"

#include "ui_document-sprite.h"

#include <QDragEnterEvent>
#include <QDragLeaveEvent>
#include <QDragMoveEvent>
#include <QJsonDocument>
#include <QMessageBox>
#include <QJsonObject>
#include <QJsonArray>
#include <QDropEvent>
#include <QMimeData>
#include <QFileInfo>
#include <QAction>

using namespace crpg;

namespace editor {

document_sprite::document_sprite(QWidget *parent, sprite_asset *asset_) noexcept
        : QWidget(parent), ui(new Ui_document_sprite), asset(asset_)
{
	setAcceptDrops(true);

	ui->setupUi(this);
	view        = ui->view;
	view->asset = asset;

	last_orientation = asset->orientation();
	last_offset      = asset->offset();

	/* clang-format off */
	ui->orientation->addItem(tr("Top Left"),      (uint32_t)orientation::top_left);
	ui->orientation->addItem(tr("Top Center"),    (uint32_t)orientation::top_center);
	ui->orientation->addItem(tr("Top Right"),     (uint32_t)orientation::top_right);
	ui->orientation->addItem(tr("Center Left"),   (uint32_t)orientation::center_left);
	ui->orientation->addItem(tr("Center"),        (uint32_t)orientation::center);
	ui->orientation->addItem(tr("Center Right"),  (uint32_t)orientation::center_right);
	ui->orientation->addItem(tr("Bottom Left"),   (uint32_t)orientation::bottom_left);
	ui->orientation->addItem(tr("Bottom Center"), (uint32_t)orientation::bottom_center);
	ui->orientation->addItem(tr("Bottom Right"),  (uint32_t)orientation::bottom_right);
	/* clang-format on */

	int idx = ui->orientation->findData((uint32_t)last_orientation);
	ui->orientation->setCurrentIndex(idx);
	ui->offset_x->setValue(last_offset.x);
	ui->offset_y->setValue(last_offset.y);

	connect(ui->offset_x, &QDoubleSpinBox::valueChanged, this, &document_sprite::origin_changed);
	connect(ui->offset_y, &QDoubleSpinBox::valueChanged, this, &document_sprite::origin_changed);
	connect(ui->offset_x, &better_double_spin_box::editing_actually_finished, this, &document_sprite::origin_save);
	connect(ui->offset_y, &better_double_spin_box::editing_actually_finished, this, &document_sprite::origin_save);
	connect(ui->orientation, &QComboBox::currentIndexChanged, this, &document_sprite::origin_changed);
	connect(ui->orientation, &QComboBox::currentIndexChanged, this, &document_sprite::origin_save);
	connect(ui->sequences, &QComboBox::currentIndexChanged, this, &document_sprite::set_current_sequence);

	connect(ui->play, &QPushButton::clicked, this, &document_sprite::play_pause);
	connect(ui->stop, &QPushButton::clicked, this, &document_sprite::stop);
	connect(ui->prev, &QPushButton::clicked, this, &document_sprite::prev_frame);
	connect(ui->next, &QPushButton::clicked, this, &document_sprite::next_frame);

	QAction *shortcut = new QAction(this);
	shortcut->setShortcut(QKeySequence(Qt::Key_G));
	connect(shortcut, &QAction::triggered, [this] {
		view->show_grid = !view->show_grid;
		view->update();
	});
	addAction(shortcut);

	shortcut = new QAction(this);
	shortcut->setShortcut(QKeySequence(Qt::Key_B));
	connect(shortcut, &QAction::triggered, [this] {
		view->show_background = !view->show_background;
		view->update();
	});
	addAction(shortcut);
}

document_sprite::~document_sprite() noexcept {}

void document_sprite::origin_changed() noexcept
{
	crpg::orientation orientation = ui->orientation->currentData().value<crpg::orientation>();
	math::vec2        offset((float)ui->offset_x->value(), (float)ui->offset_y->value());

	asset->set_orientation_offset(orientation, offset);
	update_base_only = true;
	view->update();
}

void document_sprite::origin_save() noexcept
{
	if (last_orientation == ui->orientation->currentData().value<crpg::orientation>() &&
	    last_offset.x == (float)ui->offset_x->value() && last_offset.y == (float)ui->offset_y->value())
		return;

	auto undo_redo = [](crpg::serializer &s) {
		type::asset_id    asset_id;
		crpg::orientation orientation;
		math::vec2        offset;

		s >> asset_id >> orientation >> offset;

		sprite_asset *asset = static_cast<sprite_asset *>(asset_manager::get_asset(asset_id));
		asset->set_orientation_offset(orientation, offset);
		asset->open();

		document_sprite *doc  = static_cast<document_sprite *>(asset->document());
		doc->last_orientation = orientation;
		doc->last_offset      = offset;

		auto *ui  = doc->ui.get();
		int   idx = ui->orientation->findData((uint32_t)orientation);
		ui->orientation->setCurrentIndex(idx);
		ui->offset_x->setValue(offset.x);
		ui->offset_y->setValue(offset.y);
	};

	auto save_undo = [this](crpg::serializer &s) { s << asset->id() << last_orientation << last_offset; };
	auto save_redo = [this](crpg::serializer &s) {
		math::vec2 offset = asset->offset();
		s << asset->id() << (uint32_t)asset->orientation() << offset;

		last_orientation = asset->orientation();
		last_offset      = offset;
	};

	QString undo_name = tr("change origin point of sprite '%1'");
	main_window::add_undo_action(undo_name.arg(asset->name()), undo_redo, undo_redo, save_undo, save_redo);
}

void document_sprite::reload_sequences() noexcept
{
	QSignalBlocker blocker(ui->sequences);
	ui->sequences->clear();

	if (!asset->sequences.size())
		return;

	for (auto &[name, seq] : asset->sequences)
		ui->sequences->addItem(name.c_str());

	auto it = asset->sequences.find("default");
	if (it == asset->sequences.end()) {
		it = asset->sequences.find("still");
		if (it == asset->sequences.end()) {
			it = asset->sequences.begin();
		}
	}

	int idx = ui->sequences->findText(it->first.c_str());
	ui->sequences->setCurrentIndex(idx);

	set_current_sequence(idx);
}

void document_sprite::set_current_sequence(int idx) noexcept
{
	auto name = ui->sequences->itemText(idx).toStdString();

	view->stop();
	view->cur_seq     = asset->sequences[name];
	view->cur_reverse = false;
	view->cur_frame   = view->cur_seq.start;
	view->update();
}

void document_sprite::load_animated_sprite(const QString &path) noexcept
{
	QFile file(path);
	if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
		QMessageBox::warning(
		        main_window::get(), tr("Failed to open file"), tr("Failed to open the json file, I guess"));
		return;
	}

	QFileInfo json_fi(path);

	QJsonDocument doc        = QJsonDocument::fromJson(file.readAll());
	QJsonObject   base       = doc.object();
	QJsonArray    frame_data = base["frames"].toArray();
	QJsonObject   meta       = base["meta"].toObject();
	QJsonObject   size       = meta["size"].toObject();
	QJsonArray    tags       = meta["frameTags"].toArray();

	uint32_t   image_cx = (uint32_t)size["w"].toInt();
	uint32_t   image_cy = (uint32_t)size["h"].toInt();
	uint32_t   frame_cx = 0;
	uint32_t   frame_cy = 0;
	math::vec2 uv_size((float)image_cx, (float)image_cy);

	std::vector<frame_info> frames;

	for (int i = 0; i < frame_data.size(); i++) {
		QJsonObject frame_obj = frame_data[i].toObject();
		QJsonObject data      = frame_obj["frame"].toObject();

		if (!frame_cx) {
			frame_cx = (uint32_t)data["w"].toInt();
			frame_cy = (uint32_t)data["h"].toInt();
		}

		uint32_t x  = (uint32_t)data["x"].toInt();
		uint32_t y  = (uint32_t)data["y"].toInt();
		uint32_t x2 = x + frame_cx;
		uint32_t y2 = y + frame_cy;

		frame_info info;
		info.start_vertex = i * 4;
		info.duration_ms  = (uint32_t)frame_obj["duration"].toInt();
		info.min          = math::vec2((float)x, (float)y) / uv_size + math::tiny_epsilon;
		info.max          = math::vec2((float)x2, (float)y2) / uv_size - math::tiny_epsilon;

		frames.push_back(info);
	}

	std::map<std::string, sequence> sequences;

	for (int i = 0; i < tags.size(); i++) {
		QJsonObject tag  = tags[i].toObject();
		QString     type = tag["direction"].toString();
		QString     name = tag["name"].toString();

		sequence seq;
		seq.start = (uint32_t)tag["from"].toInt();
		seq.end   = (uint32_t)tag["to"].toInt();

		if (seq.start == seq.end)
			seq.type = sequence_type::still;
		else if (type == "forward")
			seq.type = sequence_type::forward;
		else if (type == "reverse")
			seq.type = sequence_type::reverse;
		else
			seq.type = sequence_type::cycle;

		sequences.emplace(name.toUtf8().constData(), seq);
	}

	if (!sequences.size()) {
		sequence seq;
		seq.start = 0;
		seq.end   = (uint32_t)frames.size() - 1;
		seq.type  = (frames.size() > 1) ? sequence_type::forward : sequence_type::still;
		sequences.emplace("default", seq);
	}

	QString   image_path = json_fi.absolutePath() + "/" + meta["image"].toString();
	QFileInfo image_fi(image_path);

	std::string ext  = image_fi.suffix().toLower().toUtf8().constData();
	std::string data = load_memory_from_file(image_path.toUtf8().constData());

	auto undo_redo = [](crpg::serializer &s) {
		type::asset_id id;
		s >> id;

		sprite_asset *asset = static_cast<sprite_asset *>(asset_manager::get()->get_asset(id));
		asset->load_state(s);
		asset->select();
		asset->update();
	};
	auto save_undo = [&](crpg::serializer &s) {
		s << this->asset->id();
		this->asset->save_state(s);
	};
	auto save_redo = [&](crpg::serializer &s) {
		this->asset->data      = std::move(data);
		this->asset->ext       = std::move(ext);
		this->asset->frame_cx  = frame_cx;
		this->asset->frame_cy  = frame_cy;
		this->asset->sheet_cx  = image_cx;
		this->asset->sheet_cy  = image_cy;
		this->asset->sequences = std::move(sequences);
		this->asset->frames    = std::move(frames);

		s << this->asset->id();
		this->asset->save_state(s);
	};

	main_window::add_undo_action(
	        tr("update sprite '%1'").arg(asset->name()), undo_redo, undo_redo, save_undo, save_redo);

	asset->update();
}

void document_sprite::load_static_sprite(const QString &path, const std::string &ext) noexcept
{
	std::string           data = load_memory_from_file(path.toUtf8().constData());
	graphics::texture_ref tex  = graphics::texture::create_from_memory(ext.c_str(), data);
	if (!tex) {
		QMessageBox::warning(
		        main_window::get(), tr("Invalid image file"), tr("Image file invalid or unsupported"));
		return;
	}

	auto undo_redo = [](crpg::serializer &s) {
		type::asset_id id;
		s >> id;

		sprite_asset *asset = static_cast<sprite_asset *>(asset_manager::get()->get_asset(id));
		asset->load_state(s);
		asset->select();
		asset->update();
	};
	auto save_undo = [&](crpg::serializer &s) {
		s << this->asset->id();
		this->asset->save_state(s);
	};
	auto save_redo = [&](crpg::serializer &s) {
		this->asset->data     = std::move(data);
		this->asset->ext      = std::move(ext);
		this->asset->frame_cx = tex->cx();
		this->asset->frame_cy = tex->cy();
		this->asset->sheet_cx = tex->cx();
		this->asset->sheet_cy = tex->cy();

		this->asset->sequences.clear();
		this->asset->frames.clear();

		sequence seq;
		seq.type  = sequence_type::still;
		seq.start = 0;
		seq.end   = 0;

		frame_info frame;
		frame.start_vertex = 0;
		frame.duration_ms  = 0;
		frame.min.set(0.0f, 0.0f);
		frame.max.set(1.0f, 1.0f);

		this->asset->frames.push_back(frame);
		this->asset->sequences.emplace("default", seq);

		s << this->asset->id();
		this->asset->save_state(s);
	};

	main_window::add_undo_action(
	        tr("update sprite '%1'").arg(asset->name()), undo_redo, undo_redo, save_undo, save_redo);

	tex.reset();
	asset->update();
}

extern bool supported_image_file_type(const std::string &ext) noexcept;

void document_sprite::dropEvent(QDropEvent *event)
{
	const QMimeData *md = event->mimeData();
	if (!md->hasUrls())
		return;

	QList<QUrl> urls = md->urls();

	QUrl      url  = urls[0];
	QString   file = url.toLocalFile();
	QFileInfo fi(file);

	if (!fi.exists())
		return;

	std::string ext = fi.suffix().toLower().toUtf8().constData();
	if (!supported_image_file_type(ext) && ext != "json")
		return;

	if (asset->data.size()) {
		auto button = QMessageBox::question(main_window::get(),
		                                    tr("Overwrite texture?"),
		                                    tr("Are you sure you want to overwrite "
		                                       "the current sprite? All settings "
		                                       "will be reset."));
		if (button == QMessageBox::No)
			return;
	}

	if (ext == "json") {
		load_animated_sprite(file);
	} else {
		load_static_sprite(file, ext);
	}
}

void document_sprite::dragEnterEvent(QDragEnterEvent *event)
{
	if (event->source() != nullptr) {
		event->setDropAction(Qt::IgnoreAction);
		return;
	}

	event->acceptProposedAction();
}

void document_sprite::dragLeaveEvent(QDragLeaveEvent *event)
{
	event->accept();
}

void document_sprite::dragMoveEvent(QDragMoveEvent *event)
{
	event->acceptProposedAction();
}

void document_sprite::update_sprite() noexcept
{
	view->stop();

	view->texture = asset->texture();
	view->vb      = asset->vb();
	view->update();

	if (!update_base_only)
		reload_sequences();
	update_base_only = false;
}

void document_sprite::next_frame() noexcept
{
	if (view->cur_seq.type == sequence_type::still)
		return;

	if (view->animation_timer.isActive())
		view->pause();
	if (++view->cur_frame > (int)view->cur_seq.end)
		view->cur_frame = (int)view->cur_seq.start;
	view->update();
}

void document_sprite::prev_frame() noexcept
{
	if (view->cur_seq.type == sequence_type::still)
		return;

	if (view->animation_timer.isActive())
		view->pause();
	if (--view->cur_frame < (int)view->cur_seq.start)
		view->cur_frame = (int)view->cur_seq.end;
	view->update();
}

void document_sprite::play_pause() noexcept
{
	if (view->cur_seq.type == sequence_type::still)
		return;

	if (view->animation_timer.isActive())
		view->pause();
	else
		view->play();
}

void document_sprite::stop() noexcept
{
	view->stop();
}

/* ========================================================================= */

void sprite_view::init() noexcept
{
	main_view::init();

	lock_view = true;

	/* clang-format off */
	graphics::vertex_buffer::data vbd;
	vbd.points.emplace_back( 0.0f,  1.0f);
	vbd.points.emplace_back( 0.0f, -1.0f);
	vbd.points.emplace_back( 1.0f,  0.0f);
	vbd.points.emplace_back(-1.0f,  0.0f);

	vbd.points.emplace_back(-0.5f, -0.5f);
	vbd.points.emplace_back( 0.5f, -0.5f);
	vbd.points.emplace_back(-0.5f,  0.5f);
	vbd.points.emplace_back( 0.5f,  0.5f);
	vbd.points.emplace_back(-0.5f, -0.5f);
	vbd.points.emplace_back(-0.5f,  0.5f);
	vbd.points.emplace_back( 0.5f, -0.5f);
	vbd.points.emplace_back( 0.5f,  0.5f);
	/* clang-format on */

	origin_vb = graphics::vertex_buffer::create(std::move(vbd));

	animation_timer.setTimerType(Qt::PreciseTimer);
	animation_timer.setSingleShot(true);
	connect(&animation_timer, &QTimer::timeout, this, &sprite_view::next_frame);
}

void sprite_view::play() noexcept
{
	if (cur_seq.type == sequence_type::still)
		return;

	frame_info frame = asset->frames[cur_frame];
	animation_timer.start((int)frame.duration_ms);
}

void sprite_view::pause() noexcept
{
	frame_info frame = asset->frames[cur_frame];
	animation_timer.stop();
}

void sprite_view::stop() noexcept
{
	cur_frame = cur_seq.start;
	animation_timer.stop();
	update();
}

void sprite_view::next_frame() noexcept
{
	if (cur_seq.type == sequence_type::forward) {
		if (++cur_frame > (int)cur_seq.end)
			cur_frame = (int)cur_seq.start;

	} else if (cur_seq.type == sequence_type::reverse) {
		if (--cur_frame < (int)cur_seq.start)
			cur_frame = (int)cur_seq.end;

	} else if (cur_seq.type == sequence_type::cycle) {
		if (cur_reverse) {
			if (cur_frame == (int)cur_seq.start) {
				cur_frame++;
				cur_reverse = !cur_reverse;
			} else {
				cur_frame--;
			}
		} else {
			if (cur_frame == (int)cur_seq.end) {
				cur_frame--;
				cur_reverse = !cur_reverse;
			} else {
				cur_frame++;
			}
		}
	}

	play();
	update();
}

void sprite_view::calc_view() noexcept
{
	main_view::calc_view();

	if (!texture || !vb || !asset)
		return;

	constexpr float padding = 4.0f;

	sprite_size.x = (float)asset->frame_cx;
	sprite_size.y = (float)asset->frame_cy;

	float render_cx = sprite_size.x + padding * 2.0f;
	float render_cy = sprite_size.y + padding * 2.0f;

	float view_cx = (float)qt_size.width();
	float view_cy = (float)qt_size.height();

	float view_aspect   = view_cx / view_cy;
	float render_aspect = render_cx / render_cy;

	tl.set_zero();

	if (view_aspect > render_aspect) {
		br.x = render_cy * view_aspect;
		br.y = render_cy;
	} else {
		br.x = render_cx;
		br.y = render_cx / view_aspect;
	}

	view_size = br - tl;

	math::vec2 adjust = view_size * 0.5f - sprite_size * 0.5f;
	tl -= adjust;
	br -= adjust;
}

void sprite_view::render() noexcept
{
	if (!texture || !vb || !asset)
		return;

	if (show_background)
		render_transparency_background();

	/* ---------------------------- */

	const float tile_width = (float)asset->tile_width;

	vs_unlit->load();
	ps_unlit->load();
	vb->load();

	ps_unlit->set("image", texture);

	frame_info frame = asset->frames[cur_frame];

	graphics::matrix_push();
	graphics::matrix_scale(tile_width, tile_width, 1.0f);
	graphics::matrix_translate(asset->origin());
	graphics::draw(graphics::primitive::tristrip, frame.start_vertex, 4);
	graphics::matrix_pop();

	/* ---------------------------- */

	if (show_grid)
		render_grid();

	/* ---------------------------- */

	vs_solid->load();
	ps_solid->load();
	vb_square->load();

	vs_solid->set_world(math::matrix::identity());
	ps_solid->set("color", math::vec4(0.0f, 1.0f, 0.0f, 1.0f));

	graphics::matrix_push();
	graphics::matrix_scale(sprite_size.x, sprite_size.y, 1.0f);
	graphics::draw(graphics::primitive::linestrip);
	graphics::matrix_pop();

	/* ---------------------------- */

	origin_vb->load();
	ps_solid->set("color", math::vec4(1.0f, 0.0f, 0.0f, 1.0f));

	graphics::matrix_push();
	graphics::matrix_translate(asset->origin());
	graphics::draw(graphics::primitive::lines);
	graphics::matrix_pop();
}

} // namespace editor
