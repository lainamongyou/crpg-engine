#pragma once

#include "asset-sprite.hpp"
#include "main-view.hpp"

#include <engine/math/vec2.hpp>
#include <engine/types.hpp>

#include <QWidget>
#include <QTimer>

#include <memory>

class Ui_document_sprite;

namespace editor {

class sprite_view;

class document_sprite : public QWidget {
	Q_OBJECT

	friend class sprite_view;

	std::unique_ptr<Ui_document_sprite> ui;
	sprite_asset                       *asset;
	sprite_view                        *view;

	bool update_base_only = false;

	crpg::orientation last_orientation = crpg::orientation::top_left;
	crpg::math::vec2  last_offset      = crpg::math::vec2::zero();

	void dropEvent(QDropEvent *event) override;
	void dragEnterEvent(QDragEnterEvent *event) override;
	void dragLeaveEvent(QDragLeaveEvent *event) override;
	void dragMoveEvent(QDragMoveEvent *event) override;

	void load_animated_sprite(const QString &path) noexcept;
	void load_static_sprite(const QString &path, const std::string &ext) noexcept;

	void origin_changed() noexcept;
	void origin_save() noexcept;

	void reload_sequences() noexcept;
	void set_current_sequence(int idx) noexcept;

	void next_frame() noexcept;
	void prev_frame() noexcept;
	void stop() noexcept;
	void play_pause() noexcept;

public:
	document_sprite(QWidget *parent, sprite_asset *asset) noexcept;
	~document_sprite();

public slots:
	void update_sprite() noexcept;
};

class sprite_view : public main_view {
	Q_OBJECT

	friend class document_sprite;
	friend class document_object;

	sprite_asset *asset = nullptr;

	QTimer   animation_timer;
	sequence cur_seq;
	bool     cur_reverse = false;
	int      cur_frame   = 0;

	crpg::graphics::vertex_buffer_ref origin_vb;
	crpg::graphics::vertex_buffer_ref box_vb;
	crpg::graphics::vertex_buffer_ref vb;
	crpg::graphics::texture_ref       texture;
	crpg::math::vec2                  sprite_size;
	bool                              show_background = true;
	bool                              show_grid       = false;

	void next_frame() noexcept;

	void calc_view() noexcept override;

public:
	inline sprite_view(QWidget *parent) noexcept : main_view(parent) {}

	void init() noexcept override;
	void render() noexcept override;

	void play() noexcept;
	void pause() noexcept;
	void stop() noexcept;
};

} // namespace editor
