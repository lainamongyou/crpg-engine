#include <engine/utility/serializer.hpp>
#include <engine/math/math-extra.hpp>
#include <engine/utility/logging.hpp>

#include "object-properties-widget.hpp"
#include "map-layers-widget.hpp"
#include "asset-browser.hpp"
#include "asset-tileset.hpp"
#include "asset-sprite.hpp"
#include "asset-object.hpp"
#include "tile-selector.hpp"
#include "document-map.hpp"
#include "main-window.hpp"
#include "asset-map.hpp"

#include <QGuiApplication>
#include <QPainter>
#include <QAction>

using namespace crpg;

namespace editor {

void document_map::init() noexcept
{
	main_view::init();

	doc = this;

	for (auto &pair : asset->layers) {
		map_layer *layer = pair.second.get();
		add_renderable_layer(layer);
	}

	QAction *shortcut = new QAction(this);
	shortcut->setShortcut(QKeySequence(Qt::Key_E));
	connect(shortcut, &QAction::triggered, [this] {
		cur_mode = mode::erase;
		update();
	});
	addAction(shortcut);

	shortcut = new QAction(this);
	shortcut->setShortcut(QKeySequence(Qt::Key_I));
	connect(shortcut, &QAction::triggered, [this] {
		cur_mode = mode::insert;
		update();
	});
	addAction(shortcut);

	shortcut = new QAction(this);
	shortcut->setShortcut(QKeySequence(Qt::Key_S));
	connect(shortcut, &QAction::triggered, [this] {
		cur_mode = mode::select;
		update();
	});
	addAction(shortcut);

	shortcut = new QAction(this);
	shortcut->setShortcut(QKeySequence(Qt::Key_Escape));
	connect(shortcut, &QAction::triggered, this, &document_map::on_escape_pressed);
	addAction(shortcut);

	QAction *grid_toggle_shortcut = new QAction(this);
	grid_toggle_shortcut->setShortcut(QKeySequence(Qt::Key_G));
	connect(grid_toggle_shortcut, &QAction::triggered, [this] {
		show_grid = !show_grid;
		update();
	});
	addAction(grid_toggle_shortcut);

	shortcut = new QAction(this);
	shortcut->setShortcut(QKeySequence(Qt::Key_Delete));
	connect(shortcut, &QAction::triggered, this, &document_map::on_delete_pressed);
	addAction(shortcut);

	/* ------------------------------------- */
	/* create viewport mode text             */

	QFont font = this->font();
	font.setPixelSize(16);
	QFontMetrics metric(font);

	auto create_tex = [&](const QString &text) -> crpg::graphics::texture_ref {
		QRect  rect = metric.boundingRect(text);
		QImage img(rect.size(), QImage::Format_ARGB32);
		img.fill(0);

		rect.moveTo(0, 0);

		QPainter painter(&img);
		painter.setFont(font);
		painter.setPen(QColor(0xFFFFFFFF));
		painter.drawText(rect, Qt::AlignLeft | Qt::AlignTop, text);
		painter.end();

		const uint8_t *bits = img.bits();
		return crpg::graphics::texture::create(
		        rect.width(), rect.height(), 1, graphics::texture_format::rgba, &bits);
	};

	tex_insert = create_tex(tr("Insert"));
	tex_select = create_tex(tr("Select"));
	tex_erase  = create_tex(tr("Erase"));

	/* ------------------------------------- */
	/* create manipulator vertex buffers     */

	graphics::vertex_buffer::data vbd;
	vbd.points.emplace_back(0.02f, -0.02f);
	vbd.points.emplace_back(-0.02f, 0.02f);
	vbd.points.emplace_back(1.0f, -0.02f);
	vbd.points.emplace_back(1.0f, 0.02f);

	vb_manipulator_arm = graphics::vertex_buffer::create(vbd);

	vbd.clear();
	vbd.points.emplace_back(1.0f, 0.1f);
	vbd.points.emplace_back(1.0f, -0.1f);
	vbd.points.emplace_back(1.5f, 0.0f);
	vbd.points.emplace_back(1.5f, 0.0f);

	vb_manipulator_end = graphics::vertex_buffer::create(vbd);

	vbd.clear();
	vbd.points.emplace_back(0.51f, 0.0f);
	vbd.points.emplace_back(0.49f, 0.0f);
	vbd.points.emplace_back(0.51f, -0.51f);
	vbd.points.emplace_back(0.49f, -0.49f);

	vb_manipulator_inside = graphics::vertex_buffer::create(vbd);

	/* ------------------------------------- */
	/* create point object vertex buffers    */

	vbd.clear();
	vbd.points.emplace_back(0.0f, 0.25f);
	vbd.points.emplace_back(-0.25f, 0.0f);
	vbd.points.emplace_back(0.25f, 0.0f);
	vbd.points.emplace_back(0.0f, -0.25f);

	vb_point_object_solid = graphics::vertex_buffer::create(vbd);

	vbd.clear();
	vbd.points.emplace_back(0.0f, 0.5f);
	vbd.points.emplace_back(0.0f, -0.5f);
	vbd.points.emplace_back(-0.5f, 0.0f);
	vbd.points.emplace_back(0.5f, 0.0f);

	vbd.points.emplace_back(-0.25f, -0.25f);
	vbd.points.emplace_back(0.25f, -0.25f);
	vbd.points.emplace_back(-0.25f, 0.25f);
	vbd.points.emplace_back(0.25f, 0.25f);
	vbd.points.emplace_back(-0.25f, 0.25f);
	vbd.points.emplace_back(-0.25f, -0.25f);
	vbd.points.emplace_back(0.25f, 0.25f);
	vbd.points.emplace_back(0.25f, -0.25f);

	vb_point_object_lines = graphics::vertex_buffer::create(vbd);

	/* ------------------------------------- */
	/* link arrow/line vertex buffers        */

	vbd.clear();
	vbd.points.emplace_back(0.25f, -0.25f);
	vbd.points.emplace_back(0.0f, 0.0f);
	vbd.points.emplace_back(0.25f, 0.25f);

	vb_link_arrow = graphics::vertex_buffer::create(vbd);

	vbd.clear();
	vbd.points.emplace_back(0.0f, 0.0f);
	vbd.points.emplace_back(1.0f, 0.0f);

	vb_link_line = graphics::vertex_buffer::create(vbd);
}

document_map::~document_map() noexcept
{
	map_layers_model *model = map_layers_model::get();
	if (model->doc == this)
		model->clear_document();
}

void document_map::set_active_layer() noexcept
{
	auto                *mlw       = map_layers_widget::get();
	QItemSelectionModel *selection = mlw->selectionModel();
	QModelIndexList      indices   = selection->selectedIndexes();

	if (indices.size()) {
		map_layer *layer = static_cast<map_layer *>(indices[0].internalPointer());
		cur_layer_id     = layer->id;
	} else {
		cur_layer_id = 0;
	}

	last_sprite_asset  = nullptr;
	last_tileset_asset = nullptr;

	update();
}

template<typename T> static inline void add_known_segment(std::vector<T *> &segs, T *seg) noexcept
{
	auto it = std::find(segs.begin(), segs.end(), seg);
	if (it == segs.end())
		segs.push_back(seg);
}

template<typename T> static inline void remove_known_segment(std::vector<T *> &segs, T *seg) noexcept
{
	auto it = std::find(segs.begin(), segs.end(), seg);
	if (it != segs.end())
		segs.erase(it);
}

void document_map::insert_tile(tileset_layer                        *layer,
                               renderable_tileset_layer             *rlayer,
                               std::vector<crpg::segment *>         &segs,
                               std::vector<crpg::tileset_segment *> &rsegs,
                               const crpg::math::vec2               &pos,
                               tileset_data                         *tileset,
                               uint8_t                               x,
                               uint8_t                               y) noexcept
{
	crpg::map_tools::seg_pos sp = crpg::get_map_segment_position(pos);

	auto it      = layer->segments.find(sp.hash);
	bool new_seg = it == layer->segments.end();
	if (new_seg && !tileset)
		return;

	crpg::segment         *seg  = &layer->segments[sp.hash];
	crpg::tileset_segment *rseg = &rlayer->segments[sp.hash];

	if (!stored_segments[sp.hash]) {
		if (new_seg) {
			if (tileset) {
				seg->x = (int)sp.x * (int)segment::width;
				seg->y = (int)sp.y * (int)segment::width;
				created_segments.push_back(sp.hash);
			}
		} else {
			modified_tileset_segments.push_back(*seg);
		}
	}

	stored_segments[sp.hash] = true;

	uint8_t tile_x = (uint8_t)((int)pos.x - seg->x);
	uint8_t tile_y = (uint8_t)((int)pos.y - seg->y);

	/* ------------------------------------------------ */
	/* find tile */

	crpg::segment::tile &tile = seg->tiles[tile_y][tile_x];

	auto backup_tileset = [this](tileset_data *tileset) {
		if (!tileset)
			return;
		if (modified_tilesets.find(tileset->id) == modified_tilesets.end()) {
			modified_tilesets.emplace(tileset->id, *tileset);
		}
	};

	tileset_data *cur_tileset = tile.tileset_id == 0 ? nullptr : &layer->tilesets[tile.tileset_id];

	backup_tileset(tileset);
	backup_tileset(cur_tileset);

	if (cur_tileset != tileset) {
		if (cur_tileset) {
			cur_tileset->refs--;
			if (!cur_tileset->refs) {
				layer->tilesets.erase(cur_tileset->id);
			}
		}
		if (tileset)
			tileset->refs++;
	}

	if (!cur_tileset && tileset) {
		seg->refs++;
	} else if (cur_tileset && !tileset) {
		seg->refs--;
	}

	tile.tileset_id = tileset ? tileset->id : 0;
	tile.vals[0].x  = x;
	tile.vals[0].y  = y;
	tile.type       = crpg::tile_type::normal;

	if (seg->refs == 0) {
		remove_known_segment(segs, seg);
		remove_known_segment(rsegs, rseg);

		layer->segments.erase(sp.hash);
		rlayer->segments.erase(sp.hash);
	} else {
		add_known_segment(segs, seg);
		add_known_segment(rsegs, rseg);
	}
}

void document_map::on_insert_tileset_selection() noexcept
{
	tile_selector *selector  = tile_selector::get();
	tileset_layer *layer     = static_cast<tileset_layer *>(get_cur_layer());
	bool           collision = !!dynamic_cast<collision_layer *>(layer);
	if (!layer)
		return;
	if (cur_mode == mode::insert && !selector->selected())
		return;

	tileset_asset *ts_asset = selector->asset();
	if (!ts_asset || collision && ts_asset->ext != "mask" || !collision && ts_asset->ext == "mask")
		return;

	std::vector<crpg::segment *>         segs;
	std::vector<crpg::tileset_segment *> rsegs;

	tileset_data     *tileset = nullptr;
	tileset_selection sel     = cur_mode == mode::erase ? tileset_selection::zero() : selector->selection();
	math::vec2        offset;

	renderable_tileset_layer *rlayer =
	        static_cast<renderable_tileset_layer *>(renderable_layers[cur_layer_id].get());

	if (cur_mode == mode::insert) {
		for (auto &pair : layer->tilesets) {
			tileset_data &cur_tileset = pair.second;
			if (cur_tileset.asset == selector->asset()) {
				tileset = &cur_tileset;
				break;
			}
		}

		if (!tileset) {
			tileset_data new_tileset;
			new_tileset.asset.set(asset, selector->asset());
			new_tileset.id = layer->tileset_id_counter++;
			layer->tilesets.emplace(new_tileset.id, new_tileset);

			tileset = &layer->tilesets[new_tileset.id];
		}
	}

	for (uint8_t y = sel.min.y; y <= sel.max.y; y++) {
		offset.y = (float)(y - sel.min.y);

		for (uint8_t x = sel.min.x; x <= sel.max.x; x++) {
			offset.x       = (float)(x - sel.min.x);
			math::vec2 pos = (cursor_pos + offset).floor();

			insert_tile(layer, rlayer, segs, rsegs, pos, tileset, x, y);
		}
	}

	for (size_t i = 0; i < segs.size(); i++) {
		crpg::segment         *seg  = segs[i];
		crpg::tileset_segment *rseg = rsegs[i];
		build_tileset_segment(layer, *seg, *rseg);
	}

	last_insert_pos = cursor_pos.floor();
}

void document_map::add_sprite(sprite_layer *layer, renderable_sprite_layer *rlayer, sprite_data &sprite) noexcept
{
	layer->add_sprite_to_segments(sprite);

	renderable_sprite rsprite;
	rsprite.pos = sprite.pos;
	rsprite.tex = sprite.asset->texture();
	rsprite.vb  = sprite.asset->vb();
	rlayer->sprites.emplace(sprite.id, std::move(rsprite));
	layer->sprites.emplace(sprite.id, std::move(sprite));
}

void document_map::remove_sprite(sprite_layer            *layer,
                                 renderable_sprite_layer *rlayer,
                                 type::sprite_id          sprite_id) noexcept
{
	sprite_data &sprite = layer->sprites[sprite_id];

	layer->remove_sprite_from_segments(sprite);

	layer->sprites.erase(sprite_id);
	rlayer->sprites.erase(sprite_id);
}

void document_map::add_object(object_layer *layer, renderable_sprite_layer *rlayer, object_data &object) noexcept
{
	layer->add_object_to_segments(object);

	renderable_sprite rsprite;
	build_object(rsprite, object);
	rlayer->sprites.emplace(object.id, std::move(rsprite));
	layer->objects.emplace(object.id, std::move(object));
}

void document_map::remove_object(object_layer            *layer,
                                 renderable_sprite_layer *rlayer,
                                 type::object_id          object_id) noexcept
{
	object_data &object = layer->objects[object_id];
	object_properties_widget::remove_object(object);
	layer->remove_object_from_segments(object);
	layer->objects.erase(object_id);
	rlayer->sprites.erase(object_id);
}

void document_map::on_insert_sprite() noexcept
{
	sprite_layer *layer = static_cast<sprite_layer *>(get_cur_layer());
	if (!layer)
		return;

	renderable_sprite_layer *rlayer = static_cast<renderable_sprite_layer *>(renderable_layers[cur_layer_id].get());

	if (cur_mode == mode::erase) {
		type::sprite_id hover_sprite = get_hover_sprite();
		if (hover_sprite) {
			removed_sprites.push_back(layer->sprites[hover_sprite]);
			remove_sprite(layer, rlayer, hover_sprite);
			update();
		}

		return;
	}

	editor::asset *selected_asset = asset_manager::selected_asset();
	if (!selected_asset || !selected_asset->is_file(asset_category::sprite))
		return;

	sprite_data sprite;
	sprite.asset.set(asset, selected_asset);
	sprite.id  = layer->sprite_id_counter++;
	sprite.pos = ((cursor_pos * 2.0f).round() / 2.0f);
	add_sprite(layer, rlayer, sprite);

	added_sprites.push_back(sprite.id);
}

void document_map::on_insert_object() noexcept
{
	object_layer *layer = static_cast<object_layer *>(get_cur_layer());
	if (!layer)
		return;

	renderable_sprite_layer *rlayer = static_cast<renderable_sprite_layer *>(renderable_layers[cur_layer_id].get());

	if (cur_mode == mode::erase) {
		type::object_id hover_object = get_hover_object();
		if (hover_object) {
			if (hover_object == selected_object) {
				manipulator = manipulator_mode::none;
				set_selected_object(0);
			}
			removed_objects.push_back(layer->objects[hover_object]);
			remove_object(layer, rlayer, hover_object);
			update();
		}

		return;
	}

	editor::asset *selected_asset = asset_manager::selected_asset();
	if (!selected_asset || !selected_asset->is_file(asset_category::object))
		return;

	object_data object;
	object.asset.set(asset, selected_asset);
	object.id  = layer->object_id_counter++;
	object.pos = ((cursor_pos * 2.0f).floor() / 2.0f);
	object.custom_size.set(0.5f, 0.5f);
	object.properties.resize(object.asset->properties.size());
	add_object(layer, rlayer, object);

	inserted_pos    = object.pos;
	inserted_object = object.id;
	last_insert_pos = inserted_pos;

	added_objects.push_back(object.id);

	renderable_sprite &rsprite = rlayer->sprites[object.id];
	build_object(rsprite, object);
}

inline map_layer *document_map::get_cur_layer() const noexcept
{
	auto it = asset->layers.find(cur_layer_id);
	if (it == asset->layers.end())
		return nullptr;
	return it->second.get();
}

inline crpg::map_layer_type document_map::cur_layer_type() const noexcept
{
	map_layer *layer = get_cur_layer();
	return layer ? layer->type : map_layer_type::invalid;
}

void document_map::on_insert() noexcept
{
	switch (cur_layer_type()) {
	case map_layer_type::tileset:
	case map_layer_type::collision:
		on_insert_tileset_selection();
		break;
	case map_layer_type::sprite:
		on_insert_sprite();
		break;
	case map_layer_type::object:
		on_insert_object();
		break;
	}
}

void document_map::on_select() noexcept
{
	auto *layer = static_cast<object_layer *>(get_cur_layer());
	if (!layer || layer->type != map_layer_type::object)
		return;

	crpg::type::object_id id = get_hover_object();
	set_selected_object(id);

	update();
}

document_map::manipulator_mode document_map::get_manipulator_mode(math::vec2 obj_window_pos) noexcept
{
	math::box x_and_y_area(math::vec2(0.0f, -50.0f), math::vec2(50.0f, 0.0f));
	math::box x_area(math::vec2(0.0f, -10.0f), math::vec2(150.0f, 10.0f));
	math::box y_area(math::vec2(-10.0f, -150.0f), math::vec2(10.0f, 0.0f));

	x_and_y_area += obj_window_pos;
	x_area += obj_window_pos;
	y_area += obj_window_pos;

	math::vec2 window_cursor_pos((float)qt_cursor_pos.x(), (float)qt_cursor_pos.y());

	if (x_and_y_area.inside(window_cursor_pos)) {
		return manipulator_mode::x_and_y;

	} else if (x_area.inside(window_cursor_pos)) {
		return manipulator_mode::x;

	} else if (y_area.inside(window_cursor_pos)) {
		return manipulator_mode::y;

	} else {
		return manipulator_mode::none;
	}
}

bool document_map::on_manipulator_clicked() noexcept
{
	object_layer *layer = static_cast<object_layer *>(get_cur_layer());

	if (!layer || layer->type != map_layer_type::object || !selected_object) {
		manipulator = manipulator_mode::none;
		return false;
	}

	/* ----------------------------------------------- */
	/* get object position in window                   */

	object_data &obj = layer->objects[selected_object];
	math::vec2   pos = world_to_window(obj.pos);

	/* ----------------------------------------------- */
	/* get manipulator mode                            */

	manipulator = get_manipulator_mode(pos);
	if (manipulator == manipulator_mode::none)
		return false;

	manipulator_offset = cursor_pos - obj.pos;
	inserted_pos       = obj.pos;
	last_insert_pos    = inserted_pos;

	return true;
}

void document_map::on_link_objects(object_layer *layer)
{
	renderable_sprite_layer *rlayer = static_cast<renderable_sprite_layer *>(renderable_layers[cur_layer_id].get());

	/* ------------------------------------------------------------ */
	/* if no object is currently selected, just do a regular select */

	if (selected_object == 0) {
		on_select();
		return;
	}

	/* ------------------------------------------------------------ */
	/* get linked object; ignore self when linking                  */

	object_data &object1 = layer->objects[selected_object];

	crpg::type::object_id id = get_hover_object();
	if (id == 0 || id == selected_object) {
		return;
	}

	/* ------------------------------------------------------------ */
	/* get and link to object 2                                     */

	object_data &object2 = layer->objects[id];

	auto it = std::find(object1.links.begin(), object1.links.end(), id);
	if (it == object1.links.end()) {
		object1.links.push_back(id);
	} else {
		return;
	}

	/* ------------------------------------------------------------ */
	/* handle undo/redo                                             */

	auto undo_redo = [](crpg::serializer &s) {
		type::asset_id  asset_id;
		type::layer_id  layer_id;
		type::object_id object1_id;
		type::object_id object2_id;
		bool            undo;

		s >> asset_id >> layer_id >> undo;

		constexpr auto get_asset = asset_manager::get_asset;
		map_asset     *asset     = static_cast<map_asset *>(get_asset(asset_id));
		document_map  *doc       = static_cast<document_map *>(asset->document());
		object_layer  *layer     = static_cast<object_layer *>(asset->layers[layer_id].get());

		asset->open();

		s >> object1_id >> object2_id;

		object_data &object1 = layer->objects[object1_id];

		if (undo) {
			auto it = std::find(object1.links.begin(), object1.links.end(), object2_id);
			if (it != object1.links.end()) {
				object1.links.erase(it);
			}
		} else {
			auto it = std::find(object1.links.begin(), object1.links.end(), object2_id);
			if (it == object1.links.end()) {
				object1.links.push_back(object2_id);
			}
		}

		doc->update();
	};

	auto save_undo = [&](crpg::serializer &s) {
		s << asset->id() << cur_layer_id << true << selected_object << id;
	};
	auto save_redo = [&](crpg::serializer &s) {
		s << asset->id() << cur_layer_id << false << selected_object << id;
	};

	QString text = tr("link objects in layer '%1'").arg(layer->name);
	main_window::add_undo_action(text, undo_redo, undo_redo, save_undo, save_redo);

	/* ------------------------------------------------------------ */
	/* if control is held down, select object as well               */

	Qt::KeyboardModifiers modifiers = QGuiApplication::keyboardModifiers();
	if ((modifiers & Qt::ControlModifier) != 0) {
		on_select();
	}

	update();
}

void document_map::mousePressEvent(QMouseEvent *event)
{
	main_view::mousePressEvent(event);

	if (event->button() == Qt::LeftButton) {
		if (cur_mode == mode::select) {
			object_layer *layer = static_cast<object_layer *>(get_cur_layer());

			if (layer->type == crpg::map_layer_type::object) {
				Qt::KeyboardModifiers modifiers = QGuiApplication::keyboardModifiers();
				if ((modifiers & Qt::AltModifier) != 0) {
					on_link_objects(layer);
					return;
				}
			}

			if (!on_manipulator_clicked()) {
				on_select();
			}
		} else if (cur_mode == mode::insert || cur_mode == mode::erase) {
			on_insert();
		}

		update();

		mouse_down  = true;
		mouse_moved = false;
	}
}

void document_map::save_tileset_edit_undo_redo() noexcept
{
	tile_selector *selector = tile_selector::get();
	tileset_layer *layer    = static_cast<tileset_layer *>(get_cur_layer());

	if (cur_mode == mode::insert && !selector->selected())
		return;

	auto undo_redo = [](crpg::serializer &s) {
		type::asset_id asset_id;
		type::layer_id layer_id;
		size_t         count;

		s >> asset_id >> layer_id;

		constexpr auto            get_asset = asset_manager::get_asset;
		map_asset                *asset     = static_cast<map_asset *>(get_asset(asset_id));
		document_map             *doc       = static_cast<document_map *>(asset->document());
		tileset_layer            *layer     = static_cast<tileset_layer *>(asset->layers[layer_id].get());
		renderable_tileset_layer *rlayer =
		        static_cast<renderable_tileset_layer *>(doc->renderable_layers[layer_id].get());

		asset->open();

		/* tilesets */

		s >> count;
		for (size_t i = 0; i < count; i++) {
			type::asset_id   tileset_asset_id;
			type::tileset_id tileset_id;
			int              refs;

			s >> tileset_asset_id >> tileset_id >> refs;

			auto it = layer->tilesets.find(tileset_id);
			if (it == layer->tilesets.end()) {
				/* create */
				tileset_data new_tileset;
				new_tileset.asset.set(asset, get_asset(tileset_asset_id));
				new_tileset.id   = tileset_id;
				new_tileset.refs = refs;
				layer->tilesets.emplace(new_tileset.id, new_tileset);

			} else if (refs == 0) {
				/* remove */
				layer->tilesets.erase(it);

			} else {
				/* modify */
				it->second.refs = refs;
			}
		}

		/* segments */

		s >> count;
		for (size_t i = 0; i < count; i++) {
			type::segment_id hash;
			bool             remove;

			s >> hash >> remove;

			if (remove) {
				layer->segments.erase(hash);
				rlayer->segments.erase(hash);
			} else {
				crpg::segment         &seg  = layer->segments[hash];
				crpg::tileset_segment &rseg = rlayer->segments[hash];

				s >> seg;

				doc->build_tileset_segment(layer, seg, rseg);
			}
		}

		doc->update();
	};

	auto save_undo = [&](crpg::serializer &s) {
		s << asset->id() << cur_layer_id;

		size_t count = modified_tilesets.size();
		s << count;

		for (auto &pair : modified_tilesets) {
			tileset_data &tileset = pair.second;
			s << tileset.asset->id() << tileset.id << tileset.refs;
		}

		count = modified_tileset_segments.size() + created_segments.size();
		s << count;

		for (crpg::segment &seg : modified_tileset_segments) {
			s << seg.hash() << false << seg;
		}
		for (type::segment_id hash : created_segments) {
			s << hash << true;
		}
	};
	auto save_redo = [&](crpg::serializer &s) {
		s << asset->id() << cur_layer_id;

		size_t count = modified_tilesets.size();
		s << count;

		for (auto &pair : modified_tilesets) {
			tileset_data &tileset = pair.second;
			auto          new_it  = layer->tilesets.find(tileset.id);
			s << tileset.asset->id() << tileset.id
			  << (new_it != layer->tilesets.end() ? new_it->second.refs : 0);
		}

		count = modified_tileset_segments.size() + created_segments.size();
		s << count;

		for (crpg::segment &seg : modified_tileset_segments) {
			const type::segment_id hash   = seg.hash();
			bool                   remove = layer->segments.find(hash) == layer->segments.end();
			s << hash << remove;

			if (!remove) {
				crpg::segment &new_seg = layer->segments[hash];
				s >> new_seg;
			}
		}
		for (type::segment_id hash : created_segments) {
			s << hash << false << layer->segments[hash];
		}
	};

	QString text = tr("edit layer '%1' in '%2'").arg(layer->name, asset->name());
	main_window::add_undo_action(text, undo_redo, undo_redo, save_undo, save_redo);

	stored_segments.clear();
	created_segments.clear();
	modified_tileset_segments.clear();
	modified_tilesets.clear();
}

void document_map::save_sprite_edit_undo_redo() noexcept
{
	if (!added_sprites.size() && !removed_sprites.size())
		return;

	auto undo_redo = [](crpg::serializer &s) {
		type::asset_id asset_id;
		type::layer_id layer_id;
		size_t         count;

		s >> asset_id >> layer_id;

		constexpr auto           get_asset = asset_manager::get_asset;
		map_asset               *asset     = static_cast<map_asset *>(get_asset(asset_id));
		document_map            *doc       = static_cast<document_map *>(asset->document());
		sprite_layer            *layer     = static_cast<sprite_layer *>(asset->layers[layer_id].get());
		renderable_sprite_layer *rlayer =
		        static_cast<renderable_sprite_layer *>(doc->renderable_layers[layer_id].get());

		asset->open();

		s >> count;
		for (size_t i = 0; i < count; i++) {
			type::asset_id  sprite_asset_id;
			type::sprite_id sprite_id;
			math::vec2      pos;

			s >> sprite_asset_id >> sprite_id >> pos;

			auto it = layer->sprites.find(sprite_id);
			if (it == layer->sprites.end()) {
				/* create */
				sprite_data new_sprite;
				new_sprite.asset.set(asset, get_asset(sprite_asset_id));
				new_sprite.id  = sprite_id;
				new_sprite.pos = pos;

				doc->add_sprite(layer, rlayer, new_sprite);
			} else {
				/* remove */
				doc->remove_sprite(layer, rlayer, sprite_id);
			}
		}

		doc->update();
	};

	sprite_layer *layer = static_cast<sprite_layer *>(get_cur_layer());

	auto save_undo_redo = [&](crpg::serializer &s) {
		s << asset->id() << cur_layer_id;
		s << (added_sprites.size() + removed_sprites.size());

		for (type::sprite_id sprite_id : added_sprites) {
			sprite_data &sprite = layer->sprites[sprite_id];
			s << sprite.asset->id() << sprite_id << sprite.pos;
		}
		for (sprite_data &sprite : removed_sprites) {
			s << sprite.asset->id() << sprite.id << sprite.pos;
		}
	};

	QString text = tr("edit layer '%1' in '%2'").arg(layer->name, asset->name());
	main_window::add_undo_action(text, undo_redo, undo_redo, save_undo_redo, save_undo_redo);

	added_sprites.clear();
	removed_sprites.clear();
}

void document_map::save_object_edit_undo_redo() noexcept
{
	if (!added_objects.size() && !removed_objects.size())
		return;

	auto undo_redo = [](crpg::serializer &s) {
		type::asset_id asset_id;
		type::layer_id layer_id;
		size_t         count;

		s >> asset_id >> layer_id;

		constexpr auto           get_asset = asset_manager::get_asset;
		map_asset               *asset     = static_cast<map_asset *>(get_asset(asset_id));
		document_map            *doc       = static_cast<document_map *>(asset->document());
		object_layer            *layer     = static_cast<object_layer *>(asset->layers[layer_id].get());
		renderable_sprite_layer *rlayer =
		        static_cast<renderable_sprite_layer *>(doc->renderable_layers[layer_id].get());

		asset->open();

		s >> count;
		for (size_t i = 0; i < count; i++) {
			type::asset_id           object_asset_id;
			type::object_id          object_id;
			math::vec2               pos;
			math::vec2               custom_size;
			std::vector<std::string> properties;

			s >> object_asset_id >> object_id >> pos >> custom_size >> properties;

			auto it = layer->objects.find(object_id);
			if (it == layer->objects.end()) {
				/* create */
				object_data new_object;
				new_object.asset.set(asset, get_asset(object_asset_id));
				new_object.id          = object_id;
				new_object.pos         = pos;
				new_object.custom_size = custom_size;
				new_object.properties  = properties;

				doc->add_object(layer, rlayer, new_object);
			} else {
				/* remove */
				doc->remove_object(layer, rlayer, object_id);
			}
		}

		doc->update();
	};

	object_layer *layer = static_cast<object_layer *>(get_cur_layer());

	auto save_undo_redo = [&](crpg::serializer &s) {
		s << asset->id() << cur_layer_id;
		s << (added_objects.size() + removed_objects.size());

		for (type::object_id object_id : added_objects) {
			object_data &object = layer->objects[object_id];
			s << object.asset->id() << object_id << object.pos << object.custom_size << object.properties;
		}
		for (object_data &object : removed_objects) {
			s << object.asset->id() << object.id << object.pos << object.custom_size << object.properties;
		}
	};

	QString text = tr("edit layer '%1' in '%2'").arg(layer->name, asset->name());
	main_window::add_undo_action(text, undo_redo, undo_redo, save_undo_redo, save_undo_redo);

	added_objects.clear();
	removed_objects.clear();
}

void document_map::save_edit_undo_redo() noexcept
{
	switch (cur_layer_type()) {
	case map_layer_type::tileset:
	case map_layer_type::collision:
		save_tileset_edit_undo_redo();
		break;
	case map_layer_type::sprite:
		save_sprite_edit_undo_redo();
		break;
	case map_layer_type::object:
		save_object_edit_undo_redo();
		break;
	}
}

void document_map::save_object_move_undo_redo() noexcept
{
	auto undo_redo = [](crpg::serializer &s) noexcept {
		type::asset_id  asset_id;
		type::layer_id  layer_id;
		type::object_id object_id;
		math::vec2      pos;

		s >> asset_id >> layer_id >> object_id >> pos;

		constexpr auto           get_asset = asset_manager::get_asset;
		map_asset               *asset     = static_cast<map_asset *>(get_asset(asset_id));
		document_map            *doc       = static_cast<document_map *>(asset->document());
		object_layer            *layer     = static_cast<object_layer *>(asset->layers[layer_id].get());
		renderable_sprite_layer *rlayer =
		        static_cast<renderable_sprite_layer *>(doc->renderable_layers[layer_id].get());

		asset->open();

		object_data &object = layer->objects[object_id];
		object.pos          = pos;

		layer->remove_object_from_segments(object);
		layer->add_object_to_segments(object);

		renderable_sprite &rsprite = rlayer->sprites[object_id];
		doc->build_object(rsprite, object);
		doc->update();
	};

	auto save_undo = [&](crpg::serializer &s) noexcept {
		s << asset->id() << cur_layer_id << selected_object << inserted_pos;
	};
	auto save_redo = [&](crpg::serializer &s) noexcept {
		s << asset->id() << cur_layer_id << selected_object << last_insert_pos;
	};

	object_layer *layer = static_cast<object_layer *>(get_cur_layer());

	QString text = tr("move object in layer '%1' in '%2'").arg(layer->name, asset->name());
	main_window::add_undo_action(text, undo_redo, undo_redo, save_undo, save_redo);
}

void document_map::mouseReleaseEvent(QMouseEvent *event)
{
	main_view::mouseReleaseEvent(event);

	if (event->button() == Qt::LeftButton) {
		if (manipulator != manipulator_mode::none && !last_insert_pos.close(inserted_pos)) {
			save_object_move_undo_redo();
		}

		mouse_down      = false;
		last_insert_pos = math::vec2(crpg::math::infinite, crpg::math::infinite);
		manipulator     = manipulator_mode::none;
		save_edit_undo_redo();
	}
}

type::sprite_id document_map::get_hover_sprite() noexcept
{
	sprite_layer *layer = static_cast<sprite_layer *>(get_cur_layer());
	if (!layer)
		return 0;

	map_tools::seg_pos pos = get_map_segment_position(cursor_pos);

	auto it = layer->segments.find(pos.hash);
	if (it == layer->segments.end()) {
		return 0;
	}

	sprite_segment &seg = it->second;

	for (type::sprite_id sprite_id : seg.sprites) {
		sprite_data  &data   = layer->sprites[sprite_id];
		sprite_asset *sprite = data.asset;

		math::box box = sprite->box() + data.pos;
		if (box.inside(cursor_pos)) {
			return sprite_id;
		}
	}

	return 0;
}

type::object_id document_map::get_hover_object() noexcept
{
	object_layer *layer = static_cast<object_layer *>(get_cur_layer());
	if (!layer)
		return 0;

	map_tools::seg_pos pos = get_map_segment_position(cursor_pos);

	auto it = layer->segments.find(pos.hash);
	if (it == layer->segments.end()) {
		return 0;
	}

	object_segment &seg = it->second;

	for (type::object_id object_id : seg.objects) {
		object_data &object = layer->objects[object_id];

		if (object.box().inside(cursor_pos)) {
			return object_id;
		}
	}

	return 0;
}

void document_map::on_resize_inserted_object(const crpg::math::vec2 &insert_pos) noexcept
{
	if (math::vec2::close(insert_pos, last_insert_pos))
		return;
	if (!inserted_object)
		return;

	object_layer *layer  = static_cast<object_layer *>(get_cur_layer());
	object_data  &object = layer->objects[inserted_object];
	if (object.asset->obj_type != object_type::box && object.asset->obj_type != object_type::boundary)
		return;

	renderable_sprite_layer *rlayer = static_cast<renderable_sprite_layer *>(renderable_layers[cur_layer_id].get());

	math::vec2 min = math::vec2::min(insert_pos, inserted_pos);
	math::vec2 max = math::vec2::max(insert_pos, inserted_pos) + 0.5f;

	object.pos         = min;
	object.custom_size = max - min;

	last_insert_pos = insert_pos;

	layer->remove_object_from_segments(object);
	layer->add_object_to_segments(object);

	renderable_sprite &rsprite = rlayer->sprites[inserted_object];
	build_object(rsprite, object);
}

void document_map::on_escape_pressed() noexcept
{
	if (mouse_down && manipulator != manipulator_mode::none) {
		object_layer            *layer = static_cast<object_layer *>(get_cur_layer());
		renderable_sprite_layer *rlayer =
		        static_cast<renderable_sprite_layer *>(renderable_layers[cur_layer_id].get());
		object_data &object = layer->objects[selected_object];

		object.pos = inserted_pos;

		layer->remove_object_from_segments(object);
		layer->add_object_to_segments(object);

		renderable_sprite &rsprite = rlayer->sprites[selected_object];
		build_object(rsprite, object);

		manipulator = manipulator_mode::none;

		update();
	}
}

void document_map::on_delete_pressed() noexcept
{
	if (cur_mode != mode::select)
		return;

	object_layer *layer = static_cast<object_layer *>(get_cur_layer());
	if (!layer || layer->type != crpg::map_layer_type::object)
		return;
	if (selected_object == 0)
		return;

	renderable_sprite_layer *rlayer = static_cast<renderable_sprite_layer *>(renderable_layers[cur_layer_id].get());

	removed_objects.push_back(layer->objects[selected_object]);
	remove_object(layer, rlayer, selected_object);
	set_selected_object(0);
	update();

	last_insert_pos = math::vec2(crpg::math::infinite, crpg::math::infinite);
	manipulator     = manipulator_mode::none;
	save_edit_undo_redo();
}

void document_map::on_move_object() noexcept
{
	math::vec2 new_obj_pos     = (cursor_pos - manipulator_offset);
	math::vec2 snapped_obj_pos = (new_obj_pos * 2.0f).floor() * 0.5f;

	if (math::vec2::close(snapped_obj_pos, last_insert_pos))
		return;

	object_layer            *layer  = static_cast<object_layer *>(get_cur_layer());
	renderable_sprite_layer *rlayer = static_cast<renderable_sprite_layer *>(renderable_layers[cur_layer_id].get());
	object_data             &object = layer->objects[selected_object];

	if (manipulator == manipulator_mode::x)
		object.pos.x = snapped_obj_pos.x;
	else if (manipulator == manipulator_mode::y)
		object.pos.y = snapped_obj_pos.y;
	else
		object.pos = snapped_obj_pos;

	last_insert_pos = snapped_obj_pos;

	layer->remove_object_from_segments(object);
	layer->add_object_to_segments(object);

	renderable_sprite &rsprite = rlayer->sprites[selected_object];
	build_object(rsprite, object);
}

void document_map::update_status_bar()
{
	QString     pos_str;
	QTextStream ts(&pos_str);
	ts.setRealNumberPrecision(2);

	if (!selected_object_type.isEmpty())
		ts << selected_object_type << ": ";
	ts << "{" << cursor_pos.x << ", " << cursor_pos.y << "}";

	main_window::show_message(pos_str);
}

void document_map::mouseMoveEvent(QMouseEvent *event)
{
	main_view::mouseMoveEvent(event);

	update_status_bar();

	if (mouse_down) {
		math::vec2 insert_pos;

		mouse_moved = true;

		if (cur_mode == mode::select) {
			if (manipulator != manipulator_mode::none) {
				on_move_object();
			}
			return;
		}

		switch (cur_layer_type()) {
		case map_layer_type::tileset:
		case map_layer_type::collision:
			insert_pos = cursor_pos.floor();

			if (!math::vec2::close(insert_pos, last_insert_pos))
				on_insert_tileset_selection();
			break;
		case map_layer_type::sprite:
			if (cur_mode == mode::erase)
				on_insert_sprite();
			break;
		case map_layer_type::object:
			insert_pos = (cursor_pos * 2.0f).floor() / 2.0f;

			if (cur_mode == mode::erase)
				on_insert_object();
			else
				on_resize_inserted_object(insert_pos);
			break;
		}
	}
}

void document_map::enterEvent(QEnterEvent *event)
{
	main_view::enterEvent(event);
	mouse_hovering = true;
}

void document_map::leaveEvent(QEvent *event)
{
	main_view::leaveEvent(event);
	mouse_hovering = false;

	update();
}

void document_map::build_tileset_segment(tileset_layer         *layer,
                                         const crpg::segment   &seg,
                                         crpg::tileset_segment &rseg) noexcept
{
	math::vec3 seg_pos((float)seg.x, (float)seg.y);

	rseg.render_data.clear();
	rseg.render_data.reserve(layer->tilesets.size());

	for (auto &[_, data] : layer->tilesets) {
		data.asset->load_if_not_loaded();

		graphics::vertex_buffer::data vbd = map_tools::generate_tileset_seg(
		        seg, data.asset->cx, data.asset->cy, data.asset->tile_width, data.id);
		if (vbd.valid()) {
			crpg::tileset_render_data rdata{data.asset->texture(),
			                                graphics::vertex_buffer::create(std::move(vbd))};
			rseg.render_data.push_back(std::move(rdata));
		}
	}
}

void document_map::build_tileset_layer(tileset_layer *layer, renderable_tileset_layer *rlayer) noexcept
{
	rlayer->segments.clear();

	for (auto &pair : layer->segments) {
		segment               &seg  = pair.second;
		crpg::tileset_segment &rseg = rlayer->segments[pair.first];

		if (!rseg.render_data.size())
			build_tileset_segment(layer, seg, rseg);
	}
}

void document_map::build_sprite_layer(sprite_layer *layer, renderable_sprite_layer *rlayer) noexcept
{
	rlayer->sprites.clear();

	for (auto &pair : layer->sprites) {
		sprite_data       &sprite  = pair.second;
		renderable_sprite &rsprite = rlayer->sprites[pair.first];

		rsprite.pos = sprite.pos;
		rsprite.tex = sprite.asset->texture();
		rsprite.vb  = sprite.asset->vb();
	}
}

void document_map::build_object(renderable_sprite &rsprite, object_data &object) noexcept
{
	if (object.asset->obj_type == object_type::sprite && object.asset->sprite()) {
		rsprite.pos = object.pos;
		rsprite.tex = object.asset->texture();
		rsprite.vb  = object.asset->vb();

	} else if (object.asset->obj_type == object_type::boundary) {
		rsprite.pos.set_zero();
		rsprite.tex.reset();

		math::vec2 p1 = object.pos;
		math::vec2 p2 = object.pos + object.custom_size;

		graphics::vertex_buffer::data vbd;
		vbd.points.reserve(5);
		vbd.points.emplace_back(p1.x, p1.y);
		vbd.points.emplace_back(p1.x, p2.y);
		vbd.points.emplace_back(p2.x, p2.y);
		vbd.points.emplace_back(p2.x, p1.y);
		vbd.points.emplace_back(p1.x, p1.y);

		rsprite.vb = graphics::vertex_buffer::create(std::move(vbd));

	} else {
		rsprite.pos.set_zero();
		rsprite.tex.reset();

		math::vec2 p1 = object.pos;
		math::vec2 p2 = object.pos + object.custom_size;

		graphics::vertex_buffer::data vbd;
		vbd.points.reserve(4);
		vbd.points.emplace_back(p1.x, p1.y);
		vbd.points.emplace_back(p1.x, p2.y);
		vbd.points.emplace_back(p2.x, p1.y);
		vbd.points.emplace_back(p2.x, p2.y);

		rsprite.vb = graphics::vertex_buffer::create(std::move(vbd));
	}
}

void document_map::build_object_layer(object_layer *layer, renderable_sprite_layer *rlayer) noexcept
{
	rlayer->sprites.clear();

	for (auto &pair : layer->objects) {
		object_data       &object  = pair.second;
		renderable_sprite &rsprite = rlayer->sprites[pair.first];

		build_object(rsprite, object);
	}
}

void document_map::build_renderable_layer(map_layer *layer, renderable_map_layer *rlayer) noexcept
{
	switch (layer->type) {
	case crpg::map_layer_type::tileset:
	case crpg::map_layer_type::collision:
		build_tileset_layer(static_cast<tileset_layer *>(layer),
		                    static_cast<renderable_tileset_layer *>(rlayer));
		break;
	case crpg::map_layer_type::sprite:
		build_sprite_layer(static_cast<sprite_layer *>(layer), static_cast<renderable_sprite_layer *>(rlayer));
		break;
	case crpg::map_layer_type::object:
		build_object_layer(static_cast<object_layer *>(layer), static_cast<renderable_sprite_layer *>(rlayer));
		break;
	}
}

void document_map::add_renderable_layer(map_layer *layer) noexcept
{
	renderable_map_layer *rlayer = nullptr;

	switch (layer->type) {
	case crpg::map_layer_type::tileset:
	case crpg::map_layer_type::collision:
		rlayer = new renderable_tileset_layer;
		break;
	case crpg::map_layer_type::sprite:
		rlayer = new renderable_sprite_layer;
		break;
	case crpg::map_layer_type::object:
		/* yes, this is on purpose */
		rlayer = new renderable_sprite_layer;
		break;
	}

	if (rlayer) {
		renderable_layers.erase(layer->id);
		renderable_layers.emplace(layer->id, rlayer);
		build_renderable_layer(layer, rlayer);
	}
}

void document_map::build_tileset_selection_vb() noexcept
{
	tile_selector    *selector  = tile_selector::get();
	tileset_selection selection = selector->selection();
	tileset_asset    *tileset   = selector->asset();

	if (!tileset || !selection) {
		selection_vb.reset();
		return;
	}

	int   tile_width = tileset->tile_width;
	float cx_mul     = 1.0f / (float)(tileset->cx / tile_width);
	float cy_mul     = 1.0f / (float)(tileset->cy / tile_width);

	last_tileset_selection = selection;

	graphics::vertex_buffer::data vbd;
	for (uint8_t y = selection.min.y; y <= selection.max.y; y++) {
		for (uint8_t x = selection.min.x; x <= selection.max.x; x++) {
			math::vec2 uv1((float)x * cx_mul, (float)y * cy_mul);
			math::vec2 uv4((float)(x + 1) * cx_mul, (float)(y + 1) * cy_mul);

			uv1 += math::tiny_epsilon;
			uv4 -= math::tiny_epsilon;

			uint8_t    vert_x = x - selection.min.x;
			uint8_t    vert_y = y - selection.min.y;
			math::vec3 p1((float)vert_x, (float)vert_y, 0.0f);
			math::vec3 p4((float)(vert_x + 1), (float)(vert_y + 1), 0.0f);

			map_tools::add_tile_sprite(vbd, p1, p4, uv1, uv4);
		}
	}

	selection_vb = graphics::vertex_buffer::create(std::move(vbd));
}

inline void document_map::update_layer_tex() noexcept
{
	if (!layer_tex || layer_tex->cx() != qt_size.width() || layer_tex->cy() != qt_size.height())
		layer_tex = graphics::texture::create(qt_size.width(),
		                                      qt_size.height(),
		                                      1,
		                                      graphics::texture_format::rgba,
		                                      nullptr,
		                                      graphics::texture::type::renderable);
}

void document_map::render_tileset_layer(tileset_layer *layer, renderable_tileset_layer *rlayer) noexcept
{
	tile_selector     *selector = tile_selector::get();
	map_tools::seg_pos tl_pos   = get_map_segment_position(tl);
	map_tools::seg_pos br_pos   = get_map_segment_position(br);

	tileset_asset *ts_asset  = selector->asset();
	bool           collision = !!dynamic_cast<collision_layer *>(layer);

	if (collision && cur_layer_id != layer->id) {
		return;
	}

	bool valid = ts_asset && ((collision && ts_asset->ext == "mask") || (!collision && ts_asset->ext != "mask"));
	bool render_selected = valid && cur_mode != mode::select && mouse_hovering && cur_layer_id == layer->id &&
	                       (selector->selected() || cur_mode == mode::erase);

	graphics::render_target rt;

	if (render_selected) {
		update_layer_tex();
		rt.set(layer_tex);
		graphics::clear(0.0f, 0.0f, 0.0f, 0.0f);
		graphics::push_blend(graphics::blend::one, graphics::blend::zero);
	}

	graphics::shader_ref cur_ps;

	if (collision) {
		vs_unlit_color->load();
		ps_unlit_color->load();
		ps_unlit_color->set("color", math::vec4(0.0, 1.0f, 0.0f, 0.5f));

		cur_ps = ps_unlit_color;
	} else if (render_selected) {
		vs_unlit->load();
		ps_unlit->load();

		cur_ps = ps_unlit;
	} else {
		vs_unlit_color->load();
		ps_unlit_color->load();
		apply_layer_opacity(ps_unlit_color, layer->blend, layer->opacity);

		cur_ps = ps_unlit;
	}

	for (int16_t y = tl_pos.y; y <= br_pos.y; y++) {
		for (int16_t x = tl_pos.x; x <= br_pos.x; x++) {
			map_tools::seg_pos pos(x, y);

			auto seg_it = rlayer->segments.find(pos.hash);
			if (seg_it == rlayer->segments.end())
				continue;

			crpg::tileset_segment &seg = seg_it->second;
			for (crpg::tileset_render_data &data : seg.render_data) {
				cur_ps->set("image", data.texture);
				data.vb->load();

				graphics::draw(graphics::primitive::triangles);
			}
		}
	}

	if (render_selected) {
		tileset_asset *tileset = selector->asset();

		if (last_tileset_asset != tileset || last_tileset_selection != selector->selection()) {
			build_tileset_selection_vb();
			last_tileset_asset = tileset;
		}

		if (cur_mode == mode::erase) {
			ps_solid->load();
			vs_solid->load();

			ps_solid->set("color", math::vec4(0.0f, 0.0f, 0.0f, 0.0f));
			graphics::draw(floorf(cursor_pos.x), floorf(cursor_pos.y), 1.0f, 1.0f);
		} else if (selection_vb) {
			selection_tex = tileset->texture();

			graphics::matrix_guard g;
			graphics::matrix_translate(floorf(cursor_pos.x), floorf(cursor_pos.y));

			selection_vb->load();
			cur_ps->set("image", selection_tex);
			graphics::draw(graphics::primitive::triangles);
		}

		/* ------------ */

		ps_unlit_color->load();
		vs_unlit_color->load();
		apply_layer_opacity(ps_unlit_color, layer->blend, layer->opacity);

		rt.clear();
		graphics::pop_blend();

		graphics::matrix_guard g;
		graphics::matrix_translate(tl.x, tl.y);
		draw(layer_tex, 0.0f, 0.0f, view_size.x, view_size.y);
	}
}

void document_map::render_sprite_layer(sprite_layer *layer, renderable_sprite_layer *rlayer) noexcept
{
	map_tools::seg_pos tl_pos = get_map_segment_position(tl);
	map_tools::seg_pos br_pos = get_map_segment_position(br);

	cur_sprites.resize(0);

	for (int16_t y = tl_pos.y; y <= br_pos.y; y++) {
		for (int16_t x = tl_pos.x; x <= br_pos.x; x++) {
			map_tools::seg_pos pos(x, y);

			auto seg_it = layer->segments.find(pos.hash);
			if (seg_it == layer->segments.end())
				continue;

			sprite_segment &seg = seg_it->second;
			for (type::sprite_id sprite_id : seg.sprites) {
				auto it = std::find(cur_sprites.begin(), cur_sprites.end(), sprite_id);
				if (it == cur_sprites.end())
					cur_sprites.push_back(sprite_id);
			}
		}
	}

	ps_unlit_color->load();
	vs_unlit_color->load();
	apply_layer_opacity(ps_unlit_color, layer->blend, layer->opacity);

	for (type::sprite_id sprite_id : cur_sprites) {
		renderable_sprite &sprite = rlayer->sprites[sprite_id];

		sprite.vb->load();
		ps_unlit_color->set("image", sprite.tex);

		math::matrix world;
		world.set_identity();
		world.t.x = sprite.pos.x;
		world.t.y = sprite.pos.y;
		vs_unlit_color->set_world(world);

		graphics::draw(graphics::primitive::tristrip, 0, 4);
	}

	vs_unlit_color->set_world(math::matrix::identity());

	/* ----------------------------------------------- */

	bool render_selected = mouse_hovering && cur_layer_id == layer->id;
	if (render_selected && cur_mode == mode::insert) {
		sprite_asset *sprite = static_cast<sprite_asset *>(asset_manager::selected_asset());
		if (!sprite->is_file(asset_category::sprite))
			return;

		if (last_sprite_asset != sprite) {
			selection_vb      = sprite->vb();
			selection_tex     = sprite->texture();
			last_sprite_asset = sprite;
		}
		if (!selection_vb || !selection_tex)
			return;

		graphics::matrix_guard g;
		graphics::matrix_translate((cursor_pos * 2.0f).round() / 2.0f);

		selection_vb->load();
		ps_unlit_color->set("image", selection_tex);
		graphics::draw(graphics::primitive::tristrip, 0, 4);

	} else if (render_selected && cur_mode == mode::erase) {
		type::sprite_id sprite_id = get_hover_sprite();

		if (sprite_id) {
			sprite_data &sprite = layer->sprites[sprite_id];
			math::box    box    = sprite.asset->box();
			math::vec2   scale  = box.max - box.min;

			vs_solid->load();
			ps_solid->load();
			vb_square->load();

			ps_solid->set("color", math::vec4(1.0f, 0.0f, 0.0f, 1.0f));

			graphics::matrix_guard g;
			graphics::matrix_scale(scale.x, scale.y);
			graphics::matrix_translate(sprite.pos);

			graphics::draw(graphics::primitive::linestrip);
		}
	}
}

void document_map::render_object_layer_sprites(object_layer *layer, renderable_sprite_layer *rlayer) noexcept
{
	map_tools::seg_pos tl_pos = get_map_segment_position(tl);
	map_tools::seg_pos br_pos = get_map_segment_position(br);

	cur_sprites.resize(0);

	for (int16_t y = tl_pos.y; y <= br_pos.y; y++) {
		for (int16_t x = tl_pos.x; x <= br_pos.x; x++) {
			map_tools::seg_pos pos(x, y);

			auto seg_it = layer->segments.find(pos.hash);
			if (seg_it == layer->segments.end())
				continue;

			object_segment &seg = seg_it->second;
			for (type::object_id object_id : seg.objects) {
				renderable_sprite &sprite = rlayer->sprites[object_id];
				object_data       &object = layer->objects[object_id];

				if (sprite.tex && object.asset->obj_type == object_type::sprite) {
					auto it = std::find(cur_sprites.begin(), cur_sprites.end(), object_id);
					if (it == cur_sprites.end()) {
						cur_sprites.push_back(object_id);
					}
				}
			}
		}
	}

	ps_unlit_color->load();
	vs_unlit_color->load();
	apply_layer_opacity(ps_unlit_color, layer->blend, layer->opacity);

	for (type::object_id object_id : cur_sprites) {
		renderable_sprite &sprite = rlayer->sprites[object_id];

		sprite.vb->load();
		ps_unlit_color->set("image", sprite.tex);

		math::matrix world;
		world.set_identity();
		world.t.x = sprite.pos.x;
		world.t.y = sprite.pos.y;
		vs_unlit_color->set_world(world);

		graphics::draw(graphics::primitive::tristrip, 0, 4);
	}
}

void document_map::render_object_layer_objects(object_layer *layer, renderable_sprite_layer *rlayer) noexcept
{
	map_tools::seg_pos tl_pos = get_map_segment_position(tl);
	map_tools::seg_pos br_pos = get_map_segment_position(br);

	cur_objects.resize(0);
	cur_links.resize(0);

	/* ----------------------------------------------- */
	/* get objects to render                           */

	for (int16_t y = tl_pos.y; y <= br_pos.y; y++) {
		for (int16_t x = tl_pos.x; x <= br_pos.x; x++) {
			map_tools::seg_pos pos(x, y);

			auto seg_it = layer->segments.find(pos.hash);
			if (seg_it == layer->segments.end())
				continue;

			object_segment &seg = seg_it->second;
			for (type::object_id object_id : seg.objects) {
				renderable_sprite &sprite = rlayer->sprites[object_id];
				object_data       &object = layer->objects[object_id];

				if (!sprite.tex || object.asset->obj_type != object_type::sprite) {
					auto it = std::find(cur_objects.begin(), cur_objects.end(), object_id);
					if (it == cur_objects.end()) {
						cur_objects.push_back(object_id);
					}
				}

				for (type::object_id link_id : object.links) {
					object_data &object2 = layer->objects[link_id];
					cur_links.emplace_back(object.pos, object2.pos);
				}
			}
		}
	}

	/* ----------------------------------------------- */
	/* render objects                                  */

	vs_solid->load();
	ps_solid->load();

	for (type::object_id object_id : cur_objects) {
		object_data       &object = layer->objects[object_id];
		renderable_sprite &render = rlayer->sprites[object_id];

		object_type obj_type = object.asset->obj_type;

		if (obj_type == object_type::boundary) {
			ps_solid->set("color", math::vec4(0.0f, 0.75f, 0.0f, 1.0f));
			render.vb->load();
			graphics::draw(graphics::primitive::linestrip);

		} else if (obj_type == object_type::box) {
			ps_solid->set("color", math::vec4(1.0f, 0.0f, 0.0f, 0.5f));
			render.vb->load();
			graphics::draw(graphics::primitive::tristrip);

		} else if (obj_type == object_type::point) {
			ps_solid->set("color", math::vec4(1.0f, 0.0f, 0.0f, 0.5f));
			vb_point_object_solid->load();

			graphics::matrix_guard g;
			graphics::matrix_translate(object.pos);
			graphics::draw(graphics::primitive::tristrip);

			ps_solid->set("color", math::vec4(1.0f, 0.0f, 0.0f, 0.5f));
			vb_point_object_lines->load();
			graphics::draw(graphics::primitive::lines);
		}
	}

	/* ----------------------------------------------- */
	/* render object links                             */

	ps_solid->set("color", math::vec4(0.0f, 1.0f, 0.0f, 1.0f));

	for (auto &link : cur_links) {
		math::vec2 vec  = link.second - link.first;
		float      len  = vec.len();
		math::vec2 norm = vec / len;

		math::matrix mat;
		mat.x = math::vec4(norm.x * len, norm.y * len, 0.0f, 0.0f);
		mat.y = math::vec4(-norm.y * len, norm.x * len, 0.0f, 0.0f);
		mat.z = math::vec4(0.0f, 0.0f, 1.0f * len, 0.0f);
		mat.t = math::vec4(link.first.x, link.first.y, 0.0f, 1.0f);

		vb_link_line->load();

		graphics::matrix_push();
		graphics::matrix_multiply(mat);
		graphics::draw(graphics::primitive::lines);
		graphics::matrix_pop();

		vb_link_arrow->load();

		mat.x = math::vec4(-norm.x, -norm.y, 0.0f, 0.0f);
		mat.y = math::vec4(norm.y, -norm.x, 0.0f, 0.0f);
		mat.z = math::vec4(0.0f, 0.0f, 1.0f, 0.0f);
		mat.t = math::vec4(link.second.x, link.second.y, 0.0f, 1.0f);

		graphics::matrix_push();
		graphics::matrix_multiply(mat);
		graphics::draw(graphics::primitive::linestrip);
		graphics::matrix_pop();
	}

	/* ----------------------------------------------- */
	/* render insertion object or highlight erased obj */

	ps_unlit_color->load();
	vs_unlit_color->load();
	apply_layer_opacity(ps_unlit_color, layer->blend, layer->opacity);

	auto render_highlight = [this](object_data &object) {
		math::box  box   = object.box();
		math::vec2 scale = box.max - box.min;
		math::vec2 pos   = object.pos;

		if (object.asset->obj_type == object_type::point) {
			scale = math::vec2(1.0f, 1.0f);
			pos -= 0.5f;

		} else if (object.asset->obj_type == object_type::sprite) {
			const sprite_ref &sprite = object.asset->sprite();
			pos -= sprite->origin() / (float)sprite->tile_width;
		}

		vs_solid->load();
		ps_solid->load();
		vb_square->load();

		ps_solid->set("color", math::vec4(1.0f, 0.0f, 0.0f, 1.0f));

		graphics::matrix_guard g;
		graphics::matrix_scale(scale.x, scale.y);
		graphics::matrix_translate(pos);

		graphics::draw(graphics::primitive::linestrip);
	};

	bool render_selected = mouse_hovering && cur_layer_id == layer->id;
	if (render_selected && cur_mode == mode::insert) {
		object_asset *object = static_cast<object_asset *>(asset_manager::selected_asset());
		if (!object->is_file(asset_category::object))
			return;

		if (last_object_asset != object) {
			selection_vb      = object->vb();
			selection_tex     = object->texture();
			last_object_asset = object;
		}
		if (!selection_vb || !selection_tex)
			return;

		graphics::matrix_guard g;
		graphics::matrix_translate((cursor_pos * 2.0f).round() / 2.0f);

		selection_vb->load();
		ps_unlit_color->set("image", selection_tex);
		graphics::draw(graphics::primitive::tristrip, 0, 4);

	} else if (render_selected && cur_mode == mode::erase) {
		type::object_id object_id = get_hover_object();

		if (object_id) {
			object_data &object = layer->objects[object_id];
			render_highlight(object);
		}
	}

	if (cur_mode == mode::select && cur_layer_id == layer->id && selected_object != 0) {
		auto it = layer->objects.find(selected_object);
		if (it != layer->objects.end()) {
			render_highlight(it->second);
		}
	}
}

void document_map::render_layer(map_layer *layer) noexcept
{
	if (!layer->visible)
		return;

	if (layer->type == map_layer_type::folder) {
		auto *folder = static_cast<folder_layer *>(layer);

		for (size_t i = folder->children.size(); i > 0; i--) {
			map_layer *child_layer = folder->children[i - 1];
			render_layer(child_layer);
		}
		return;
	}

	renderable_map_layer *rlayer = renderable_layers[layer->id].get();

	graphics::blend_guard bg;
	apply_layer_blend(layer->blend);

	switch (layer->type) {
	case crpg::map_layer_type::tileset:
	case crpg::map_layer_type::collision:
		render_tileset_layer(static_cast<tileset_layer *>(layer),
		                     static_cast<renderable_tileset_layer *>(rlayer));
		break;
	case crpg::map_layer_type::sprite:
		render_sprite_layer(static_cast<sprite_layer *>(layer), static_cast<renderable_sprite_layer *>(rlayer));
		break;
	case crpg::map_layer_type::object:
		render_object_layer_sprites(static_cast<object_layer *>(layer),
		                            static_cast<renderable_sprite_layer *>(rlayer));
		break;
	}
}

void document_map::render_manipulator(math::vec2 pos) noexcept
{
	math::box x_and_y_area;
	math::box x_area;
	math::box y_area;

	manipulator_mode axii = manipulator != manipulator_mode::none ? manipulator : get_manipulator_mode(pos);

	int selected_axii = (int)axii;

	vs_solid->load();
	ps_solid->load();

	constexpr float manipulator_size = 100.0f;

	graphics::matrix_guard g;

	static const math::vec4 red(1.0f, 0.0f, 0.0f, 1.0f);
	static const math::vec4 green(0.0f, 1.0f, 0.0f, 1.0f);
	static const math::vec4 yellow(1.0f, 1.0f, 0.0f, 1.0f);

	/* --------------------------------------- */
	/* draw y manipulator arm                  */

	graphics::matrix_rotate(0.0f, 0.0f, 1.0f, math::to_rad(-90.0f));
	graphics::matrix_scale(-manipulator_size, manipulator_size);
	graphics::matrix_translate(pos);

	/* inner arm */
	vb_manipulator_arm->load();
	ps_solid->set("color", (selected_axii & 2) == 2 ? yellow : green);
	graphics::draw(graphics::primitive::tristrip);

	/* arm end */
	vb_manipulator_end->load();
	ps_solid->set("color", green);
	graphics::draw(graphics::primitive::tristrip);

	/* inside */
	vb_manipulator_inside->load();
	ps_solid->set("color", selected_axii == 3 ? yellow : green);
	graphics::draw(graphics::primitive::tristrip);

	/* --------------------------------------- */
	/* draw x manipulator arm                  */

	g.reset();

	graphics::matrix_scale(manipulator_size, manipulator_size);
	graphics::matrix_translate(pos);

	/* inner arm */
	vb_manipulator_arm->load();
	ps_solid->set("color", (selected_axii & 1) == 1 ? yellow : red);
	graphics::draw(graphics::primitive::tristrip);

	/* arm end */
	vb_manipulator_end->load();
	ps_solid->set("color", red);
	graphics::draw(graphics::primitive::tristrip);

	/* inside */
	vb_manipulator_inside->load();
	ps_solid->set("color", selected_axii == 3 ? yellow : red);
	graphics::draw(graphics::primitive::tristrip);
}

void document_map::render() noexcept
{
	vs_unlit->load();
	ps_unlit->load();

	vs_unlit->set_world(crpg::math::matrix::identity());

	render_layer(asset->root);

	if (show_grid)
		render_grid();

	/* ------------------------------- */
	/* draw current object layer       */

	object_layer *layer = static_cast<object_layer *>(get_cur_layer());

	if (layer && layer->type == map_layer_type::object) {
		renderable_sprite_layer *rlayer =
		        static_cast<renderable_sprite_layer *>(renderable_layers[cur_layer_id].get());

		render_object_layer_objects(layer, rlayer);
	}

	/* ------------------------------- */
	/* draw in window space            */

	float cx = (float)qt_size.width();
	float cy = (float)qt_size.height();

	graphics::ortho(0.0f, cx, 0.0f, cy);

	/* ------------------------------- */
	/* draw movement manipulator       */

	if (layer && cur_mode == mode::select && layer->type == map_layer_type::object && selected_object) {
		object_data &obj = layer->objects[selected_object];

		math::vec2 pos = world_to_window(obj.pos);
		render_manipulator(pos);
	}

	/* ------------------------------- */
	/* draw text                       */

	vs_unlit->load();
	ps_unlit->load();

	graphics::texture_ref tex;
	switch (cur_mode) {
	case mode::select:
		tex = tex_select;
		break;
	case mode::insert:
		tex = tex_insert;
		break;
	case mode::erase:
		tex = tex_erase;
		break;
	}

	graphics::draw(tex, cx - (float)tex->cx() - 10.0f, 10.0f);
}

void document_map::reset_data() noexcept
{
	renderable_layers.clear();
	for (auto &pair : asset->layers) {
		map_layer *layer = pair.second.get();
		add_renderable_layer(layer);
	}

	update();
}

void document_map::set_selected_object(crpg::type::object_id id) noexcept
{
	if (selected_object == id)
		return;

	auto *layer = static_cast<object_layer *>(get_cur_layer());
	if (!layer || layer->type != map_layer_type::object)
		return;

	auto *opw = object_properties_widget::get();

	selected_object = id;
	if (selected_object) {
		object_data &object  = layer->objects[selected_object];
		selected_object_type = object.asset->full_name();
		opw->set_object(asset, &object);
	} else {
		selected_object_type.clear();
		opw->set_object(nullptr, nullptr);
	}

	update_status_bar();
}

} // namespace editor
