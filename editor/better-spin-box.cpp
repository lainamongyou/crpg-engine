#include "better-spin-box.hpp"

#include <QMouseEvent>

namespace editor {

/* ======================================================================== */

better_spin_box::better_spin_box(QWidget *parent) noexcept : QSpinBox(parent)
{
	connect(this, &QAbstractSpinBox::editingFinished, [this]() {
		int val = value();
		if (last_val != val) {
			emit editing_actually_finished();
			last_val = val;
		}
	});
}

void better_spin_box::setMinimum(int min) noexcept
{
	QSpinBox::setMinimum(min);
	if (last_val < min)
		last_val = min;
}

void better_spin_box::mouseReleaseEvent(QMouseEvent *event)
{
	QSpinBox::mouseReleaseEvent(event);

	if (event->button() == Qt::LeftButton) {
		int val = value();
		if (last_val != val) {
			emit editing_actually_finished();
			last_val = val;
		}
	}
}

/* ======================================================================== */

better_double_spin_box::better_double_spin_box(QWidget *parent) noexcept : QDoubleSpinBox(parent)
{
	connect(this, &QAbstractSpinBox::editingFinished, [this]() {
		double val = value();
		if (last_val != val) {
			emit editing_actually_finished();
			last_val = val;
		}
	});
}

void better_double_spin_box::setMinimum(double min) noexcept
{
	QDoubleSpinBox::setMinimum(min);
	if (last_val < min)
		last_val = min;
}

void better_double_spin_box::mouseReleaseEvent(QMouseEvent *event)
{
	QDoubleSpinBox::mouseReleaseEvent(event);

	if (event->button() == Qt::LeftButton) {
		double val = value();
		if (last_val != val) {
			emit editing_actually_finished();
			last_val = val;
		}
	}
}

/* ======================================================================== */

} // namespace editor
