#pragma once

#include <QString>

#include <functional>
#include <vector>
#include <string>
#include <deque>

namespace crpg {
class serializer;
};

namespace editor {

typedef std::function<void(crpg::serializer &s)> undo_redo_cb;

struct undo_item {
	QString      name;
	std::string  undo_data;
	std::string  redo_data;
	undo_redo_cb undo;
	undo_redo_cb redo;
};

class undo_stack {
	std::deque<undo_item> items;
	size_t                index          = 0;
	int64_t               last_action_ts = 0;

public:
	void add_action(const QString &name,
	                undo_redo_cb   undo,
	                undo_redo_cb   redo,
	                undo_redo_cb   save_undo,
	                undo_redo_cb   save_redo,
	                bool           repeatable) noexcept;
	void add_action(const QString     &name,
	                undo_redo_cb       undo,
	                undo_redo_cb       redo,
	                const std::string &undo_data,
	                const std::string &redo_data,
	                bool               repeatable) noexcept;
	void undo(QString &undo_name, QString &redo_name) noexcept;
	void redo(QString &undo_name, QString &redo_name) noexcept;

	inline void clear() noexcept
	{
		items.clear();
		index = 0;
	}
};
} // namespace editor
