#pragma once

#include <engine/graphics/graphics.hpp>
#include <engine/math/math-defs.hpp>
#include <engine/math/matrix.hpp>
#include <engine/math/quat.hpp>
#include <engine/math/box.hpp>
#include "render-view.hpp"

namespace editor {

class main_view : public render_view {
	Q_OBJECT

	friend class main_window;

	QSize            prev_size;
	QPoint           prev_qt_cursor_pos;
	crpg::math::vec2 prev_cam_pos;
	float            prev_zoom = default_zoom;

	QPoint           first_pos;
	crpg::math::vec2 first_cam_pos;
	crpg::math::vec2 first_cursor_pos;
	crpg::math::vec2 first_view_pos;
	crpg::math::vec2 first_view_size;
	int              moving_ref = 0;

	crpg::graphics::texture_ref dark_transparency_background;

protected:
	crpg::graphics::shader_ref vs_grid;
	crpg::graphics::shader_ref ps_grid;
	crpg::graphics::shader_ref vs_solid;
	crpg::graphics::shader_ref ps_solid;
	crpg::graphics::shader_ref vs_unlit;
	crpg::graphics::shader_ref ps_unlit;
	crpg::graphics::shader_ref vs_unlit_color;
	crpg::graphics::shader_ref ps_unlit_color;
	crpg::graphics::shader_ref vs_transparency_background;
	crpg::graphics::shader_ref ps_transparency_background;

	crpg::graphics::vertex_buffer_ref vb_square;
	crpg::graphics::vertex_buffer_ref vb_sprite;
	crpg::graphics::vertex_buffer_ref vb_line_horizontal;
	crpg::graphics::vertex_buffer_ref vb_line_vertical;

	static constexpr float default_zoom = 12.0f;

	crpg::math::vec4 background_color{0.0f, 0.0f, 0.0f, 1.0f};

	QSize            qt_size;
	QPoint           qt_cursor_pos = {};
	crpg::math::vec2 tl            = crpg::math::vec2::zero();
	crpg::math::vec2 br            = crpg::math::vec2::zero();
	crpg::math::vec2 view_size     = crpg::math::vec2::zero();
	crpg::math::vec2 cursor_pos    = crpg::math::vec2::zero();
	crpg::math::vec2 cam_pos       = crpg::math::vec2::zero();
	float            zoom          = default_zoom;
	bool             lock_view     = false;
	bool             center_cam    = true;

	crpg::math::box viewable_area{crpg::math::vec2(-crpg::math::infinite, -crpg::math::infinite),
	                              crpg::math::vec2(crpg::math::infinite, crpg::math::infinite)};

	inline crpg::math::vec2 window_to_world(crpg::math::vec2 pos)
	{
		crpg::math::vec2 s((float)qt_size.width(), (float)qt_size.height());

		pos /= s;
		pos *= view_size;
		pos += tl;
		return pos;
	}

	inline crpg::math::vec2 world_to_window(crpg::math::vec2 pos)
	{
		crpg::math::vec2 s((float)qt_size.width(), (float)qt_size.height());

		pos -= tl;
		pos /= view_size;
		pos *= s;
		return pos;
	}

	void mousePressEvent(QMouseEvent *event) override;
	void mouseReleaseEvent(QMouseEvent *event) override;
	void mouseMoveEvent(QMouseEvent *event) override;
	void wheelEvent(QWheelEvent *event) override;
	void paintEvent(QPaintEvent *event) override;

	virtual void calc_view() noexcept;

	void         render_grid() noexcept;
	void         render_transparency_background() noexcept;
	void         init() noexcept override;
	virtual void render() noexcept;

public:
	inline main_view(QWidget *parent = nullptr, Qt::WindowFlags flags = Qt::WindowFlags()) noexcept
	        : render_view(parent, flags)
	{
	}

	void update() noexcept;
};
} // namespace editor
