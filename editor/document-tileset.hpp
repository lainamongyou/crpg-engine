#pragma once

#include <QPointer>
#include <QWidget>

#include <memory>

#include <engine/graphics/graphics.hpp>

#include "main-view.hpp"

class Ui_document_tileset;

namespace editor {

class tileset_asset;

class document_tileset : public QWidget {
	Q_OBJECT

	std::unique_ptr<Ui_document_tileset> ui;
	tileset_asset                       *asset;

	void dropEvent(QDropEvent *event) override;
	void dragEnterEvent(QDragEnterEvent *event) override;
	void dragLeaveEvent(QDragLeaveEvent *event) override;
	void dragMoveEvent(QDragMoveEvent *event) override;

public:
	document_tileset(QWidget *parent, tileset_asset *asset) noexcept;
	~document_tileset();

public slots:
	void update_texture() noexcept;
};

} // namespace editor
