#include "app.hpp"
#include "engine-opts.h"
#include "game-config.h"
#include "main-window.hpp"

#include <engine/utility/logging.hpp>

#include <QDir>
#include <QFile>
#include <QLibrary>
#include <QMessageBox>
#include <QStandardPaths>

#ifdef _WIN32
#include <windows.h>
#endif

namespace editor {

app::app(int &argc, char **argv) noexcept : QApplication(argc, argv)
{
	setApplicationName(QStringLiteral("crpg-engine"));

	QString path = QStandardPaths::locate(QStandardPaths::AppDataLocation, QStringLiteral("settings.json"));

	if (!path.isEmpty()) {
		QFile file;
		file.setFileName(path);
		file.open(QIODevice::ReadOnly | QIODevice::Text);
		settings_ = QJsonDocument::fromJson(file.readAll());
	}

#if !defined(_WIN32) && !defined(__APPLE__)
	setAttribute(Qt::AA_DontCreateNativeWidgetSiblings);
#endif

	QFile file(":/app/theme.qss");
	file.open(QFile::ReadOnly);
	QString stylesheet = file.readAll();
	setStyleSheet(stylesheet);
}

app::~app() noexcept
{
	QString path = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
	QDir().mkdir(path);

	path.append(QStringLiteral("/settings.json"));

	QFile file;
	file.setFileName(path);
	file.open(QIODevice::WriteOnly | QIODevice::Text);
	file.write(settings_.toJson());
}

/* ---- */
} // namespace editor

/* -------------------------------------------------------------------- */

int main(int argc, char *argv[]) noexcept
{
#if !defined(_WIN32) && !defined(__APPLE__)
	const char *session_type = getenv("XDG_SESSION_TYPE");
	if (session_type && strcmp(session_type, "wayland") == 0)
		setenv("QT_QPA_PLATFORM", "wayland", false);
#endif

	editor::app          app(argc, argv);
	editor::main_window *main_window;

#if YOU_CANT_MAKE_ME_USE_DPI
	QFont increased_font = app.font();
	increased_font.setPointSize(increased_font.pointSize() * 2);
	app.setFont(increased_font);
#endif

	auto debug_log = [](crpg::log_level level, const char *format, va_list args) noexcept {
		char msg[4096];
		vsnprintf(msg, sizeof(msg) - 10, format, args);
		strcat(msg, "\n");
#ifdef _WIN32
		(void)level;
		OutputDebugStringA(msg);
#else
		switch (level) {
		case crpg::log_level::debug:
			printf("debug: %s", msg);
			break;
		case crpg::log_level::info:
			printf("info: %s", msg);
			break;
		case crpg::log_level::warning:
			printf("warning: %s", msg);
			break;
		case crpg::log_level::error:
			printf("error: %s", msg);
			break;
		}
#endif
	};

	crpg::set_log_callback(debug_log);

	/* ---------------------------------------------- */
	/* Start editor main window                       */

	try {
		main_window = new editor::main_window();

	} catch (const std::string &text) {
		QMessageBox::critical(nullptr, QStringLiteral("Could not create window"), text.c_str());
		return 0;
	}

	main_window->show();

	app.exec();

	return 0;
}
