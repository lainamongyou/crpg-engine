#version 450

layout(push_constant) uniform constants
{
	mat4x4 viewproj;
	vec2   add;
	vec2   mul;
	float  uv_mul;
};

layout(location = 0) in vec4 pos_in;
layout(location = 0) out vec2 uv;

void main(void)
{
	vec4 pos    = pos_in;
	pos.xy      = fma(pos.xy, mul, add);
	uv          = pos.xy * uv_mul;
	gl_Position = vec4(pos.xyz, 1.0) * viewproj;
}
