#version 450

layout(push_constant) uniform constants
{
	mat4x4 viewproj;
	vec4   center;
	vec4   pos_offset;
	float  radius_i;
	bool   x_axis;
};

layout(location = 0) in vec4 pos;
layout(location = 1) in vec4 color;
layout(location = 0) out vec4 color_out;

void main(void)
{
	float dist     = distance(center.xy, pos.xy) * radius_i;
	float fade_val = 1.0 - clamp(dist, 0.0, 1.0);

	color_out = color;
	color_out.a *= fade_val;
}
