#version 450

layout(push_constant) uniform constants
{
	mat4x4 viewproj;
	bool   x_axis;
};

layout(location = 0) in vec4 pos;
layout(location = 0) out vec4 color_out;

void main(void)
{
	vec4 pos_out = vec4(pos.xyz, 1.0);
	if (x_axis)
		pos_out.xz = pos_out.zx;

	if (pos.x == 0.0 || pos.z == 0.0)
		color_out = vec4(1.0, 1.0, 1.0, 0.6);
	else
		color_out = vec4(1.0, 1.0, 1.0, 0.2);
	gl_Position = pos_out * viewproj;
}
