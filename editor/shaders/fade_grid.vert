#version 450

layout(push_constant) uniform constants
{
	mat4x4 viewproj;
	vec4   center;
	vec4   pos_offset;
	float  radius_i;
	bool   x_axis;
};

layout(location = 0) in vec4 pos_in;
layout(location = 0) out vec4 pos;
layout(location = 1) out vec4 color;

void main(void)
{
	pos = vec4(pos_in.xyz, 1.0);
	if (x_axis)
		pos.xy = pos.yx;

	pos.xy += floor(pos_offset.xy);

	color = vec4(1.0, 1.0, 1.0, 0.5);

	gl_Position = pos * viewproj;
}
