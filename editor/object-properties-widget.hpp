#pragma once

#include <QAbstractTableModel>
#include <QTableView>

namespace editor {

class map_asset;
class object_asset;
class document_map;
struct object_data;
struct object_property;

class object_properties_model : public QAbstractTableModel {
	Q_OBJECT

	friend class object_properties_widget;

	QTableView  *view;
	map_asset   *map    = nullptr;
	object_data *object = nullptr;

	void set_widget(int row) noexcept;
	void set_object(map_asset *map, object_data *object) noexcept;

public:
	object_properties_model(QTableView *view) noexcept;

	int           rowCount(const QModelIndex &parent) const override;
	int           columnCount(const QModelIndex &parent) const override;
	QModelIndex   index(int row, int column, const QModelIndex &parent) const override;
	QVariant      data(const QModelIndex &index, int role) const override;
	QVariant      headerData(int section, Qt::Orientation orientation, int role) const override;
	Qt::ItemFlags flags(const QModelIndex &index) const override;
};

class object_properties_widget : public QTableView {
	Q_OBJECT

	friend class object_properties_model;

	static object_properties_widget *current;

public:
	object_properties_widget(QWidget *parent) noexcept;

	inline void set_object(map_asset *map, object_data *object) noexcept
	{
		auto *m = static_cast<object_properties_model *>(model());
		m->set_object(map, object);
	}

	static inline void remove_object(object_data &object) noexcept
	{
		auto  this_ = get();
		auto *m     = static_cast<object_properties_model *>(this_->model());
		if (m->object == &object)
			m->set_object(nullptr, nullptr);
	}

	static inline object_properties_widget *get() noexcept { return current; }
};

} // namespace editor
