#pragma once

#include <engine/map-tools.hpp>

#include "main-view.hpp"

namespace editor {

class tileset_asset;

enum class tileset_view_type {
	view_only,
	single_selection,
	multi_selection,
};

struct tileset_selection {
	crpg::segment::tile_pos min;
	crpg::segment::tile_pos max;

	static constexpr tileset_selection invalid() noexcept { return tileset_selection(0xFFFFFFFF); }

	constexpr tileset_selection() noexcept : tileset_selection(invalid()) {}
	constexpr tileset_selection(uint32_t val) noexcept : min((uint16_t)(val >> 16)), max((uint16_t)(val & 0xFFFF))
	{
	}

	static inline tileset_selection zero() noexcept { return tileset_selection(0); }

	inline bool operator==(const tileset_selection &b) const noexcept
	{
		return reinterpret_cast<const uint32_t &>(*this) == reinterpret_cast<const uint32_t &>(b);
	}

	inline bool operator!=(const tileset_selection &b) const noexcept
	{
		return reinterpret_cast<const uint32_t &>(*this) != reinterpret_cast<const uint32_t &>(b);
	}

	inline operator bool() const noexcept { return reinterpret_cast<const uint32_t &>(*this) != 0xFFFFFFFF; }
};

class tileset_view : public main_view {
	Q_OBJECT

	crpg::graphics::texture_ref texture;

	tileset_asset    *asset_     = nullptr;
	tileset_view_type type       = tileset_view_type::view_only;
	float             tile_width = 0.0f;

	crpg::segment::tile_pos first_tile;
	crpg::segment::tile_pos last_tile;
	tileset_selection       selection_;
	bool                    selected_  = false;
	bool                    mouse_down = false;

	void mousePressEvent(QMouseEvent *event) override;
	void mouseReleaseEvent(QMouseEvent *event) override;
	void mouseMoveEvent(QMouseEvent *event) override;

	void clear() noexcept;

	void render_selection_box(const float thickness) noexcept;

public:
	inline tileset_view(QWidget *parent = nullptr) noexcept : main_view(parent) { clear(); }

	inline void           set_asset(tileset_asset *asset) noexcept { asset_ = asset; }
	inline tileset_asset *asset() const noexcept { return asset_; }
	inline void           set_type(tileset_view_type type_) noexcept { type = type_; }

	inline bool              selected() const noexcept { return selected_; }
	inline tileset_selection selection() const noexcept { return selection_; }

	void init() noexcept override;
	void render() noexcept override;

public slots:
	void update_texture() noexcept;
};

} // namespace editor
