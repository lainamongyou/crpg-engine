#include <QGuiApplication>
#include <QMouseEvent>
#include <QWheelEvent>
#include <QKeyEvent>

#include "main-window.hpp"
#include "main-view.hpp"

#include <engine/utility/logging.hpp>
#include <engine/math/math-defs.hpp>
#include <engine/math/axisang.hpp>
#include <engine/resources.hpp>
#include <engine/map-tools.hpp>

using namespace std;
using namespace crpg;
using namespace crpg::graphics;

#define FADE_GRID_RADIUS 2

namespace editor {

static main_view *current_ = nullptr;

/* ---------------------------------------------------------- */

void main_view::init() noexcept
{
	vs_grid                    = resources::shader("editor:grid.vert");
	vs_solid                   = resources::shader("engine:solid.vert");
	vs_unlit                   = resources::shader("engine:unlit.vert");
	vs_unlit_color             = resources::shader("engine:unlit-color.vert");
	vs_transparency_background = resources::shader("editor:transparency-background.vert");
	ps_grid                    = resources::shader("editor:grid.frag");
	ps_solid                   = resources::shader("engine:solid.frag");
	ps_unlit                   = resources::shader("engine:unlit.frag");
	ps_unlit_color             = resources::shader("engine:unlit-color.frag");
	ps_transparency_background = resources::shader("editor:transparency-background.frag");

	setMouseTracking(true);

	/* --------------------------------------- *
	 * build grids                             */

	vertex_buffer::data data;

	data.points.reserve(2);
	data.points.emplace_back(0.0f, 0.0f, 0.0f);
	data.points.emplace_back(1.0f, 0.0f, 0.0f);
	vb_line_horizontal = vertex_buffer::create(std::move(data));

	data.points.reserve(2);
	data.points.emplace_back(0.0f, 0.0f, 0.0f);
	data.points.emplace_back(0.0f, 1.0f, 0.0f);
	vb_line_vertical = vertex_buffer::create(std::move(data));

	data.points.reserve(5);
	data.points.emplace_back(0.0f, 0.0f, 0.0f);
	data.points.emplace_back(0.0f, 1.0f, 0.0f);
	data.points.emplace_back(1.0f, 1.0f, 0.0f);
	data.points.emplace_back(1.0f, 0.0f, 0.0f);
	data.points.emplace_back(0.0f, 0.0f, 0.0f);
	vb_square = vertex_buffer::create(std::move(data));
	vb_sprite = resources::sprite_vb();

	const uint8_t  dark_tr_pixels[] = {0, 128, 128, 0};
	const uint8_t *dark_tr_ptr      = dark_tr_pixels;
	dark_transparency_background    = texture::create(2, 2, 1, texture_format::l8, &dark_tr_ptr);
}

void main_view::mousePressEvent(QMouseEvent *event)
{
	bool middle_down = event->buttons() & Qt::MiddleButton;

	calc_view();

	if (middle_down && !moving_ref++) {
		first_pos = event->pos();

		math::vec2 s((float)qt_size.width(), (float)qt_size.height());

		first_view_pos  = tl;
		first_view_size = view_size;

		first_cursor_pos.set((float)qt_cursor_pos.x(), (float)qt_cursor_pos.y());
		first_cursor_pos /= s;
		first_cursor_pos *= first_view_size;
		first_cursor_pos += first_view_pos;

		first_cam_pos = cam_pos;
	}
}

void main_view::mouseReleaseEvent(QMouseEvent *event)
{
	bool middle_down = event->buttons() & Qt::MiddleButton;

	if (moving_ref && !middle_down && --moving_ref == 0) {
		releaseMouse();
		update();
	}
}

void main_view::mouseMoveEvent(QMouseEvent *event)
{
	qt_cursor_pos = event->pos();

	if (moving_ref) {
		QPoint new_pos = event->pos();

		math::vec2 s((float)qt_size.width(), (float)qt_size.height());
		math::vec2 p((float)new_pos.x(), (int)new_pos.y());

		math::vec2 cursor_pos((float)qt_cursor_pos.x(), (float)qt_cursor_pos.y());
		cursor_pos /= s;
		cursor_pos *= first_view_size;
		cursor_pos += first_view_pos;

		cam_pos = first_cam_pos + (first_cursor_pos - cursor_pos);
	}

	update();
}

void main_view::wheelEvent(QWheelEvent *event)
{
	if (moving_ref) {
		return;
	}

	QPoint angle  = event->angleDelta();
	float  anglef = (float)angle.y();
	float  val    = fabsf(anglef) * 0.01f;
	if (anglef > 0.0f)
		val = 1.0f / val;

	zoom *= val;

	if (!center_cam) {
		cam_pos = cursor_pos;
		calc_view();
		cam_pos -= cursor_pos - cam_pos;
	}

	update();
}

void main_view::calc_view() noexcept
{
	qt_size            = size() * devicePixelRatio();
	float cx           = (float)qt_size.width();
	float cy           = (float)qt_size.height();
	bool  changed_view = false;

	if (lock_view) {
		view_size = br - tl;

	} else if (qt_size != prev_size || cam_pos != prev_cam_pos || zoom != prev_zoom) {
		float aspect = cx / cy;

		/* > 1.0 = inner radius
		 * < 1.0 = outer radius */
		if (aspect > 1.0f) {
			br.x = zoom;
			br.y = zoom / aspect;
		} else {
			br.x = zoom * aspect;
			br.y = zoom;
		}

		if (center_cam) {
			tl.x = -br.x;
			tl.y = -br.y;
		} else {
			tl.set_zero();
		}

		tl += cam_pos;
		br += cam_pos;

		if (br.x < viewable_area.min.x) {
			float diff = viewable_area.min.x - br.x;
			tl.x += diff;
			br.x += diff;
			cam_pos.x += diff;
		} else if (tl.x > viewable_area.max.x) {
			float diff = tl.x - viewable_area.max.x;
			tl.x -= diff;
			br.x -= diff;
			cam_pos.x -= diff;
		}

		if (br.y < viewable_area.min.y) {
			float diff = viewable_area.min.y - br.y;
			tl.y += diff;
			br.y += diff;
			cam_pos.y += diff;
		} else if (tl.y > viewable_area.max.y) {
			float diff = tl.y - viewable_area.max.y;
			tl.y -= diff;
			br.y -= diff;
			cam_pos.y -= diff;
		}

		view_size = br - tl;

		prev_size    = qt_size;
		prev_cam_pos = cam_pos;
		prev_zoom    = zoom;
		changed_view = true;
	}

	if (changed_view || qt_cursor_pos != prev_qt_cursor_pos) {
		cursor_pos.set((float)qt_cursor_pos.x(), (float)qt_cursor_pos.y());
		cursor_pos /= math::vec2(cx, cy);
		cursor_pos *= view_size;
		cursor_pos += tl;
		prev_qt_cursor_pos = qt_cursor_pos;
	}
}

void main_view::render_grid() noexcept
{
	vs_solid->load();
	ps_solid->load();
	vb_line_vertical->load();

	constexpr float segment_size = (float)segment::width;
	constexpr float light        = 0.2f;
	constexpr float mid          = 0.5f;
	constexpr float heavy        = 0.8f;

	math::matrix ident = math::matrix::identity();
	math::vec4   white = math::vec4(1.0f, 1.0f, 1.0f, 1.0f);

	for (float x = ceilf(tl.x); x < br.x; x += 1.0f) {
		if (math::close_float(x, 0.0f))
			white.w = heavy;
		else if (math::close_float(fmodf(x, segment_size), 0.0f))
			white.w = mid;
		else
			white.w = light;

		vs_solid->set_world(ident);
		vs_solid->set("color", white);

		matrix_guard g;
		matrix_scale(1.0f, view_size.y);
		matrix_translate(x, tl.y);
		draw(primitive::lines);
	}

	/* ------------------- */

	vb_line_horizontal->load();

	for (float y = ceilf(tl.y); y < br.y; y += 1.0f) {
		if (math::close_float(y, 0.0f))
			white.w = heavy;
		else if (math::close_float(fmodf(y, segment_size), 0.0f))
			white.w = mid;
		else
			white.w = light;

		vs_solid->set_world(ident);
		vs_solid->set("color", white);

		matrix_guard g;
		matrix_scale(view_size.x, 1.0f);
		matrix_translate(tl.x, y);
		draw(primitive::lines);
	}
}

void main_view::render_transparency_background() noexcept
{
	vs_transparency_background->load();
	ps_transparency_background->load();

	/* note: this number is entirely arbitrary */
	constexpr float mul = 1.0f / 16.0f;

	vs_transparency_background->set("add", tl);
	vs_transparency_background->set("mul", br - tl);
	vs_transparency_background->set("uv_mul", mul);
	ps_transparency_background->set("image", dark_transparency_background);

	vb_sprite->load();
	draw(primitive::tristrip);
}

void main_view::paintEvent(QPaintEvent *event)
{
	render_view::paintEvent(event);

	if (!load())
		return;

	graphics::clear(background_color.x, background_color.y, background_color.z, background_color.w);

	calc_view();

	set_viewport(0, 0, qt_size.width(), qt_size.height());

	ortho(tl.x, br.x, tl.y, br.y, -100.0f, 100.0f);
	render();
	swap();
}

void main_view::update() noexcept
{
	calc_view();
	QWidget::update();
}

void main_view::render() noexcept {}

/* ---------------------------------------------------------- */
} // namespace editor
