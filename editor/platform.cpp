#include "platform.hpp"

#include <QFileInfo>

namespace editor {

bool valid_filename(const QString &name) noexcept
{
	if (name.size() >= 200)
		return false;
	if (name.contains('/') || name.contains('\\') || name.contains('"') || name.contains('\'') ||
	    name.contains('<') || name.contains('>') || name.contains('?') || name.contains('*'))
		return false;
	return true;
}

QString get_safe_filename(const QString &path, const QString &name, const QString &ignore_name) noexcept
{
	QString adjusted_name = name;
	for (QChar &c : adjusted_name) {
		if (!c.isLetterOrNumber() && c != '.' && c != '-')
			c = '_';
		else
			c = c.toLower();
	}

	qsizetype insert_idx = adjusted_name.indexOf('.');
	if (insert_idx == -1)
		insert_idx = adjusted_name.size();

	QString   new_name;
	QFileInfo fi;
	int       inc = 0;
	do {
		new_name = adjusted_name;
		if (inc++ > 0)
			new_name.insert(insert_idx, QString::number(inc));

		if (!ignore_name.isEmpty() && new_name == ignore_name)
			return new_name;

		QString new_path = path + "/" + new_name;
		fi.setFile(new_path);
	} while (fi.exists());

	return new_name;
}

} // namespace editor
