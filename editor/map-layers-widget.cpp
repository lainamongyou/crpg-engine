#include <engine/utility/serializer.hpp>

#include "map-layers-widget.hpp"
#include "asset-browser.hpp"
#include "document-map.hpp"
#include "main-window.hpp"
#include "asset-map.hpp"

#include "ui_map-layers-dock.h"

#include <DockManager.h>

#include <QHBoxLayout>
#include <QDropEvent>
#include <QLineEdit>
#include <QCheckBox>
#include <QLabel>
#include <QMenu>

using namespace ads;
using namespace crpg;

namespace editor {

map_layers_widget_item::map_layers_widget_item(QWidget *parent, map_layer *layer_) noexcept
        : QWidget(parent), layer(layer_)
{
	setAttribute(Qt::WA_TranslucentBackground);
	setMouseTracking(true);

	vis = new QCheckBox(this);
	vis->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
	vis->setFixedSize(16, 16);
	vis->setChecked(layer->visible);
	vis->setProperty("theme", "visibility");

	label = new QLabel(layer->name);
	label->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
	label->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
	label->setAttribute(Qt::WA_TranslucentBackground);
	label->setAttribute(Qt::WA_TransparentForMouseEvents);

	box_layout = new QHBoxLayout(this);
	box_layout->setContentsMargins(0, 0, 0, 0);
	box_layout->addSpacing(1);
	box_layout->addWidget(label);
	box_layout->addWidget(vis);

	setLayout(box_layout);

	/* ----------------------- */

	connect(vis, &QAbstractButton::clicked, this, &map_layers_widget_item::on_toggle_visible);
}

void map_layers_widget_item::on_toggle_visible(bool visible) noexcept
{
	auto      *model = map_layers_model::get();
	map_asset *asset = model->asset;

	auto undo_redo = [](crpg::serializer &s) {
		type::asset_id parent_id;
		type::layer_id layer_id;
		bool           visible;

		s >> parent_id >> layer_id >> visible;

		map_asset *map = static_cast<map_asset *>(asset_manager::get_asset(parent_id));
		map->open();

		map_layer *layer = map->layers[layer_id].get();

		auto *model = map_layers_model::get();
		model->set_layer_visible(layer, visible);
	};

	auto save_undo = [&](crpg::serializer &s) { s << model->asset->id() << layer->id << layer->visible; };
	auto save_redo = [&](crpg::serializer &s) { s << model->asset->id() << layer->id << visible; };

	QString undo_name =
	        visible ? tr("set layer '%1' visible in map '%2'") : tr("set layer '%1' invisible in map '%2'");
	main_window::add_undo_action(
	        undo_name.arg(layer->name, asset->name()), undo_redo, undo_redo, save_undo, save_redo);

	layer->visible = visible;

	document_map *doc = map_layers_model::get()->doc;
	if (doc)
		doc->update();
}

void map_layers_widget_item::edit(std::function<void()> func) noexcept
{
	action_after_edit = func;

	setFocusPolicy(Qt::StrongFocus);
	int pos = box_layout->indexOf(label);

	box_layout->removeWidget(label);

	editor = new QLineEdit(label->text());
	editor->setStyleSheet("background: none");
	editor->selectAll();
	editor->installEventFilter(this);

	box_layout->insertWidget(pos, editor);
	setFocusProxy(editor);
	editor->setFocus();
}

void map_layers_widget_item::close_editor(bool save) noexcept
{
	if (!editor)
		return;

	QString new_name = editor->text();

	setFocusProxy(nullptr);

	int pos = box_layout->indexOf(editor);
	box_layout->removeWidget(editor);
	delete editor;
	editor = nullptr;

	setFocusPolicy(Qt::NoFocus);
	box_layout->insertWidget(pos, label);
	label->setFocus();

	if (save) {
		label->setText(new_name);
		layer->name = new_name;
	}

	action_after_edit();
}

bool map_layers_widget_item::eventFilter(QObject *object, QEvent *event)
{
	if (editor != object)
		return false;

	if (event->type() == QEvent::KeyPress) {
		QKeyEvent *ke = static_cast<QKeyEvent *>(event);

		bool save = true;
		switch (ke->key()) {
		case Qt::Key_Escape:
			save = false;
			[[fallthrough]];
		case Qt::Key_Tab:
			[[fallthrough]];
		case Qt::Key_Backtab:
			[[fallthrough]];
		case Qt::Key_Enter:
			[[fallthrough]];
		case Qt::Key_Return:
			QMetaObject::invokeMethod(this, "close_editor", Qt::QueuedConnection, Q_ARG(bool, save));
			return true;
		}
	} else if (event->type() == QEvent::FocusOut) {
		QMetaObject::invokeMethod(this, "close_editor", Qt::QueuedConnection, Q_ARG(bool, true));
		return true;
	}

	return false;
}

/* ========================================================================= */

map_layers_model *map_layers_model::current = nullptr;

map_layers_model::map_layers_model(QObject *parent) noexcept : QAbstractItemModel(parent)
{
	CDockManager *dockman = main_window::get()->get_dock_manager();
	connect(dockman, &CDockManager::focusedDockWidgetChanged, this, &map_layers_model::dock_focus_changed);
	current = this;
}

map_layers_model::~map_layers_model() noexcept
{
	current = nullptr;
}

void map_layers_model::clear_document() noexcept
{
	beginResetModel();
	doc   = nullptr;
	asset = nullptr;
	endResetModel();
}

void map_layers_model::update_layer_widgets(folder_layer *parent) noexcept
{
	for (size_t i = 0; i < parent->children.size(); i++) {
		map_layer         *layer = parent->children[i];
		QModelIndex        idx   = createIndex((int)i, 0, layer);
		map_layers_widget *mlw   = map_layers_widget::get();
		mlw->setIndexWidget(idx, new map_layers_widget_item(mlw, layer));

		if (layer->type == map_layer_type::folder)
			update_layer_widgets(static_cast<folder_layer *>(layer));
	}
}

void map_layers_model::dock_focus_changed(ads::CDockWidget *, ads::CDockWidget *current) noexcept
{
	asset_category category = current->property("asset_category").value<asset_category>();
	if (category == asset_category::map) {
		document_map *new_doc = static_cast<document_map *>(current->widget());
		if (doc == new_doc)
			return;

		beginResetModel();
		doc   = new_doc;
		asset = doc->asset;
		endResetModel();

		update_layer_widgets(static_cast<folder_layer *>(asset->root));
	}
}

static const QString map_layer_blend_name_alpha    = QStringLiteral("alpha");
static const QString map_layer_blend_name_add      = QStringLiteral("add");
static const QString map_layer_blend_name_subtract = QStringLiteral("subtract");
static const QString map_layer_blend_name_multiply = QStringLiteral("multiply");

static inline crpg::map_layer_blend get_blend_from_name(const QString &name) noexcept
{
	if (name == map_layer_blend_name_alpha) {
		return crpg::map_layer_blend::alpha;
	} else if (name == map_layer_blend_name_add) {
		return crpg::map_layer_blend::add;
	} else if (name == map_layer_blend_name_subtract) {
		return crpg::map_layer_blend::subtract;
	} else if (name == map_layer_blend_name_multiply) {
		return crpg::map_layer_blend::multiply;
	}

	return crpg::map_layer_blend::alpha;
}

static inline const QString get_blend_name(crpg::map_layer_blend blend) noexcept
{
	/* clang-format off */
	switch (blend) {
	case crpg::map_layer_blend::alpha:    return map_layer_blend_name_alpha;
	case crpg::map_layer_blend::add:      return map_layer_blend_name_add;
	case crpg::map_layer_blend::subtract: return map_layer_blend_name_subtract;
	case crpg::map_layer_blend::multiply: return map_layer_blend_name_multiply;
	}
	/* clang-format on */

	return map_layer_blend_name_alpha;
}

static inline void set_combo_by_data(QComboBox *combo, const QString &data) noexcept
{
	int idx;
	idx = combo->findData(data);
	combo->setCurrentIndex(idx);
}

QModelIndex map_layers_model::create_layer(const QString                   &name,
                                           int                              itype,
                                           int                              row,
                                           type::layer_id                   id,
                                           const QModelIndex               &parent_idx,
                                           folder_layer                    *parent,
                                           map_layer                      *&layer,
                                           renderable_map_layer           *&rlayer,
                                           const QString                   &blend_name,
                                           std::function<void(map_layer *)> create_cb) noexcept
{
	crpg::map_layer_type type = static_cast<crpg::map_layer_type>(itype);

	switch (type) {
	case crpg::map_layer_type::tileset:
		layer = new tileset_layer;
		if (doc)
			rlayer = new renderable_tileset_layer;
		break;
	case crpg::map_layer_type::sprite:
		layer = new sprite_layer;
		if (doc)
			rlayer = new renderable_sprite_layer;
		break;
	case crpg::map_layer_type::object:
		layer = new object_layer;
		if (doc)
			rlayer = new renderable_sprite_layer;
		break;
	case crpg::map_layer_type::collision:
		layer = new collision_layer;
		if (doc)
			rlayer = new renderable_tileset_layer;
		break;
	case crpg::map_layer_type::folder:
		layer = new folder_layer;
		if (doc)
			rlayer = nullptr;
		break;
	case crpg::map_layer_type::custom:
		layer = new map_layer;
		if (doc)
			rlayer = nullptr;
		break;
	default:
		return QModelIndex();
	}

	layer->id     = id;
	layer->map_id = asset->id();
	layer->parent = parent;
	layer->name   = name;
	layer->type   = type;
	layer->blend  = get_blend_from_name(blend_name);

	beginInsertRows(parent_idx, row, row);
	asset->layers.emplace(layer->id, layer);
	parent->children.emplace(parent->children.begin() + row, layer);
	if (doc && rlayer) {
		doc->renderable_layers.emplace(layer->id, rlayer);
	}
	if (create_cb)
		create_cb(layer);
	endInsertRows();

	QModelIndex idx = createIndex(row, 0, layer);

	map_layers_widget *mlw = map_layers_widget::get();
	mlw->setIndexWidget(idx, new map_layers_widget_item(mlw, layer));

	return idx;
}

void map_layers_model::add_layer(QModelIndex parent_idx, int itype) noexcept
{
	crpg::map_layer_type type = static_cast<crpg::map_layer_type>(itype);

	map_layers_widget *widget = map_layers_widget::get();
	if (!parent_idx.isValid())
		parent_idx = createIndex(0, 0, asset->root);

	map_layer *parent = static_cast<map_layer *>(parent_idx.internalPointer());
	if (parent->type != map_layer_type::folder) {
		parent     = parent->parent;
		parent_idx = get_layer_index(parent);
	}

	QString base_name;
	switch (type) {
	case crpg::map_layer_type::tileset:
		base_name = tr("Tileset Layer");
		break;
	case crpg::map_layer_type::sprite:
		base_name = tr("Sprite Layer");
		break;
	case crpg::map_layer_type::object:
		base_name = tr("Object Layer");
		break;
	case crpg::map_layer_type::collision:
		base_name = tr("Collision Layer");
		break;
	case crpg::map_layer_type::custom:
		base_name = tr("Programmable Layer");
		break;
	case crpg::map_layer_type::folder:
		base_name = tr("Folder");
		break;
	}

	QString new_name;
	int     idx = 0;
	do {
		new_name = base_name;
		if (idx++ > 0) {
			new_name += " ";
			new_name += QString::number(idx);
		}
	} while (asset->has_layer(new_name));

	map_layer            *layer;
	renderable_map_layer *rlayer;
	QModelIndex           child_idx = create_layer(new_name,
                                             itype,
                                             0,
                                             asset->layer_id_counter++,
                                             parent_idx,
                                             static_cast<folder_layer *>(parent),
                                             layer,
                                             rlayer,
                                             map_layer_blend_name_alpha);

	if (doc && layer->type != crpg::map_layer_type::folder) {
		doc->build_renderable_layer(layer, rlayer);
		doc->update();
	}

	QMetaObject::invokeMethod(widget, "setCurrentIndex", Q_ARG(QModelIndex, child_idx));
	QMetaObject::invokeMethod(widget, "edit", Q_ARG(QModelIndex, child_idx));
}

void map_layers_model::on_remove(const QModelIndex &index) noexcept
{
	map_layer *layer = static_cast<map_layer *>(index.internalPointer());

	auto undo = [this](crpg::serializer &s) {
		type::asset_id map_id;
		QString        name;
		QString        blend;
		int            type;
		type::layer_id parent_id;
		type::layer_id id;
		int            row;

		s >> map_id >> name >> type >> blend >> parent_id >> id >> row;

		auto *map = asset_manager::get_asset(map_id);
		map->open();

		folder_layer *parent     = static_cast<folder_layer *>(asset->layers[parent_id].get());
		QModelIndex   parent_idx = get_layer_index(parent);

		auto load_layer = [&s](map_layer *layer) { layer->load_state(s); };

		map_layer            *layer;
		renderable_map_layer *rlayer;
		QModelIndex           idx =
		        create_layer(name, type, row, id, parent_idx, parent, layer, rlayer, blend, load_layer);

		if (doc) {
			doc->build_renderable_layer(layer, rlayer);
			doc->update();
		}

		QMetaObject::invokeMethod(map_layers_widget::get(), "setCurrentIndex", Q_ARG(QModelIndex, idx));
	};
	auto save_undo = [&](crpg::serializer &s) {
		s << asset->id() << layer->name << layer->type << get_blend_name(layer->blend) << layer->parent->id
		  << layer->id << index.row();
		layer->save_state(s);
	};

	auto redo = [this](crpg::serializer &s) {
		type::asset_id map_id;
		type::layer_id parent_id;
		int            row;

		s >> map_id >> parent_id >> row;

		map_asset *map = static_cast<map_asset *>(asset_manager::get_asset(map_id));
		map->open();

		folder_layer *parent     = static_cast<folder_layer *>(map->layers[parent_id].get());
		QModelIndex   parent_idx = get_layer_index(parent);

		remove_layer(parent_idx, row);
	};
	auto save_redo = [&](crpg::serializer &s) { s << asset->id() << layer->parent->id << index.row(); };

	QString undo_name = tr("remove layer '%1' from '%2'");
	main_window::add_undo_action(undo_name.arg(layer->name, asset->name()), undo, redo, save_undo, save_redo);

	remove_layer(parent(index), index.row());
}

void map_layers_model::remove_children(folder_layer *parent) noexcept
{
	if (!parent->children.size())
		return;

	for (size_t i = 0; i < parent->children.size(); i++) {
		map_layer *layer = parent->children[i];
		if (layer->type == map_layer_type::folder) {
			remove_children(static_cast<folder_layer *>(layer));
		}
	}
	for (auto *layer : parent->children) {
		asset->layers.erase(layer->id);
		if (doc) {
			doc->renderable_layers.erase(layer->id);
		}
	}
	parent->children.clear();
}

void map_layers_model::remove_layer(const QModelIndex &parent_idx, int row) noexcept
{
	folder_layer *parent = static_cast<folder_layer *>(parent_idx.internalPointer());
	map_layer    *layer  = parent->children[row];

	beginRemoveRows(parent_idx, row, row);
	if (layer->type == map_layer_type::folder) {
		remove_children(static_cast<folder_layer *>(layer));
	}
	parent->children.erase(parent->children.begin() + row);
	asset->layers.erase(layer->id);
	if (doc) {
		doc->renderable_layers.erase(layer->id);
		doc->update();
	}
	endRemoveRows();
}

void map_layers_model::rename_layer(map_layer *layer, const QString &name) noexcept
{
	QModelIndex idx = get_layer_index(layer);
	setData(idx, name, Qt::EditRole);
}

void map_layers_model::set_layer_visible(map_layer *layer, bool visible) noexcept
{
	QModelIndex idx = get_layer_index(layer);

	auto *mlw  = map_layers_widget::get();
	auto *item = static_cast<map_layers_widget_item *>(mlw->indexWidget(idx));

	item->vis->blockSignals(true);
	item->vis->setChecked(visible);
	item->vis->blockSignals(false);

	layer->visible = visible;

	mlw->update();
}

void map_layers_model::set_layer_blend(map_layer *layer, const QString &blend_name) noexcept
{
	auto       *mlw     = map_layers_widget::get();
	QModelIndex cur_idx = mlw->currentIndex();

	if (cur_idx.isValid() && cur_idx.internalPointer() == (void *)layer) {
		QComboBox *blend = container->ui->blend;
		blend->blockSignals(true);
		set_combo_by_data(blend, blend_name);
		blend->blockSignals(false);
	}

	layer->blend = get_blend_from_name(blend_name);
	if (doc)
		doc->update();
}

void map_layers_model::set_layer_opacity(map_layer *layer, float opacity) noexcept
{
	auto       *mlw     = map_layers_widget::get();
	QModelIndex cur_idx = mlw->currentIndex();

	if (cur_idx.isValid() && cur_idx.internalPointer() == (void *)layer) {
		editor::better_double_spin_box *widget = container->ui->opacity;
		widget->blockSignals(true);
		widget->setValue(opacity);
		widget->blockSignals(false);
	}

	layer->opacity = opacity;
	if (doc)
		doc->update();
}

void map_layers_model::on_create(const QModelIndex &index) noexcept
{
	map_layer *layer = static_cast<map_layer *>(index.internalPointer());

	auto undo = [this](crpg::serializer &s) {
		type::asset_id map_id;
		type::layer_id parent_id;
		s >> map_id >> parent_id;

		map_asset *map = static_cast<map_asset *>(asset_manager::get_asset(map_id));
		map->open();

		folder_layer *parent = static_cast<folder_layer *>(map->layers[parent_id].get());
		remove_layer(get_layer_index(parent), 0);
	};
	auto save_undo = [&](crpg::serializer &s) { s << asset->id() << layer->parent->id; };

	auto redo = [this](crpg::serializer &s) {
		type::asset_id map_id;
		type::layer_id id;
		type::layer_id parent_id;
		QString        name;
		QString        blend;
		int            type;

		s >> map_id >> id >> parent_id >> name >> type >> blend;

		map_asset *map = static_cast<map_asset *>(asset_manager::get_asset(map_id));
		map->open();

		folder_layer *parent     = static_cast<folder_layer *>(map->layers[parent_id].get());
		QModelIndex   parent_idx = get_layer_index(parent);

		map_layer            *layer;
		renderable_map_layer *rlayer;
		QModelIndex           idx = create_layer(name, type, 0, id, parent_idx, parent, layer, rlayer, blend);

		QMetaObject::invokeMethod(map_layers_widget::get(), "setCurrentIndex", Q_ARG(QModelIndex, idx));
	};
	auto save_redo = [&](crpg::serializer &s) {
		s << asset->id() << layer->id << layer->parent->id << layer->name << (int)layer->type
		  << get_blend_name(layer->blend);
	};

	QString undo_name = tr("add layer '%1' to '%2'");
	main_window::add_undo_action(undo_name.arg(layer->name, asset->name()), undo, redo, save_undo, save_redo);

	on_select(index);
}

void map_layers_model::on_rename(const QModelIndex &index, const QString &prev_name) noexcept
{
	map_layer *layer = static_cast<map_layer *>(index.internalPointer());
	if (layer->name == prev_name)
		return;

	auto undo_redo = [this](crpg::serializer &s) {
		type::asset_id map_id;
		type::layer_id layer_id;
		QString        name;

		s >> map_id >> name >> layer_id;

		map_asset *map = static_cast<map_asset *>(asset_manager::get_asset(map_id));
		map->open();

		map_layer *layer = map->layers[layer_id].get();
		rename_layer(layer, name);
	};

	auto save_undo = [&](crpg::serializer &s) { s << asset->id() << prev_name << layer->id; };
	auto save_redo = [&](crpg::serializer &s) { s << asset->id() << layer->name << layer->id; };

	QString undo_name = tr("rename map layer '%1' to '%2' in '%3'");
	main_window::add_undo_action(
	        undo_name.arg(prev_name, layer->name, asset->name()), undo_redo, undo_redo, save_undo, save_redo);
}

void map_layers_model::on_select(const QModelIndex &index) noexcept
{
	if (!container)
		return;

	if (!index.isValid()) {
		if (doc)
			doc->set_active_layer();
		container->ui->blend->setEnabled(false);
		container->ui->opacity->setEnabled(false);
		return;
	}

	doc->set_active_layer();

	map_layer *layer = static_cast<map_layer *>(index.internalPointer());

	container->ui->blend->setEnabled(true);
	container->ui->blend->blockSignals(true);
	set_combo_by_data(container->ui->blend, get_blend_name(layer->blend));
	container->ui->blend->blockSignals(false);

	container->ui->opacity->setEnabled(true);
	container->ui->opacity->blockSignals(true);
	container->ui->opacity->setValue(layer->opacity);
	container->ui->opacity->blockSignals(false);
}

void map_layers_model::on_blend_change(const QModelIndex &index, const QString &blend_name) noexcept
{
	if (!index.isValid() || !asset)
		return;

	auto undo_redo = [this](crpg::serializer &s) {
		type::asset_id map_id;
		type::layer_id layer_id;
		QString        blend_name;

		s >> map_id >> layer_id >> blend_name;

		map_asset *map = static_cast<map_asset *>(asset_manager::get_asset(map_id));
		map->open();

		map_layer *layer = map->layers[layer_id].get();
		set_layer_blend(layer, blend_name);
	};

	editor::map_layer *layer = static_cast<map_layer *>(index.internalPointer());

	auto save_undo = [&](crpg::serializer &s) { s << asset->id() << layer->id << get_blend_name(layer->blend); };
	auto save_redo = [&](crpg::serializer &s) { s << asset->id() << layer->id << blend_name; };

	QString undo_name = tr("change blend of layer '%1' in map '%2'");
	main_window::add_undo_action(
	        undo_name.arg(layer->name, asset->name()), undo_redo, undo_redo, save_undo, save_redo);

	layer->blend = get_blend_from_name(blend_name);
	if (doc)
		doc->update();
}

void map_layers_model::on_opacity_change(const QModelIndex &index, float opacity) noexcept
{
	if (!index.isValid() || !asset)
		return;

	auto undo_redo = [this](crpg::serializer &s) {
		type::asset_id map_id;
		type::layer_id layer_id;
		float          opacity;

		s >> map_id >> layer_id >> opacity;

		map_asset *map = static_cast<map_asset *>(asset_manager::get_asset(map_id));
		map->open();

		map_layer *layer = map->layers[layer_id].get();
		set_layer_opacity(layer, opacity);
	};

	editor::map_layer *layer = static_cast<map_layer *>(index.internalPointer());

	auto save_undo = [&](crpg::serializer &s) { s << asset->id() << layer->id << layer->opacity; };
	auto save_redo = [&](crpg::serializer &s) { s << asset->id() << layer->id << opacity; };

	QString undo_name = tr("change opacity of layer '%1' in map '%2'");
	main_window::add_undo_action(
	        undo_name.arg(layer->name, asset->name()), undo_redo, undo_redo, save_undo, save_redo);

	layer->opacity = opacity;
	if (doc)
		doc->update();
}

QVariant map_layers_model::data(const QModelIndex &index, int role) const
{
	if (!index.isValid())
		return QVariant();

	auto *layer = static_cast<map_layer *>(index.internalPointer());
	if (role == Qt::EditRole)
		return layer->name;
	return QVariant();
}

bool map_layers_model::setData(const QModelIndex &index, const QVariant &val, int role)
{
	if (!index.isValid() || role != Qt::EditRole)
		return false;
	if (!doc)
		return false;

	map_layer *layer    = static_cast<map_layer *>(index.internalPointer());
	QString    new_name = val.toString().simplified();

	if (new_name.isEmpty())
		return false;
	if (asset->has_layer(new_name))
		return false;

	QList<int> roles;
	roles.push_back(role);

	layer->name = new_name;
	emit dataChanged(index, index, roles);

	map_layers_widget *mlw  = map_layers_widget::get();
	auto              *item = static_cast<map_layers_widget_item *>(mlw->indexWidget(index));
	item->label->setText(new_name);
	return true;
}

Qt::ItemFlags map_layers_model::flags(const QModelIndex &idx) const
{
	if (!idx.isValid())
		return Qt::ItemIsDropEnabled;

	map_layer *layer = static_cast<map_layer *>(idx.internalPointer());
	if (layer == asset->root)
		return Qt::ItemIsDropEnabled;

	Qt::ItemFlags flags = Qt::ItemIsDropEnabled | Qt::ItemIsEnabled | Qt::ItemIsDragEnabled | Qt::ItemIsSelectable |
	                      Qt::ItemIsEditable;
	if (layer->type != map_layer_type::folder)
		flags |= Qt::ItemNeverHasChildren;

	return flags;
}

Qt::DropActions map_layers_model::supportedDropActions() const
{
	return QAbstractItemModel::supportedDropActions() | Qt::MoveAction;
}

QModelIndex map_layers_model::index(int row, int column, const QModelIndex &idx) const
{
	if (!asset || row < 0 || column != 0)
		return QModelIndex();

	QModelIndex parent_idx = !idx.isValid() ? createIndex(0, 0, asset->root) : idx;

	folder_layer *parent = static_cast<folder_layer *>(parent_idx.internalPointer());
	if (parent->type != map_layer_type::folder)
		return QModelIndex();
	if (row >= (int)parent->children.size())
		return QModelIndex();

	return createIndex(row, 0, parent->children[row]);
}

QModelIndex map_layers_model::parent(const QModelIndex &idx) const
{
	if (!idx.isValid())
		return QModelIndex();

	map_layer *layer  = static_cast<map_layer *>(idx.internalPointer());
	map_layer *parent = layer->parent;
	if (!parent)
		return QModelIndex();

	return get_layer_index(parent);
}

int map_layers_model::rowCount(const QModelIndex &idx) const
{
	if (idx.column() > 0)
		return 0;
	if (!idx.isValid())
		return asset ? (int)asset->root->children.size() : 0;

	folder_layer *layer = static_cast<folder_layer *>(idx.internalPointer());
	if (layer->type != map_layer_type::folder)
		return 0;

	return (int)layer->children.size();
}

int map_layers_model::columnCount(const QModelIndex &) const
{
	return 1;
}

QModelIndex map_layers_model::get_layer_index(map_layer *layer) const noexcept
{
	return layer ? createIndex(layer->get_row(), 0, layer) : QModelIndex();
}

/* ========================================================================= */

map_layers_widget *map_layers_widget::current = nullptr;

map_layers_widget::map_layers_widget(QWidget *parent) noexcept : QTreeView(parent)
{
	current = this;

	setEditTriggers(QAbstractItemView::EditKeyPressed);
	setContextMenuPolicy(Qt::CustomContextMenu);
	setModel(new map_layers_model(this));
	setHeaderHidden(true);
	setDragEnabled(true);
	setDropIndicatorShown(true);
	setDragDropMode(QAbstractItemView::InternalMove);
	setDefaultDropAction(Qt::TargetMoveAction);

	auto right_click = [this]() {
		if (!map_layers_model::get()->asset)
			return;

		QPoint      pos   = QCursor::pos();
		QModelIndex idx   = indexAt(mapFromGlobal(pos));
		map_layer  *layer = static_cast<map_layer *>(idx.internalPointer());

		QMenu menu;
		menu.addAction(tr("Add Tileset Layer"),
		               [idx]() { map_layers_model::get()->add_layer(idx, (int)map_layer_type::tileset); });
		menu.addAction(tr("Add Sprite Layer"),
		               [idx]() { map_layers_model::get()->add_layer(idx, (int)map_layer_type::sprite); });
		menu.addAction(tr("Add Object Layer"),
		               [idx]() { map_layers_model::get()->add_layer(idx, (int)map_layer_type::object); });
		menu.addAction(tr("Add Collision Layer"),
		               [idx]() { map_layers_model::get()->add_layer(idx, (int)map_layer_type::collision); });
		menu.addAction(tr("Add Programmable Layer"),
		               [idx]() { map_layers_model::get()->add_layer(idx, (int)map_layer_type::custom); });
		menu.addSeparator();
		menu.addAction(tr("Add Folder"),
		               [idx]() { map_layers_model::get()->add_layer(idx, (int)map_layer_type::folder); });
		if (layer) {
			menu.addSeparator();
			menu.addAction(tr("Remove Layer"), [idx]() { map_layers_model::get()->on_remove(idx); });
		}

		menu.exec(pos);
	};

	connect(this, &QWidget::customContextMenuRequested, right_click);
}

map_layers_widget::~map_layers_widget() noexcept
{
	current = nullptr;
}

void map_layers_widget::selectionChanged(const QItemSelection &selected, const QItemSelection &deselected)
{
	QAbstractItemView::selectionChanged(selected, deselected);

	auto *mlm = map_layers_model::get();
	if (!mlm)
		return;

	QModelIndexList indices = selected.indexes();
	QModelIndex     idx;

	if (indices.size())
		idx = indices[0];

	map_layers_model::get()->on_select(idx);
}

bool map_layers_widget::edit(const QModelIndex &index, QAbstractItemView::EditTrigger trigger, QEvent *event)
{
	if ((trigger != QAbstractItemView::AllEditTriggers || event) &&
	    (trigger != QAbstractItemView::EditKeyPressed || !event)) {
		return false;
	}

	std::function<void()> action_after_edit;
	map_layer            *layer = static_cast<map_layer *>(index.internalPointer());
	if (!event) {
		/* edit invoked manually, item created */
		action_after_edit = [=]() { map_layers_model::get()->on_create(index); };
	} else {
		/* standard rename event */
		QString prev_name = layer->name;
		action_after_edit = [=]() { map_layers_model::get()->on_rename(index, prev_name); };
	}

	auto *item = static_cast<map_layers_widget_item *>(indexWidget(index));
	item->edit(action_after_edit);
	return true;
}

static bool is_descendant(const QModelIndex &idx_parent, const QModelIndex &idx) noexcept
{
	if (!idx.isValid())
		return false;
	if (idx == idx_parent)
		return true;
	return is_descendant(idx_parent, idx.parent());
}

void map_layers_widget::move_layer(const QModelIndex &src_parent_idx,
                                   int                src_row,
                                   const QModelIndex &dst_parent_idx,
                                   int                dst_row) noexcept
{
	auto         *m   = map_layers_model::get();
	document_map *doc = m->doc;

	folder_layer *src_parent = static_cast<folder_layer *>(src_parent_idx.internalPointer());
	folder_layer *dst_parent = static_cast<folder_layer *>(dst_parent_idx.internalPointer());

	map_layer *layer = src_parent->children[src_row];

	m->beginMoveRows(src_parent_idx, src_row, src_row, dst_parent_idx, dst_row);

	if (src_parent == dst_parent && src_row < dst_row)
		dst_row--;

	layer->parent = dst_parent;
	src_parent->children.erase(src_parent->children.begin() + src_row);
	dst_parent->children.insert(dst_parent->children.begin() + dst_row, layer);

	m->endMoveRows();

	if (doc) {
		doc->update();
		doc->set_active_layer();
	}
}

void map_layers_widget::dropEvent(QDropEvent *event)
{
	if (event->source() != this) {
		QTreeView::dropEvent(event);
		return;
	}

	QModelIndexList indices = selectedIndexes();
	if (!indices.size()) {
		QTreeView::dropEvent(event);
		return;
	}

	auto      *m     = map_layers_model::get();
	map_asset *asset = m->asset;

	DropIndicatorPosition indicator = dropIndicatorPosition();
	QModelIndex           drag_idx  = indices[0];
	QModelIndex           drop_idx  = indexAt(event->position().toPoint());
	map_layer            *layer     = static_cast<map_layer *>(drag_idx.internalPointer());

	if (!drop_idx.isValid()) {
		drop_idx  = m->createIndex(0, 0, asset->root);
		indicator = QAbstractItemView::BelowItem;
	}

	map_layer *drop_layer = static_cast<map_layer *>(drop_idx.internalPointer());

	QModelIndex drag_parent_idx = drag_idx.parent();
	QModelIndex drop_parent_idx;

	int drop_row = drop_idx.row();
	if (drop_layer->type == map_layer_type::folder && indicator != QAbstractItemView::AboveItem) {
		folder_layer *folder = static_cast<folder_layer *>(drop_layer);

		drop_parent_idx = drop_idx;
		drop_row        = (int)folder->children.size();
	} else {
		drop_parent_idx = drop_idx.parent();

		if (drag_parent_idx == drop_parent_idx &&
		    (indicator == QAbstractItemView::BelowItem || indicator == QAbstractItemView::OnItem)) {
			drop_row++;
		}
	}

	if (drag_parent_idx == drop_parent_idx && (drop_row == drag_idx.row() || drop_row == drag_idx.row() + 1)) {
		QTreeView::dropEvent(event);
		return;
	}

	/* ------------------------------------------------ */

	move_layer(drag_parent_idx, drag_idx.row(), drop_parent_idx, drop_row);

	/* ------------------------------------------------ */

	folder_layer *drag_parent = static_cast<folder_layer *>(drag_parent_idx.internalPointer());
	folder_layer *drop_parent = static_cast<folder_layer *>(drop_parent_idx.internalPointer());

	auto undo_redo = [](crpg::serializer &s) {
		type::asset_id map_id;
		type::layer_id src_parent_id;
		type::layer_id dst_parent_id;
		int            src_row;
		int            dst_row;

		s >> map_id >> src_parent_id >> dst_parent_id >> src_row >> dst_row;

		map_asset *map = static_cast<map_asset *>(asset_manager::get_asset(map_id));
		map->open();

		folder_layer *src_parent = static_cast<folder_layer *>(map->layers[src_parent_id].get());
		folder_layer *dst_parent = static_cast<folder_layer *>(map->layers[dst_parent_id].get());

		auto       *mlm            = map_layers_model::get();
		QModelIndex src_parent_idx = mlm->get_layer_index(src_parent);
		QModelIndex dst_parent_idx = mlm->get_layer_index(dst_parent);

		auto *mlw = map_layers_widget::get();
		mlw->move_layer(src_parent_idx, src_row, dst_parent_idx, dst_row);
	};
	auto save_undo = [&](crpg::serializer &s) {
		s << asset->id() << drop_parent->id << drag_parent->id;

		if (drag_parent == drop_parent) {
			if (drag_idx.row() < drop_row)
				s << (drop_row - 1) << drag_idx.row();
			else
				s << drop_row << (drag_idx.row() + 1);
		} else {
			s << drop_row << drag_idx.row();
		}
	};
	auto save_redo = [&](crpg::serializer &s) {
		s << asset->id() << drag_parent->id << drop_parent->id << drag_idx.row() << drop_row;
	};

	QString undo_name = tr("move layer '%1' in map '%2'");
	main_window::add_undo_action(
	        undo_name.arg(layer->name, asset->name()), undo_redo, undo_redo, save_undo, save_redo);

	/* ------------------------------------------------ */

	event->accept();
	event->setDropAction(Qt::MoveAction);
	QTreeView::dropEvent(event);
}

/* ========================================================================= */

map_layers::map_layers(QWidget *parent) noexcept : QWidget(parent), ui(new Ui_map_layers_dock)
{
	ui->setupUi(this);
	map_layers_model::get()->container = this;

	/* clang-format off */
	ui->blend->addItem(tr("Alpha"),    map_layer_blend_name_alpha);
	ui->blend->addItem(tr("Add"),      map_layer_blend_name_add);
	ui->blend->addItem(tr("Subtract"), map_layer_blend_name_subtract);
	ui->blend->addItem(tr("Multiply"), map_layer_blend_name_multiply);
	/* clang-format on */

	set_combo_by_data(ui->blend, map_layer_blend_name_alpha);

	connect(ui->blend, &QComboBox::currentIndexChanged, [this]() {
		QString blend_name = ui->blend->currentData().toString();
		map_layers_model::get()->on_blend_change(ui->layers->currentIndex(), blend_name);
	});

	connect(ui->opacity, &better_double_spin_box::editing_actually_finished, [this]() {
		float opacity = (float)ui->opacity->value();
		map_layers_model::get()->on_opacity_change(ui->layers->currentIndex(), opacity);
	});
}

map_layers::~map_layers() noexcept {}

} // namespace editor
