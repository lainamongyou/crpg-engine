#include <engine/utility/stringserializer.hpp>
#include <engine/mask.hpp>

#include <QImage>

#include <cstdint>
#include <vector>
#include <string>

using namespace crpg;

namespace editor {

std::string image_to_mask(const QString &path) noexcept
{
	QImage image(path);
	image.convertTo(QImage::Format_Grayscale8);

	const uchar *data = image.constBits();
	uint32_t     cx   = (uint32_t)image.width();
	uint32_t     cy   = (uint32_t)image.height();

	constexpr uint32_t align    = (uint32_t)mask::block::size;
	uint32_t           block_cx = (cx + (align - 1)) / align;
	uint32_t           block_cy = (cy + (align - 1)) / align;

	std::vector<mask::block> blocks;
	blocks.resize(block_cx * block_cy);

	for (uint32_t y = 0; y < cy; y++) {
		uint32_t y_offset       = y * cx;
		uint32_t block_y_offset = (y / align) * block_cx;
		uint32_t row            = y % align;

		for (uint32_t x = 0; x < cx; x++) {
			uchar    val     = data[y_offset + x];
			uint32_t block_x = x / align;
			uint32_t sub_x   = x % align;

			auto     &block = blocks[block_y_offset + block_x];
			uint16_t &out   = block.rows[row];
			out             = out | ((uint16_t) !!val) << sub_x;
		}
	}

	std::string       out;
	ostringserializer s(out);
	s << (block_cx * align) << (block_cy * align);
	s.serialize(blocks.data(), blocks.size() * sizeof(mask::block));
	return out;
}

} // namespace editor
