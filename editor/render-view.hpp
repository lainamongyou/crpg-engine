#pragma once

#include <QScopedPointer>
#include <QWidget>

#include <engine/graphics/graphics.hpp>

namespace editor {

class surf_filter;

class render_view : public QWidget {
	Q_OBJECT

	friend class surf_filter;

	bool create_display(bool now = false) noexcept;
	void destroy_display() noexcept;

	crpg::graphics::display_ref display;
	bool                        initialized = false;
	bool                        failed      = false;
	QScopedPointer<surf_filter> filter;

protected:
	virtual void          resizeEvent(QResizeEvent *event) override;
	virtual void          paintEvent(QPaintEvent *event) override;
	virtual QPaintEngine *paintEngine() const override;

	virtual void init() noexcept;

public:
	render_view(QWidget *parent = nullptr, Qt::WindowFlags flags = Qt::WindowFlags()) noexcept;
	~render_view();

	bool load() noexcept;
	void swap() noexcept;

signals:
	void created();
	void resized();
	void painted();
};
} // namespace editor
