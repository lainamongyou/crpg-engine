#include "asset-tileset.hpp"
#include "tile-selector.hpp"
#include "tileset-view.hpp"

namespace editor {

tile_selector *tile_selector::current = nullptr;

tile_selector::tile_selector(const QString &name, QWidget *parent) noexcept : ads::CDockWidget(name, parent)
{
	view = new tileset_view(this);
	setWidget(view);
	view->set_type(tileset_view_type::multi_selection);

	current = this;
}

tile_selector::~tile_selector() noexcept
{
	current = nullptr;
}

void tile_selector::set_asset(tileset_asset *asset) noexcept
{
	view->set_asset(asset);
}

tileset_asset *tile_selector::asset() const noexcept
{
	return view->asset();
}

} // namespace editor
