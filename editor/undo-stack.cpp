#include "undo-stack.hpp"

#include <engine/utility/stringserializer.hpp>
#include <engine/utility/time.hpp>

namespace editor {

/* ------------------------------------------------------------------------ */

void undo_stack::add_action(const QString &name,
                            undo_redo_cb   undo,
                            undo_redo_cb   redo,
                            undo_redo_cb   save_undo,
                            undo_redo_cb   save_redo,
                            bool           repeatable) noexcept
{
	int64_t ts        = crpg::get_time_ms();
	bool    is_repeat = repeatable && index == 0 && !!items.size() && items[0].name == name;

	if (is_repeat && (ts - last_action_ts) < 1000) {
		undo_item &item = items[0];
		item.redo_data.clear();
		crpg::ostringserializer s_redo(item.redo_data);
		save_redo(s_redo);
	} else {
		undo_item item;
		item.undo = undo;
		item.redo = redo;
		item.name = name;

		crpg::ostringserializer s_undo(item.undo_data);
		crpg::ostringserializer s_redo(item.redo_data);
		save_undo(s_undo);
		save_redo(s_redo);

		while (index > 0) {
			items.pop_front();
			index--;
		}

		items.push_front(item);
	}

	last_action_ts = repeatable ? ts : 0;
}

void undo_stack::add_action(const QString     &name,
                            undo_redo_cb       undo,
                            undo_redo_cb       redo,
                            const std::string &undo_data,
                            const std::string &redo_data,
                            bool               repeatable) noexcept
{
	int64_t ts        = crpg::get_time_ms();
	bool    is_repeat = repeatable && index == 0 && !!items.size() && items[0].name == name;

	if (is_repeat && (ts - last_action_ts) < 1000) {
		undo_item &item = items[0];
		item.redo_data  = redo_data;
	} else {
		undo_item item;
		item.undo      = undo;
		item.redo      = redo;
		item.undo_data = undo_data;
		item.redo_data = redo_data;
		item.name      = name;

		while (index > 0) {
			items.pop_front();
			index--;
		}

		items.push_front(item);
	}

	last_action_ts = repeatable ? ts : 0;
}

void undo_stack::undo(QString &undo_name, QString &redo_name) noexcept
{
	if (index == items.size())
		return;

	undo_item              &item = items[index++];
	crpg::istringserializer s(item.undo_data);
	item.undo(s);

	redo_name = item.name;
	if (index < items.size())
		undo_name = items[index].name;
}

void undo_stack::redo(QString &undo_name, QString &redo_name) noexcept
{
	if (index == 0)
		return;

	undo_item              &item = items[--index];
	crpg::istringserializer s(item.redo_data);
	item.redo(s);

	undo_name = item.name;
	if (index > 0)
		redo_name = items[index - 1].name;
}

/* ------------------------------------------------------------------------ */
} // namespace editor
