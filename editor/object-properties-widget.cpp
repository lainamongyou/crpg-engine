#include <engine/utility/variant.hpp>

#include "object-properties-widget.hpp"
#include "map-layers-widget.hpp"
#include "asset-browser.hpp"
#include "asset-object.hpp"
#include "main-window.hpp"
#include "asset-map.hpp"

#include <QDoubleSpinBox>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QHeaderView>
#include <QCheckBox>
#include <QComboBox>
#include <QLineEdit>
#include <QSpinBox>

namespace editor {

enum class property_column : int {
	name,
	value,
	reset,

	count,
};

/* ========================================================================= */

class property_widget : public QWidget {
protected:
	property_widget(QWidget *parent, object_data *object_, int row_) noexcept
	        : QWidget(parent), object(object_), row(row_)
	{
	}

	template<typename T> void set_value(const T &value) noexcept
	{
		auto undo_redo = [](crpg::serializer &s) {
			crpg::type::asset_id  asset_id;
			crpg::type::object_id object_id;
			int                   layer_id;
			int                   row;
			std::string           var;

			s >> asset_id >> layer_id >> object_id >> row >> var;

			map_asset    *asset = static_cast<map_asset *>(asset_manager::get_asset(asset_id));
			object_layer *layer = static_cast<object_layer *>(asset->layers[layer_id].get());

			asset->open();

			auto *mlw = map_layers_widget::get();
			auto *mlm = map_layers_model::get();

			QItemSelectionModel *selection = mlw->selectionModel();
			selection->setCurrentIndex(mlm->get_layer_index(layer), QItemSelectionModel::Select);

			object_data &object    = layer->objects[object_id];
			object.properties[row] = var;

			auto *opw = object_properties_widget::get();
			opw->set_object(asset, &object);

			auto    *model  = opw->model();
			QWidget *widget = opw->indexWidget(model->index(row, (int)property_column::value));
			if (widget)
				static_cast<property_widget *>(widget)->update_value();
		};

		auto save = [&](crpg::serializer &s, const std::string &val) {
			auto           *mlw       = map_layers_widget::get();
			auto           *selection = mlw->selectionModel();
			QModelIndexList indices   = selection->selectedIndexes();
			map_layer      *layer     = static_cast<map_layer *>(indices[0].internalPointer());

			s << object->asset.owner()->id() << layer->id << object->id << row << val;
		};

		auto save_undo = std::bind(save, std::placeholders::_1, object->properties[row]);
		auto save_redo = std::bind(save, std::placeholders::_1, crpg::variant::create(value));

		QString text = tr("change object properties in '%1'").arg(object->asset.owner()->name());
		main_window::add_undo_action(text, undo_redo, undo_redo, save_undo, save_redo);

		object->properties[row] = crpg::variant::create(value);
	}

	virtual void update_value() = 0;

	object_data *object;
	int          row;
};

class boolean_widget : public property_widget {
public:
	QCheckBox *widget;

	boolean_widget(QWidget *parent, object_data *object_, int row_) noexcept
	        : property_widget(parent, object_, row_)
	{
		widget = new QCheckBox(this);

		QVBoxLayout *layout = new QVBoxLayout;
		layout->setContentsMargins(0, 0, 0, 0);
		layout->addWidget(widget);
		setLayout(layout);

		connect(widget, &QAbstractButton::clicked, [this](bool checked) { set_value(checked); });

		update_value();
	}

	void update_value() noexcept override
	{
		const std::string &v_val = object->properties[row];
		bool val = v_val.empty() ? crpg::variant::get<bool>(object->asset->properties[row].default_val)
		                         : crpg::variant::get<bool>(v_val);

		widget->blockSignals(true);
		widget->setChecked(val);
		widget->blockSignals(false);
	}
};

class integer_widget : public property_widget {
public:
	QSpinBox *widget;

	integer_widget(QWidget *parent, object_data *object_, int row_) noexcept
	        : property_widget(parent, object_, row_)
	{
		widget = new QSpinBox(this);

		int min;
		int max;

		crpg::istringserializer s(object->asset->properties[row].config_info);
		s >> min >> max;

		widget->setRange(min, max);

		QVBoxLayout *layout = new QVBoxLayout;
		layout->setContentsMargins(0, 0, 0, 0);
		layout->addWidget(widget);
		setLayout(layout);

		connect(widget, &QSpinBox::valueChanged, [this](int val) { set_value(val); });

		update_value();
	}

	void update_value() noexcept override
	{
		const std::string &v_val = object->properties[row];
		int val = v_val.empty() ? crpg::variant::get<int>(object->asset->properties[row].default_val)
		                        : crpg::variant::get<int>(v_val);

		widget->blockSignals(true);
		widget->setValue(val);
		widget->blockSignals(false);
	}
};

class real_widget : public property_widget {
public:
	QDoubleSpinBox *widget;

	real_widget(QWidget *parent, object_data *object_, int row_) noexcept : property_widget(parent, object_, row_)
	{
		widget = new QDoubleSpinBox(this);

		float min;
		float max;

		crpg::istringserializer s(object->asset->properties[row].config_info);
		s >> min >> max;

		widget->setRange(min, max);
		widget->setSingleStep(0.01);

		QVBoxLayout *layout = new QVBoxLayout;
		layout->setContentsMargins(0, 0, 0, 0);
		layout->addWidget(widget);
		setLayout(layout);

		connect(widget, &QDoubleSpinBox::valueChanged, [this](double val) { set_value((float)val); });

		update_value();
	}

	void update_value() noexcept override
	{
		const std::string &v_val = object->properties[row];
		double val = v_val.empty() ? crpg::variant::get<float>(object->asset->properties[row].default_val)
		                           : crpg::variant::get<float>(v_val);

		widget->blockSignals(true);
		widget->setValue(val);
		widget->blockSignals(false);
	}
};

class string_widget : public property_widget {
public:
	QLineEdit *widget;

	string_widget(QWidget *parent, object_data *object_, int row_) noexcept : property_widget(parent, object_, row_)
	{
		widget = new QLineEdit(this);

		QVBoxLayout *layout = new QVBoxLayout;
		layout->setContentsMargins(0, 0, 0, 0);
		layout->addWidget(widget);
		setLayout(layout);

		connect(widget, &QLineEdit::textChanged, [this](const QString &text) { set_value(text); });

		update_value();
	}

	void update_value() noexcept override
	{
		const std::string &v_val = object->properties[row];
		QString val = v_val.empty() ? QString() : crpg::variant::get<QString>(object->properties[row]);

		widget->blockSignals(true);
		widget->setText(val);
		widget->blockSignals(false);
	}
};

class enumeration_widget : public property_widget {
public:
	QComboBox *widget;

	enumeration_widget(QWidget *parent, object_data *object_, int row_) noexcept
	        : property_widget(parent, object_, row_)
	{
		widget = new QComboBox(this);

		QStringList             list;
		crpg::istringserializer s(object->asset->properties[row].config_info);
		s >> list;

		widget->addItems(list);

		QVBoxLayout *layout = new QVBoxLayout;
		layout->setContentsMargins(0, 0, 0, 0);
		layout->addWidget(widget);
		setLayout(layout);

		connect(widget, &QComboBox::currentTextChanged, [this](const QString &text) { set_value(text); });

		update_value();
	}

	void update_value() noexcept override
	{
		const std::string &v_val = object->properties[row];
		QString val = v_val.empty() ? crpg::variant::get<QString>(object->asset->properties[row].default_val)
		                            : crpg::variant::get<QString>(v_val);

		widget->blockSignals(true);
		int val_row = widget->findText(val);
		widget->setCurrentIndex(val_row);
		widget->blockSignals(false);
	}
};

class asset_widget : public property_widget {
public:
	QLineEdit   *name;
	QPushButton *select;

	asset_widget(QWidget *parent, object_data *object_, int row_) noexcept : property_widget(parent, object_, row_)
	{
		name = new QLineEdit(this);
		name->setReadOnly(true);

		select = new QPushButton(this);
		select->setText(tr("set"));

		QHBoxLayout *layout = new QHBoxLayout;
		layout->addWidget(name);
		layout->addWidget(select);
		layout->setContentsMargins(0, 0, 0, 0);
		setLayout(layout);

		connect(select, &QAbstractButton::clicked, [this]() {
			editor::asset *asset = asset_manager::selected_asset();
			if (asset) {
				crpg::type::asset_id id = asset->id();
				set_value(id);
				update_value();
			}
		});

		update_value();
	}

	void update_value() noexcept override
	{
		auto           id    = crpg::variant::get<crpg::type::asset_id>(object->properties[row]);
		editor::asset *asset = asset_manager::get_asset(id);
		QString        val   = asset ? asset->name() : tr("None");

		name->setText(val);
	}
};

/* ========================================================================= */

object_properties_model::object_properties_model(QTableView *view_) noexcept : QAbstractTableModel(), view(view_) {}

int object_properties_model::rowCount(const QModelIndex &) const
{
	return object ? (int)object->asset->properties.size() : 0;
}

int object_properties_model::columnCount(const QModelIndex &) const
{
	return (int)property_column::count;
}

Qt::ItemFlags object_properties_model::flags(const QModelIndex &index) const
{
	Qt::ItemFlags flags = QAbstractTableModel::flags(index);
	if (!object || !index.isValid())
		return flags;
	return flags | Qt::ItemIsEnabled | Qt::ItemNeverHasChildren;
}

QModelIndex object_properties_model::index(int row, int column, const QModelIndex &parent) const
{
	if (!object || !hasIndex(row, column, parent))
		return QModelIndex();
	return createIndex(row, column);
}

QVariant object_properties_model::data(const QModelIndex &index, int role) const
{
	if (!index.isValid() || role != Qt::DisplayRole || index.column() != (int)property_column::name)
		return QVariant();

	return object->asset->properties[index.row()].name;
}

QVariant object_properties_model::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (role != Qt::DisplayRole || orientation != Qt::Orientation::Horizontal)
		return QVariant();

	switch (section) {
	case (int)property_column::name:
		return tr("Name");
	case (int)property_column::value:
		return tr("Value");
	}

	return QVariant();
}

void object_properties_model::set_widget(int row) noexcept
{
	QModelIndex idx = index(row, (int)property_column::value, QModelIndex());

	switch (object->asset->properties[row].type) {
	case crpg::object_property_type::invalid:
		assert(0);
		break;
	case crpg::object_property_type::boolean:
		view->setIndexWidget(idx, new boolean_widget(view, object, row));
		break;
	case crpg::object_property_type::integer:
		view->setIndexWidget(idx, new integer_widget(view, object, row));
		break;
	case crpg::object_property_type::real:
		view->setIndexWidget(idx, new real_widget(view, object, row));
		break;
	case crpg::object_property_type::string:
		view->setIndexWidget(idx, new string_widget(view, object, row));
		break;
	case crpg::object_property_type::enumeration:
		view->setIndexWidget(idx, new enumeration_widget(view, object, row));
		break;
	case crpg::object_property_type::asset:
		view->setIndexWidget(idx, new asset_widget(view, object, row));
		break;
	}

	view->resizeRowToContents(row);
}

void object_properties_model::set_object(map_asset *map_, object_data *object_) noexcept
{
	beginResetModel();
	map    = map_;
	object = object_;
	endResetModel();

	if (object) {
		object_asset *asset = object->asset;
		for (int row = 0; row < (int)asset->properties.size(); row++)
			set_widget(row);
	}

	view->update();
}

/* ========================================================================= */

object_properties_widget *object_properties_widget::current = nullptr;

object_properties_widget::object_properties_widget(QWidget *parent) noexcept : QTableView(parent)
{
	current = this;

	setEditTriggers(QAbstractItemView::NoEditTriggers);
	setModel(new object_properties_model(this));
	horizontalHeader()->setMinimumSectionSize(23);
	horizontalHeader()->setDefaultSectionSize(23);
	horizontalHeader()->setSectionResizeMode((int)property_column::name, QHeaderView::ResizeMode::ResizeToContents);
	horizontalHeader()->setSectionResizeMode((int)property_column::value, QHeaderView::ResizeMode::Stretch);
	horizontalHeader()->setSectionResizeMode((int)property_column::reset, QHeaderView::ResizeMode::Fixed);
}

} // namespace editor
