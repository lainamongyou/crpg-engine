#pragma once

#include <QPointer>
#include <QWidget>

#include <engine/map-tools.hpp>

#include "asset-map.hpp"
#include "tileset-view.hpp"

#include <memory>
#include <vector>

namespace crpg {
struct segment;
}

namespace editor {

struct renderable_map_layer {};

struct renderable_sprite {
	crpg::math::vec2                  pos;
	crpg::graphics::texture_ref       tex;
	crpg::graphics::vertex_buffer_ref vb;
};

struct renderable_sprite_layer : renderable_map_layer {
	std::map<crpg::type::sprite_id, renderable_sprite> sprites;
};

struct renderable_tileset_layer : renderable_map_layer {
	std::map<crpg::type::segment_id, crpg::tileset_segment> segments;
};

class document_map : public main_view {
	Q_OBJECT

	friend class map_view;
	friend class asset_map;
	friend struct sprite_layer;
	friend struct object_layer;
	friend struct tileset_layer;
	friend class map_layers_model;
	friend class map_layers_widget;

	enum class mode {
		insert,
		select,
		erase,
	};

	enum class manipulator_mode {
		none,
		x,
		y,
		x_and_y,
	};

	map_asset    *asset;
	document_map *doc;

	std::map<crpg::type::layer_id, std::unique_ptr<renderable_map_layer>> renderable_layers;

	crpg::graphics::texture_ref layer_tex;
	crpg::type::layer_id        cur_layer_id    = 0;
	bool                        mouse_hovering  = false;
	bool                        mouse_down      = false;
	bool                        mouse_moved     = false;
	bool                        show_grid       = true;
	mode                        cur_mode        = mode::insert;
	crpg::math::vec2            last_insert_pos = crpg::math::vec2(crpg::math::infinite, crpg::math::infinite);

	/* upper-right viewport text for the current mode */
	crpg::graphics::texture_ref tex_insert;
	crpg::graphics::texture_ref tex_select;
	crpg::graphics::texture_ref tex_erase;

	/* movement manipulator arm vertex buffer */
	crpg::graphics::vertex_buffer_ref vb_manipulator_arm;
	crpg::graphics::vertex_buffer_ref vb_manipulator_end;
	crpg::graphics::vertex_buffer_ref vb_manipulator_inside;

	/* point object vertex buffer */
	crpg::graphics::vertex_buffer_ref vb_point_object_solid;
	crpg::graphics::vertex_buffer_ref vb_point_object_lines;

	/* point object vertex buffer */
	crpg::graphics::vertex_buffer_ref vb_link_arrow;
	crpg::graphics::vertex_buffer_ref vb_link_line;

	/* undo/redo stuff */
	std::map<crpg::type::segment_id, bool>         stored_segments;
	std::vector<crpg::type::segment_id>            created_segments;
	std::vector<crpg::segment>                     modified_tileset_segments;
	std::map<crpg::type::tileset_id, tileset_data> modified_tilesets;
	std::vector<crpg::type::sprite_id>             added_sprites;
	std::vector<sprite_data>                       removed_sprites;
	std::vector<crpg::type::object_id>             added_objects;
	std::vector<object_data>                       removed_objects;

	crpg::math::vec2      inserted_pos;
	crpg::math::vec2      manipulator_offset;
	QString               selected_object_type;
	crpg::type::object_id selected_object = 0;
	crpg::type::object_id inserted_object = 0;
	manipulator_mode      manipulator     = manipulator_mode::none;

#pragma message("--------------------------------------------------------------------")
#pragma message("remember to do something here, use insert_pos + inserted_pos to calc")
#pragma message("--------------------------------------------------------------------")
	crpg::math::vec2 insert_tl;
	crpg::math::vec2 insert_br;

	crpg::graphics::vertex_buffer_ref selection_vb;
	crpg::graphics::texture_ref       selection_tex;
	tileset_asset                    *last_tileset_asset = nullptr;
	tileset_selection                 last_tileset_selection;

	object_asset *last_object_asset = nullptr;
	sprite_asset *last_sprite_asset = nullptr;

	crpg::type::sprite_id get_hover_sprite() noexcept;
	crpg::type::object_id get_hover_object() noexcept;

	void on_link_objects(object_layer *layer);

	void set_selected_object(crpg::type::object_id id) noexcept;
	void update_status_bar() noexcept;

	void mousePressEvent(QMouseEvent *event) override;
	void mouseReleaseEvent(QMouseEvent *event) override;
	void mouseMoveEvent(QMouseEvent *event) override;
	void enterEvent(QEnterEvent *event) override;
	void leaveEvent(QEvent *event) override;

	inline map_layer           *get_cur_layer() const noexcept;
	inline crpg::map_layer_type cur_layer_type() const noexcept;

	void save_tileset_edit_undo_redo() noexcept;
	void save_sprite_edit_undo_redo() noexcept;
	void save_object_edit_undo_redo() noexcept;
	void save_edit_undo_redo() noexcept;
	void save_object_move_undo_redo() noexcept;
	void insert_tile(tileset_layer                        *layer,
	                 renderable_tileset_layer             *rlayer,
	                 std::vector<crpg::segment *>         &segs,
	                 std::vector<crpg::tileset_segment *> &rsegs,
	                 const crpg::math::vec2               &pos,
	                 tileset_data                         *tileset,
	                 uint8_t                               x,
	                 uint8_t                               y) noexcept;

	void set_active_layer() noexcept;

	void on_insert_tileset_selection() noexcept;
	void on_insert_sprite() noexcept;
	void on_insert_object() noexcept;
	void on_insert() noexcept;
	void on_select() noexcept;
	bool on_manipulator_clicked() noexcept;
	void on_move_object() noexcept;
	void on_resize_inserted_object(const crpg::math::vec2 &insert_pos) noexcept;
	void on_escape_pressed() noexcept;
	void on_delete_pressed() noexcept;

	manipulator_mode get_manipulator_mode(crpg::math::vec2 obj_window_pos) noexcept;

	void add_sprite(sprite_layer *layer, renderable_sprite_layer *rlayer, sprite_data &sprite) noexcept;
	void remove_sprite(sprite_layer            *layer,
	                   renderable_sprite_layer *rlayer,
	                   crpg::type::sprite_id    sprite_id) noexcept;

	void add_object(object_layer *layer, renderable_sprite_layer *rlayer, object_data &object) noexcept;
	void remove_object(object_layer            *layer,
	                   renderable_sprite_layer *rlayer,
	                   crpg::type::object_id    object_id) noexcept;

	inline void update_layer_tex() noexcept;
	void        build_tileset_selection_vb() noexcept;
	void        render_layer(map_layer *layer) noexcept;
	void        render_tileset_layer(tileset_layer *layer, renderable_tileset_layer *rlayer) noexcept;
	void        render_sprite_layer(sprite_layer *layer, renderable_sprite_layer *rlayer) noexcept;
	void        render_object_layer_sprites(object_layer *layer, renderable_sprite_layer *rlayer) noexcept;
	void        render_object_layer_objects(object_layer *layer, renderable_sprite_layer *rlayer) noexcept;

	void render_manipulator(crpg::math::vec2 pos) noexcept;

	void build_object(renderable_sprite &rsprite, object_data &object) noexcept;

	void build_tileset_segment(tileset_layer         *layer,
	                           const crpg::segment   &seg,
	                           crpg::tileset_segment &rseg) noexcept;
	void build_tileset_layer(tileset_layer *layer, renderable_tileset_layer *rlayer) noexcept;
	void build_sprite_layer(sprite_layer *layer, renderable_sprite_layer *rlayer) noexcept;
	void build_object_layer(object_layer *layer, renderable_sprite_layer *rlayer) noexcept;
	void build_renderable_layer(map_layer *layer, renderable_map_layer *rlayer) noexcept;

	/* used as temporary array for rendering */
	std::vector<crpg::type::sprite_id> cur_sprites;
	std::vector<crpg::type::object_id> cur_objects;
	std::vector<std::pair<crpg::math::vec2, crpg::math::vec2>> cur_links;

private slots:
	void reset_data() noexcept;

public:
	inline document_map(QWidget *parent, map_asset *asset) noexcept : main_view(parent), asset(asset) {}
	~document_map();

	void init() noexcept override;
	void render() noexcept override;

	void add_renderable_layer(map_layer *layer) noexcept;
};

} // namespace editor
