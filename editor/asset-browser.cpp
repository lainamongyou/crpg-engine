#include "asset-browser.hpp"
#include "asset-tileset.hpp"
#include "asset-sprite.hpp"
#include "asset-object.hpp"
#include "asset-map.hpp"
#include "main-window.hpp"
#include "game-config.h"
#include "platform.hpp"
#include "app.hpp"

#include <engine/utility/stringserializer.hpp>
#include <engine/utility/file-helpers.hpp>
#include <engine/utility/logging.hpp>
#include <engine/utility/config.hpp>

#include <DockWidget.h>

#include <QMenu>
#include <QDir>

using namespace crpg;

#define BASE_PATH WORK_DIR "/project"

namespace editor {

/* ========================================================================= */

asset_model::asset_model(QObject *parent) noexcept : QAbstractItemModel(parent)
{
	icon_category_maps     = QIcon(":/toolbar/icons/tile.svg");
	icon_category_objects  = icon_category_maps;
	icon_category_sprites  = icon_category_maps;
	icon_category_tilesets = icon_category_maps;
	icon_category_sounds   = icon_category_maps;
	icon_category_music    = icon_category_maps;
}

void asset_model::init() noexcept
{
	root.reset(asset::load_asset(BASE_PATH, nullptr));
	if (!root)
		create_new_project();
}

void asset_model::create_new_project() noexcept
{
	QDir().mkdir(BASE_PATH);

	root.reset(new asset("root", BASE_PATH, QIcon(), nullptr, asset_type::root, asset_category::root));

	QStyle *style = app::get()->style();
	icon_folder   = style->standardIcon(QStyle::SP_DirIcon);
	icon_file     = style->standardIcon(QStyle::SP_FileIcon);

	root->children.emplace_back(new asset(
	        tr("Maps"), "maps", icon_category_maps, root.get(), asset_type::category, asset_category::map));
	root->children.emplace_back(new asset(tr("Objects"),
	                                      "objects",
	                                      icon_category_objects,
	                                      root.get(),
	                                      asset_type::category,
	                                      asset_category::object));
	root->children.emplace_back(new asset(tr("Sprites"),
	                                      "sprites",
	                                      icon_category_sprites,
	                                      root.get(),
	                                      asset_type::category,
	                                      asset_category::sprite));
	root->children.emplace_back(new asset(tr("Tilesets"),
	                                      "tilesets",
	                                      icon_category_tilesets,
	                                      root.get(),
	                                      asset_type::category,
	                                      asset_category::tileset));
	root->children.emplace_back(new asset(
	        tr("Sounds"), "sounds", icon_category_sounds, root.get(), asset_type::category, asset_category::sound));
	root->children.emplace_back(new asset(
	        tr("Music"), "music", icon_category_music, root.get(), asset_type::category, asset_category::music));

	auto mkdir_for = [](const char *dir) {
		QString loc = BASE_PATH;
		loc += "/";
		loc += dir;
		QDir().mkdir(loc);
		return loc;
	};

	mkdir_for("maps");
	mkdir_for("objects");
	mkdir_for("sprites");
	mkdir_for("tilesets");
	mkdir_for("sounds");
	mkdir_for("music");
	asset::set_base_counter();
}

QIcon asset_model::get_icon_internal(asset_type type, asset_category category) noexcept
{
	if (type == asset_type::category) {
		switch (category) {
		case asset_category::map:
			return icon_category_maps;
		case asset_category::object:
			return icon_category_objects;
		case asset_category::sprite:
			return icon_category_sprites;
		case asset_category::tileset:
			return icon_category_tilesets;
		case asset_category::sound:
			return icon_category_sounds;
		case asset_category::music:
			return icon_category_music;
		}
	} else if (type == asset_type::folder) {
		return icon_folder;
	} else if (type == asset_type::file) {
		return icon_file;
	}
	return QIcon();
}

QVariant asset_model::data(const QModelIndex &index, int role) const
{
	if (!index.isValid())
		return QVariant();

	asset *item = static_cast<asset *>(index.internalPointer());
	return item->data(role);
}

bool asset_model::setData(const QModelIndex &index, const QVariant &val, int role)
{
	if (!index.isValid() || role != Qt::EditRole)
		return false;

	asset  *item     = static_cast<asset *>(index.internalPointer());
	QString new_name = val.toString().simplified();

	if (new_name.isEmpty())
		return false;
	if (new_name.compare(item->name_, Qt::CaseInsensitive) == 0)
		return false;
	if (item->parent->has_child(new_name))
		return false;

	QList<int> roles;
	roles.push_back(role);

	item->name_ = new_name;
	emit dataChanged(index, index, roles);

	int old_row = item->parent->get_child_idx(item);
	int new_row = item->parent->get_sorted_child_idx(new_name, item->type == asset_type::folder);

	if (new_row != old_row && new_row != (old_row + 1)) {
		QModelIndex parent = index.parent();
		beginMoveRows(parent, old_row, old_row, parent, new_row);

		std::unique_ptr<asset> ptr = std::move(item->parent->children[old_row]);
		item->parent->children.insert(item->parent->children.begin() + new_row, std::move(ptr));
		if (old_row >= new_row)
			old_row++;
		item->parent->children.erase(item->parent->children.begin() + old_row);

		endMoveRows();
	}

	if (item->dock)
		item->dock->setWindowTitle(item->name_);

	return true;
}

Qt::ItemFlags asset_model::flags(const QModelIndex &index) const
{
	if (!index.isValid())
		return Qt::NoItemFlags;

	asset *item = static_cast<asset *>(index.internalPointer());

	switch (item->type) {
	case asset_type::root:
		return Qt::NoItemFlags;
	case asset_type::category:
		return Qt::ItemIsDropEnabled | Qt::ItemIsEnabled | Qt::ItemIsSelectable;
	case asset_type::folder:
		return Qt::ItemIsDropEnabled | Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsDragEnabled |
		       Qt::ItemIsEditable;
	case asset_type::file:
		return Qt::ItemIsDropEnabled | Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsDragEnabled |
		       Qt::ItemIsEditable | Qt::ItemNeverHasChildren;
	}

	return Qt::NoItemFlags;
}

Qt::DropActions asset_model::supportedDropActions() const
{
	return QAbstractItemModel::supportedDropActions() | Qt::MoveAction;
}

QModelIndex asset_model::index(int row, int column, const QModelIndex &parent) const
{
	if (!hasIndex(row, column, parent))
		return QModelIndex();

	asset *item = parent.isValid() ? static_cast<asset *>(parent.internalPointer()) : root.get();
	if (row >= item->children.size())
		return QModelIndex();

	return createIndex(row, column, item->children[row].get());
}

QModelIndex asset_model::parent(const QModelIndex &index) const
{
	if (!index.isValid())
		return QModelIndex();

	asset *child  = static_cast<asset *>(index.internalPointer());
	asset *parent = child->parent;

	if (parent == root.get())
		return QModelIndex();

	return createIndex(parent->parent->get_child_idx(parent), 0, parent);
}

int asset_model::rowCount(const QModelIndex &parent) const
{
	if (parent.column() > 0)
		return 0;

	asset *item;
	item = parent.isValid() ? static_cast<asset *>(parent.internalPointer()) : root.get();
	return (int)item->children.size();
}

int asset_model::columnCount(const QModelIndex &) const
{
	return 1;
}

QString asset_model::get_first_available_name(asset *item, const QString &name) noexcept
{
	QString base_name = name;
	QString new_name  = base_name;
	int     inc       = 2;
	while (item->has_child(new_name))
		new_name = base_name + " " + QString::number(inc++);
	return new_name;
}

QModelIndex asset_model::create(const QModelIndex   &parent_idx,
                                crpg::type::asset_id id,
                                const QString       &name,
                                bool                 folder) noexcept
{
	asset *item  = static_cast<asset *>(parent_idx.internalPointer());
	asset *child = nullptr;

	if (folder) {
		child = new asset(name, QString(), icon_folder, item, asset_type::folder, item->category, id);
	} else if (item->category == asset_category::map) {
		child = new map_asset(name, QString(), icon_file, item, asset_type::file, item->category, id);
	} else if (item->category == asset_category::sprite) {
		child = new sprite_asset(name, QString(), icon_file, item, asset_type::file, item->category, id);
	} else if (item->category == asset_category::tileset) {
		child = new tileset_asset(name, QString(), icon_file, item, asset_type::file, item->category, id);
	} else if (item->category == asset_category::object) {
		child = new object_asset(name, QString(), icon_file, item, asset_type::file, item->category, id);
	}

	int new_idx = item->get_sorted_child_idx(name, folder);
	beginInsertRows(parent_idx, new_idx, new_idx);
	item->children.emplace(item->children.begin() + new_idx, child);
	endInsertRows();

	return createIndex(new_idx, 0, child);
}

void asset_model::add_new(const QString &default_name, bool folder) noexcept
{
	asset_browser_widget *widget = asset_browser_widget::get();

	QModelIndex index = widget->currentIndex();
	asset      *item  = static_cast<asset *>(index.internalPointer());

	if (item->type == asset_type::file) {
		index = index.parent();
		item  = item->parent;
	}

	QString     new_name  = get_first_available_name(item, default_name);
	QModelIndex child_idx = create(index, 0, new_name, folder);
	QMetaObject::invokeMethod(widget, "setCurrentIndex", Q_ARG(QModelIndex, child_idx));
	QMetaObject::invokeMethod(widget, "edit", Q_ARG(QModelIndex, child_idx));
}

void asset_model::rename_item() noexcept
{
	asset_browser_widget *widget = asset_browser_widget::get();
	QModelIndex           index  = widget->currentIndex();
	widget->edit(index, QAbstractItemView::EditKeyPressed, nullptr);
}

void asset_model::remove_item() noexcept
{
	asset_browser_widget *widget = asset_browser_widget::get();
	QModelIndex           index  = widget->currentIndex();
	on_remove(index);
}

void asset_model::move_asset(const QModelIndex &src_parent_idx,
                             int                src_row,
                             const QModelIndex &dst_parent_idx,
                             int                dst_row) noexcept
{
	auto *src_parent = static_cast<asset *>(src_parent_idx.internalPointer());
	auto *dst_parent = static_cast<asset *>(dst_parent_idx.internalPointer());

	auto item = std::move(src_parent->children[src_row]);
	item->load_recursively();

	beginMoveRows(src_parent_idx, src_row, src_row, dst_parent_idx, dst_row);

	if (src_parent == dst_parent && src_row < dst_row)
		dst_row--;

	item->parent = dst_parent;
	src_parent->children.erase(src_parent->children.begin() + src_row);
	dst_parent->children.insert(dst_parent->children.begin() + dst_row, std::move(item));

	endMoveRows();
}

void asset_model::remove_id(type::asset_id id) noexcept
{
	asset *item = items[id];
	if (!item) {
		error("%s: %s", __FUNCTION__, "huh, this should definitely never happen");
		return;
	}

	if (item->dock)
		item->dock->closeDockWidget();

	item->remove_xrefs();

	asset *parent = item->parent;

	QModelIndex idx = item->idx();
	beginRemoveRows(idx.parent(), idx.row(), idx.row());
	parent->children.erase(parent->children.begin() + idx.row());
	endRemoveRows();

	items.erase(id);
}

void asset_model::rename_id(type::asset_id id, const QString &name) noexcept
{
	asset *item = items[id];
	if (!item) {
		error("%s: %s", __FUNCTION__, "huh, this should definitely never happen");
		return;
	}

	QModelIndex idx = item->idx();
	setData(idx, name, Qt::EditRole);
}

void asset_model::add_folder() noexcept
{
	add_new(tr("Folder"), true);
}

void asset_model::add_tileset() noexcept
{
	add_new(tr("Tileset"), false);
}

void asset_model::add_sprite() noexcept
{
	add_new(tr("Sprite"), false);
}

void asset_model::add_object() noexcept
{
	add_new("object", false);
}

void asset_model::add_map() noexcept
{
	add_new(tr("Map"), false);
}

void asset_model::add_sound() noexcept
{
	add_new(tr("Sound"), false);
}

void asset_model::add_music() noexcept
{
	add_new(tr("Music"), false);
}

void asset_model::on_create(const QModelIndex &index) noexcept
{
	asset *item     = static_cast<asset *>(index.internalPointer());
	item->file_name = get_safe_filename(item->parent->path(), item->name_);
	item->select();
	item->open();

	auto undo = [this](crpg::serializer &s) {
		type::asset_id id;
		s >> id;
		remove_id(id);
	};
	auto save_undo = [&](crpg::serializer &s) { s << item->id(); };

	auto redo = [this](crpg::serializer &s) {
		QModelIndex    parent_idx;
		type::asset_id parent_id;
		type::asset_id id;
		QString        name;
		bool           folder;

		s >> parent_id >> id;

		asset *parent = items[parent_id];
		parent_idx    = parent->idx();

		s >> name >> folder;
		QModelIndex child_idx = create(parent_idx, id, name, folder);

		asset *child = static_cast<asset *>(child_idx.internalPointer());
		child->load_state(s);
		child->select();
		child->open();
	};
	auto save_redo = [&](crpg::serializer &s) {
		s << item->parent->id_ << item->id_ << item->name_ << (item->type == asset_type::folder);
		item->save_state(s);
	};

	main_window::add_undo_action(tr("add asset '%1'").arg(item->name()), undo, redo, save_undo, save_redo);
}

void asset_model::on_rename(const QModelIndex &index, const QString &prev_name) noexcept
{
	asset *item = static_cast<asset *>(index.internalPointer());

	auto undo_redo = [this](crpg::serializer &s) {
		QString        name;
		type::asset_id id;
		s >> id;
		s >> name;

		asset *item = items[id];
		s >> item->file_name;
		s >> item->rename_idx;

		rename_id(id, name);
	};

	auto save_undo = [&](crpg::serializer &s) {
		s << item->id();
		s << prev_name;
		s << item->file_name;
		s << item->rename_idx;
	};
	auto save_redo = [&](crpg::serializer &s) {
		item->load_if_not_loaded();
		item->file_name = get_safe_filename(item->parent->path(), item->name_, item->file_name);

		s << item->id();
		s << item->name();
		s << item->file_name;
		s << ++item->rename_idx;
	};

	QString undo_name = (item->type == asset_type::folder) ? tr("rename asset folder '%1' to '%2'")
	                                                       : tr("rename asset '%1' to '%2'");
	main_window::add_undo_action(
	        undo_name.arg(prev_name, item->name()), undo_redo, undo_redo, save_undo, save_redo);
}

crpg::serializer &operator<<(crpg::serializer &s, const asset_model::asset_save_data &asd) noexcept
{
	return s << asd.id << asd.parent_id << asd.name << asd.folder << asd.data;
}

crpg::serializer &operator>>(crpg::serializer &s, asset_model::asset_save_data &asd) noexcept
{
	return s >> asd.id >> asd.parent_id >> asd.name >> asd.folder >> asd.data;
}

void asset_model::get_assets_recursive(asset *item, std::vector<asset_save_data> &assets) noexcept
{
	bool folder = item->is_type(asset_type::folder);

	{
		asset_save_data asd;
		asd.id        = item->id_;
		asd.parent_id = item->parent->id_;
		asd.name      = item->name_;
		asd.folder    = folder;

		crpg::ostringserializer s(asd.data);
		item->load_if_not_loaded();
		item->save_state(s);

		assets.emplace_back(std::move(asd));
	}

	if (folder) {
		for (auto &child : item->children) {
			get_assets_recursive(child.get(), assets);
		}
	}
}

void asset_model::on_remove(const QModelIndex &index) noexcept
{
	asset *item = static_cast<asset *>(index.internalPointer());

	/* ------------------------------ */
	/* get assets to delete           */

	std::vector<asset_save_data> assets;
	get_assets_recursive(item, assets);

	/* ------------------------------ */
	/* get xrefs                      */

	std::vector<asset_save_data> xrefs;
	for (auto &asd : assets) {
		asset *cur_item = items[asd.id];

		for (auto &[id, _] : cur_item->xrefs_) {
			auto it = std::find_if(assets.begin(), assets.end(), [&id](const asset_save_data &asd) {
				return asd.id == id;
			});
			if (it != assets.end())
				continue;

			it = std::find_if(
			        xrefs.begin(), xrefs.end(), [&id](const asset_save_data &asd) { return asd.id == id; });
			if (it != xrefs.end())
				continue;

			asset *xref = items[id];

			asset_save_data new_asd;
			new_asd.id        = xref->id();
			new_asd.parent_id = xref->parent->id();
			new_asd.name      = xref->name_;
			new_asd.folder    = false;

			crpg::ostringserializer s(new_asd.data);
			xref->load_if_not_loaded();
			xref->save_state(s);

			xrefs.emplace_back(std::move(new_asd));
		}
	}

	/* ------------------------------ */
	/* undo data                      */

	auto undo = [this](crpg::serializer &s) {
		std::vector<asset_save_data> assets;
		std::vector<asset_save_data> xrefs;

		s >> assets >> xrefs;

		asset::asset_loading_refs++;

		for (auto &asd : assets) {
			asset *parent = items[asd.parent_id];
			create(parent->idx(), asd.id, asd.name, asd.folder);
		}
		for (auto &asd : assets) {
			asset *item = items[asd.id];

			crpg::istringserializer s2(asd.data);
			item->load_state(s2);
			item->loaded = true;
		}
		for (auto &asd : xrefs) {
			asset *xref = items[asd.id];

			crpg::istringserializer s2(asd.data);
			xref->load_state(s2);

			if (xref->is_open()) {
				auto *doc = xref->document();
				QMetaObject::invokeMethod(doc, "reset_data");
			}
		}

		asset::asset_loading_refs--;
	};
	auto save_undo = [&](crpg::serializer &s) { s << assets << xrefs; };

	auto redo = [this](crpg::serializer &s) {
		crpg::type::asset_id id;
		s >> id;
		remove_id(id);
	};
	auto save_redo = [&](crpg::serializer &s) {
		s << item->id_;
		remove_id(item->id_);
	};

	main_window::add_undo_action(tr("remove asset '%1'").arg(item->name()), undo, redo, save_undo, save_redo);
}

/* ========================================================================= */

asset_manager *asset_manager::current = nullptr;

asset_manager::asset_manager(QObject *parent_) noexcept : asset_model(parent_)
{
	current = this;
	init();
}

asset_manager::~asset_manager() noexcept
{
	current = nullptr;
}

asset *asset_manager::selected_asset_internal() const noexcept
{
	auto       *widget = asset_browser_widget::get();
	QModelIndex idx    = widget->currentIndex();
	if (!idx.isValid())
		return nullptr;

	return static_cast<asset *>(idx.internalPointer());
}

void asset_manager::save() noexcept
{
	get()->root->save(BASE_PATH);
}

/* ========================================================================= */

asset_browser_widget *asset_browser_widget::current = nullptr;

asset_browser_widget::asset_browser_widget(QWidget *parent_) noexcept : QTreeView(parent_)
{
	current = this;

	setHeaderHidden(true);
	setDragEnabled(true);
	setDropIndicatorShown(true);
	setDragDropMode(QAbstractItemView::InternalMove);
	setEditTriggers(QAbstractItemView::EditKeyPressed);
	setContextMenuPolicy(Qt::CustomContextMenu);
	setModel(new asset_manager(this));

	auto right_click = [this]() {
		QModelIndex index = currentIndex();
		asset      *item  = static_cast<asset *>(index.internalPointer());

		if (!item)
			return;

		QMenu menu;
		menu.addAction(tr("Add Folder"), asset_manager::get(), SLOT(add_folder()));

		switch (item->category) {
		case asset_category::map:
			menu.addAction(tr("Add Map"), asset_manager::get(), SLOT(add_map()));
			break;
		case asset_category::sprite:
			menu.addAction(tr("Add Sprite"), asset_manager::get(), SLOT(add_sprite()));
			break;
		case asset_category::tileset:
			menu.addAction(tr("Add Tileset"), asset_manager::get(), SLOT(add_tileset()));
			break;
		case asset_category::object:
			menu.addAction(tr("Add Object"), asset_manager::get(), SLOT(add_object()));
			break;
		case asset_category::sound:
			menu.addAction(tr("Add Sound"), asset_manager::get(), SLOT(add_sound()));
			break;
		case asset_category::music:
			menu.addAction(tr("Add Music"), asset_manager::get(), SLOT(add_music()));
			break;
		}

		if (item->type >= asset_type::folder) {
			menu.addSeparator();
			menu.addAction(tr("Rename"), asset_manager::get(), SLOT(rename_item()));
			menu.addSeparator();
			menu.addAction(tr("Remove"), asset_manager::get(), SLOT(remove_item()));
		}

		menu.exec(QCursor::pos());
	};

	connect(this, &QWidget::customContextMenuRequested, right_click);

	auto double_click = [this](const QModelIndex &index) {
		asset *item = static_cast<asset *>(index.internalPointer());
		if (item->type != asset_type::file)
			return;

		item->open();
	};

	connect(this, &QAbstractItemView::doubleClicked, double_click);

	auto click = [this](const QModelIndex &index) {
		asset *item = static_cast<asset *>(index.internalPointer());
		if (item->type != asset_type::file)
			return;

		item->select();
	};

	connect(this, &QAbstractItemView::clicked, click);
}

asset_browser_widget::~asset_browser_widget() noexcept
{
	current = nullptr;
}

bool asset_browser_widget::edit(const QModelIndex &index, QAbstractItemView::EditTrigger trigger, QEvent *event)
{
	if (!QTreeView::edit(index, trigger, event)) {
		return false;
	}

	asset *item = static_cast<asset *>(index.internalPointer());

	if (!event && trigger != QAbstractItemView::EditKeyPressed) {
		/* edit invoked manually, item created */
		action_after_edit = [=]() { asset_manager::get()->on_create(index); };
	} else {
		/* standard rename event */
		QString prev_name = item->name();
		action_after_edit = [=]() { asset_manager::get()->on_rename(index, prev_name); };
	}

	return true;
}

void asset_browser_widget::closeEditor(QWidget *editor, QAbstractItemDelegate::EndEditHint hint)
{
	QTreeView::closeEditor(editor, hint);
	if (action_after_edit) {
		action_after_edit();
		action_after_edit = std::function<void()>();
	}
}

void asset_browser_widget::dropEvent(QDropEvent *event)
{
	if (event->source() != this) {
		QTreeView::dropEvent(event);
		return;
	}

	QModelIndexList indices = selectedIndexes();
	if (!indices.size()) {
		QTreeView::dropEvent(event);
		return;
	}

	auto *m = asset_manager::get();

	DropIndicatorPosition indicator = dropIndicatorPosition();
	QModelIndex           drag_idx  = indices[0];
	QModelIndex           drop_idx  = indexAt(event->position().toPoint());
	asset                *drag_item = static_cast<asset *>(drag_idx.internalPointer());

	/* must drop on another item */
	if (!drop_idx.isValid()) {
		QTreeView::dropEvent(event);
		return;
	}

	/* must drop on same category */
	asset *drop_item = static_cast<asset *>(drop_idx.internalPointer());
	if (drag_item->category != drop_item->category) {
		QTreeView::dropEvent(event);
		return;
	}

	QModelIndex drag_parent_idx = drag_idx.parent();
	QModelIndex drop_parent_idx;

	int drop_row = drop_idx.row();
	if (drop_item->type <= asset_type::folder && indicator != QAbstractItemView::AboveItem) {
		drop_parent_idx = drop_idx;
		drop_row        = (int)drop_item->children.size();
	} else {
		drop_parent_idx = drop_idx.parent();

		if (drag_parent_idx == drop_parent_idx &&
		    (indicator == QAbstractItemView::BelowItem || indicator == QAbstractItemView::OnItem)) {
			drop_row++;
		}
	}

	if (drag_parent_idx == drop_parent_idx && (drop_row == drag_idx.row() || drop_row == drag_idx.row() + 1)) {
		QTreeView::dropEvent(event);
		return;
	}

	/* ------------------------------------------------ */

	m->move_asset(drag_parent_idx, drag_idx.row(), drop_parent_idx, drop_row);

	/* ------------------------------------------------ */

	auto *drag_parent = static_cast<asset *>(drag_parent_idx.internalPointer());
	auto *drop_parent = static_cast<asset *>(drop_parent_idx.internalPointer());

	auto undo_redo = [](crpg::serializer &s) {
		type::asset_id src_parent_id;
		type::asset_id dst_parent_id;
		int            src_row;
		int            dst_row;

		s >> src_parent_id >> dst_parent_id >> src_row >> dst_row;

		auto  *am         = asset_manager::get();
		asset *src_parent = am->items[src_parent_id];
		asset *dst_parent = am->items[dst_parent_id];

		QModelIndex src_parent_idx = src_parent->idx();
		QModelIndex dst_parent_idx = dst_parent->idx();

		am->move_asset(src_parent_idx, src_row, dst_parent_idx, dst_row);
	};
	auto save_undo = [&](crpg::serializer &s) {
		s << drop_parent->id_ << drag_parent->id_;

		if (drag_parent == drop_parent) {
			if (drag_idx.row() < drop_row)
				s << (drop_row - 1) << drag_idx.row();
			else
				s << drop_row << (drag_idx.row() + 1);
		} else {
			s << drop_row << drag_idx.row();
		}
	};
	auto save_redo = [&](crpg::serializer &s) {
		s << drag_parent->id_ << drop_parent->id_ << drag_idx.row() << drop_row;
	};

	QString undo_name = tr("move asset '%1'");
	main_window::add_undo_action(undo_name.arg(drag_item->name()), undo_redo, undo_redo, save_undo, save_redo);

	/* ------------------------------------------------ */

	event->accept();
	event->setDropAction(Qt::MoveAction);
	QTreeView::dropEvent(event);
}

} // namespace editor
