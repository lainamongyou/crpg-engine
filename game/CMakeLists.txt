project(game)

if(BUILD_STATIC)
	set(ACTUAL_GAME_LIBRARY ${GAME_LIBRARY})
	set(ACTUAL_SDL2 SDL2::SDL2-static)
else()
	set(ACTUAL_SDL2 SDL2::SDL2)
endif()

add_executable(game WIN32)
target_sources(game PRIVATE
	game-main.cpp
	sdl-helpers.hpp
)
target_link_libraries(game
	${ACTUAL_SDL2}
	SDL2::SDL2main
	engine
	${ACTUAL_GAME_LIBRARY}
)
crpg_autogroup_all()

set(GAME_BINARY_NAME "" CACHE STRING "Game binary name. If blank, will just be 'game'")
if(DEFINED GAME_BINARY_NAME AND NOT GAME_BINARY_NAME STREQUAL "")
	set_target_properties(game
		PROPERTIES
			OUTPUT_NAME "${GAME_BINARY_NAME}"
	)
endif()

install_bin_main(game)
