#include "sdl-helpers.hpp"
#include "game-config.h"

#define NOT_GAME_MODULE
#include <engine/app.hpp>
#include <engine/font.hpp>
#include <engine/widget.hpp>
#include <engine/resources.hpp>
#include <engine/graphics/graphics.hpp>
#include <engine/utility/logging.hpp>
#include <engine/utility/time.hpp>

#include <condition_variable>
#include <memory>
#include <thread>
#include <vector>
#include <mutex>

#include <SDL_syswm.h>
#include <SDL_main.h>

using namespace std;

namespace crpg {

#ifdef STATIC_ONLY
extern crpg::game *create_app(void);
#endif

class os_event {
	mutex              m;
	condition_variable cv;
	bool               signaled = false;

public:
	inline void signal()
	{
		{
			lock_guard<mutex> lock(m);
			signaled = true;
		}
		cv.notify_all();
	}

	inline void wait()
	{
		unique_lock<mutex> lock(m);
		if (!signaled)
			cv.wait(lock);
		signaled = false;
	}
};

struct ui_manager {
	sdl::window   window;
	thread        game_thread;
	os_event      game_event;
	os_event      ui_event;
	SDL_SysWMinfo wmi;
	bool          game_loop_error = false;
	uint32_t      real_quit_event;

	mutex             sdl_events_mutex;
	vector<SDL_Event> sdl_events;

	void ui_loop();

	inline ~ui_manager()
	{
		if (game_thread.joinable())
			game_thread.join();
	}
};

struct game_manager {
	ui_manager &ui;

	graphics::renderer_ref renderer;
	graphics::display_ref  display;
	unique_ptr<crpg::app>  game;
	std::string            game_name;

	int     cx = 640;
	int     cy = 480;
	int64_t last_ts;
	int64_t cur_time;

	/* run everything internally at 60fps */
	static constexpr long double sec_to_nsec_ld = 1000000000.0l;
	static constexpr int64_t     target_fps_num = 60LL;
	static constexpr int64_t     target_fps_den = 1LL;
	static constexpr long double target_fps_ld  = (long double)target_fps_num / (long double)target_fps_den;
	static constexpr float       frame_time_sec = (float)(1.0l / target_fps_ld);
	static constexpr int64_t     sec_to_nsec    = 1000000000LL;
	static constexpr int64_t     frame_time_ns  = sec_to_nsec * target_fps_den / target_fps_num;

	graphics::texture_ref center_target;
	graphics::shader_ref  center_vs;
	graphics::shader_ref  center_ps;
	int                   center_x    = 0;
	int                   center_y    = 0;
	int                   relative_cx = 0;
	int                   relative_cy = 0;

	void recalc_cursor_pos(int &x, int &y);
	void process_event(SDL_Event &event);
	void window_event(SDL_WindowEvent &event);

	void        setup_stretched_render_view();
	void        setup_centered_render_view();
	inline void setup_render_view();

	void game_loop();
	void render_frame();

	inline game_manager(ui_manager &ui_) : ui(ui_) {}
};

void game_manager::recalc_cursor_pos(int &x, int &y)
{
	if (game->center_view) {
		x = (x - center_x) * relative_cx / cx;
		y = (y - center_y) * relative_cy / cy;
	}
}

void game_manager::window_event(SDL_WindowEvent &event)
{
	if (event.event == SDL_WINDOWEVENT_RESIZED) {
		cx = (int)event.data1;
		cy = (int)event.data2;
		display->resize((uint32_t)cx, (uint32_t)cy);
	}
}

void game_manager::process_event(SDL_Event &event)
{
	switch (event.type) {
	case SDL_WINDOWEVENT:
		window_event(event.window);
		break;
	case SDL_QUIT:
		game->quit_event();
		break;
	case SDL_KEYUP:
	case SDL_KEYDOWN:
		widget::sdl_key_event(event.key);
		break;
	case SDL_MOUSEMOTION:
		recalc_cursor_pos(event.motion.x, event.motion.y);
		widget::sdl_mouse_move_event(event.motion);
		break;
	case SDL_MOUSEWHEEL:
		recalc_cursor_pos(event.wheel.x, event.wheel.y);
		widget::sdl_mouse_wheel_event(event.wheel);
		break;
	case SDL_MOUSEBUTTONDOWN:
	case SDL_MOUSEBUTTONUP:
		recalc_cursor_pos(event.button.x, event.button.y);
		widget::sdl_mouse_button_event(event.button);
		break;
	}
}

void game_manager::setup_stretched_render_view()
{
	graphics::ortho(0.0f, (float)cx, 0.0f, (float)cy);
	graphics::set_viewport(0, 0, cx, cy);
}

void game_manager::setup_centered_render_view()
{
	float base_cx       = (float)game->center_cx;
	float base_cy       = (float)game->center_cy;
	float fcx           = (float)cx;
	float fcy           = (float)cy;
	float x             = 0.0f;
	float y             = 0.0f;
	float base_aspect   = base_cx / base_cy;
	float render_aspect = fcx / fcy;

	if (render_aspect < base_aspect) {
		float new_cy = fcx / base_aspect;
		y            = (fcy - new_cy) * 0.5f;
		fcy          = new_cy;
	} else {
		float new_cx = fcy * base_aspect;
		x            = (fcx - new_cx) * 0.5f;
		fcx          = new_cx;
	}

	x   = floorf(x);
	y   = floorf(y);
	fcx = ceilf(fcx);
	fcy = ceilf(fcy);

	graphics::ortho(0.0f, base_cx, 0.0f, base_cy);
	graphics::set_viewport((int)x, cy - (int)y - (int)fcy, (int)fcx, (int)fcy);

	center_x = (int)x;
	center_y = (int)y;

	if (render_aspect < base_aspect) {
		relative_cx = game->center_cx;
		relative_cy = (int)(base_cx / render_aspect);
	} else {
		relative_cx = (int)(base_cy * render_aspect);
		relative_cy = game->center_cy;
	}
}

inline void game_manager::setup_render_view()
{
	if (game->center_view)
		setup_centered_render_view();
	else
		setup_stretched_render_view();
}

static inline int to_pow2(int size)
{
	int ret = 1;
	while (ret < size)
		ret *= 2;
	return ret;
}

// #define DEBUG_FRAME_TIMING 1

void game_manager::render_frame()
{
#if DEBUG_FRAME_TIMING
	int64_t ts1 = get_time_ns();
#endif

	graphics::clear(0.0f, 0.0f, 0.0f, 1.0f);

	constexpr int max_ticks  = 3;
	int           tick_count = 0;

	while (cur_time >= frame_time_ns) {
		if (++tick_count == max_ticks)
			break;

		game->tick_all(frame_time_sec);
		cur_time -= frame_time_ns;
	}

#if DEBUG_FRAME_TIMING
	int64_t ts2 = get_time_ns();
#endif

	setup_render_view();

	graphics::push_projection_matrix();
	graphics::render_target rt;

	math::vec2 size;
	if (game->center_view) {
		rt.set(center_target);
		graphics::push_viewport(0, 0, game->center_cx, game->center_cy);
		graphics::clear(0.0f, 0.0f, 0.0f, 0.0f);

		size.set((float)game->center_cx, (float)game->center_cy);
		game->prerender(game->center_cx, game->center_cy);
	} else {
		size.set((float)cx, (float)cy);
		game->prerender(cx, cy);
	}

	game->render();
	graphics::pop_projection_matrix();
	graphics::push_projection_matrix();
	graphics::ortho(0.0f, size.x, 0.0f, size.y, -100.0f, 100.0f);
	widget::render_all(size);
	graphics::pop_projection_matrix();

	if (game->center_view) {
		rt.clear();
		center_target->generate_mipmaps();

		graphics::pop_viewport();

		center_vs->load();
		center_ps->load();

		using namespace crpg::graphics;

		blend_guard bf(blend::one, blend::zero, blend::zero, blend::one);
		draw(center_target);
	}

#if DEBUG_FRAME_TIMING
	int64_t ts3        = get_time_ns();
	bool    multi_tick = tick_count != 1;
#endif

	display->swap();

	int64_t ts_finish = get_time_ns();

#if DEBUG_FRAME_TIMING
	static int frame_count = 0;
	crpg::debug("frame: %d, cur_time: %lld, frame time: %lld, frame_time_ns: %lld, tick time: %lld, "
	            "render time: %lld, swap time: %lld, tick_count: %d",
	            frame_count++,
	            cur_time,
	            ts_finish - last_ts,
	            frame_time_ns,
	            ts2 - ts1,
	            ts3 - ts2,
	            ts_finish - ts3,
	            tick_count);
#endif

	if (tick_count == max_ticks) {
		cur_time = frame_time_ns;
	} else {
		cur_time += ts_finish - last_ts;
	}

	last_ts = ts_finish;
}

void game_manager::game_loop()
try {
	/* ---------------------------------------------- */
	/* Start up graphics subsystem                    */

	graphics::renderer::error renderer_error;
	renderer = graphics::renderer::create(&renderer_error);

	if (!renderer) {
		if (renderer_error == graphics::renderer::error::unsupported)
			throw std::string("Unsupported GPU apparently");
		throw std::string("Could not create renderer");
	}

	/* ---------------------------------------------- */
	/* Load main game library object                  */

#ifndef STATIC_ONLY
	void *game_lib = SDL_LoadObject(GAME_LIBRARY_NAME);
	if (!game_lib)
		throw strprintf("%s", SDL_GetError());

	create_app_cb create_app = (create_app_cb)SDL_LoadFunction(game_lib, "create_app");
	if (!create_app)
		throw strprintf("Failed to find entry point for '%s'", "create_app");
#endif

	game.reset(create_app());
	if (!game)
		throw strprintf("Failed to create game for '%s'", GAME_LIBRARY_NAME);

	game->startup(cx, cy);
	game_name = game->name();

	ui.ui_event.signal(); // signal 1
	ui.game_event.wait(); // signal 2

	graphics::display::info info;
	info.format = graphics::texture_format::rgba;

	switch (ui.wmi.subsystem) {
#if defined(_WIN32)
	case SDL_SYSWM_WINDOWS:
		info.hwnd = (void *)ui.wmi.info.win.window;
		break;
#elif defined(__APPLE__)
#else
	case SDL_SYSWM_WAYLAND:
		info.wayland_surf    = ui.wmi.info.wl.surface;
		info.wayland_display = ui.wmi.info.wl.display;
		break;
	case SDL_SYSWM_X11:
		info.x11_display = ui.wmi.info.x11.display;
		info.x11_id      = ui.wmi.info.x11.window;
		break;
#endif
	default:
		throw std::string("Unknown display subsystem");
	}

	display = graphics::display::create(info, (uint32_t)cx, (uint32_t)cy);
	if (!display)
		throw std::string("Failed to create display");

	display->load();
	game->init();

	/* ---------------------------------------------- */
	/* Setup framebuffer if centering view            */

	if (game->center_view) {
		center_vs     = resources::shader("engine:unlit.vert");
		center_ps     = resources::shader("engine:unlit.frag");
		center_target = graphics::texture::create(to_pow2(game->center_cx),
		                                          to_pow2(game->center_cy),
		                                          4,
		                                          graphics::texture_format::rgba,
		                                          nullptr,
		                                          graphics::texture::type::renderable);
		if (!center_target)
			throw std::string("Failed to create render target");
	}

	/* ---------------------------------------------- */
	/* Execute main render loop                       */

	last_ts  = get_time_ns();
	cur_time = frame_time_ns;

	while (!game->quitting()) {
		vector<SDL_Event> events;
		{
			lock_guard<mutex> lock(ui.sdl_events_mutex);
			events = ui.sdl_events;
			ui.sdl_events.resize(0);
		}
		for (SDL_Event &ev : events) {
			process_event(ev);
		}

		render_frame();
	}

	SDL_Event ev = {};
	ev.type      = ui.real_quit_event;
	SDL_PushEvent(&ev);

	ui.ui_event.signal(); // signal 3

} catch (const std::string &text) {
	error("%s: %s", __FUNCTION__, text.c_str());
	ui.game_loop_error = true;
	ui.ui_event.signal();
}

void ui_manager::ui_loop()
try {
	game_manager *gm;

	game_thread = thread([&]() {
		game_manager actual_gm(*this);
		gm = &actual_gm;
		actual_gm.game_loop();
	});

	ui_event.wait(); // signal 1
	if (game_loop_error)
		throw string("Game loop error");

	/* ---------------------------------------------- */
	/* Create main window                             */

	window = SDL_CreateWindow(gm->game_name.c_str(),
	                          SDL_WINDOWPOS_UNDEFINED,
	                          SDL_WINDOWPOS_UNDEFINED,
	                          gm->cx,
	                          gm->cy,
	                          SDL_WINDOW_MOUSE_CAPTURE);
	if (!window)
		throw strprintf("Failed to create main SDL window: %s", SDL_GetError());

	/* ---------------------------------------------- */
	/* Create display for window                      */

	SDL_VERSION(&wmi.version);
	SDL_GetWindowWMInfo(window, &wmi);
	SDL_SetWindowResizable(window, SDL_TRUE);
	SDL_CaptureMouse(SDL_TRUE);

	game_event.signal(); // signal 2

	/* ---------------------------------------------- */
	/* Add event watch for resizing                   */

	auto window_event_watcher = [](void *data, SDL_Event *ev) -> int {
		ui_manager &ui = *reinterpret_cast<ui_manager *>(data);

		if (ev->type == SDL_WINDOWEVENT) {
			lock_guard<mutex> lock(ui.sdl_events_mutex);
			ui.sdl_events.push_back(*ev);
		}

		return 0;
	};

	SDL_AddEventWatch(window_event_watcher, this);

	/* ---------------------------------------------- */
	/* Execute main ui loop                           */

	static const uint32_t queueable_event_types[] = {
	        SDL_QUIT,
	        SDL_KEYUP,
	        SDL_KEYDOWN,
	        SDL_MOUSEMOTION,
	        SDL_MOUSEWHEEL,
	        SDL_MOUSEBUTTONDOWN,
	        SDL_MOUSEBUTTONUP,
	};

	SDL_Event ev;
	while (SDL_WaitEvent(&ev)) {
		if (ev.type == real_quit_event)
			break;

		for (uint32_t queueable_event_type : queueable_event_types) {
			if (ev.type == queueable_event_type) {
				lock_guard<mutex> lock(sdl_events_mutex);
				sdl_events.push_back(ev);
				break;
			}
		}
	}

	SDL_DelEventWatch(window_event_watcher, this);

	ui_event.wait(); // signal 3
	if (game_loop_error)
		throw string("Game loop error");

} catch (const std::string &text) {
	error("%s: %s", __FUNCTION__, text.c_str());
	throw;
}

} // namespace crpg

int main(int, char *[])
try {
	/* TODO: proper logging */
	auto debug_log = [](crpg::log_level level, const char *format, va_list args) noexcept {
		char msg[4096];
		vsnprintf(msg, sizeof(msg) - 10, format, args);
		strcat(msg, "\n");
#ifdef _WIN32
		(void)level;
		OutputDebugStringA(msg);
#else
		if (level <= crpg::log_level::debug)
			printf("debug: %s", msg);
		else if (level <= crpg::log_level::info)
			printf("info: %s", msg);
		else if (level <= crpg::log_level::warning)
			printf("warning: %s", msg);
		else if (level <= crpg::log_level::error)
			printf("error: %s", msg);
#endif
	};

#if !defined(_WIN32) && !defined(__APPLE__)
	const char *session_type = getenv("XDG_SESSION_TYPE");
	if (session_type && strcmp(session_type, "wayland") == 0)
		setenv("SDL_VIDEODRIVER", "wayland", 1);
#endif

	crpg::set_log_callback(debug_log);

	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) != 0) {
		crpg::error("Failed to initialize SDL: %s", SDL_GetError());
		return -1;
	}

	if (!crpg::font::ft2_init())
		throw std::string("Failed to load freetype2");

	/* ----------------------------- */
	/* start ui loop                 */
	{
		crpg::ui_manager ui;
		ui.real_quit_event = SDL_RegisterEvents(1);
		ui.ui_loop();
	}

	crpg::font::ft2_free();

	SDL_Quit();
	return 0;

} catch (const std::string &text) {
	SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Failed to start game", text.c_str(), NULL);
	SDL_Quit();
	return -1;
}
