#pragma once

#include <SDL.h>

namespace sdl {

template<typename T, void deleter(T)> class sdl_ptr;

using window = sdl_ptr<SDL_Window *, SDL_DestroyWindow>;

template<typename T, void deleter(T)> class sdl_ptr {
	T ptr;

public:
	inline sdl_ptr() : ptr(nullptr) {}
	inline sdl_ptr(T ptr_) : ptr(ptr_) {}
	inline sdl_ptr(const sdl_ptr &ref) = delete;
	inline sdl_ptr(sdl_ptr &&ref) : ptr(ref.ptr) { ref.ptr = nullptr; }

	inline ~sdl_ptr()
	{
		if (ptr)
			deleter(ptr);
	}

	inline sdl_ptr &replace(T ptr_ = nullptr)
	{
		if (ptr)
			deleter(ptr);
		ptr = ptr_;
		return *this;
	}

	inline sdl_ptr &operator=(T ptr_) { return replace(ptr_); }

	inline sdl_ptr &operator=(sdl_ptr &&ref)
	{
		if (this != &ref) {
			if (ptr)
				deleter(ptr);
			ptr     = ref.ptr;
			ref.ptr = nullptr;
		}

		return *this;
	}

	inline operator T() const { return ptr; }

	inline bool operator==(T p) const { return ptr == p; }
	inline bool operator!=(T p) const { return ptr != p; }
	inline bool operator!() const { return !ptr; }
};
} // namespace sdl
