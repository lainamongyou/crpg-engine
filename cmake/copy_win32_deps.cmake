if(NOT MSVC)
	return()
endif()

if(DEPS_HAVE_BEEN_COPIED)
	return()
else()
	file(REMOVE_RECURSE "${CMAKE_BINARY_DIR}/copied_deps")
endif()

file(GLOB DEP_BIN_FILES
	"${DEPS_DIR}/bin/SDL2.dll"
	"${DEPS_DIR}/bin/libpng*.dll"
	"${DEPS_DIR}/bin/zlib.dll"
)
if(BUILD_EDITOR)
	file(GLOB QT_BIN_REL_FILES
		"${QT_DIR}/bin/Qt6Core.dll"
		"${QT_DIR}/bin/Qt6Gui.dll"
		"${QT_DIR}/bin/Qt6Widgets.dll"
		"${QT_DIR}/bin/Qt6Svg.dll"
		"${DEPS_DIR}/binr/qtadvanceddocking.dll"
	)
	file(GLOB QT_PLAT_BIN_REL_FILES
		"${QT_DIR}/plugins/platforms/qwindows.dll"
	)
	file(GLOB QT_STYLES_BIN_REL_FILES
		"${QT_DIR}/plugins/styles/qwindowsvistastyle.dll"
	)
	file(GLOB QT_ICONENGINE_BIN_REL_FILES
		"${QT_DIR}/plugins/iconengines/qsvgicon.dll"
	)	
	file(GLOB QT_IMAGEFORMATS_BIN_REL_FILES
		"${QT_DIR}/plugins/imageformats/qsvg.dll"
		"${QT_DIR}/plugins/imageformats/qgif.dll"
		"${QT_DIR}/plugins/imageformats/qtga.dll"
		"${QT_DIR}/plugins/imageformats/qwbmp.dll"
		"${QT_DIR}/plugins/imageformats/qjpeg.dll"
	)

	file(GLOB QT_BIN_DEB_FILES
		"${QT_DIR}/bin/Qt6Cored.dll"
		"${QT_DIR}/bin/Qt6Guid.dll"
		"${QT_DIR}/bin/Qt6Widgetsd.dll"
		"${QT_DIR}/bin/Qt6Svgd.dll"
		"${DEPS_DIR}/bind/qtadvanceddocking.dll"
	)
	file(GLOB QT_PLAT_BIN_DEB_FILES
		"${QT_DIR}/plugins/platforms/qwindowsd.dll"
	)
	file(GLOB QT_STYLES_BIN_DEB_FILES
		"${QT_DIR}/plugins/styles/qwindowsvistastyled.dll"
	)
	file(GLOB QT_ICONENGINE_BIN_DEB_FILES
		"${QT_DIR}/plugins/iconengines/qsvgicond.dll"
	)	
	file(GLOB QT_IMAGEFORMATS_BIN_DEB_FILES
		"${QT_DIR}/plugins/imageformats/qsvgd.dll"
		"${QT_DIR}/plugins/imageformats/qgifd.dll"
		"${QT_DIR}/plugins/imageformats/qtgad.dll"
		"${QT_DIR}/plugins/imageformats/qwbmpd.dll"
		"${QT_DIR}/plugins/imageformats/qjpegd.dll"
	)
endif()

if(NOT BUILD_STATIC)
	foreach(CopyFile ${DEP_BIN_FILES})
		message(STATUS "copying ${CopyFile} to ${CMAKE_BINARY_DIR}/copied_deps/bin64")
		file(COPY "${CopyFile}" DESTINATION "${CMAKE_BINARY_DIR}/copied_deps/bin64/")
	endforeach()
endif()

if(BUILD_EDITOR)
	make_directory("${CMAKE_BINARY_DIR}/copied_deps/bin64r/platforms")
	make_directory("${CMAKE_BINARY_DIR}/copied_deps/bin64r/styles")
	make_directory("${CMAKE_BINARY_DIR}/copied_deps/bin64r/iconengines")
	make_directory("${CMAKE_BINARY_DIR}/copied_deps/bin64r/imageformats")

	foreach(CopyFile ${QT_BIN_REL_FILES})
		message(STATUS "copying ${CopyFile} to ${CMAKE_BINARY_DIR}/copied_deps/bin64r")
		file(COPY "${CopyFile}" DESTINATION "${CMAKE_BINARY_DIR}/copied_deps/bin64r/")
	endforeach()
	foreach(CopyFile ${QT_PLAT_BIN_REL_FILES})
		message(STATUS "copying ${CopyFile} to ${CMAKE_BINARY_DIR}/copied_deps/bin64r/platforms")
		file(COPY "${CopyFile}" DESTINATION "${CMAKE_BINARY_DIR}/copied_deps/bin64r/platforms/")
	endforeach()
	foreach(CopyFile ${QT_STYLES_BIN_REL_FILES})
		message(STATUS "copying ${CopyFile} to ${CMAKE_BINARY_DIR}/copied_deps/bin64r/styles")
		file(COPY "${CopyFile}" DESTINATION "${CMAKE_BINARY_DIR}/copied_deps/bin64r/styles/")
	endforeach()
	foreach(CopyFile ${QT_ICONENGINE_BIN_REL_FILES})
		message(STATUS "copying ${CopyFile} to ${CMAKE_BINARY_DIR}/copied_deps/bin64r/iconengines")
		file(COPY "${CopyFile}" DESTINATION "${CMAKE_BINARY_DIR}/copied_deps/bin64r/iconengines/")
	endforeach()
	foreach(CopyFile ${QT_IMAGEFORMATS_BIN_REL_FILES})
		message(STATUS "copying ${CopyFile} to ${CMAKE_BINARY_DIR}/copied_deps/bin64r/imageformats")
		file(COPY "${CopyFile}" DESTINATION "${CMAKE_BINARY_DIR}/copied_deps/bin64r/imageformats/")
	endforeach()

	make_directory("${CMAKE_BINARY_DIR}/copied_deps/bin64d/platforms")
	make_directory("${CMAKE_BINARY_DIR}/copied_deps/bin64d/styles")
	make_directory("${CMAKE_BINARY_DIR}/copied_deps/bin64d/iconengines")
	make_directory("${CMAKE_BINARY_DIR}/copied_deps/bin64d/imageformats")

	foreach(CopyFile ${QT_BIN_DEB_FILES})
		message(STATUS "copying ${CopyFile} to ${CMAKE_BINARY_DIR}/copied_deps/bin64d")
		file(COPY "${CopyFile}" DESTINATION "${CMAKE_BINARY_DIR}/copied_deps/bin64d/")
	endforeach()
	foreach(CopyFile ${QT_PLAT_BIN_DEB_FILES})
		message(STATUS "copying ${CopyFile} to ${CMAKE_BINARY_DIR}/copied_deps/bin64d/platforms")
		file(COPY "${CopyFile}" DESTINATION "${CMAKE_BINARY_DIR}/copied_deps/bin64d/platforms/")
	endforeach()
	foreach(CopyFile ${QT_STYLES_BIN_DEB_FILES})
		message(STATUS "copying ${CopyFile} to ${CMAKE_BINARY_DIR}/copied_deps/bin64d/styles")
		file(COPY "${CopyFile}" DESTINATION "${CMAKE_BINARY_DIR}/copied_deps/bin64d/styles/")
	endforeach()
	foreach(CopyFile ${QT_ICONENGINE_BIN_DEB_FILES})
		message(STATUS "copying ${CopyFile} to ${CMAKE_BINARY_DIR}/copied_deps/bin64d/iconengines")
		file(COPY "${CopyFile}" DESTINATION "${CMAKE_BINARY_DIR}/copied_deps/bin64d/iconengines/")
	endforeach()
	foreach(CopyFile ${QT_IMAGEFORMATS_BIN_DEB_FILES})
		message(STATUS "copying ${CopyFile} to ${CMAKE_BINARY_DIR}/copied_deps/bin64d/imageformats")
		file(COPY "${CopyFile}" DESTINATION "${CMAKE_BINARY_DIR}/copied_deps/bin64d/imageformats/")
	endforeach()
endif()

set(DEPS_HAVE_BEEN_COPIED TRUE CACHE BOOL "Dependencies have been copied, uncheck if you need to recopy them" FORCE)
