set(OUTPUT_DIR "${CMAKE_BINARY_DIR}/rundir")

if(NOT BUILD_DIR OR BUILD_DIR STREQUAL "")
	set(COPY_HELPER "${CMAKE_CURRENT_SOURCE_DIR}/crpg-engine/cmake/copy_helper.cmake")
else()
	set(COPY_HELPER "${BUILD_DIR}/../crpg-engine/cmake/copy_helper.cmake")
endif()

macro(crpg_precompile_headers)
	if(NOT DISABLE_PCH)
		target_precompile_headers(${ARGN})
	endif()
endmacro()

macro(crpg_autogroup_other)
	source_group("_other" REGULAR_EXPRESSION "(.*\.(hxx|cxx|txt)|mocs_.*|ui_.*|qrc_.*|.*\.qrc|.*\.ui)")
endmacro()

macro(crpg_autogroup_sources)
	source_group("source" REGULAR_EXPRESSION ".*\.(c|cpp)")
	source_group("headers" REGULAR_EXPRESSION ".*\.(h|hpp)")
	source_group("shaders" REGULAR_EXPRESSION ".*\.(vert|frag)")
endmacro()

macro(crpg_autogroup_all)
	crpg_autogroup_sources()
	crpg_autogroup_other()
endmacro()

set(glslc ${Vulkan_GLSLC_EXECUTABLE})

function(target_shaders target)
	set(target_dir "${WORK_DIR}/$<IF:$<CONFIG:Debug>,debug,bin>/resources/${target}/shaders/")
	add_custom_command(TARGET ${target} POST_BUILD
		COMMAND ${CMAKE_COMMAND} -E make_directory ${target_dir})
	foreach(source ${ARGN})
		get_filename_component(source_file ${source} NAME)
		set(spv_file "${target_dir}/${source_file}.spv")
		add_custom_command(TARGET ${target} POST_BUILD
			COMMAND ${glslc} -o ${spv_file}
				${CMAKE_CURRENT_SOURCE_DIR}/${source}
			VERBATIM)
	endforeach()
	target_sources(${target} PRIVATE ${ARGN})
endfunction()

function(copy_dumb_deps_i_hate_this target target_configs source dest)
	add_custom_command(TARGET ${target} POST_BUILD
		COMMAND "${CMAKE_COMMAND}"
			"-DCONFIG=$<CONFIGURATION>"
			"-DTARGET_CONFIGS=${target_configs}"
			"-DINPUT=${source}"
			"-DOUTPUT=${dest}"
			-P "${COPY_HELPER}"
		VERBATIM)
endfunction()

function(install_deps maintarget)
	set(depsdir "${CMAKE_BINARY_DIR}/copied_deps")

	install(DIRECTORY "${depsdir}/bin64d/"
		DESTINATION "./"
		USE_SOURCE_PERMISSIONS
		CONFIGURATIONS Debug
		PATTERN ".gitignore" EXCLUDE)
	install(DIRECTORY "${depsdir}/bin64r/"
		DESTINATION "./"
		USE_SOURCE_PERMISSIONS
		CONFIGURATIONS Release RelWithDebInfo MinSizeRel
		PATTERN ".gitignore" EXCLUDE)
	install(DIRECTORY "${depsdir}/bin64/"
		DESTINATION "./"
		USE_SOURCE_PERMISSIONS
		CONFIGURATIONS Debug Release RelWithDebInfo MinSizeRel
		PATTERN ".gitignore" EXCLUDE)

	copy_dumb_deps_i_hate_this(${maintarget} "Debug"
		"${depsdir}/bin64d/"
		"${WORK_DIR}/debug/")
	copy_dumb_deps_i_hate_this(${maintarget} "Debug"
		"${depsdir}/bin64/"
		"${WORK_DIR}/debug/")
	copy_dumb_deps_i_hate_this(${maintarget} "Release;MinSizeRel;RelWithDebInfo"
		"${depsdir}/bin64r/"
		"${WORK_DIR}/bin/")
	copy_dumb_deps_i_hate_this(${maintarget} "Release;MinSizeRel;RelWithDebInfo"
		"${depsdir}/bin64/"
		"${WORK_DIR}/bin/")
endfunction()

function(export_bin target exportname)
	install(TARGETS ${target}
		EXPORT "${exportname}Target"
		LIBRARY DESTINATION "./"
		ARCHIVE DESTINATION "./"
		RUNTIME DESTINATION "./")

	export(TARGETS ${target} FILE "${CMAKE_CURRENT_BINARY_DIR}/${exportname}Target.cmake")
	export(PACKAGE "${exportname}")

	install(EXPORT "${exportname}Target"
		DESTINATION "cmake/${exportname}")
endfunction()

function(install_module_data target copytarget module)
	install(DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/${copytarget}/"
		DESTINATION "${WORK_DIR}/resources/${module}"
		USE_SOURCE_PERMISSIONS)
	add_custom_command(TARGET ${target} POST_BUILD
		COMMAND "${CMAKE_COMMAND}" -E copy_directory
			"${CMAKE_CURRENT_SOURCE_DIR}/${copytarget}/"
			"${WORK_DIR}/$<IF:$<CONFIG:Debug>,debug,bin>/resources/${module}"
		VERBATIM)
endfunction()

function(install_data_to_base target copytarget)
	install(DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/${copytarget}/"
		DESTINATION "${WORK_DIR}"
		USE_SOURCE_PERMISSIONS)
	add_custom_command(TARGET ${target} POST_BUILD
		COMMAND "${CMAKE_COMMAND}" -E copy_directory
			"${CMAKE_CURRENT_SOURCE_DIR}/${copytarget}/"
			"${WORK_DIR}/$<IF:$<CONFIG:Debug>,debug,bin>"
		VERBATIM)
endfunction()

function(install_game_data target)
	install(DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/project/"
		DESTINATION "${WORK_DIR}/game"
		USE_SOURCE_PERMISSIONS)
	add_custom_command(TARGET ${target} POST_BUILD
		COMMAND "$<$<NOT:$<CONFIG:Debug>>:${CMAKE_COMMAND};-E;rm;-rf;\"${WORK_DIR}/bin/game\">"
		COMMAND_EXPAND_LISTS)
	add_custom_command(TARGET ${target} POST_BUILD
		COMMAND "$<$<NOT:$<CONFIG:Debug>>:${CMAKE_COMMAND};-E;copy_directory;\"${CMAKE_CURRENT_SOURCE_DIR}/project/\";\"${WORK_DIR}/bin/game\">"
		COMMAND_EXPAND_LISTS)
endfunction()

function(install_file_to_base target copytarget)
	install(FILES "${CMAKE_CURRENT_SOURCE_DIR}/${copytarget}"
		DESTINATION "${WORK_DIR}")
	add_custom_command(TARGET ${target} POST_BUILD
		COMMAND "${CMAKE_COMMAND}" -E copy
			"${CMAKE_CURRENT_SOURCE_DIR}/${copytarget}"
			"${WORK_DIR}/$<IF:$<CONFIG:Debug>,debug,bin>"
		VERBATIM)
endfunction()

function(install_bin_main target)
	if("${ARGV1}" STREQUAL "EXPORT")
		export_bin("${target}" "${ARGV2}")
	else()
		install(TARGETS ${target}
			LIBRARY DESTINATION "./"
			RUNTIME DESTINATION "./")
	endif()

	copy_dumb_deps_i_hate_this(${target} "Release;MinSizeRel;RelWithDebInfo"
		"$<TARGET_FILE:${target}>"
		"${WORK_DIR}/bin")
	copy_dumb_deps_i_hate_this(${target} "Debug"
		"$<TARGET_FILE:${target}>"
		"${WORK_DIR}/debug")

	export(TARGETS ${target} FILE "${CMAKE_CURRENT_BINARY_DIR}/${target}_target.cmake")
endfunction()

function(install_bin target)
	if(BUILD_STATIC)
		return()
	endif()
	install_bin_main(${target})
endfunction()
