add_library(SDL2::SDL2 SHARED IMPORTED)
set_target_properties(SDL2::SDL2 PROPERTIES INTERFACE_INCLUDE_DIRECTORIES "${DEPS_DIR}/include/SDL2")
set_target_properties(SDL2::SDL2 PROPERTIES IMPORTED_IMPLIB "${DEPS_DIR}/bin/sdl2.lib")
set_target_properties(SDL2::SDL2 PROPERTIES IMPORTED_LOCATION "${DEPS_DIR}/bin/sdl2.dll")

add_library(SDL2::SDL2-static STATIC IMPORTED)
set_target_properties(SDL2::SDL2-static PROPERTIES INTERFACE_INCLUDE_DIRECTORIES "${DEPS_DIR}/include/SDL2")
set_target_properties(SDL2::SDL2-static PROPERTIES IMPORTED_LOCATION "${DEPS_DIR}/bin/SDL2-static.lib")
target_link_libraries(SDL2::SDL2-static INTERFACE winmm imm32 version Setupapi)

add_library(SDL2::SDL2main STATIC IMPORTED)
set_target_properties(SDL2::SDL2main PROPERTIES IMPORTED_IMPLIB_DEBUG "${DEPS_DIR}/bin/sdl2maind.lib")
set_target_properties(SDL2::SDL2main PROPERTIES IMPORTED_LOCATION "${DEPS_DIR}/bin/sdl2main.lib")
