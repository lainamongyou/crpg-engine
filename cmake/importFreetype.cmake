add_library(Freetype::Freetype STATIC IMPORTED)
set_target_properties(Freetype::Freetype PROPERTIES INTERFACE_INCLUDE_DIRECTORIES "${DEPS_DIR}/include")
set_target_properties(Freetype::Freetype PROPERTIES IMPORTED_LOCATION "${DEPS_DIR}/bin/freetype.lib")
