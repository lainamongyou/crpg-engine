#include "utility/file-helpers.hpp"
#include "utility/fserializer.hpp"
#include "utility/logging.hpp"
#include "utility/dict.hpp"
#include "tileset.hpp"
#include "assets.hpp"
#include "sprite.hpp"
#include "mask.hpp"
#include "map.hpp"
#include "app.hpp"

#include <unordered_map>

#include <assert.h>

namespace crpg {

asset_manager *asset_manager::current = nullptr;

struct asset_info {
	std::string    path;
	asset_type     type;
	asset_category category;

	std::string name;
};

class asset_manager_internal : public asset_manager {
public:
	asset_manager_internal() noexcept;

	type::asset_id get_id(const std::string &name) noexcept override;
	bool           load_map_now(type::asset_id id) noexcept override;
	sprite_ref     get_sprite(type::asset_id id) noexcept override;
	tileset_ref    get_tileset(type::asset_id id) noexcept override;
	mask_ref       get_tileset_mask(type::asset_id id) noexcept override;
	object_ref     get_object(type::asset_id id) noexcept override;

	void load_asset(const std::string &path, const std::string &parent_name = std::string()) noexcept;

	std::unordered_map<std::string, type::asset_id> name_to_id;
	std::unordered_map<type::asset_id, asset_info>  id_to_asset;

	std::unordered_map<type::asset_id, weak_sprite_ref>  sprites;
	std::unordered_map<type::asset_id, weak_tileset_ref> tilesets;
	std::unordered_map<type::asset_id, weak_mask_ref>    tileset_masks;
	std::unordered_map<type::asset_id, object_ref>       objects;

	std::unique_ptr<map> current_map;
};

#ifdef _DEBUG
#define GAME_LOCATION "../project"
#else
#define GAME_LOCATION "game"
#endif

asset_manager_internal::asset_manager_internal() noexcept
{
	current = this;
	load_asset(GAME_LOCATION);
}

static inline const char *category_to_name(asset_category category) noexcept
{
	/* clang-format off */
	switch (category) {
	case asset_category::root:    return "root";
	case asset_category::map:     return "map";
	case asset_category::object:  return "object";
	case asset_category::sprite:  return "sprite";
	case asset_category::tileset: return "tileset";
	case asset_category::sound:   return "sound";
	case asset_category::music:   return "music";
	}
	/* clang-format on */

	assert(0);
	return "unknown";
}

void asset_manager_internal::load_asset(const std::string &path, const std::string &parent_name) noexcept
try {
	ifserializer s(path + "/info.data");
	if (s.fail())
		throw strprintf("Failed to load asset in '%s'", path.c_str());

	type::asset_id id;
	std::string    name;
	asset_type     type;
	asset_category category;

	crpg::dict dict(s);
	dict.get("id", id);
	dict.get("name", name);
	dict.get("type", type);
	dict.get("category", category);

	std::string full_name;
	if (type != asset_type::root) {
		if (type == asset_type::category) {
			full_name = std::string(category_to_name(category)) + ":";
		} else {
			if (type == asset_type::file)
				full_name = parent_name + name;
			else
				full_name = parent_name + name + "/";
		}
	}

	if (type == asset_type::file) {
		name_to_id[full_name] = id;
		id_to_asset[id]       = {path, type, category, full_name};
	} else {
		os_file_entry_t entry = os_open_dir(path);
		if (entry->valid()) {
			do {
				if (entry->directory()) {
					load_asset(path + "/" + entry->name(), full_name);
				}
			} while (entry->next());
		}
	}

} catch (const std::string &text) {
	error("%s: %s", __FUNCTION__, text.c_str());
}

type::asset_id asset_manager_internal::get_id(const std::string &name) noexcept
{
	return name_to_id[name];
}

bool asset_manager_internal::load_map_now(type::asset_id id) noexcept
try {
	auto it = id_to_asset.find(id);
	if (it == id_to_asset.end())
		throw std::string("Invalid asset ID");

	asset_info &asset = it->second;
	if (asset.category != asset_category::map)
		throw strprintf("Unexpected category: %s: %s", category_to_name(asset.category), asset.name.c_str());

	map *new_map = map::load(asset.path);
	if (!new_map)
		return false;

	current_map.reset(new_map);
	return true;

} catch (const std::string &text) {
	error("%s: %s", __FUNCTION__, text.c_str());
	return false;
}

sprite_ref asset_manager_internal::get_sprite(type::asset_id id) noexcept
try {
	auto sprites_it = sprites.find(id);
	if (sprites_it != sprites.end()) {
		weak_sprite_ref &weak_sprite = sprites_it->second;
		if (!weak_sprite.expired())
			return weak_sprite.lock();
	}

	auto it = id_to_asset.find(id);
	if (it == id_to_asset.end())
		throw std::string("Invalid asset ID");

	asset_info &asset = it->second;
	if (asset.category != asset_category::sprite)
		throw strprintf("Unexpected category: %s: %s", category_to_name(asset.category), asset.name.c_str());

	sprite_ref sprite = crpg::sprite::create(asset.path);
	if (!sprite)
		throw strprintf("Failed to create asset: %s", asset.name.c_str());

	sprites[id] = sprite;
	return sprite;

} catch (const std::string &text) {
	error("%s: %s", __FUNCTION__, text.c_str());
	return sprite_ref();
}

tileset_ref asset_manager_internal::get_tileset(type::asset_id id) noexcept
try {
	auto tilesets_it = tilesets.find(id);
	if (tilesets_it != tilesets.end()) {
		weak_tileset_ref &weak_tileset = tilesets_it->second;
		if (!weak_tileset.expired())
			return weak_tileset.lock();
	}

	auto it = id_to_asset.find(id);
	if (it == id_to_asset.end())
		throw std::string("Invalid asset ID");

	asset_info &asset = it->second;
	if (asset.category != asset_category::tileset)
		throw strprintf("Unexpected category: %s: %s", category_to_name(asset.category), asset.name.c_str());

	tileset_ref tileset = crpg::tileset::create(asset.path);
	if (!tileset)
		throw strprintf("Failed to create asset: %s", asset.name.c_str());

	tilesets[id] = tileset;
	return tileset;

} catch (const std::string &text) {
	error("%s: %s", __FUNCTION__, text.c_str());
	return tileset_ref();
}

mask_ref asset_manager_internal::get_tileset_mask(type::asset_id id) noexcept
try {
	auto tileset_masks_it = tileset_masks.find(id);
	if (tileset_masks_it != tileset_masks.end()) {
		weak_mask_ref &weak_tileset_mask = tileset_masks_it->second;
		if (!weak_tileset_mask.expired())
			return weak_tileset_mask.lock();
	}

	auto it = id_to_asset.find(id);
	if (it == id_to_asset.end())
		throw std::string("Invalid asset ID");

	asset_info &asset = it->second;
	if (asset.category != asset_category::tileset)
		throw strprintf("Unexpected category: %s: %s", category_to_name(asset.category), asset.name.c_str());

	mask_ref mask = mask::create(asset.path + "/image.mask");
	if (!mask)
		throw strprintf("Failed to create asset: %s", asset.name.c_str());

	tileset_masks[id] = mask;
	return mask;

} catch (const std::string &text) {
	error("%s: %s", __FUNCTION__, text.c_str());
	return mask_ref();
}

object_ref asset_manager_internal::get_object(type::asset_id id) noexcept
try {
	auto objects_it = objects.find(id);
	if (objects_it != objects.end()) {
		return objects_it->second;
	}

	auto it = id_to_asset.find(id);
	if (it == id_to_asset.end())
		throw std::string("Invalid asset ID");

	asset_info &asset = it->second;
	if (asset.category != asset_category::object)
		throw strprintf("Unexpected category: %s: %s", category_to_name(asset.category), asset.name.c_str());

	std::string  path = asset.path + "/asset.data";
	ifserializer s(path);
	if (s.fail())
		throw strprintf("Failed to load object asset: %s", path.c_str());

	object_type type;

	crpg::dict dict(s);
	dict.get("type", type);

	auto s_properties = dict.get_serializer("properties");

	size_t size;
	s_properties >> size;

	object_info *obj = new object_info;
	obj->name_       = asset.name;
	obj->type_       = type;

	if (obj->type_ == object_type::unused)
		obj->type_ = object_type::box;

	for (size_t i = 0; i < size; i++) {
		object_info::property prop;
		crpg::dict            prop_dict(s_properties);
		prop_dict.get("name", prop.name);
		prop_dict.get("type", prop.type);
		obj->properties_.push_back(std::move(prop));
	}

	object_ref ref(obj);
	objects[id] = ref;
	return ref;

} catch (const std::string &text) {
	error("%s: %s", __FUNCTION__, text.c_str());
	return object_ref();
}

std::unique_ptr<asset_manager> asset_manager::create() noexcept
try {
	if (current)
		throw "A resource manager already exists";

	return std::unique_ptr<asset_manager>(new asset_manager_internal);

} catch (const char *str) {
	error("%s: %s", __FUNCTION__, str);
	return nullptr;
}

asset_manager::~asset_manager() noexcept
{
	current = nullptr;
}

void asset_manager::load_map(type::asset_id id, load_callback_t callback) noexcept
{
	if (id) {
		crpg::app *app    = crpg::app::get();
		app->next_map     = id;
		app->map_callback = callback;
	}
}

namespace assets {
bool load_map_now(const std::string &name) noexcept
try {
	auto          *am = asset_manager::get();
	type::asset_id id = am->get_id(name);
	if (!id)
		throw strprintf("Invalid map: %s", name.c_str());

	return am->load_map_now(id);

} catch (const std::string &text) {
	error("%s: %s", __FUNCTION__, text.c_str());
	return false;
}

void load_map(const std::string &name) noexcept
try {
	auto          *am = asset_manager::get();
	type::asset_id id = am->get_id(name);
	if (!id)
		throw strprintf("Invalid map: %s", name.c_str());

	am->load_map(id);

} catch (const std::string &text) {
	error("%s: %s", __FUNCTION__, text.c_str());
}

sprite_ref sprite(const std::string &name, bool fail_if_not_found) noexcept
try {
	auto *am = asset_manager::get();

	type::asset_id id = am->get_id(name);
	if (!id) {
		if (fail_if_not_found)
			throw strprintf("Invalid sprite: %s", name.c_str());
		else
			return sprite_ref();
	}

	return am->get_sprite(id);

} catch (const std::string &text) {
	error("%s: %s", __FUNCTION__, text.c_str());
	return sprite_ref();
}
} // namespace assets

} // namespace crpg
