#pragma once

#include <string>

#include "export.h"

namespace crpg {

class entity;

class EXPORT class_info {
public:
	typedef entity *(*create_cb)();
	class_info(const char *name, create_cb create) noexcept;

	inline entity *create() const noexcept { return construct(); }

private:
	create_cb construct;
};

#define define_class(name, asset_path)                                                                                 \
	static crpg::entity *name##_class_create() noexcept                                                            \
	{                                                                                                              \
		return new name();                                                                                     \
	}                                                                                                              \
	static crpg::class_info name##_class_info(asset_path, name##_class_create)

EXPORT entity *factory_create(const std::string &asset_path) noexcept;
} // namespace crpg
