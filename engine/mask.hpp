#pragma once

#include <vector>
#include <memory>

namespace crpg {

class mask;

using mask_ref      = std::shared_ptr<mask>;
using weak_mask_ref = std::weak_ptr<mask>;

class mask {
	friend class asset_manager_internal;

public:
	struct block {
		static constexpr uint16_t size = 16;
		uint16_t                  rows[size];
	};

private:
	std::vector<block> blocks;
	size_t             cx_;
	size_t             cy_;

	static mask_ref create(const std::string &path) noexcept;

public:
	inline size_t cx() const noexcept { return cx_; }
	inline size_t cy() const noexcept { return cy_; }

	inline block &get(size_t x, size_t y) noexcept { return blocks[y * cx_ + x]; }
};

} // namespace crpg
