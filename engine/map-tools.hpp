#pragma once

#pragma once

#include "math/box.hpp"
#include "math/plane.hpp"
#include "graphics/graphics.hpp"
#include "entity.hpp"
#include "types.hpp"

#include "export.h"

/* this file is typically only used internally and not included by games */

namespace crpg {

struct pixel_box;

class sprite;
using sprite_ref = std::shared_ptr<sprite>;

class mask;
using mask_ref = std::shared_ptr<mask>;

class collidable;
struct collision_layer;

/* ======================================================================== */
/* map layers                                                               */

enum class map_layer_type {
	invalid,
	folder,
	tileset,
	sprite,
	object,
	collision,
	custom,
};

enum class map_layer_blend {
	alpha,
	add,
	subtract,
	multiply,
};

struct map_layer {
	type::layer_id  id;
	std::string     name;
	map_layer_type  type;
	map_layer_blend blend;
	float           opacity;
	bool            visible;

	virtual ~map_layer() noexcept = default;
};

/* ------------------------------------------------------------------------ */

struct folder_layer : map_layer {
	std::vector<map_layer *> children;
};

/* ------------------------------------------------------------------------ */

enum class tile_type : uint8_t {
	normal,
	floor_autotile,
	advanced_floor_autotile,
};

struct segment {
	static constexpr size_t width = 16;

	struct tile_pos {
		uint8_t x;
		uint8_t y;

		inline tile_pos() noexcept {}
		constexpr tile_pos(uint8_t x, uint8_t y) noexcept : x(x), y(y) {}
		constexpr tile_pos(uint16_t val) noexcept : x((uint8_t)(val >> 8)), y((uint8_t)(val & 0xFF)) {}
	};

	struct tile {
		type::tileset_id tileset_id;
		tile_pos         vals[4];
		tile_type        type;
		uint8_t          autotile_id;
		uint8_t          unused[4];
	};

	tile     tiles[width][width] = {};
	uint32_t refs                = 0;
	int32_t  x;
	int32_t  y;

	inline uint32_t hash() const noexcept;
};

struct tileset_render_data {
	graphics::texture_ref       texture;
	graphics::vertex_buffer_ref vb;
};

struct tileset_segment {
	std::vector<tileset_render_data> render_data;
};

struct tileset_layer : map_layer {
	std::unordered_map<type::segment_id, tileset_segment> segments;
};

/* ------------------------------------------------------------------------ */

struct sprite_instance {
	type::index idx;
	math::vec2  pos;
};

struct sprite_segment {
	std::vector<type::index> instances;
};

struct sprite_layer : map_layer {
	std::vector<sprite_instance>                         instances;
	std::unordered_map<type::segment_id, sprite_segment> segments;
};

/* ------------------------------------------------------------------------ */

struct object_segment {
	std::vector<entity *> entities;
};

struct entity_data {
	std::vector<type::segment_id> segments;
};

struct object_layer : map_layer {
	std::unordered_map<type::entity_id, entity_data>     entities;
	std::unordered_map<type::segment_id, object_segment> segments;
	collision_layer                                     *collision;

	/* reusable array */
	std::vector<collidable *> cur_objects;

	void get_triggers(collidable *ent, const math::box &box, std::vector<collidable *> &triggers) noexcept;
	bool blocked(collidable *ent, const pixel_box &box, std::vector<collidable *> &collided_entities) noexcept;
};

/* ------------------------------------------------------------------------ */

struct collision_segment {
	static constexpr size_t width = segment::width;

	struct tile {
		type::tileset_id  tileset_id;
		segment::tile_pos val;
	};

	tile    tiles[width][width] = {};
	int32_t x;
	int32_t y;

	inline uint32_t hash() const noexcept;

	bool blocked(const std::vector<mask_ref> &tilesets, const pixel_box &box) const noexcept;
};

struct collision_layer : map_layer {
	std::vector<mask_ref>                                   tilesets;
	std::unordered_map<type::segment_id, collision_segment> segments;

	bool blocked(const pixel_box &box) const noexcept;
};

/* ------------------------------------------------------------------------ */

struct programmable_layer : map_layer {
	/* TODO */
};

/* ======================================================================== */
/* utility stuff                                                            */

namespace map_tools {

union seg_pos {
	type::segment_id hash;
	struct {
		int16_t x;
		int16_t y;
	};

	inline seg_pos() noexcept {}
	inline seg_pos(type::segment_id id) noexcept : hash(id) {}
	inline seg_pos(int16_t x_, int16_t y_) noexcept : x(x_), y(y_) {}
};

EXPORT void add_tile_sprite(graphics::vertex_buffer::data &vbd,
                            const crpg::math::vec3        &p1,
                            const crpg::math::vec3        &p4,
                            const crpg::math::vec2        &uv1,
                            const crpg::math::vec2        &uv4) noexcept;

EXPORT graphics::vertex_buffer::data generate_tileset_seg(const crpg::segment &seg,
                                                          uint32_t             cx,
                                                          uint32_t             cy,
                                                          uint32_t             tile_width,
                                                          type::tileset_id     tileset_id) noexcept;
} // namespace map_tools

inline uint32_t segment::hash() const noexcept
{
	constexpr int      width = (int)segment::width;
	map_tools::seg_pos pos((int16_t)(x / width), (int16_t)(y / width));
	return pos.hash;
}

inline uint32_t collision_segment::hash() const noexcept
{
	constexpr int      width = (int)collision_segment::width;
	map_tools::seg_pos pos((int16_t)(x / width), (int16_t)(y / width));
	return pos.hash;
}

EXPORT map_tools::seg_pos get_map_segment_position(const math::vec2 &pos) noexcept;

static inline void apply_layer_blend(crpg::map_layer_blend blend) noexcept
{
	switch (blend) {
	case crpg::map_layer_blend::alpha:
		graphics::blend_func(graphics::blend::src_alpha, graphics::blend::inv_src_alpha);
		break;
	case crpg::map_layer_blend::add:
		graphics::blend_func(graphics::blend::one, graphics::blend::one);
		break;
	case crpg::map_layer_blend::subtract:
		graphics::blend_func(graphics::blend::one, graphics::blend::one);
		graphics::blend_equation(graphics::equation::reverse_subtract);
		break;
	case crpg::map_layer_blend::multiply:
		graphics::blend_func(graphics::blend::dst_color, graphics::blend::zero);
		break;
	}
}

static inline void apply_layer_opacity(graphics::shader_ref &shader, crpg::map_layer_blend blend, float opacity)
{
	if (blend == crpg::map_layer_blend::alpha) {
		shader->set("color", math::vec4(1.0f, 1.0f, 1.0f, opacity));

	} else if (blend != crpg::map_layer_blend::multiply) {
		shader->set("color", math::vec4(opacity, opacity, opacity, opacity));

	} else {
		/* can't apply opacity to a multiplication blend */
		shader->set("color", math::vec4(1.0f, 1.0f, 1.0f, 1.0f));
	}
}

/* ======================================================================== */

} // namespace crpg
