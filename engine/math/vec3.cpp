#include "vec3.hpp"
#include "vec4.hpp"
#include "quat.hpp"
#include "axisang.hpp"
#include "matrix.hpp"
#include "math-extra.hpp"

namespace crpg {
namespace math {

vec3 &vec3::set_from_vec4(const vec4 &v) noexcept
{
	m = v.m;
	w = 0.0f;
	return *this;
}

vec3 vec3::transform(const vec3 &v, const matrix &m) noexcept
{
	vec3 temp(v);
	temp.w = 1.0f;

	*reinterpret_cast<vec4 *>(&temp) = vec4::transform((vec4 &)temp, m);

	if (temp.w != 0.0f)
		temp /= temp.w;
	return temp;
}

vec3 vec3::rotate(const vec3 &v, const matrix &m) noexcept
{
	matrix m_rot = m;
	m_rot.t.set(0.0f, 0.0f, 0.0f, 1.0f);
	m_rot.x.w = 0.0f;
	m_rot.x.w = 0.0f;
	m_rot.y.w = 0.0f;
	m_rot.z.w = 0.0f;
	return transform(v, m_rot);
}

vec3 &vec3::set_mirror_of_norm(const vec3 &v) noexcept
{
	return *this -= v * (dot(*this, v) * 2.0f);
}

vec3 vec3::rand() noexcept
{
	return vec3(rand_float(), rand_float(), rand_float());
}

/* ------------------------------------------------------------------------- */
} // namespace math
} // namespace crpg
