#pragma once

#include "../export.h"
#include "vec3.hpp"
#include "vec4.hpp"
#include "axisang.hpp"

/* 4x4 Matrix */

namespace crpg {
namespace math {

struct EXPORT matrix {
	struct vec4 x, y, z, t;

	inline matrix() noexcept {}
	matrix(const vec4 &x, const vec4 &y, const vec4 &z, const vec4 &t) noexcept;
	explicit inline matrix(const quat &q) noexcept { set_from_quat(q); }
	explicit inline matrix(const axisang &aa) noexcept { set_from_axisang(aa); }

	inline matrix &set_identity() noexcept
	{
		*this = _base_ident;
		return *this;
	}

	static inline const matrix &identity() noexcept { return _base_ident; }

	matrix &set_from_quat(const quat &q) noexcept;
	matrix &set_from_axisang(const axisang &aa) noexcept;

	static inline matrix from_quat(const quat &q) noexcept { return matrix().set_from_quat(q); }
	static inline matrix from_axisang(const axisang &aa) noexcept { return matrix().set_from_axisang(aa); }

	matrix         operator*(const matrix &m) const noexcept;
	inline matrix &operator*=(const matrix &m) noexcept { return *this = *this * m; }

	float determinant() const noexcept;

	static matrix translate(const matrix &m, const vec3 &v) noexcept;
	static matrix translate(const matrix &m, const vec4 &v) noexcept;
	static matrix rotate(const matrix &m, const quat &q) noexcept;
	static matrix rotate(const matrix &m, const axisang &aa) noexcept;
	static matrix scale(const matrix &m, const vec3 &v) noexcept;

	inline matrix &translate(const vec3 &v) noexcept { return *this = translate(*this, v); }
	inline matrix &translate(const vec4 &v) noexcept { return *this = translate(*this, v); }
	inline matrix &rotate(const quat &q) noexcept { return *this = rotate(*this, q); }
	inline matrix &rotate(const axisang &aa) noexcept { return *this = rotate(*this, aa); }
	inline matrix &scale(const vec3 &v) noexcept { return *this = scale(*this, v); }

	static matrix translate_i(const matrix &m, const vec3 &v) noexcept;
	static matrix translate_i(const matrix &m, const vec4 &v) noexcept;
	static matrix rotate_i(const matrix &m, const quat &q) noexcept;
	static matrix rotate_i(const matrix &m, const axisang &aa) noexcept;
	static matrix scale_i(const matrix &m, const vec3 &v) noexcept;

	inline matrix &translate_i(const vec3 &v) noexcept { return *this = translate_i(*this, v); }
	inline matrix &translate_i(const vec4 &v) noexcept { return *this = translate_i(*this, v); }
	inline matrix &rotate_i(const quat &q) noexcept { return *this = rotate_i(*this, q); }
	inline matrix &rotate_i(const axisang &aa) noexcept { return *this = rotate_i(*this, aa); }
	inline matrix &scale_i(const vec3 &v) noexcept { return *this = scale_i(*this, v); }

	static matrix invert(const matrix &m) noexcept;
	static matrix transpose(const matrix &m) noexcept;

	inline matrix &invert() noexcept { return *this = invert(*this); }
	inline matrix &transpose() noexcept { return *this = transpose(*this); }

	inline matrix inverted() const noexcept { return invert(*this); }
	inline matrix transposed() const noexcept { return transpose(*this); }

	static inline matrix translated(const matrix &m, float x, float y, float z = 0.0f) noexcept
	{
		return translate(m, vec3(x, y, z));
	}

	static inline matrix rotated(const matrix &m, float x, float y, float z, float rot) noexcept
	{
		return rotate(m, axisang(x, y, z, rot));
	}

	static inline matrix scaled(const matrix &m, float x, float y, float z = 1.0f) noexcept
	{
		return scale(m, vec3(x, y, z));
	};

	inline matrix &translate(float x, float y, float z = 0.0f) noexcept { return translate(vec3(x, y, z)); }
	inline matrix &rotate(float x, float y, float z, float rot) noexcept { return rotate(axisang(x, y, z, rot)); }
	inline matrix &scale(float x, float y, float z = 1.0f) noexcept { return scale(vec3(x, y, z)); }

	inline matrix &translate_i(float x, float y, float z = 0.0f) noexcept { return translate_i(vec3(x, y, z)); }
	inline matrix &rotate_i(float x, float y, float z, float rot) noexcept
	{
		return rotate_i(axisang(x, y, z, rot));
	}
	inline matrix &scale_i(float x, float y, float z = 1.0f) noexcept { return scale_i(vec3(x, y, z)); }

	matrix &set_ortho(float left, float right, float top, float bottom, float znear, float zfar) noexcept;
	matrix &set_frustum(float left, float right, float top, float bottom, float znear, float zfar) noexcept;

	inline matrix &set_perspective(float fov, float aspect, float znear, float zfar) noexcept
	{
		float ymax = znear * tanf(math::to_rad(fov) * 0.5f);
		float ymin = -ymax;

		float xmin = ymin * aspect;
		float xmax = ymax * aspect;

		return set_frustum(xmin, xmax, ymin, ymax, znear, zfar);
	}

	static inline matrix ortho(float left, float right, float top, float bottom, float znear, float zfar) noexcept
	{
		return matrix().set_ortho(left, right, top, bottom, znear, zfar);
	}
	static inline matrix frustum(float left, float right, float top, float bottom, float znear, float zfar) noexcept
	{
		return matrix().set_frustum(left, right, top, bottom, znear, zfar);
	}
	static inline matrix perspective(float fov, float aspect, float znear, float zfar) noexcept
	{
		return matrix().set_perspective(fov, aspect, znear, zfar);
	}

private:
	static matrix _base_ident;
};

/* ------------------------------------------------------------------------- */
} // namespace math
} // namespace crpg
