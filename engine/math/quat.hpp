#pragma once

#include "../export.h"
#include "math-defs.hpp"
#include "vec3.hpp"
#include <xmmintrin.h>

namespace crpg {
namespace math {

struct matrix;
struct axisang;

struct EXPORT quat {
	union {
		struct {
			float x, y, z, w;
		};
		float  ptr[4];
		__m128 m;
	};

	inline quat() noexcept {}
	inline quat(const quat &q) noexcept : m(q.m) {}
	inline quat(float x_, float y_, float z_, float w_) noexcept : m(_mm_set_ps(w_, z_, y_, x_)) {}

	explicit inline quat(__m128 m_) noexcept : m(m_) {}
	explicit inline quat(const matrix &m4) noexcept { set_from_matrix(m4); }

	inline quat(const axisang &aa) noexcept { set_from_axisang(aa); }

	inline quat &set_identity() noexcept
	{
		m = _mm_set_ps(1.0f, 0.0f, 0.0f, 0.0f);
		return *this;
	}

	static inline quat identity() noexcept { return quat().set_identity(); }

	inline quat &set(float x_, float y_, float z_, float w_) noexcept
	{
		m = _mm_set_ps(w_, z_, y_, x_);
		return *this;
	}

	inline quat &operator=(const quat &q) noexcept
	{
		m = q.m;
		return *this;
	}

	inline quat &operator+=(const quat &q) noexcept
	{
		m = _mm_add_ps(m, q.m);
		return *this;
	}

	inline quat &operator-=(const quat &q) noexcept
	{
		m = _mm_sub_ps(m, q.m);
		return *this;
	}

	quat &operator*=(const quat &q) noexcept;

	inline quat &operator+=(float f) noexcept
	{
		m = _mm_add_ps(m, _mm_set1_ps(f));
		return *this;
	}

	inline quat &operator-=(float f) noexcept
	{
		m = _mm_sub_ps(m, _mm_set1_ps(f));
		return *this;
	}

	inline quat &operator*=(float f) noexcept
	{
		m = _mm_mul_ps(m, _mm_set1_ps(f));
		return *this;
	}

	inline quat &operator/=(float f) noexcept
	{
		m = _mm_div_ps(m, _mm_set1_ps(f));
		return *this;
	}

	inline quat operator+(const quat &v) const noexcept { return quat(*this) += v; }
	inline quat operator-(const quat &v) const noexcept { return quat(*this) -= v; }
	inline quat operator*(const quat &v) const noexcept { return quat(*this) *= v; }
	inline quat operator+(float f) const noexcept { return quat(*this) += f; }
	inline quat operator-(float f) const noexcept { return quat(*this) -= f; }
	inline quat operator*(float f) const noexcept { return quat(*this) *= f; }
	inline quat operator/(float f) const noexcept { return quat(*this) /= f; }

	static inline float dot(const quat &q1, const quat &q2) noexcept
	{
		vec3   add;
		__m128 mul = _mm_mul_ps(q1.m, q2.m);
		add.m      = _mm_add_ps(_mm_movehl_ps(mul, mul), mul);
		add.m      = _mm_add_ps(_mm_shuffle_ps(add.m, add.m, 0x55), add.m);
		return add.x;
	}

	inline quat &set_inv() noexcept
	{
		x = -x;
		y = -y;
		z = -z;
		return *this;
	}

	inline quat inv() const noexcept { return quat(-x, -y, -z, w); }

	inline quat neg() const noexcept { return quat(-x, -y, -z, -w); }

	inline float len() const noexcept
	{
		float dot_val = dot(*this, *this);
		return (dot_val > 0.0f) ? sqrtf(dot_val) : 0.0f;
	}

	static inline float dist(const quat &q1, const quat &q2) noexcept
	{
		quat  temp    = q1 - q2;
		float dot_val = dot(temp, temp);
		return (dot_val > 0.0f) ? sqrtf(dot_val) : 0.0f;
	}

	inline quat &norm() noexcept
	{
		float dot_val = dot(*this, *this);
		m             = (dot_val > 0.0f) ? _mm_mul_ps(m, _mm_set1_ps(1.0f / sqrtf(dot_val))) : _mm_setzero_ps();
		return *this;
	}

	static inline quat norm(const quat &q) noexcept { return quat(q).norm(); }

	static inline bool close(const quat &q1, const quat &q2, float precision = math::epsilon) noexcept
	{
		quat test = q1 - q2;
		return fabsf(test.x) < epsilon && fabsf(test.y) < precision && fabsf(test.z) < precision &&
		       fabsf(test.w) < precision;
	}

	quat &set_from_axisang(const axisang &aa) noexcept;
	quat &set_from_matrix(const matrix &m) noexcept;

	static inline quat from_axisang(const axisang &aa) noexcept { return quat().set_from_axisang(aa); }
	static inline quat from_matrix(const matrix &m) noexcept { return quat().set_from_matrix(m); }

	vec3  dir() const noexcept;
	quat &set_from_dir(const vec3 &dir) noexcept;

	static inline quat from_dir(const vec3 &dir) noexcept { return quat().set_from_dir(dir); }

	static quat log(const quat &q) noexcept;
	static quat exp(const quat &q) noexcept;

	static quat tangent(const quat &prev, const quat &q, const quat &next) noexcept;

	static quat interpolate(const quat &q1, const quat &q2, float t) noexcept;
	static quat interpolate_cubic(const quat &q1, const quat &q2, const quat &m1, const quat &m2, float t) noexcept;
};

/* ------------------------------------------------------------------------- */
} // namespace math
} // namespace crpg
