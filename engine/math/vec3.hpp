#pragma once

#include "../export.h"
#include "math-defs.hpp"
#include <xmmintrin.h>

namespace crpg {
namespace math {

struct matrix;
struct quat;
struct vec4;

struct EXPORT vec3 {
	union {
		struct {
			float x, y, z, w;
		};
		float  ptr[4];
		__m128 m;
	};

	inline vec3() noexcept {}
	inline vec3(float x_, float y_, float z_ = 0.0f) noexcept : m(_mm_set_ps(0.0f, z_, y_, x_)) {}

	explicit inline vec3(__m128 m_) noexcept : m(m_) {}
	explicit inline vec3(const vec4 &v4) noexcept { set_from_vec4(v4); }

	inline vec3 &set_zero() noexcept
	{
		m = _mm_setzero_ps();
		return *this;
	}

	static inline vec3 zero() noexcept { return vec3().set_zero(); }

	inline vec3 &set(float x_, float y_, float z_ = 0.0f) noexcept
	{
		m = _mm_set_ps(0.0f, z_, y_, x_);
		return *this;
	}

	vec3 &set_from_vec4(const vec4 &v) noexcept;

	static inline vec3 from_vec4(const vec4 &v) noexcept { return vec3().set_from_vec4(v); }

	inline vec3 &operator+=(const vec3 &v) noexcept
	{
		m = _mm_add_ps(m, v.m);
		return *this;
	}

	inline vec3 &operator-=(const vec3 &v) noexcept
	{
		m = _mm_sub_ps(m, v.m);
		return *this;
	}

	inline vec3 &operator*=(const vec3 &v) noexcept
	{
		m = _mm_mul_ps(m, v.m);
		return *this;
	}

	inline vec3 &operator/=(const vec3 &v) noexcept
	{
		m = _mm_div_ps(m, v.m);
		w = 0.0f;
		return *this;
	}

	inline vec3 &operator+=(float f) noexcept
	{
		m = _mm_add_ps(m, _mm_set1_ps(f));
		w = 0.0f;
		return *this;
	}

	inline vec3 &operator-=(float f) noexcept
	{
		m = _mm_sub_ps(m, _mm_set1_ps(f));
		w = 0.0f;
		return *this;
	}

	inline vec3 &operator*=(float f) noexcept
	{
		m = _mm_mul_ps(m, _mm_set1_ps(f));
		return *this;
	}

	inline vec3 &operator/=(float f) noexcept
	{
		m = _mm_div_ps(m, _mm_set1_ps(f));
		w = 0.0f;
		return *this;
	}

	inline vec3 operator+(const vec3 &v) const noexcept { return vec3(*this) += v; }
	inline vec3 operator-(const vec3 &v) const noexcept { return vec3(*this) -= v; }
	inline vec3 operator*(const vec3 &v) const noexcept { return vec3(*this) *= v; }
	inline vec3 operator/(const vec3 &v) const noexcept { return vec3(*this) /= v; }
	inline vec3 operator+(float f) const noexcept { return vec3(*this) += f; }
	inline vec3 operator-(float f) const noexcept { return vec3(*this) -= f; }
	inline vec3 operator*(float f) const noexcept { return vec3(*this) *= f; }
	inline vec3 operator/(float f) const noexcept { return vec3(*this) /= f; }

	static inline float dot(const vec3 &v1, const vec3 &v2) noexcept
	{
		vec3   add;
		__m128 mul = _mm_mul_ps(v1.m, v2.m);
		add.m      = _mm_add_ps(_mm_movehl_ps(mul, mul), mul);
		add.m      = _mm_add_ps(_mm_shuffle_ps(add.m, add.m, 0x55), add.m);
		return add.x;
	}

	static inline vec3 cross(const vec3 &v1, const vec3 &v2) noexcept
	{
		__m128 s1v1 = _mm_shuffle_ps(v1.m, v1.m, _MM_SHUFFLE(3, 0, 2, 1));
		__m128 s1v2 = _mm_shuffle_ps(v2.m, v2.m, _MM_SHUFFLE(3, 1, 0, 2));
		__m128 s2v1 = _mm_shuffle_ps(v1.m, v1.m, _MM_SHUFFLE(3, 1, 0, 2));
		__m128 s2v2 = _mm_shuffle_ps(v2.m, v2.m, _MM_SHUFFLE(3, 0, 2, 1));
		return vec3(_mm_sub_ps(_mm_mul_ps(s1v1, s1v2), _mm_mul_ps(s2v1, s2v2)));
	}

	inline vec3 operator-() const noexcept { return vec3(-x, -y, -z); }

	inline float len() const noexcept
	{
		float dot_val = dot(*this, *this);
		return (dot_val > 0.0f) ? sqrtf(dot_val) : 0.0f;
	}

	static inline float len(const vec3 &v) noexcept { return v.len(); }

	static inline float dist(const vec3 &v1, const vec3 &v2) noexcept
	{
		vec3  temp    = v1 - v2;
		float dot_val = dot(temp, temp);
		return (dot_val > 0.0f) ? sqrtf(dot_val) : 0.0f;
	}

	inline vec3 &set_norm() noexcept
	{
		float dot_val = dot(*this, *this);
		m             = (dot_val > 0.0f) ? _mm_mul_ps(m, _mm_set1_ps(1.0f / sqrtf(dot_val))) : _mm_setzero_ps();
		return *this;
	}

	inline vec3 norm() const noexcept { return vec3(*this).set_norm(); }

	static inline vec3 norm(const vec3 &v) noexcept { return v.norm(); }

	static inline int close(const vec3 &v1, const vec3 &v2, float precision = math::epsilon) noexcept
	{
		vec3 test = (v1 - v2).abs();
		return test.x < precision && test.y < precision && test.z < precision;
	}

	static inline vec3 min(const vec3 &v1, const vec3 &v2) noexcept
	{
		vec3 temp;
		temp.m = _mm_min_ps(v1.m, v2.m);
		return temp;
	}

	static inline vec3 min(const vec3 &v, float f) noexcept
	{
		vec3 temp;
		temp.m = _mm_min_ps(v.m, _mm_set1_ps(f));
		return temp;
	}

	static inline vec3 max(const vec3 &v1, const vec3 &v2) noexcept
	{
		vec3 temp;
		temp.m = _mm_max_ps(v1.m, v2.m);
		return temp;
	}

	static inline vec3 max(const vec3 &v, float f) noexcept
	{
		vec3 temp;
		temp.m = _mm_max_ps(v.m, _mm_set1_ps(f));
		return temp;
	}

	inline vec3 &abs() noexcept { return set(fabsf(x), fabsf(y), fabsf(z)); }
	inline vec3 &floor() noexcept { return set(floorf(x), floorf(y), floorf(z)); }
	inline vec3 &ceil() noexcept { return set(ceilf(x), ceilf(y), ceilf(z)); }
	inline vec3 &round() noexcept { return set(roundf(x), roundf(y), roundf(z)); }

	static inline vec3 abs(const vec3 &v) noexcept { return vec3(v).abs(); }
	static inline vec3 ceil(const vec3 &v) noexcept { return vec3(v).ceil(); }
	static inline vec3 floor(const vec3 &v) noexcept { return vec3(v).floor(); }
	static inline vec3 round(const vec3 &v) noexcept { return vec3(v).round(); }

	static vec3  transform(const vec3 &v, const matrix &m) noexcept;
	inline vec3  transformeded(const matrix &m) const noexcept { return transform(*this, m); }
	inline vec3 &transform(const matrix &m) noexcept { return *this = transform(*this, m); }

	static vec3  rotate(const vec3 &v, const matrix &m) noexcept;
	inline vec3  rotated(const matrix &m) const noexcept { return rotate(*this, m); }
	inline vec3 &rotate(const matrix &m) noexcept { return *this = rotate(*this, m); }

	vec3 &set_mirror_of_norm(const vec3 &norm) noexcept;

	inline vec3 mirror_of_norm(const vec3 &n) const noexcept { return vec3(*this).set_mirror_of_norm(n); }

	static inline vec3 mirror_of_norm(const vec3 &v, const vec3 &n) noexcept { return v.mirror_of_norm(n); }

	static vec3 rand() noexcept;
};

using point3 = vec3;
using norm3  = vec3;

/* ------------------------------------------------------------------------- */
} // namespace math
} // namespace crpg
