#pragma once

#include "../export.h"

namespace crpg {
namespace math {

struct quat;

struct EXPORT axisang {
	union {
		struct {
			float x, y, z, w;
		};
		float ptr[4];
	};

	inline axisang() noexcept {}
	inline axisang(float x_, float y_, float z_, float w_) noexcept : x(x_), y(y_), z(z_), w(w_) {}

	explicit inline axisang(const quat &q) noexcept { set_from_quat(q); }

	inline axisang &operator=(const axisang &aa) noexcept
	{
		x = aa.x;
		y = aa.y;
		z = aa.z;
		w = aa.w;
		return *this;
	}

	inline axisang &set_zero() noexcept
	{
		x = 0.0f;
		y = 0.0f;
		z = 0.0f;
		w = 0.0f;
		return *this;
	}

	static inline axisang zero() noexcept { return axisang(0.0f, 0.0f, 0.0f, 0.0f); }

	inline axisang &set(float x_, float y_, float z_, float w_) noexcept
	{
		x = x_;
		y = y_;
		z = z_;
		w = w_;
		return *this;
	}

	axisang              &set_from_quat(const quat &q) noexcept;
	static inline axisang from_quat(const quat &q) noexcept { return axisang().set_from_quat(q); }
};

/* ------------------------------------------------------------------------- */
} // namespace math
} // namespace crpg
