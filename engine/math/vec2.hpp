#pragma once

#include <math.h>
#include "../export.h"
#include "math-defs.hpp"

namespace crpg {
namespace math {

struct matrix;
struct plane;

struct EXPORT vec2 {
	union {
		struct {
			float x, y;
		};
		float ptr[2];
	};

	constexpr vec2() noexcept {}
	constexpr vec2(float x_, float y_) noexcept : x(x_), y(y_) {}

	constexpr vec2 &set_zero() noexcept
	{
		x = 0.0f;
		y = 0.0f;
		return *this;
	}

	static constexpr vec2 zero() noexcept { return vec2(0.0f, 0.0f); }

	constexpr vec2 &set(float x_, float y_) noexcept
	{
		x = x_;
		y = y_;
		return *this;
	}

	constexpr vec2 &operator+=(const vec2 &v2) noexcept { return set(x + v2.x, y + v2.y); }
	constexpr vec2 &operator-=(const vec2 &v2) noexcept { return set(x - v2.x, y - v2.y); }
	constexpr vec2 &operator*=(const vec2 &v2) noexcept { return set(x * v2.x, y * v2.y); }
	constexpr vec2 &operator/=(const vec2 &v2) noexcept { return set(x / v2.x, y / v2.y); }
	constexpr vec2 &operator+=(float f) noexcept { return set(x + f, y + f); }
	constexpr vec2 &operator-=(float f) noexcept { return set(x - f, y - f); }
	constexpr vec2 &operator*=(float f) noexcept { return set(x * f, y * f); }
	constexpr vec2 &operator/=(float f) noexcept { return set(x / f, y / f); }

	constexpr vec2 operator+(const vec2 &v) const noexcept { return vec2(*this) += v; }
	constexpr vec2 operator-(const vec2 &v) const noexcept { return vec2(*this) -= v; }
	constexpr vec2 operator*(const vec2 &v) const noexcept { return vec2(*this) *= v; }
	constexpr vec2 operator/(const vec2 &v) const noexcept { return vec2(*this) /= v; }
	constexpr vec2 operator+(float f) const noexcept { return vec2(*this) += f; }
	constexpr vec2 operator-(float f) const noexcept { return vec2(*this) -= f; }
	constexpr vec2 operator*(float f) const noexcept { return vec2(*this) *= f; }
	constexpr vec2 operator/(float f) const noexcept { return vec2(*this) /= f; }

	constexpr vec2 operator-() const noexcept { return vec2().set(-x, -y); }

	constexpr bool operator==(const vec2 &v) const noexcept { return x == v.x && y == v.y; }
	constexpr bool operator!=(const vec2 &v) const noexcept { return x != v.x || y != v.y; }

	constexpr float dot(const vec2 &v) const noexcept { return x * v.x + y * v.y; }

	static constexpr float dot(const vec2 &v1, const vec2 &v2) noexcept { return v1.x * v2.x + v1.y * v2.y; }

	inline float len() const noexcept { return sqrtf(dot(*this, *this)); }

	constexpr float dist(const plane &pl) const noexcept;
	inline float    dist(const vec2 &p) const noexcept { return (*this - p).len(); }

	static inline float    len(const vec2 &v) noexcept { return v.len(); }
	static constexpr float dist(const vec2 &v, const plane &pl) noexcept;
	static inline float    dist(const vec2 &v1, const vec2 &v2) noexcept { return (v1 - v2).len(); }

	static constexpr vec2 min(const vec2 &v, float f) noexcept { return vec2(fminf(v.x, f), fminf(v.y, f)); }
	static constexpr vec2 min(const vec2 &v1, const vec2 &v2) noexcept
	{
		return vec2(fminf(v1.x, v2.x), fminf(v1.y, v2.y));
	}
	static constexpr vec2 max(const vec2 &v, float f) noexcept { return vec2(fmaxf(v.x, f), fmaxf(v.y, f)); }
	static constexpr vec2 max(const vec2 &v1, const vec2 &v2) noexcept
	{
		return vec2(fmaxf(v1.x, v2.x), fmaxf(v1.y, v2.y));
	}

	constexpr vec2 &set_abs() noexcept { return set(fabsf(x), fabsf(y)); }
	constexpr vec2 &set_ceil() noexcept { return set(ceilf(x), ceilf(y)); }
	constexpr vec2 &set_floor() noexcept { return set(floorf(x), floorf(y)); }
	constexpr vec2 &set_round() noexcept { return set(roundf(x), roundf(y)); }

	inline vec2 &set_norm() noexcept
	{
		float l = len();
		if (l > 0.0f)
			*this *= (1.0f / l);
		return *this;
	}

	constexpr vec2 abs() const noexcept { return vec2(*this).set_abs(); }
	constexpr vec2 ceil() const noexcept { return vec2(*this).set_ceil(); }
	constexpr vec2 floor() const noexcept { return vec2(*this).set_floor(); }
	constexpr vec2 round() const noexcept { return vec2(*this).set_round(); }
	inline vec2    norm() const noexcept { return vec2(*this).set_norm(); }

	static constexpr vec2 abs(struct vec2 &v) noexcept { return vec2(v).set_abs(); }
	static constexpr vec2 ceil(struct vec2 &v) noexcept { return vec2(v).set_ceil(); }
	static constexpr vec2 floor(struct vec2 &v) noexcept { return vec2(v).set_floor(); }
	static inline vec2    norm(const vec2 &v) noexcept { return vec2(v).set_norm(); }

	constexpr vec2 cross() const noexcept { return vec2(y, -x); }
	constexpr vec2 cross_ccw() const noexcept { return cross(); }
	constexpr vec2 cross_cw() const noexcept { return vec2(-y, x); }

	static constexpr bool close(const vec2 &v1, const vec2 &v2, float epsilon = math::epsilon) noexcept
	{
		return close_float(v1.x, v2.x, epsilon) && close_float(v1.y, v2.y, epsilon);
	}
	constexpr bool close(const vec2 &v2, float precision = math::epsilon) const noexcept
	{
		return close(*this, v2, precision);
	}

	static vec2  transform(const vec2 &v, const matrix &m) noexcept;
	inline vec2  transformeded(const matrix &m) const noexcept { return transform(*this, m); }
	inline vec2 &transform(const matrix &m) noexcept { return *this = transform(*this, m); }

	inline vec2 rotate_cw(float rad) const noexcept
	{
		vec2 out;
		vec2 mul(cosf(-rad), sinf(-rad));
		out.x = dot(mul);
		out.y = dot(mul.cross_cw());
		return out;
	}
};

using point2 = vec2;

/* ------------------------------------------------------------------------- */
} // namespace math
} // namespace crpg
