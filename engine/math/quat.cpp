#include "quat.hpp"
#include "vec3.hpp"
#include "matrix.hpp"
#include "axisang.hpp"

namespace crpg {
namespace math {

static inline vec3 quat_vec3(const quat &q) noexcept
{
	vec3 v;
	v.m = q.m;
	v.w = 0.0f;
	return v;
}

quat &quat::operator*=(const quat &q) noexcept
{
	vec3 q1axis = quat_vec3(*this);
	vec3 q2axis = quat_vec3(q);

	vec3 temp1 = q2axis * w;
	vec3 temp2 = q1axis * q.w;
	temp1 += temp2;
	temp2 = vec3::cross(q1axis, q2axis);

	temp1 += temp2;
	temp1.w = (w * q.w) - vec3::dot(q1axis, q2axis);
	m       = temp1.m;
	return *this;
}

quat &quat::set_from_axisang(const axisang &aa) noexcept
{
	float halfa = aa.w * 0.5f;
	float sine  = sinf(halfa);

	set(aa.x * sine, aa.y * sine, aa.z * sine, cosf(halfa));
	return *this;
}

struct f4x4 {
	float ptr[4][4];
};

quat &quat::set_from_matrix(const matrix &m) noexcept
{
	float tr = (m.x.x + m.y.y + m.z.z);
	float inv_half;
	float four_d;
	int   i, j, k;

	if (tr > 0.0f) {
		four_d = sqrtf(tr + 1.0f);

		inv_half = 0.5f / four_d;

		set((m.y.z - m.z.y) * inv_half, (m.z.x - m.x.z) * inv_half, (m.x.y - m.y.x) * inv_half, four_d * 0.5f);
	} else {
		f4x4 &val = (f4x4 &)m;

		i = (m.x.x > m.y.y) ? 0 : 1;

		if (m.z.z > val.ptr[i][i])
			i = 2;

		j = (i + 1) % 3;
		k = (i + 2) % 3;

		/* ---------------------------------- */

		four_d   = sqrtf((val.ptr[i][i] - val.ptr[j][j] - val.ptr[k][k]) + 1.0f);
		inv_half = 0.5f / four_d;

		ptr[i] = four_d * 0.5f;
		ptr[j] = (val.ptr[i][j] + val.ptr[j][i]) * inv_half;
		ptr[k] = (val.ptr[i][k] + val.ptr[k][i]) * inv_half;
		w      = (val.ptr[j][k] - val.ptr[k][j]) * inv_half;
	}

	return *this;
}

vec3 quat::dir() const noexcept
{
	return vec3(matrix(*this).z);
}

quat &quat::set_from_dir(const vec3 &d) noexcept
{
	vec3    new_dir = -d.norm();
	quat    xz_rot  = identity();
	quat    yz_rot  = identity();
	axisang aa;

	bool xz_valid = !close_float(new_dir.x, 0.0f) || !close_float(new_dir.z, 0.0f);
	bool yz_valid = !close_float(new_dir.y, 0.0f);

	if (xz_valid)
		xz_rot.set_from_axisang(axisang(0.0f, 1.0f, 0.0f, atan2f(new_dir.x, new_dir.z)));
	if (yz_valid)
		yz_rot.set_from_axisang(axisang(-1.0f, 0.0f, 0.0f, asinf(new_dir.y)));

	if (!xz_valid)
		*this = yz_rot;
	else if (!yz_valid)
		*this = xz_rot;
	else
		*this = xz_rot * yz_rot;
	return *this;
}

quat quat::log(const quat &q) noexcept
{
	quat  out   = q;
	float angle = acosf(q.w);
	float sine  = sinf(angle);
	float w     = q.w;

	out   = q;
	out.w = 0.0f;

	if (fabsf(w) < 1.0f && fabsf(sine) >= math::epsilon) {
		sine = angle / sine;
		out *= sine;
	}

	return out;
}

quat quat::exp(const quat &q) noexcept
{
	quat  out    = q;
	float length = sqrtf(q.x * q.x + q.y * q.y + q.z * q.z);
	float sine   = sinf(length);

	sine = (length > math::epsilon) ? (sine / length) : 1.0f;
	out *= sine;
	out.w = cosf(length);
	return out;
}

quat quat::tangent(const quat &prev, const quat &q, const quat &next) noexcept
{
	return (q - prev + next - q) * 0.5f;
}

quat quat::interpolate(const quat &q1, const quat &q2, float t) noexcept
{
	float dot_val = dot(q1, q2);
	float anglef  = acosf(dot_val);
	float sine, sinei, sinet, sineti;
	quat  temp, out;

	if (anglef >= math::epsilon) {
		sine   = sinf(anglef);
		sinei  = 1.0f / sine;
		sinet  = sinf(anglef * t) * sinei;
		sineti = sinf(anglef * (1.0f - t)) * sinei;

		temp = q1 * sineti;
		out  = q2 * sinet;
		out += temp;
	} else {
		temp = q2 - q1;
		temp *= t;
		out = temp + q1;
	}

	return out;
}

quat quat::interpolate_cubic(const quat &q1, const quat &q2, const quat &m1, const quat &m2, float t) noexcept
{
	quat temp1 = interpolate(q1, q2, t);
	quat temp2 = interpolate(m1, m2, t);
	return interpolate(temp1, temp2, 2.0f * (1.0f - t) * t);
}

/* ------------------------------------------------------------------------- */
} // namespace math
} // namespace crpg
