#pragma once

#include "../export.h"
#include "math-defs.hpp"

namespace crpg {
namespace math {

struct plane;
struct vec3;
struct vec2;
struct box;

EXPORT vec3 polar_to_cart(const vec3 &v) noexcept;
EXPORT vec3 cart_to_polar(const vec3 &v) noexcept;

EXPORT vec3 norm_to_polar(const vec3 &norm) noexcept;
EXPORT vec3 polar_to_norm(const vec2 &polar) noexcept;

EXPORT float calc_smoothing(float val1,
                            float val2,
                            float smoothing,
                            float min_adjust,
                            float t,
                            float precision = epsilon) noexcept;
EXPORT vec2  calc_smoothing(const vec2 &v1,
                            const vec2 &v2,
                            float       smoothing,
                            float       min_adjust,
                            float       t,
                            float       precision = epsilon) noexcept;

static constexpr float get_percentage(float start, float end, float mid) noexcept
{
	return (mid - start) / (end - start);
}

static constexpr float get_percentage(int start, int end, int mid) noexcept
{
	return (float)(mid - start) / (float)(end - start);
}

template<typename T> static constexpr T lerp(T a, T b, float t) noexcept
{
	return (b - a) * t + a;
}

static constexpr float cubic_ease(float t) noexcept
{
	if (t < 0.5f) {
		return 4.0f * t * t * t;
	} else {
		float temp = 2.0f * t - 2.0f;
		return (t - 1.0f) * temp * temp + 1.0f;
	}
}

EXPORT float rand_float() noexcept;

EXPORT int box_plane_test(const vec2 &pos, const math::box &box, const math::plane &plane) noexcept;
EXPORT int circle_plane_test(const vec2 &pos, const float radius, const math::plane &plane) noexcept;

EXPORT bool ray_plane_intersection(const vec2        &ray_orig,
                                   const vec2        &ray_dir,
                                   const math::plane &plane,
                                   float             &t) noexcept;
EXPORT bool ray_ray_intersection(const vec2 &ray1_orig,
                                 const vec2 &ray1_dir,
                                 const vec2 &ray2_orig,
                                 const vec2 &ray2_dir,
                                 float      &t) noexcept;
EXPORT bool ray_line_intersection(const vec2 &ray_orig,
                                  const vec2 &ray_dir,
                                  const vec2 &line_p1,
                                  const vec2 &line_p2,
                                  float      &t) noexcept;
EXPORT bool ray_box_intersection(const vec2      &ray_orig,
                                 const vec2      &ray_dir,
                                 const math::box &box,
                                 float           &t,
                                 vec2            *norm) noexcept;
EXPORT bool ray_circle_intersection(const vec2 &ray_orig,
                                    const vec2 &ray_dir,
                                    const vec2 &circle_orig,
                                    const float circle_radius,
                                    float      &t) noexcept;

EXPORT bool line_plane_intersection(const vec2        &line_p1,
                                    const vec2        &line_p2,
                                    const math::plane &plane,
                                    float             &t) noexcept;
EXPORT bool line_line_intersection(const vec2 &line1_p1,
                                   const vec2 &line1_p2,
                                   const vec2 &line2_p1,
                                   const vec2 &line2_p2,
                                   float      &t) noexcept;
EXPORT bool line_box_intersection(const vec2      &line_p1,
                                  const vec2      &line_p2,
                                  const math::box &box,
                                  float           &t,
                                  vec2            *norm) noexcept;
EXPORT bool line_circle_intersection(const vec2 &line_p1,
                                     const vec2 &line_p2,
                                     const vec2 &circle_orig,
                                     const float circle_radius,
                                     float      &t) noexcept;

EXPORT bool box_plane_intersection(const vec2        &box_p1,
                                   const vec2        &box_p2,
                                   const box         &box,
                                   const math::plane &plane,
                                   float             &t) noexcept;
EXPORT bool box_point_intersection(const vec2 &box_p1,
                                   const vec2 &box_p2,
                                   const box  &box,
                                   const vec2 &p,
                                   float      &t,
                                   vec2       *norm) noexcept;
EXPORT bool box_box_intersection(const vec2 &box1_p1,
                                 const vec2 &box1_p2,
                                 const box  &box1,
                                 const box  &box2,
                                 float      &t,
                                 vec2       *norm) noexcept;
EXPORT bool box_circle_intersection(const vec2 &box_p1,
                                    const vec2 &box_p2,
                                    const box  &box,
                                    const vec2 &circle_orig,
                                    const float circle_radius,
                                    float      &t,
                                    vec2       *norm) noexcept;

EXPORT bool circle_plane_intersection(const vec2        &circle_p1,
                                      const vec2        &circle_p2,
                                      const float        circle_radius,
                                      const math::plane &plane,
                                      float             &t) noexcept;
EXPORT bool circle_point_intersection(const vec2 &circle_p1,
                                      const vec2 &circle_p2,
                                      const float circle_radius,
                                      const vec2 &p,
                                      float      &t) noexcept;
EXPORT bool circle_box_intersection(const vec2      &circle_p1,
                                    const vec2      &circle_p2,
                                    const float      circle_radius,
                                    const math::box &box,
                                    float           &t,
                                    vec2            *norm) noexcept;
EXPORT bool circle_circle_intersection(const vec2 &circle1_p1,
                                       const vec2 &circle1_p2,
                                       const float circle1_radius,
                                       const vec2 &circle2_orig,
                                       const float circle2_radius,
                                       float      &t) noexcept;

/* ------------------------------------------------------------------------- */
} // namespace math
} // namespace crpg
