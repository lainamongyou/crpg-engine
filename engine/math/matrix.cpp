#include "math-defs.hpp"
#include "matrix.hpp"
#include "quat.hpp"

namespace crpg {
namespace math {

matrix::matrix(const vec4 &x_, const vec4 &y_, const vec4 &z_, const vec4 &t_) noexcept : x(x_), y(y_), z(z_), t(t_) {}

matrix matrix::_base_ident(vec4(1.0f, 0.0f, 0.0f, 0.0f),
                           vec4(0.0f, 1.0f, 0.0f, 0.0f),
                           vec4(0.0f, 0.0f, 1.0f, 0.0f),
                           vec4(0.0f, 0.0f, 0.0f, 1.0f));

matrix &matrix::set_from_quat(const quat &q) noexcept
{
	float norm = quat::dot(q, q);
	float s    = (norm > 0.0f) ? (2.0f / norm) : 0.0f;

	float xx = q.x * q.x * s;
	float yy = q.y * q.y * s;
	float zz = q.z * q.z * s;
	float xy = q.x * q.y * s;
	float xz = q.x * q.z * s;
	float yz = q.y * q.z * s;
	float wx = q.w * q.x * s;
	float wy = q.w * q.y * s;
	float wz = q.w * q.z * s;

	x.set(1.0f - (yy + zz), xy + wz, xz - wy, 0.0f);
	y.set(xy - wz, 1.0f - (xx + zz), yz + wx, 0.0f);
	z.set(xz + wy, yz - wx, 1.0f - (xx + yy), 0.0f);
	t.set(0.0f, 0.0f, 0.0f, 1.0f);
	return *this;
}

matrix &matrix::set_from_axisang(const axisang &aa) noexcept
{
	return matrix::set_from_quat(quat(aa));
}

matrix matrix::operator*(const matrix &m) const noexcept
{
	matrix       temp;
	vec4        *out = (vec4 *)&temp;
	const vec4  *m1v = (const vec4 *)this;
	const float *m2f = (const float *)&m;
	int          i, j;

	for (i = 0; i < 4; i++) {
		for (j = 0; j < 4; j++) {
			vec4 temp2(m2f[j], m2f[j + 4], m2f[j + 8], m2f[j + 12]);
			out[i].ptr[j] = vec4::dot(m1v[i], temp2);
		}
	}

	return temp;
}

static inline void get_3x3_submatrix(float *dst, const matrix *m, int i, int j) noexcept
{
	const float *mf = (const float *)m;
	int          ti, tj, idst, jdst;

	for (ti = 0; ti < 4; ti++) {
		if (ti < i)
			idst = ti;
		else if (ti > i)
			idst = ti - 1;
		else
			continue;

		for (tj = 0; tj < 4; tj++) {
			if (tj < j)
				jdst = tj;
			else if (tj > j)
				jdst = tj - 1;
			else
				continue;

			dst[(idst * 3) + jdst] = mf[(ti * 4) + tj];
		}
	}
}

static inline float get_3x3_determinant(const float *m) noexcept
{
	return (m[0] * ((m[4] * m[8]) - (m[7] * m[5]))) - (m[1] * ((m[3] * m[8]) - (m[6] * m[5]))) +
	       (m[2] * ((m[3] * m[7]) - (m[6] * m[4])));
}

float matrix::determinant() const noexcept
{
	const float *mf = (const float *)this;
	float        det, result = 0.0f, i = 1.0f;
	float        m3x3[9];
	int          n;

	for (n = 0; n < 4; n++, i *= -1.0f) {
		get_3x3_submatrix(m3x3, this, 0, n);

		det = get_3x3_determinant(m3x3);
		result += mf[n] * det * i;
	}

	return result;
}

matrix matrix::translate(const matrix &m, const vec3 &v) noexcept
{
	matrix temp;
	temp.x.set(1.0f, 0.0f, 0.0f, 0.0f);
	temp.y.set(0.0f, 1.0f, 0.0f, 0.0f);
	temp.z.set(0.0f, 0.0f, 1.0f, 0.0f);
	temp.t.set_from_vec3(v);
	return m * temp;
}

matrix matrix::translate(const matrix &m, const vec4 &v) noexcept
{
	matrix temp;
	temp.x.set(1.0f, 0.0f, 0.0f, 0.0f);
	temp.y.set(0.0f, 1.0f, 0.0f, 0.0f);
	temp.z.set(0.0f, 0.0f, 1.0f, 0.0f);
	temp.t = v;
	return m * temp;
}

matrix matrix::rotate(const matrix &m, const quat &q) noexcept
{
	return m * matrix(q);
}

matrix matrix::rotate(const matrix &m, const axisang &aa) noexcept
{
	return m * matrix(aa);
}

matrix matrix::scale(const matrix &m, const vec3 &v) noexcept
{
	matrix temp;
	temp.x.set(v.x, 0.0f, 0.0f, 0.0f);
	temp.y.set(0.0f, v.y, 0.0f, 0.0f);
	temp.z.set(0.0f, 0.0f, v.z, 0.0f);
	temp.t.set(0.0f, 0.0f, 0.0f, 1.0f);
	return m * temp;
}

matrix matrix::translate_i(const matrix &m, const vec3 &v) noexcept
{
	matrix temp;
	temp.x.set(1.0f, 0.0f, 0.0f, 0.0f);
	temp.y.set(0.0f, 1.0f, 0.0f, 0.0f);
	temp.z.set(0.0f, 0.0f, 1.0f, 0.0f);
	temp.t.set_from_vec3(v);
	return temp * m;
}

matrix matrix::translate_i(const matrix &m, const vec4 &v) noexcept
{
	matrix temp;
	temp.x.set(1.0f, 0.0f, 0.0f, 0.0f);
	temp.y.set(0.0f, 1.0f, 0.0f, 0.0f);
	temp.z.set(0.0f, 0.0f, 1.0f, 0.0f);
	temp.t = v;
	return temp * m;
}

matrix matrix::rotate_i(const matrix &m, const quat &q) noexcept
{
	return matrix(q) * m;
}

matrix matrix::rotate_i(const matrix &m, const axisang &aa) noexcept
{
	return matrix(aa) * m;
}

matrix matrix::scale_i(const matrix &m, const vec3 &v) noexcept
{
	matrix temp;
	temp.x.set(v.x, 0.0f, 0.0f, 0.0f);
	temp.y.set(0.0f, v.y, 0.0f, 0.0f);
	temp.z.set(0.0f, 0.0f, v.z, 0.0f);
	temp.t.set(0.0f, 0.0f, 0.0f, 1.0f);
	return temp * m;
}

matrix matrix::invert(const matrix &m) noexcept
{
	matrix temp;
	vec4  *dstv;
	float  det;
	float  m3x3[9];
	int    i, j, sign;

	dstv = (vec4 *)&temp;
	det  = m.determinant();

	if (fabs(det) >= 0.0005f) {
		for (i = 0; i < 4; i++) {
			for (j = 0; j < 4; j++) {
				sign = 1 - ((i + j) % 2) * 2;
				get_3x3_submatrix(m3x3, &m, i, j);
				dstv[j].ptr[i] = get_3x3_determinant(m3x3) * (float)sign / det;
			}
		}

		return temp;
	}

	return matrix::identity();
}

matrix matrix::transpose(const matrix &m) noexcept
{
	matrix temp;
	__m128 a0 = _mm_unpacklo_ps(m.x.m, m.z.m);
	__m128 a1 = _mm_unpacklo_ps(m.y.m, m.t.m);
	__m128 a2 = _mm_unpackhi_ps(m.x.m, m.z.m);
	__m128 a3 = _mm_unpackhi_ps(m.y.m, m.t.m);

	temp.x.m = _mm_unpacklo_ps(a0, a1);
	temp.y.m = _mm_unpackhi_ps(a0, a1);
	temp.z.m = _mm_unpacklo_ps(a2, a3);
	temp.t.m = _mm_unpackhi_ps(a2, a3);
	return temp;
}

matrix &matrix::set_ortho(float left, float right, float top, float bottom, float znear, float zfar) noexcept
{
	float rml = right - left;
	float bmt = bottom - top;
	float fmn = zfar - znear;

	x.set_zero();
	y.set_zero();
	z.set_zero();
	t.set_zero();

	x.x = 2.0f / rml;
	t.x = (left + right) / -rml;

	y.y = 2.0f / -bmt;
	t.y = (bottom + top) / bmt;

	z.z = 1.0f / fmn;
	t.z = znear / -fmn;

	t.w = 1.0f;

	return *this;
}

matrix &matrix::set_frustum(float left, float right, float top, float bottom, float znear, float zfar) noexcept
{
	float rml     = right - left;
	float bmt     = bottom - top;
	float nmf     = znear - zfar;
	float znearx2 = 2.0f * znear;

	x.set_zero();
	y.set_zero();
	z.set_zero();
	t.set_zero();

	x.x = znearx2 / -rml;
	z.x = (left + right) / rml;

	y.y = znearx2 / -bmt;
	z.y = (bottom + top) / bmt;

	z.z = zfar / nmf;
	t.z = (znear * zfar) / nmf;

	z.w = -1.0f;

	return *this;
}

/* ------------------------------------------------------------------------- */
} // namespace math
} // namespace crpg
