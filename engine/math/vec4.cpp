#include "vec4.hpp"
#include "vec3.hpp"
#include "matrix.hpp"

namespace crpg {
namespace math {

vec4 &vec4::set_from_vec3(const vec3 &v) noexcept
{
	m = v.m;
	w = 1.0f;
	return *this;
}

vec4 vec4::transform(const vec4 &v, const matrix &m) noexcept
{
	matrix transpose = matrix::transpose(m);

	return vec4(vec4::dot(transpose.x, v),
	            vec4::dot(transpose.y, v),
	            vec4::dot(transpose.z, v),
	            vec4::dot(transpose.t, v));
}

vec4 vec4::transform_i(const vec4 &v, const matrix &m) noexcept
{
	return vec4(vec4::dot(m.x, v), vec4::dot(m.y, v), vec4::dot(m.z, v), vec4::dot(m.t, v));
}

/* ------------------------------------------------------------------------- */
} // namespace math
} // namespace crpg
