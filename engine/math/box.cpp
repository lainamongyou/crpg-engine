#include "box.hpp"
#include "matrix.hpp"

namespace crpg {
namespace math {

vec2 box::point(unsigned int i) const noexcept
{
	if (i < 4) {
		/*
		 * Note:
		 * 0 = min.x,min.y
		 * 1 = MAX.x,min.y
		 * 2 = min.x,MAX.y
		 * 3 = MAX.x,MAX.y
		 */

		vec2 p;
		p.x = (i & 1) ? max.x : min.x;
		p.y = (i & 2) ? max.y : min.y;
		return p;
	}

	return vec2::zero();
}

vec2 box::center() const noexcept
{
	vec2 center;
	center = max - min;
	center *= 0.5f;
	center += min;
	return center;
}

bool box::intersection_ray(const vec2 &orig, const vec2 &dir, float &t) const noexcept
{
	float t_max = math::infinite;
	float t_min = -math::infinite;
	int   i;

	vec2 c          = center();
	vec2 max_offset = max - c;
	vec2 box_offset = c - orig;

	for (i = 0; i < 2; i++) {
		float e = box_offset.ptr[i];
		float f = dir.ptr[i];

		if (fabsf(f) > 0.0f) {
			float fi = 1.0f / f;
			float t1 = (e + max_offset.ptr[i]) * fi;
			float t2 = (e - max_offset.ptr[i]) * fi;
			if (t1 > t2) {
				if (t2 > t_min)
					t_min = t2;
				if (t1 < t_max)
					t_max = t1;
			} else {
				if (t1 > t_min)
					t_min = t1;
				if (t2 < t_max)
					t_max = t2;
			}
			if (t_min > t_max)
				return false;
			if (t_max < 0.0f)
				return false;
		} else if ((-e - max_offset.ptr[i]) > 0.0f || (-e + max_offset.ptr[i]) < 0.0f) {
			return false;
		}
	}

	t = (t_min > 0.0f) ? t_min : t_max;
	return true;
}

bool box::intersection_line(const vec2 &p1, const vec2 &p2, float &t) const noexcept
{
	float length;

	vec2 dir = p2 - p1;
	length   = dir.len();
	if (length <= math::tiny_epsilon)
		return false;

	dir *= 1.0f / length;

	if (!intersection_ray(p1, dir, t))
		return false;

	t /= length;
	return true;
}

bool box::intersects(const box &test, float precision) const noexcept
{
	return ((min.x - test.max.x) <= precision) && ((test.min.x - max.x) <= precision) &&
	       ((min.y - test.max.y) <= precision) && ((test.min.y - max.y) <= precision);
}

/* ------------------------------------------------------------------------- */
} // namespace math
} // namespace crpg
