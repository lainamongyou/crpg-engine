#include <stdlib.h>
#include "box.hpp"
#include "vec2.hpp"
#include "vec3.hpp"
#include "plane.hpp"
#include "math-defs.hpp"
#include "math-extra.hpp"

namespace crpg {
namespace math {

vec3 polar_to_cart(const vec3 &v) noexcept
{
	vec3  cart;
	float sinx   = cosf(v.x);
	float sinx_z = v.z * sinx;

	cart.x = sinx_z * sinf(v.y);
	cart.z = sinx_z * cosf(v.y);
	cart.y = v.z * sinf(v.x);

	return cart;
}

vec3 cart_to_polar(const vec3 &v) noexcept
{
	vec3 polar;
	polar.z = v.len();

	if (close_float(polar.z, 0.0f))
		return vec3::zero();

	polar.x = asinf(v.y / polar.z);
	polar.y = atan2f(v.x, v.z);
	return polar;
}

vec3 norm_to_polar(const vec3 &norm) noexcept
{
	vec3 dst;
	dst.x = atan2f(norm.x, norm.z);
	dst.y = asinf(norm.y);
	return dst;
}

vec3 polar_to_norm(const vec2 &polar) noexcept
{
	float sinx = sinf(polar.x);

	vec3 dst;
	dst.x = sinx * cosf(polar.y);
	dst.y = sinx * sinf(polar.y);
	dst.z = cosf(polar.x);
	return dst;
}

float calc_smoothing(float val1, float val2, float smoothing, float min_adjust, float t, float precision) noexcept
{
	float out = val1;
	float dist;
	bool  over;

	if (close_float(val1, val2, precision))
		return val2;

	dist = (val2 - val1) * smoothing;
	over = dist > 0.0f;

	if (over) {
		if (dist < min_adjust) /* prevents from going too slow */
			dist = min_adjust;
		out += dist * t; /* add smoothing */
		if (out > val2)  /* clamp if overshoot */
			out = val2;
	} else {
		if (dist > -min_adjust)
			dist = -min_adjust;
		out += dist * t;
		if (out < val2)
			out = val2;
	}

	return out;
}

vec2 calc_smoothing(const vec2 &v1,
                    const vec2 &v2,
                    float       smoothing,
                    float       min_adjust,
                    float       t,
                    float       precision) noexcept
{
	vec2  vec, dir;
	float orig_dist, smoothing_dist, adjust_dist;

	if (vec2::close(v1, v2, precision))
		return v1;

	vec       = v2 - v1;
	orig_dist = vec.len();
	dir       = vec * (1.0f / orig_dist);

	smoothing_dist = orig_dist * smoothing; /* use distance to determine speed */
	if (smoothing_dist < min_adjust)        /* prevent from going too slow */
		smoothing_dist = min_adjust;

	adjust_dist = smoothing_dist * t;

	if (adjust_dist <= (orig_dist - math::large_epsilon)) {
		vec2 dst = dir * adjust_dist + v1; /* add smoothing */
		return dst;
	}

	return v2; /* clamp if overshoot */
}

float rand_float() noexcept
{
	float val = (float)((double)rand() / (double)RAND_MAX);
	if (rand() % 2 == 1)
		val = -val;
	return val;
}

int box_plane_test(const vec2 &pos, const math::box &base_box, const math::plane &plane) noexcept
{
	const math::box box = base_box + pos;

	math::vec2 min_val;
	math::vec2 max_val;

	if (plane.dir.x > 0.0f) {
		min_val.x = box.min.x;
		max_val.x = box.max.x;
	} else {
		min_val.x = box.max.x;
		max_val.x = box.min.x;
	}

	if (plane.dir.y > 0.0f) {
		min_val.y = box.min.y;
		max_val.y = box.max.y;
	} else {
		min_val.y = box.max.y;
		max_val.y = box.min.y;
	}

	if (min_val.dist(plane) > 0.0f)
		return 1;
	if (max_val.dist(plane) <= 0.0f)
		return -1;
	return 0;
}

int circle_plane_test(const vec2 &pos, const float radius, const math::plane &plane) noexcept
{
	const float dist = pos.dist(plane);
	if (dist > radius)
		return 1;
	if (dist < -radius)
		return -1;
	return 0;
}

bool ray_plane_intersection(const vec2 &ray_orig, const vec2 &ray_dir, const math::plane &plane, float &t) noexcept
{
	const float dot = plane.dir.dot(ray_dir);

	if (dot >= 0.0f)
		return false;

	t = (plane.dist - plane.dir.dot(ray_orig)) / dot;
	return true;
}

bool ray_ray_intersection(const vec2 &ray1_orig,
                          const vec2 &ray1_dir,
                          const vec2 &ray2_orig,
                          const vec2 &ray2_dir,
                          float      &t) noexcept
{
	return ray_plane_intersection(ray1_orig, ray1_dir, plane::from_ray(ray2_orig, ray2_dir), t);
}

bool ray_line_intersection(const vec2 &ray_orig,
                           const vec2 &ray_dir,
                           const vec2 &line_p1,
                           const vec2 &line_p2,
                           float      &t) noexcept
{
	const vec2  line_vec = line_p2 - line_p1;
	const float line_len = line_vec.len();

	if (close_float(line_len, 0.0f))
		return false;

	const vec2 line_dir = line_vec / line_len;

	if (!ray_ray_intersection(ray_orig, ray_dir, line_p1, line_dir, t))
		return false;

	const vec2  p    = ray_orig + ray_dir * t;
	const float dist = plane::from_norm(line_dir, line_p1).dist_from_point(p);
	if (dist < 0.0f)
		return false;
	if (dist > line_len)
		return false;

	return true;
}

bool ray_box_intersection(const vec2      &ray_orig,
                          const vec2      &ray_dir,
                          const math::box &box,
                          float           &t,
                          vec2            *norm) noexcept
{
	bool valid = true;
	vec2 edge_p1;
	vec2 edge_p2;

	if (ray_orig.x < box.min.x) {
		edge_p1.set(box.min.x, box.max.y);
		edge_p2.set(box.min.x, box.min.y);
	} else if (ray_orig.x > box.max.x) {
		edge_p1.set(box.max.x, box.min.y);
		edge_p2.set(box.max.x, box.max.y);
	} else {
		valid = false;
	}

	if (valid && ray_line_intersection(ray_orig, ray_dir, edge_p1, edge_p2, t)) {
		if (norm)
			*norm = (edge_p2 - edge_p1).set_norm().cross();
		return true;
	}

	valid = true;

	if (ray_orig.y < box.min.y) {
		edge_p1.set(box.max.x, box.min.y);
		edge_p2.set(box.min.x, box.min.y);
	} else if (ray_orig.y > box.max.y) {
		edge_p1.set(box.min.x, box.max.y);
		edge_p2.set(box.max.x, box.max.y);
	} else {
		valid = false;
	}

	if (valid && ray_line_intersection(ray_orig, ray_dir, edge_p1, edge_p2, t)) {
		if (norm)
			*norm = (edge_p2 - edge_p1).norm().cross();
		return true;
	}

	return false;
}

bool ray_circle_intersection(const vec2 &ray_orig,
                             const vec2 &ray_dir,
                             const vec2 &circle_orig,
                             const float circle_radius,
                             float      &t) noexcept
{
	const vec2  cr_vec  = circle_orig - ray_orig;
	const float cr_dist = cr_vec.dot(ray_dir);

	if (cr_dist < circle_radius)
		return false;

	const float cr_len_2 = cr_vec.dot(cr_vec);
	const float radius_2 = circle_radius * circle_radius;

	if (cr_len_2 < radius_2)
		return false;
	if (cr_dist < 0.0f && (cr_len_2 > radius_2))
		return false;

	const float dist_to_center_2 = cr_len_2 - (cr_dist * cr_dist);
	if (dist_to_center_2 > radius_2)
		return false;

	const float dist_to_circle = sqrtf(radius_2 - dist_to_center_2);
	t                          = (cr_len_2 > radius_2) ? (cr_dist - dist_to_circle) : (cr_dist + dist_to_circle);
	return true;
}

bool line_plane_intersection(const vec2 &line_p1, const vec2 &line_p2, const math::plane &plane, float &t) noexcept
{
	const float p1_dist = line_p1.dist(plane);
	const float p2_dist = line_p2.dist(plane);

	if (p1_dist > 0.0f && p2_dist < 0.0f) {
		t = p1_dist / (p1_dist + fabsf(p2_dist));
		return true;
	}

	return false;
}

bool line_line_intersection(const vec2 &line1_p1,
                            const vec2 &line1_p2,
                            const vec2 &line2_p1,
                            const vec2 &line2_p2,
                            float      &t) noexcept
{
	const math::vec2 line2_vec   = line2_p2 - line2_p1;
	const float      line2_len   = line2_vec.len();
	const math::vec2 plane2_norm = (line2_vec / line2_len).cross();

	const math::plane plane(plane2_norm, plane2_norm.dot(line2_p1));

	const float p1_dist = line1_p1.dist(plane);
	const float p2_dist = line1_p2.dist(plane);

	if (p1_dist > 0.0f != p2_dist < 0.0f) {
		const float p1_dist_abs = fabsf(p1_dist);
		const float line1_t     = p1_dist_abs / (p1_dist_abs + fabsf(p2_dist));

		const math::vec2 mid = lerp(line1_p1, line1_p2, line1_t);
		if (mid.dist(line2_p1) <= line2_len) {
			t = line1_t;
			return true;
		}
	}

	return false;
}

bool line_box_intersection(const vec2      &line_p1,
                           const vec2      &line_p2,
                           const math::box &box,
                           float           &t,
                           vec2            *norm) noexcept
{
	bool valid = true;
	vec2 edge_p1;
	vec2 edge_p2;

	if (line_p1.x < box.min.x) {
		if (line_p2.x < box.min.x)
			return false;

		edge_p1.set(box.min.x, box.max.y);
		edge_p2.set(box.min.x, box.min.y);

	} else if (line_p1.x > box.max.x) {
		if (line_p2.x > box.max.x)
			return false;

		edge_p1.set(box.max.x, box.min.y);
		edge_p2.set(box.max.x, box.max.y);
	} else {
		valid = false;
	}

	if (valid && line_line_intersection(line_p1, line_p2, edge_p1, edge_p2, t)) {
		if (norm)
			*norm = (edge_p2 - edge_p1).set_norm().cross();
		return true;
	}

	valid = true;

	if (line_p1.y < box.min.y) {
		if (line_p2.y < box.min.y)
			return false;

		edge_p1.set(box.max.x, box.min.y);
		edge_p2.set(box.min.x, box.min.y);

	} else if (line_p1.y > box.max.y) {
		if (line_p2.y > box.max.y)
			return false;

		edge_p1.set(box.min.x, box.max.y);
		edge_p2.set(box.max.x, box.max.y);
	} else {
		valid = false;
	}

	if (valid && line_line_intersection(line_p1, line_p2, edge_p1, edge_p2, t)) {
		if (norm)
			*norm = (edge_p2 - edge_p1).norm().cross();
		return true;
	}

	return false;
}

bool line_circle_intersection(const vec2 &line_p1,
                              const vec2 &line_p2,
                              const vec2 &circle_orig,
                              const float circle_radius,
                              float      &t) noexcept
{
	const vec2 line_vec = line_p2 - line_p1;
	float      line_len = line_vec.len();

	if (close_float(line_len, 0.0f))
		return false;

	const vec2 line_dir = line_vec / line_len;

	if (!ray_circle_intersection(line_p1, line_dir, circle_orig, circle_radius, t))
		return false;
	if (t < 0.0f || t > line_len)
		return false;

	t /= line_len;
	return true;
}

bool box_plane_intersection(const vec2        &box_p1,
                            const vec2        &box_p2,
                            const box         &box,
                            const math::plane &plane,
                            float             &t) noexcept
{
	const vec2 box_vec = box_p2 - box_p1;

	const bool box_x_only   = close_float(box_vec.y, 0.0f);
	const bool plane_x_only = close_float(plane.dir.y, 0.0f);
	bool       box_y_only   = close_float(box_vec.x, 0.0f);
	bool       plane_y_only = close_float(plane.dir.x, 0.0f);

	if ((box_x_only && plane_y_only) || (box_y_only && plane_x_only))
		return false;

	unsigned int corner_id;
	if (box_y_only) {
		corner_id = plane.dir.x > 0.0f ? 0 : 1;
	} else {
		corner_id = box_vec.x > 0.0f ? 1 : 0;
	}
	if (box_x_only) {
		corner_id |= plane.dir.y > 0.0f ? 0 : 2;
	} else {
		corner_id |= box_vec.y > 0.0f ? 2 : 0;
	}

	const vec2 corner = box.point(corner_id) + box_p1;
	return line_plane_intersection(corner, corner + box_vec, plane, t);
}

bool box_point_intersection(const vec2 &box_p1,
                            const vec2 &box_p2,
                            const box  &box,
                            const vec2 &p,
                            float      &t,
                            vec2       *norm) noexcept
{
	const vec2 p_adjust = p - box_p1;
	if (line_box_intersection(p_adjust, p_adjust + (box_p1 - box_p2), box, t, norm)) {
		if (norm)
			*norm = -*norm;
		return true;
	}
	return false;
}

bool box_box_intersection(const vec2 &box1_p1,
                          const vec2 &box1_p2,
                          const box  &box1,
                          const box  &box2,
                          float      &t,
                          vec2       *norm) noexcept
{
	const box expanded_box(box2.min + box1.min, box2.max + box1.max);
	return line_box_intersection(box1_p1, box1_p2, expanded_box, t, norm);
}

bool box_circle_intersection(const vec2 &box_p1,
                             const vec2 &box_p2,
                             const box  &box,
                             const vec2 &circle_orig,
                             const float circle_radius,
                             float      &t,
                             vec2       *norm) noexcept
{
	const math::box expanded_box(box.min - circle_radius, box.max + circle_radius);
	return box_point_intersection(box_p1, box_p2, expanded_box, circle_orig, t, norm);
}

bool circle_plane_intersection(const vec2        &circle_p1,
                               const vec2        &circle_p2,
                               const float        circle_radius,
                               const math::plane &plane,
                               float             &t) noexcept
{
	const vec2 circle_vec = circle_p2 - circle_p1;

	/* moving away from the line */
	if (plane.dir.dot(circle_vec) > 0.0f)
		return false;

	const float move_len   = circle_vec.len();
	const vec2  circle_dir = circle_vec / move_len;

	if (!ray_plane_intersection(circle_p1 - (plane.dir * circle_radius), circle_dir, plane, t))
		return false;
	if (t < 0.0f || t > move_len)
		return false;

	t /= move_len;
	return true;
}

static inline bool circle_point_intersection_from_dir(const vec2  circle_p1,
                                                      const float circle_radius,
                                                      const vec2 &circle_dir,
                                                      const float move_len,
                                                      const vec2 &p,
                                                      float      &t) noexcept
{
	if (!ray_circle_intersection(p, circle_dir, circle_p1, circle_radius, t))
		return false;
	if (t < 0.0f || t > move_len)
		return false;

	t /= move_len;
	return true;
}

bool circle_point_intersection(const vec2 &circle_p1,
                               const vec2 &circle_p2,
                               const float circle_radius,
                               const vec2 &p,
                               float      &t) noexcept
{
	const vec2  circle_vec = circle_p1 - circle_p2;
	const float move_len   = circle_vec.len();
	const vec2  circle_dir = circle_vec / move_len;

	return circle_point_intersection_from_dir(circle_p1, circle_radius, circle_dir, move_len, p, t);
}

bool circle_box_intersection(const vec2      &circle_p1,
                             const vec2      &circle_p2,
                             const float      circle_radius,
                             const math::box &box,
                             float           &t,
                             vec2            *norm) noexcept
{
	const math::box adjusted_box(box.min - circle_radius, box.max + circle_radius);
	return line_box_intersection(circle_p1, circle_p2, adjusted_box, t, norm);
}

bool circle_circle_intersection(const vec2 &circle1_p1,
                                const vec2 &circle1_p2,
                                const float circle1_radius,
                                const vec2 &circle2_orig,
                                const float circle2_radius,
                                float      &t) noexcept
{
	return line_circle_intersection(circle1_p1, circle1_p2, circle2_orig, circle1_radius + circle2_radius, t);
}

/* ------------------------------------------------------------------------- */
} // namespace math
} // namespace crpg
