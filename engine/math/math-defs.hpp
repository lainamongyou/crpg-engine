#pragma once

#include <cmath>

namespace crpg {
namespace math {

constexpr float pi{3.1415926535897932384626433832795f};

static constexpr float to_rad(float val) noexcept
{
	return val * 0.0174532925199432957692369076848f;
}

static constexpr float to_deg(float val) noexcept
{
	return val * 57.295779513082320876798154814105f;
}

static constexpr float large_epsilon = 1e-2f;
static constexpr float epsilon       = 1e-4f;
static constexpr float tiny_epsilon  = 1e-5f;
static constexpr float infinite      = 3.4e38f;

#if __GNUC__
#define ce_fabsf fabsf
#else
static constexpr float ce_fabsf(float f)
{
	if (std::is_constant_evaluated()) {
		return f < 0.0f ? -f : f;
	} else {
		return fabsf(f);
	}
}
#endif

static constexpr bool close_float(float f1, float f2, float precision = epsilon) noexcept
{
	return ce_fabsf(f1 - f2) <= precision;
}

/* ------------------------------------------------------------------------- */
} // namespace math
} // namespace crpg
