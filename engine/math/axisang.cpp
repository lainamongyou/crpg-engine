#include "axisang.hpp"
#include "quat.hpp"

namespace crpg {
namespace math {

axisang &axisang::set_from_quat(const quat &q) noexcept
{
	float len, leni;

	len = q.x * q.x + q.y * q.y + q.z * q.z;

	if (!close_float(len, 0.0f)) {
		leni = 1.0f / sqrtf(len);
		x    = q.x * leni;
		y    = q.y * leni;
		z    = q.z * leni;
		w    = acosf(q.w) * 2.0f;
	} else {
		x = 0.0f;
		y = 0.0f;
		z = 0.0f;
		w = 1.0f;
	}

	return *this;
}

/* ------------------------------------------------------------------------- */
} // namespace math
} // namespace crpg
