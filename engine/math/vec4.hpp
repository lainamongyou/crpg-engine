#pragma once

#include "../export.h"
#include "math-defs.hpp"
#include <cstdint>
#include <xmmintrin.h>

namespace crpg {
namespace math {

struct vec3;
struct matrix;

struct EXPORT vec4 {
	union {
		struct {
			float x, y, z, w;
		};
		float  ptr[4];
		__m128 m;
	};

	inline vec4() noexcept {}
	inline vec4(float x_, float y_, float z_, float w_) noexcept : m(_mm_set_ps(w_, z_, y_, x_)) {}

	explicit inline vec4(__m128 m_) noexcept : m(m_) {}
	explicit inline vec4(const vec3 &v3) noexcept { from_vec3(v3); }

	inline vec4 &set_zero() noexcept
	{
		m = _mm_setzero_ps();
		return *this;
	}

	static inline vec4 zero() noexcept { return vec4().set_zero(); }

	inline vec4 &set(float x_, float y_, float z_, float w_) noexcept
	{
		m = _mm_set_ps(w_, z_, y_, x_);
		return *this;
	}

	vec4 &set_from_vec3(const vec3 &v) noexcept;

	static inline vec4 from_vec3(const vec3 &v) noexcept { return vec4().set_from_vec3(v); }

	inline vec4 &operator+=(const vec4 &v2) noexcept
	{
		m = _mm_add_ps(m, v2.m);
		return *this;
	}

	inline vec4 &operator-=(const vec4 &v2) noexcept
	{
		m = _mm_sub_ps(m, v2.m);
		return *this;
	}

	inline vec4 &operator*=(const vec4 &v2) noexcept
	{
		m = _mm_mul_ps(m, v2.m);
		return *this;
	}

	inline vec4 &operator/=(const vec4 &v2) noexcept
	{
		m = _mm_div_ps(m, v2.m);
		return *this;
	}

	inline vec4 &operator+=(float f) noexcept
	{
		m = _mm_add_ps(m, _mm_set1_ps(f));
		return *this;
	}

	inline vec4 &operator-=(float f) noexcept
	{
		m = _mm_sub_ps(m, _mm_set1_ps(f));
		return *this;
	}

	inline vec4 &operator*=(float f) noexcept
	{
		m = _mm_mul_ps(m, _mm_set1_ps(f));
		return *this;
	}

	inline vec4 &operator/=(float f) noexcept
	{
		m = _mm_div_ps(m, _mm_set1_ps(f));
		return *this;
	}

	inline vec4 operator+(const vec4 &v) const noexcept { return vec4(*this) += v; }
	inline vec4 operator-(const vec4 &v) const noexcept { return vec4(*this) -= v; }
	inline vec4 operator*(const vec4 &v) const noexcept { return vec4(*this) *= v; }
	inline vec4 operator/(const vec4 &v) const noexcept { return vec4(*this) /= v; }
	inline vec4 operator+(float f) const noexcept { return vec4(*this) += f; }
	inline vec4 operator-(float f) const noexcept { return vec4(*this) -= f; }
	inline vec4 operator*(float f) const noexcept { return vec4(*this) *= f; }
	inline vec4 operator/(float f) const noexcept { return vec4(*this) /= f; }

	static inline float dot(const vec4 &v1, const vec4 &v2) noexcept
	{
		vec4   add;
		__m128 mul = _mm_mul_ps(v1.m, v2.m);
		add.m      = _mm_add_ps(_mm_movehl_ps(mul, mul), mul);
		add.m      = _mm_add_ps(_mm_shuffle_ps(add.m, add.m, 0x55), add.m);
		return add.x;
	}

	inline vec4 operator-() const noexcept { return vec4(-x, -y, -z, -w); }

	inline float len() const noexcept
	{
		float dot_val = dot(*this, *this);
		return (dot_val > 0.0f) ? sqrtf(dot_val) : 0.0f;
	}

	static inline float len(const vec4 &v) noexcept { return v.len(); }

	static inline float dist(const vec4 &v1, const vec4 &v2) noexcept
	{
		vec4  temp    = v1 - v2;
		float dot_val = dot(temp, temp);
		return (dot_val > 0.0f) ? sqrtf(dot_val) : 0.0f;
	}

	inline vec4 &set_norm() noexcept
	{
		float dot_val = dot(*this, *this);
		m             = (dot_val > 0.0f) ? _mm_mul_ps(m, _mm_set1_ps(1.0f / sqrtf(dot_val))) : _mm_setzero_ps();
		return *this;
	}

	static inline bool close(const vec4 &v1, const vec4 &v2, float precision = math::epsilon) noexcept
	{
		vec4 test = v1 - v2;
		test.set_abs();
		return test.x < precision && test.y < precision && test.z < precision && test.w < precision;
	}

	static inline vec4 min(const vec4 &v1, const vec4 &v2) noexcept
	{
		vec4 temp;
		temp.m = _mm_min_ps(v1.m, v2.m);
		return temp;
	}

	static inline vec4 min(const vec4 &v, float f) noexcept
	{
		vec4 temp;
		temp.m = _mm_min_ps(v.m, _mm_set1_ps(f));
		return temp;
	}

	static inline vec4 max(const vec4 &v1, const vec4 &v2) noexcept
	{
		vec4 temp;
		temp.m = _mm_max_ps(v1.m, v2.m);
		return temp;
	}

	static inline vec4 max(const vec4 &v, float f) noexcept
	{
		vec4 temp;
		temp.m = _mm_max_ps(v.m, _mm_set1_ps(f));
		return temp;
	}

	inline vec4 &set_abs() noexcept { return set(fabsf(x), fabsf(y), fabsf(z), fabsf(w)); }
	inline vec4 &set_floor() noexcept { return set(floorf(x), floorf(y), floorf(z), floorf(w)); }
	inline vec4 &set_ceil() noexcept { return set(ceilf(x), ceilf(y), ceilf(z), ceilf(w)); }

	static inline vec4 abs(const vec4 &v) noexcept { return vec4(v).set_abs(); }
	static inline vec4 ceil(const vec4 &v) noexcept { return vec4(v).set_ceil(); }
	static inline vec4 floor(const vec4 &v) noexcept { return vec4(v).set_floor(); }
	static inline vec4 norm(const vec4 &v) noexcept { return vec4(v).set_norm(); }

	inline uint32_t to_rgba() const noexcept
	{
		uint32_t val;
		val = (uint32_t)((double)x * 255.0);
		val |= (uint32_t)((double)y * 255.0) << 8;
		val |= (uint32_t)((double)z * 255.0) << 16;
		val |= (uint32_t)((double)w * 255.0) << 24;
		return val;
	}

	inline uint32_t to_bgra() const noexcept
	{
		uint32_t val;
		val = (uint32_t)((double)z * 255.0);
		val |= (uint32_t)((double)y * 255.0) << 8;
		val |= (uint32_t)((double)x * 255.0) << 16;
		val |= (uint32_t)((double)w * 255.0) << 24;
		return val;
	}

	static inline uint32_t to_rgba(const vec4 &v) noexcept { return v.to_rgba(); }
	static inline uint32_t to_bgra(const vec4 &v) noexcept { return v.to_bgra(); }

	inline vec4 &set_from_rgba(uint32_t rgba)
	{
		x = (float)((double)(rgba & 0xFF) * (1.0 / 255.0));
		rgba >>= 8;
		y = (float)((double)(rgba & 0xFF) * (1.0 / 255.0));
		rgba >>= 8;
		z = (float)((double)(rgba & 0xFF) * (1.0 / 255.0));
		rgba >>= 8;
		w = (float)((double)(rgba & 0xFF) * (1.0 / 255.0));
		return *this;
	}

	inline vec4 &set_from_bgra(uint32_t bgra) noexcept
	{
		z = (float)((double)(bgra & 0xFF) * (1.0 / 255.0));
		bgra >>= 8;
		y = (float)((double)(bgra & 0xFF) * (1.0 / 255.0));
		bgra >>= 8;
		x = (float)((double)(bgra & 0xFF) * (1.0 / 255.0));
		bgra >>= 8;
		w = (float)((double)(bgra & 0xFF) * (1.0 / 255.0));
		return *this;
	}

	static inline vec4 from_rgba(uint32_t rgba) noexcept { return vec4().set_from_rgba(rgba); }
	static inline vec4 from_bgra(uint32_t bgra) noexcept { return vec4().set_from_bgra(bgra); }

	static vec4  transform(const vec4 &v, const matrix &m) noexcept;
	inline vec4  transform(const matrix &m) const noexcept { return transform(*this, m); }
	inline vec4 &set_transform(const matrix &m) noexcept { return *this = transform(m); }

	static vec4  transform_i(const vec4 &v, const matrix &m) noexcept;
	inline vec4  transform_i(const matrix &m) const noexcept { return transform_i(*this, m); }
	inline vec4 &set_transform_i(const matrix &m) noexcept { return *this = transform_i(m); }
};

using point4 = vec4;

/* ------------------------------------------------------------------------- */
} // namespace math
} // namespace crpg
