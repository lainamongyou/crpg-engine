#pragma once

#include "../export.h"
#include "math-defs.hpp"
#include "vec2.hpp"

namespace crpg {
namespace math {

struct matrix;

struct EXPORT box {
	struct vec2 min, max;

	constexpr box() noexcept {}
	constexpr box(const vec2 &min_, const vec2 &max_) noexcept : min(min_), max(max_) {}
	constexpr box(float min_x, float min_y, float max_x, float max_y) noexcept
	        : min(min_x, min_y), max(max_x, max_y)
	{
	}

	constexpr box &set_zero() noexcept
	{
		min.set_zero();
		max.set_zero();
		return *this;
	}

	static constexpr box zero() noexcept { return box().set_zero(); }

	constexpr box &move(const vec2 &v) noexcept
	{
		min += v;
		max += v;
		return *this;
	}

	constexpr box &move(float f) noexcept
	{
		min += f;
		max += f;
		return *this;
	}

	constexpr box &scale(const vec2 &v) noexcept
	{
		min *= v;
		max *= v;
		return *this;
	}

	constexpr box &scale(float f) noexcept
	{
		min *= f;
		max *= f;
		return *this;
	}

	constexpr box &merge(const box &b) noexcept
	{
		min = vec2::min(min, b.min);
		max = vec2::max(max, b.max);
		return *this;
	}

	constexpr box &merge(const vec2 &v) noexcept
	{
		min = vec2::min(min, v);
		max = vec2::max(max, v);
		return *this;
	}

	constexpr box &merge(float f) noexcept
	{
		min = vec2::min(min, f);
		max = vec2::max(max, f);
		return *this;
	}

	constexpr box operator+(const vec2 &v) const noexcept { return box(*this).move(v); }
	constexpr box operator-(const vec2 &v) const noexcept { return box(*this).move(-v); }
	constexpr box operator+(float v) const noexcept { return box(*this).move(v); }
	constexpr box operator-(float v) const noexcept { return box(*this).move(-v); }

	constexpr box &operator+=(const vec2 &v) noexcept { return move(v); }
	constexpr box &operator-=(const vec2 &v) noexcept { return move(-v); }
	constexpr box &operator+=(float f) noexcept { return move(f); }
	constexpr box &operator-=(float f) noexcept { return move(-f); }

	vec2 point(unsigned int i) const noexcept;
	vec2 center() const noexcept;

	inline vec2 get_facing_point(const vec2 &dir) const noexcept
	{
		unsigned int corner_id = 0;
		if (dir.x < 0.0f)
			corner_id |= 1;
		if (dir.y < 0.0f)
			corner_id |= 2;
		return point(corner_id);
	}

	inline float norm_dist(const math::vec2 &norm) const noexcept { return -norm.dot(get_facing_point(norm)); }

	bool intersection_ray(const vec2 &orig, const vec2 &dir, float &t) const noexcept;
	bool intersection_line(const vec2 &p1, const vec2 &p2, float &t) const noexcept;

	constexpr bool inside(const box &test) const noexcept
	{
		return test.min.x >= min.x && test.min.y >= min.y && test.max.x <= max.x && test.max.y <= max.y;
	}

	constexpr bool inside(const vec2 &p, const float precision = math::epsilon) const noexcept
	{
		return p.x >= (min.x - precision) && p.x <= (max.x + precision) && p.y >= (min.y - precision) &&
		       p.y <= (max.y + precision);
	}

	bool intersects(const box &test, float precision = math::epsilon) const noexcept;
};

/* ------------------------------------------------------------------------- */
} // namespace math
} // namespace crpg
