#pragma once

#include "vec2.hpp"

namespace crpg {
namespace math {

struct plane {
	math::vec2 dir;
	float      dist;

	constexpr plane() noexcept {}
	constexpr plane(const math::vec2 &dir_, float dist_) noexcept : dir(dir_), dist(dist_) {}

	static constexpr plane from_norm(const math::vec2 &norm, const math::vec2 &point) noexcept
	{
		return plane(norm, norm.dot(point));
	}

	static constexpr plane from_ray(const math::vec2 &orig, const math::vec2 &dir) noexcept
	{
		plane val;
		val.dir  = dir.cross();
		val.dist = val.dir.dot(orig);
		return val;
	}

	static inline plane from_line(const math::vec2 &p1, const math::vec2 &p2) noexcept
	{
		plane val;
		val.dir  = (p2 - p1).cross().set_norm();
		val.dist = val.dir.dot(p2);
		return val;
	}

	constexpr float dist_from_point(const math::vec2 &p) const noexcept { return p.dist(*this); }

	static constexpr float dist_from_point(const math::plane &pl, const math::vec2 &p) noexcept
	{
		return p.dist(pl);
	}
};

constexpr float vec2::dist(const math::plane &plane) const noexcept
{
	return dot(plane.dir) - plane.dist;
}

constexpr float vec2::dist(const vec2 &p, const math::plane &plane) noexcept
{
	return p.dot(plane.dir) - plane.dist;
}

} // namespace math
} // namespace crpg
