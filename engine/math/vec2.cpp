#include "math-extra.hpp"
#include "vec2.hpp"
#include "vec4.hpp"

namespace crpg {
namespace math {

vec2 vec2::transform(const vec2 &v, const matrix &m) noexcept
{
	vec4 v4(v.x, v.y, 0.0f, 1.0f);
	v4 = v4.transform(m);
	return vec2(v4.x, v4.y);
}

/* ------------------------------------------------------------------------- */
} // namespace math
} // namespace crpg
