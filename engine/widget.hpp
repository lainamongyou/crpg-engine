#pragma once

#include <memory>
#include <vector>
#include <cassert>
#include <SDL_events.h>

#include "export.h"
#include "math/vec2.hpp"

struct game_manager;

namespace crpg {

class control_widget;
class widget_ptr_base;

enum class widget_alignment : int {
	top_left      = 0b0000,
	top_center    = 0b0001,
	top_right     = 0b0010,
	center_left   = 0b0100,
	center        = 0b0101,
	center_right  = 0b0110,
	bottom_left   = 0b1000,
	bottom_center = 0b1001,
	bottom_right  = 0b1010,
};

using widget_origin = widget_alignment;

class EXPORT widget {
	template<class> friend class widget_ptr;
	friend struct game_manager;
	friend class control_widget;
	friend class app;

	static widget *first_;
	static widget *last_;
	widget        *parent_;
	widget        *first_child_ = nullptr;
	widget        *last_child_  = nullptr;
	widget        *prev_        = nullptr;
	widget        *next_        = nullptr;

	bool             showing_       = true;
	bool             destroy_later_ = false;
	widget_ptr_base *owner          = nullptr;

	static void sdl_key_event(SDL_KeyboardEvent &event) noexcept;
	static void sdl_mouse_move_event(SDL_MouseMotionEvent &event) noexcept;
	static void sdl_mouse_wheel_event(SDL_MouseWheelEvent &event) noexcept;
	static void sdl_mouse_button_event(SDL_MouseButtonEvent &event) noexcept;

	inline void push_back(widget *&first, widget *&last) noexcept;
	inline void detach() noexcept;

	void tick_children(float seconds) noexcept;

	static void tick_list(widget *start, float seconds) noexcept;
	static void render_list(widget *start, const math::vec2 &size) noexcept;

	static inline void tick_all(float seconds) noexcept { tick_list(first(), seconds); }
	static inline void render_all(const math::vec2 &size) noexcept { render_list(first(), size); }

	math::vec2       pos_       = math::vec2::zero();
	math::vec2       size_      = math::vec2::zero();
	widget_alignment origin_    = widget_alignment::top_left;
	widget_origin    alignment_ = widget_origin::top_left;

	inline math::vec2 get_actual_pos(const math::vec2 &parent_size) noexcept;

	static control_widget *focused_widget;
	inline void            set_focus() noexcept;

	static void destroy_all() noexcept;

public:
	widget(widget *parent = nullptr) noexcept;
	virtual ~widget() noexcept;

	virtual void init() noexcept;
	virtual void tick(float seconds) noexcept;
	virtual void render() noexcept;

	void update() noexcept;

	void destroy_later() noexcept;

	inline void show() noexcept { showing_ = true; }
	inline void hide() noexcept { showing_ = false; }
	inline bool showing() const noexcept { return showing_; }

	inline void set_pos(const math::vec2 &p) noexcept { pos_ = p; }
	inline void set_pos(float x, float y) noexcept { pos_.set(x, y); }
	inline void set_size(const math::vec2 &s) noexcept { size_ = s; }
	inline void set_size(float cx, float cy) noexcept { size_.set(cx, cy); }
	inline void set_alignment(widget_alignment alignment) noexcept { alignment_ = alignment; }
	inline void set_origin(widget_origin origin) noexcept { origin_ = origin; }
	inline void set_cx(float cx) noexcept { size_.x = cx; }
	inline void set_cy(float cy) noexcept { size_.y = cy; }

	static inline widget *first() noexcept { return first_; }
	static inline widget *last() noexcept { return last_; }
	inline widget        *prev() const noexcept { return prev_; }
	inline widget        *next() const noexcept { return next_; }

	inline widget *parent() const noexcept { return parent_; }
	inline widget *first_child() const noexcept { return first_child_; }
	inline widget *last_child() const noexcept { return last_child_; }

	inline const math::vec2 &pos() const noexcept { return pos_; }
	inline const math::vec2 &size() const noexcept { return size_; }
	inline widget_alignment  alignment() const noexcept { return alignment_; }
	inline widget_origin     origin() const noexcept { return origin_; }

	inline float cx() const noexcept { return size_.x; }
	inline float cy() const noexcept { return size_.y; }
};

class EXPORT control_widget : public widget {
	friend class widget;

	bool keep_focus_    = false;
	bool mouse_over     = false;
	bool mouse_was_over = false;

	virtual bool key_event(SDL_KeyboardEvent &event) noexcept;
	virtual void mouse_move_event(SDL_MouseMotionEvent &event) noexcept;
	virtual void mouse_wheel_event(SDL_MouseWheelEvent &event) noexcept;
	virtual void mouse_button_event(SDL_MouseButtonEvent &event) noexcept;

	virtual void gain_focus() noexcept;
	virtual void lose_focus() noexcept;
	virtual void mouse_enter() noexcept;
	virtual void mouse_leave() noexcept;

public:
	inline control_widget(widget *parent = nullptr) noexcept : widget(parent) {}

	void keep_focus(bool keep) noexcept;
	bool keeping_focus() const noexcept { return keep_focus_; }
};

class widget_ptr_base {
	friend class widget;

protected:
	widget *w;

public:
	inline widget_ptr_base(widget *w) : w(w) {}
};

template<class T> class widget_ptr : public widget_ptr_base {
public:
	inline widget_ptr() noexcept : widget_ptr_base(nullptr) {}
	inline widget_ptr(widget *ptr) noexcept : widget_ptr_base(ptr)
	{
		assert(!w->owner);
		assert(!w->destroy_later_);
		w->owner = this;
	}
	inline ~widget_ptr() noexcept { destroy_later(); }

	widget_ptr(const widget_ptr &)            = delete;
	widget_ptr &operator=(const widget_ptr &) = delete;

	inline widget_ptr &operator=(widget *ptr) noexcept
	{
		if (w != ptr) {
			assert(!ptr->owner);
			assert(!ptr->destroy_later_);

			destroy_later();
			w = ptr;
		}

		return *this;
	}

	inline void destroy_later() noexcept
	{
		if (w) {
			w->destroy_later_ = true;
			w->owner          = nullptr;
			w                 = nullptr;
		}
	}

	inline T       *operator->() noexcept { return static_cast<T *>(w); }
	inline const T *operator->() const noexcept { return static_cast<const T *>(w); }

	inline operator T *() noexcept { return static_cast<T *>(w); }
	inline operator const T *() const noexcept { return static_cast<T *>(w); }
};

/* ------------------------------------------------------------------------- */
} // namespace crpg
