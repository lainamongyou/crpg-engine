#pragma once

#define CRPG_ENGINE_VERSION_MAJOR 1
#define CRPG_ENGINE_VERSION_MINOR 0
#define CRPG_ENGINE_VERSION_PATCH 0

#define CRPG_ENGINE_API_VERSION_MAJOR 1
#define CRPG_ENGINE_API_VERSION_MINOR 0
#define CRPG_ENGINE_API_VERSION_PATCH 0

#define CRPG_MAKE_VERSION(major, minor, patch)                                                                         \
	((((uint32_t)(major)) << 22) | (((uint32_t)(minor)) << 12) | ((uint32_t)(patch)))

#define CRPG_ENGINE_VERSION                                                                                            \
	CRPG_MAKE_VERSION(CRPG_ENGINE_VERSION_MAJOR, CRPG_ENGINE_VERSION_MINOR, CRPG_ENGINE_VERSION_PATCH)
#define CRPG_ENGINE_API_VERSION                                                                                        \
	CRPG_MAKE_VERSION(CRPG_ENGINE_VERSION_MAJOR, CRPG_ENGINE_VERSION_MINOR, CRPG_ENGINE_VERSION_PATCH)
