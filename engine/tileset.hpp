#pragma once

#include "math/vec2.hpp"
#include "math/box.hpp"
#include "types.hpp"

#include <string>
#include <memory>

namespace crpg {

namespace graphics {
class texture;
class vertex_buffer;
using texture_ref       = std::shared_ptr<texture>;
using vertex_buffer_ref = std::shared_ptr<vertex_buffer>;
} // namespace graphics

class tileset;
using tileset_ref      = std::shared_ptr<tileset>;
using weak_tileset_ref = std::weak_ptr<tileset>;

class tileset {
	friend class asset_manager_internal;

	graphics::texture_ref texture_;
	uint32_t              cx_;
	uint32_t              cy_;
	uint32_t              tile_width_;

	static tileset_ref create(const std::string &path) noexcept;

public:
	inline graphics::texture_ref texture() const noexcept { return texture_; }
	inline uint32_t              cx() const noexcept { return cx_; }
	inline uint32_t              cy() const noexcept { return cy_; }
	inline uint32_t              tile_width() const noexcept { return tile_width_; }
};

} // namespace crpg
