#include "utility/fserializer.hpp"
#include "graphics/graphics.hpp"
#include "utility/logging.hpp"
#include "utility/dict.hpp"
#include "sprite.hpp"

namespace crpg {

struct frame_info {
	uint32_t   start_vertex;
	uint32_t   duration_ms;
	math::vec2 min;
	math::vec2 max;
};

sprite_ref sprite::create(const std::string &path) noexcept
try {
	ifserializer s(path + "/asset.data");
	if (s.fail())
		throw strprintf("Failed to open '%s'", path.c_str());

	crpg::sprite           *sprite = new crpg::sprite;
	std::string             ext;
	uint32_t                cx;
	uint32_t                cy;
	uint32_t                tile_width;
	std::vector<frame_info> frames;

	crpg::dict dict(s);
	dict.get("orientation", sprite->orientation);
	dict.get("origin", sprite->origin);
	dict.get("box", sprite->box_);
	dict.get("frames", frames);
	dict.get("sequences", sprite->sequences);
	dict.get("ext", ext);
	dict.get("frame_cx", cx);
	dict.get("frame_cy", cy);
	dict.get("tile_width", tile_width);

	std::string image_path = path + "/image." + ext;
	sprite->texture        = graphics::texture::create_from_file(image_path);
	if (!sprite->texture)
		throw strprintf("Failed to open '%s'", image_path.c_str());

	/* -------------------------------------------------- */
	/* load frames                                        */

	math::vec2 p1 = -sprite->origin;
	math::vec2 p2((float)cx, (float)cy);
	float      f_tile_width = (float)tile_width;

	p2 += p1;
	p1 /= f_tile_width;
	p2 /= f_tile_width;

	sprite->frames.reserve(frames.size());

	graphics::vertex_buffer::data vbd;
	for (auto &info : frames) {
		vbd.points.emplace_back(p1.x, p1.y);
		vbd.points.emplace_back(p1.x, p2.y);
		vbd.points.emplace_back(p2.x, p1.y);
		vbd.points.emplace_back(p2.x, p2.y);

		vbd.coords.emplace_back(info.min.x, info.min.y);
		vbd.coords.emplace_back(info.min.x, info.max.y);
		vbd.coords.emplace_back(info.max.x, info.min.y);
		vbd.coords.emplace_back(info.max.x, info.max.y);

		sprite::frame frame = {info.start_vertex, (float)info.duration_ms / 1000.0f};
		sprite->frames.push_back(frame);
	}

	sprite->vb = graphics::vertex_buffer::create(std::move(vbd));

	/* -------------------------------------------------- */

	return std::shared_ptr<crpg::sprite>(sprite);

} catch (const std::string &text) {
	error("%s: %s", __FUNCTION__, text.c_str());
	return sprite_ref();
}

void sprite_state::render() noexcept
{
	graphics::shader *ps = graphics::shader::current_ps();
	if (!ps)
		return;

	crpg::sprite *sprite = this->sprite.get();
	if (!sprite)
		return;

	ps->set("image", sprite->texture);
	sprite->vb->load();

	graphics::draw(graphics::primitive::tristrip, sprite->frames[index].start_vertex, 4);
}

bool sprite_state::do_loop() noexcept
{
	if (mode == sprite::mode::play_and_revert) {
		sequence_name = revert_name;
		sequence      = revert_sequence;
		index         = revert_index;
		forward       = revert_forward;
		t             = revert_t;
		mode          = sprite::mode::loop;
		if (on_stop) {
			on_stop();
			on_stop = std::function<void()>();
		}
		return false;

	} else if (mode == sprite::mode::play_and_pause) {
		if (!next_sequence_name.empty()) {
			play(next_sequence_name, next_sequence_mode, next_sequence_reverse);
			next_sequence_name.clear();
		} else {
			sequence.type = sprite::sequence_type::still;
			if (on_stop) {
				on_stop();
				on_stop = std::function<void()>();
			}
		}
		return false;
	}

	return true;
}

void sprite_state::tick(float seconds) noexcept
{
	if (sequence.type == sprite::sequence_type::still)
		return;

	crpg::sprite       *sprite = this->sprite.get();
	crpg::sprite::frame frame  = sprite->frames[index];

	t += seconds * speed_;

	if (t >= frame.duration) {
		t = fmodf(t, frame.duration);

		if (sequence.type == sprite::sequence_type::forward) {
			if (index == sequence.end) {
				if (do_loop()) {
					index = sequence.start;
				}
			} else {
				index++;
			}
		} else if (sequence.type == sprite::sequence_type::reverse) {
			if (index == sequence.start) {
				if (do_loop()) {
					index = sequence.end;
				}
			} else {
				--index;
			}
		} else /* cycle */ {
			if (forward) {
				if (index == sequence.end) {
					forward = !forward;
					index--;
				} else {
					index++;
				}
			} else {
				if (index == sequence.start) {
					if (do_loop()) {
						forward = !forward;
						index++;
					}
				} else {
					index--;
				}
			}
		}
	}
}

bool sprite_state::play(const std::string &name, sprite::mode mode, bool reverse, std::function<void()> cb) noexcept
{
	if (!sprite) {
		if (cb)
			cb();
		return false;
	}

	crpg::sprite *sprite = this->sprite.get();

	auto it = sprite->sequences.find(name);
	if (it == sprite->sequences.end()) {
		if (cb)
			cb();
		return false;
	}

	if (mode == sprite::mode::play_and_revert && this->mode != mode) {
		revert_name     = sequence_name;
		revert_sequence = sequence;
		revert_index    = index;
		revert_t        = t;
		revert_forward  = forward;
	}

	sequence = sprite->sequences[name];
	if (reverse) {
		sequence.type = sequence.type == sprite::sequence_type::forward ? sprite::sequence_type::reverse
		                                                                : sprite::sequence_type::forward;
	}

	if (on_stop) {
		on_stop();
		on_stop = std::function<void()>();
	}

	sequence_name = name;
	index         = sequence.type == sprite::sequence_type::reverse ? sequence.end : sequence.start;
	on_stop       = cb;
	forward       = true;
	t             = 0.0f;
	this->mode    = mode;

	return true;
}

void sprite_state::set_next_sequence(const std::string &name, sprite::mode mode, bool reverse) noexcept
{
	if (stopped()) {
		play(name, mode);
		return;
	}

	next_sequence_name    = name;
	next_sequence_mode    = mode;
	next_sequence_reverse = reverse;
}

void sprite_state::reverse() noexcept
{
	if (sequence_name.empty())
		return;

	if (mode == sprite::mode::play_and_pause && sequence.type == sprite::sequence_type::still) {
		sequence.type = index != 0 ? sprite::sequence_type::forward : sprite::sequence_type::reverse;
	} else {
		if (sequence.type == sprite::sequence_type::forward)
			sequence.type = sprite::sequence_type::reverse;
		else if (sequence.type == sprite::sequence_type::reverse)
			sequence.type = sprite::sequence_type::forward;
	}
}

} // namespace crpg
