#pragma once

#include <cstdint>

namespace crpg {

enum class asset_type : uint32_t {
	root,
	category,
	folder,
	file,
};

enum class asset_category : uint32_t {
	root,
	map,
	object,
	sprite,
	tileset,
	sound,
	music,
};

enum class object_property_type : uint32_t {
	invalid,
	boolean,
	integer,
	real,
	string,
	enumeration,
	asset,
};

static constexpr int orientation_left     = 0b0000;
static constexpr int orientation_center_x = 0b0001;
static constexpr int orientation_right    = 0b0010;
static constexpr int orientation_top      = 0b0000;
static constexpr int orientation_center_y = 0b0100;
static constexpr int orientation_bottom   = 0b1000;

/* clang-format off */
enum class orientation : uint32_t {
	top_left      = orientation_top      | orientation_left,
	top_center    = orientation_top      | orientation_center_x,
	top_right     = orientation_top      | orientation_right,
	center_left   = orientation_center_y | orientation_left,
	center        = orientation_center_y | orientation_center_x,
	center_right  = orientation_center_y | orientation_right,
	bottom_left   = orientation_bottom   | orientation_left,
	bottom_center = orientation_bottom   | orientation_center_x,
	bottom_right  = orientation_bottom   | orientation_right,
};
/* clang-format on */

namespace type {
using entity_id  = std::uint64_t;
using asset_id   = std::uint64_t;
using segment_id = std::uint32_t;
using object_id  = std::uint32_t;
using sprite_id  = std::uint32_t;
using layer_id   = std::uint32_t;
using tileset_id = std::uint16_t;
using index      = std::uint32_t;
} // namespace type

} // namespace crpg
