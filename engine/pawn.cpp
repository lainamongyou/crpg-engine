#include "controller.hpp"
#include "pawn.hpp"

namespace crpg {

pawn::~pawn() noexcept
{
	if (cur_controller) {
		cur_controller->on_disconnect(this);
		on_disconnect(cur_controller);
		cur_controller->target_ = nullptr;
	}
}

void pawn::set_controller(controller *c) noexcept
{
	if (c->target_) {
		c->target_->on_disconnect(c);
		c->on_disconnect(c->target_);
		c->target_ = nullptr;
	}
	if (cur_controller) {
		on_disconnect(cur_controller);
		cur_controller->on_disconnect(this);
		cur_controller = nullptr;
	}

	cur_controller = c;
	c->target_     = this;

	cur_controller->on_connect(this);
	on_connect(c);
}

void pawn::on_connect(controller *) noexcept {}
void pawn::on_disconnect(controller *) noexcept {}

} // namespace crpg
