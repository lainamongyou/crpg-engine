#include "font.hpp"

#include "graphics/graphics.hpp"
#include "utility/fserializer.hpp"
#include "utility/logging.hpp"
#include "resources.hpp"

#include <freetype/freetype.h>
#include <freetype/ftstroke.h>

#include <unordered_map>
#include <cwctype>
#include <fstream>
#include <memory>
#include <string>
#include <list>

using namespace crpg::graphics;
using namespace crpg::math;

namespace crpg {

static FT_Library ft2 = nullptr;

#define MAX_GLYPHS 512

/* ========================================================================= */

using file_mem          = std::vector<char>;
using file_mem_ref      = std::shared_ptr<file_mem>;
using file_mem_weak_ref = std::weak_ptr<file_mem>;
static std::unordered_map<std::string, file_mem_weak_ref> file_mem_map;

/* ========================================================================= */

template<typename T, void done_func(T)> class ft2_obj {
	T obj = nullptr;

public:
	inline ~ft2_obj() noexcept
	{
		if (obj)
			done_func(obj);
	}

	inline T *operator&() noexcept { return &obj; }
	inline T  operator->() noexcept { return obj; }
	inline    operator T() noexcept { return obj; }

	inline T data() const noexcept { return obj; }
};

static inline void ft2_face_done(FT_Face face) noexcept
{
	FT_Done_Face(face);
}

using ft2_face    = ft2_obj<FT_Face, ft2_face_done>;
using ft2_glyph   = ft2_obj<FT_Glyph, FT_Done_Glyph>;
using ft2_stroker = ft2_obj<FT_Stroker, FT_Stroker_Done>;

/* ========================================================================= */

class ft2_font : public font {
	void   add_outline(glyph &g, FT_GlyphSlot slot) noexcept;
	glyph *add(int ch) noexcept;

	file_mem_ref ref;

public:
	ft2_face    face;
	ft2_stroker stroker;
	bool        has_kerning;
	int         internal_size;

	std::list<glyph> glyph_list;
	using glyph_iterator = decltype(glyph_list)::iterator;

	std::unordered_map<int, glyph_iterator> glyphs;

	vertex_buffer_ref sprite_vb;
	shader_ref        ps;
	shader_ref        vs;

	ft2_font(const char *file, int size, int outline_size);

	glyph *get(int ch) noexcept;

	void draw_tex(graphics::texture_ref &tex,
	              float                  x,
	              float                  y,
	              float                  bearing_x,
	              float                  bearing_y,
	              uint32_t               color) noexcept;
	void draw_char(int      ch,
	               int      prev,
	               float    orig_x,
	               float   &x,
	               float   &y,
	               uint32_t color,
	               uint32_t outline_color) noexcept override;

	glyph *advance_char(int ch, int prev, float origin_x, float &x, float &y) noexcept override;
	void   bitmap_to_texture(graphics::texture_ref &tex, FT_Bitmap &bitmap) noexcept;

	void        draw_char_init() noexcept override;
	void        draw(const char *text, float x, float y, uint32_t color, uint32_t outline_color) noexcept override;
	math::box   text_metrics(const char *text) noexcept override;
	std::string word_wrap(const char *text, float cx) noexcept override;
};

ft2_font::ft2_font(const char *path, int size, int outline_size_) : font(size, outline_size_), internal_size(size)
{
	auto pair = file_mem_map.find(path);
	if (pair != file_mem_map.end()) {
		ref = pair->second.lock();
		if (!ref)
			pair = file_mem_map.end();
	}

	if (pair == file_mem_map.end()) {
		using namespace std;

		ref = make_shared<file_mem>();

		ifstream file(os_path(path), ios_base::in | std::ios_base::binary);
		if (!file.is_open())
			throw strprintf("Failed to load font file '%s'", path);

		file.seekg(0, ios_base::end);
		auto file_size = file.tellg();
		file.seekg(0, ios_base::beg);

		if (!size)
			throw strprintf("Empty file: %s", path);

		file_mem_map[path] = file_mem_weak_ref(ref);
		ref->resize(file_size);
		file.read(ref->data(), file_size);
	}

	FT_Error error = FT_New_Memory_Face(ft2, (const FT_Byte *)ref->data(), (FT_Long)ref->size(), 0, &face);
	if (error)
		throw strprintf("Failed to load font data from file '%s', error %d", path, error);

	for (;;) {
		if (FT_Set_Pixel_Sizes(face, 0, internal_size) != 0)
			throw strprintf("Unsupported font size %d for font file '%s'", size, path);

		int full_size = face->size->metrics.height >> 6;
		if (full_size <= size) {
			height_    = (float)full_size;
			ascender_  = (float)(face->size->metrics.ascender >> 6);
			descender_ = (float)(face->size->metrics.descender >> 6);
			break;
		}

		--internal_size;
	}

	has_kerning = !!FT_HAS_KERNING(face);

	vs        = resources::shader("engine:text.vert");
	ps        = resources::shader("engine:text.frag");
	sprite_vb = resources::sprite_vb();

	if (outline()) {
		FT_Stroker_New(ft2, &stroker);
		FT_Stroker_Set(stroker, outline_size_ << 5, FT_STROKER_LINECAP_ROUND, FT_STROKER_LINEJOIN_ROUND, 0);
	}
}

void ft2_font::bitmap_to_texture(graphics::texture_ref &tex, FT_Bitmap &bitmap) noexcept
{
	int                  pitch = bitmap.pitch;
	unsigned int         rows  = bitmap.rows;
	const uint8_t       *data  = bitmap.buffer;
	std::vector<uint8_t> bmp;

	pitch = (pitch + 3) & ~3;

	if (bitmap.pitch != pitch || bitmap.rows != rows) {
		bmp.resize(pitch * rows);

		const uint8_t *in  = bitmap.buffer;
		uint8_t       *out = &bmp[0];

		for (unsigned int i = 0; i < bitmap.rows; i++) {
			memcpy(out, in, bitmap.pitch);
			in += bitmap.pitch;
			out += pitch;
		}

		data = bmp.data();
	}

	if (pitch && rows)
		tex = texture::create(pitch, rows, 1, graphics::texture_format::a8, &data);
}

void ft2_font::add_outline(glyph &g, FT_GlyphSlot slot) noexcept
{
	ft2_glyph glyph;
	FT_Get_Glyph(slot, &glyph);
	FT_Glyph_Stroke(&glyph, stroker, true);
	FT_Glyph_To_Bitmap(&glyph, FT_RENDER_MODE_NORMAL, nullptr, true);

	FT_BitmapGlyph bm_glyph = reinterpret_cast<FT_BitmapGlyph>(glyph.data());
	bitmap_to_texture(g.outline, bm_glyph->bitmap);
	g.outline_bearing_x = (float)bm_glyph->left;
	g.outline_bearing_y = (float)bm_glyph->top;
}

font::glyph *ft2_font::add(int ch) noexcept
{
	FT_GlyphSlot slot = face->glyph;

	FT_UInt idx = FT_Get_Char_Index(face, ch);
	if (FT_Load_Glyph(face, idx, FT_LOAD_NO_BITMAP) != 0)
		return nullptr;

	font::glyph g;
	if (outline() && slot->format == FT_GLYPH_FORMAT_OUTLINE)
		add_outline(g, slot);

	if (FT_Render_Glyph(slot, FT_RENDER_MODE_NORMAL) != 0)
		return nullptr;

	if (glyph_list.size() == MAX_GLYPHS) {
		glyphs.erase(glyph_list.back().ch);
		glyph_list.pop_back();
	}

	bitmap_to_texture(g.texture, slot->bitmap);
	g.bearing_x = (float)(slot->metrics.horiBearingX >> 6);
	g.bearing_y = (float)(slot->metrics.horiBearingY >> 6);
	g.advance_x = (float)(slot->advance.x >> 6);
	g.ch        = ch;
	glyph_list.push_front(g);

	glyph_iterator it = glyph_list.begin();
	glyphs[ch]        = it;
	return &*it;
}

font::glyph *ft2_font::get(int ch) noexcept
{
	auto pair = glyphs.find(ch);
	if (pair == glyphs.end())
		return add(ch);

	/* move glyph to front to reset its usage priority to the top */
	glyph_iterator it = pair->second;
	glyph_list.splice(glyph_list.begin(), glyph_list, it);
	return &*it;
}

bool font::next_utf32(const char *&text, int &ch, int &prev) noexcept
{
	int out = 0;

	prev = ch;
	ch   = *text;

#define get_next_ch()                                                                                                  \
	do {                                                                                                           \
		ch = *(++text);                                                                                        \
		if (!ch || (ch & 0xC0) != 0x80)                                                                        \
			return false;                                                                                  \
	} while (false)

	if ((ch & 0x80) == 0) {
		if (ch == 0)
			return false;
		++text;
		return true;

	} else if ((ch & 0xE0) == 0xC0) {
		out = (ch & 0x1F) << 6;
		get_next_ch();
		out |= ch & 0x3F;
		++text;
		return !!out;

	} else if ((ch & 0xF0) == 0xE0) {
		out = (ch & 0x0F) << 12;
		get_next_ch();
		out |= (ch & 0x3F) << 6;
		get_next_ch();
		out |= ch & 0x3F;
		++text;
		return !!out;

	} else if ((ch & 0xF8) == 0xF0) {
		out = (ch & 0x07) << 18;
		get_next_ch();
		out |= (ch & 0x3F) << 12;
		get_next_ch();
		out |= (ch & 0x3F) << 6;
		get_next_ch();
		out |= ch & 0x3F;
		++text;
		return !!out;

	} else if ((ch & 0xFC) == 0xF8) {
		out = (ch & 0x03) << 24;
		get_next_ch();
		out = (ch & 0x3F) << 18;
		get_next_ch();
		out |= (ch & 0x3F) << 12;
		get_next_ch();
		out |= (ch & 0x3F) << 6;
		get_next_ch();
		out |= ch & 0x3F;
		++text;
		return !!out;
	}

#undef get_next_ch

	return false;
}

font::glyph *ft2_font::advance_char(int ch, int prev, float origin_x, float &x, float &y) noexcept
{
	if (ch == '\n') {
		y += height_;
		x = origin_x;
		return nullptr;
	}

	font::glyph *g = get(ch);
	if (!g)
		return nullptr;
	if (!g->texture) {
		x += g->advance_x;
		return nullptr;
	}
	if (prev && has_kerning) {
		FT_Vector delta;
		if (FT_Get_Kerning(face, prev, ch, FT_KERNING_DEFAULT, &delta) == 0) {
			x += (float)(delta.x >> 6);
		}
	}

	return g;
}

void ft2_font::draw_tex(graphics::texture_ref &tex,
                        float                  x,
                        float                  y,
                        float                  bearing_x,
                        float                  bearing_y,
                        uint32_t               color) noexcept
{
	float char_y = y - bearing_y;

	matrix m;
	m.set_identity();
	m.scale((float)tex->cx(), (float)tex->cy(), 1.0f);
	m.translate(x + bearing_x, char_y, 0.0f);

	vs->set_world(m);
	ps->set("image", tex);
	ps->set("color", vec4::from_rgba(color));

	graphics::draw(graphics::primitive::tristrip);
}

void ft2_font::draw_char_init() noexcept
{
	vs->load();
	ps->load();
	sprite_vb->load();
}

void ft2_font::draw_char(int      ch,
                         int      prev,
                         float    origin_x,
                         float   &x,
                         float   &y,
                         uint32_t color,
                         uint32_t outline_color) noexcept
{
	font::glyph *g = ft2_font::advance_char(ch, prev, origin_x, x, y);
	if (!g)
		return;

	draw_tex(g->texture, x, y, g->bearing_x, g->bearing_y, color);

	if (outline() && g->outline)
		draw_tex(g->outline, x, y, g->outline_bearing_x, g->outline_bearing_y, outline_color);

	x += g->advance_x;
}

void ft2_font::draw(const char *text, float x, float y, uint32_t color, uint32_t outline_color) noexcept
{
	if (!text || !*text)
		return;

	vs->load();
	ps->load();
	sprite_vb->load();

	int   ch    = 0;
	int   prev  = 0;
	float new_x = x;
	float new_y = y;

	while (next_utf32(text, ch, prev))
		ft2_font::draw_char(ch, prev, x, new_x, new_y, color, outline_color);
}

math::box ft2_font::text_metrics(const char *text) noexcept
{
	if (!text || !*text)
		return math::box::zero();

	int       ch   = 0;
	int       prev = 0;
	float     x    = 0.0f;
	float     y    = 0.0f;
	math::box box  = math::box::zero();

	while (next_utf32(text, ch, prev)) {
		font::glyph *g = ft2_font::advance_char(ch, prev, 0.0f, x, y);
		if (!g)
			continue;

		texture_ref &tex       = outline() ? g->outline : g->texture;
		float        bearing_x = outline() ? g->outline_bearing_x : g->bearing_x;
		float        bearing_y = outline() ? g->outline_bearing_y : g->bearing_y;

		if (y == 0.0f) {
			float top = -bearing_y;
			if (top < box.min.y)
				box.min.y = top;
		}

		if (x == 0.0f) {
			float left = -bearing_x;
			if (left < box.min.x)
				box.min.x = left;
		}

		float bottom = y - bearing_y + (float)tex->cy();
		if (bottom > box.max.y)
			box.max.y = bottom;

		float right = x + (float)tex->cx();
		if (right > box.max.x)
			box.max.x = right;

		x += g->advance_x;
	}

	return box;
}

static inline bool is_actual_wspace(wint_t ch) noexcept
{
	return ch != '\n' && iswspace(ch);
}

std::string ft2_font::word_wrap(const char *text, float cx) noexcept
{
	if (!text || !*text)
		return std::string();

	std::string out;
	out.reserve(strlen(text));

	int         ch             = 0;
	int         prev           = 0;
	const char *start_text     = text;
	const char *next_text      = text;
	float       x              = 0.0f;
	float       y              = 0.0f;
	const char *last_space     = nullptr;
	bool        prev_was_space = false;

#define write_text()                                                                                                   \
	do {                                                                                                           \
		while (start_text < next_text) {                                                                       \
			char new_ch = *(start_text++);                                                                 \
			if (!new_ch)                                                                                   \
				break;                                                                                 \
			out.push_back(new_ch);                                                                         \
		}                                                                                                      \
	} while (false)

#define reset()                                                                                                        \
	do {                                                                                                           \
		last_space     = nullptr;                                                                              \
		prev_was_space = false;                                                                                \
		x              = 0.0f;                                                                                 \
		prev           = 0;                                                                                    \
	} while (false)

	while (next_utf32(next_text, ch, prev)) {
		font::glyph *g = ft2_font::advance_char(ch, prev, 0.0f, x, y);
		if (g)
			x += g->advance_x;

		bool ignore_space = prev_was_space;
		prev_was_space    = false;

		if (text != start_text) {
			if (is_actual_wspace((wint_t)ch)) {
				if (!ignore_space)
					last_space = text;
				prev_was_space = true;
			}
		}

		if (ch == '\n') {
			write_text();
			reset();
		} else if (x > cx) {
			next_text = last_space ? last_space : text;

			write_text();
			out.push_back('\n');

			text = next_text;
			while (next_utf32(next_text, ch, prev)) {
				start_text = text;
				if (!is_actual_wspace((wint_t)ch))
					break;
				text = next_text;
			}

			if (ch == 0)
				break;

			reset();
		}

		text = next_text;
	}

	write_text();
#undef write_text
#undef reset
	return out;
}

/* ========================================================================= */

font_ref font::create_from_file(const char *file, int size, int outline_size) noexcept
try {
	return std::make_shared<ft2_font>(file, size, outline_size);
} catch (const std::string &text) {
	error("%s: %s", __FUNCTION__, text.c_str());
	return font_ref();
}

font::~font() {}

/* ========================================================================= */

bool font::ft2_init() noexcept
{
	FT_Error err = FT_Init_FreeType(&ft2);
	if (err != 0) {
		error("Couldn't initialize freetype: %d", err);
		return false;
	}
	return true;
}

void font::ft2_free() noexcept
{
	FT_Done_FreeType(ft2);
}

/* ========================================================================= */

} // namespace crpg
