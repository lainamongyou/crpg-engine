#pragma once

#include "math/vec2.hpp"
#include "math/box.hpp"
#include "types.hpp"
#include "export.h"

#include <unordered_map>
#include <vector>
#include <memory>

namespace crpg {

namespace graphics {
class texture;
class vertex_buffer;
using texture_ref       = std::shared_ptr<texture>;
using vertex_buffer_ref = std::shared_ptr<vertex_buffer>;
} // namespace graphics

class sprite;
using sprite_ref      = std::shared_ptr<sprite>;
using weak_sprite_ref = std::weak_ptr<sprite>;

class sprite {
	friend class sprite_state;
	friend class asset_manager_internal;

	struct frame {
		uint32_t start_vertex;
		float    duration;
	};

	enum class sequence_type : uint32_t {
		still,
		forward,
		reverse,
		cycle,
	};

	struct sequence {
		sequence_type type;
		int32_t       start;
		int32_t       end;
	};

	std::unordered_map<std::string, sequence> sequences;
	std::vector<frame>                        frames;
	graphics::texture_ref                     texture;
	graphics::vertex_buffer_ref               vb;
	crpg::orientation                         orientation;
	math::vec2                                origin;
	math::box                                 box_;

	static sprite_ref create(const std::string &path) noexcept;

public:
	inline math::box box() const noexcept { return box_; }

	inline bool has_sequence(const std::string &name) const noexcept
	{
		auto it = sequences.find(name);
		return it != sequences.end();
	}

	enum class mode {
		loop,
		play_and_revert,
		play_and_pause,
	};
};

class EXPORT sprite_state {
public:
	inline sprite_state() noexcept {}
	inline sprite_state(sprite_ref sprite) noexcept : sprite(sprite) {}

	void render() noexcept;
	void tick(float seconds) noexcept;

	inline void set_sprite(sprite_ref sprite) noexcept
	{
		if (sprite != this->sprite) {
			this->sprite = sprite;
			sequence     = {};
			index        = 0;
			t            = 0.0f;
			forward      = true;
			sequence_name.clear();
		}
	}

	inline void swap_sprite(sprite_ref sprite) noexcept
	{
		if (sprite != this->sprite)
			this->sprite = sprite;
	}

	bool play(const std::string    &name,
	          sprite::mode          mode    = sprite::mode::loop,
	          bool                  reverse = false,
	          std::function<void()> cb      = std::function<void()>()) noexcept;
	void set_next_sequence(const std::string &name,
	                       sprite::mode       mode    = sprite::mode::loop,
	                       bool               reverse = false) noexcept;

	void         reverse() noexcept;
	inline void  set_speed(float new_speed) noexcept { speed_ = new_speed; }
	inline float speed() const noexcept { return speed_; }
	inline bool  valid() const noexcept { return !!sprite; }

	inline bool has_sequence(const std::string &name) const noexcept
	{
		return sprite ? sprite->has_sequence(name) : false;
	}
	inline const std::string &current_sequence() const noexcept { return sequence_name; }

	inline bool stopped() const noexcept { return sequence.type == sprite::sequence_type::still; }

private:
	sprite_ref            sprite;
	std::string           sequence_name;
	sprite::sequence      sequence = {};
	sprite::mode          mode     = sprite::mode::loop;
	int32_t               index    = 0;
	float                 t        = 0.0f;
	float                 speed_   = 1.0f;
	bool                  forward  = true;
	std::function<void()> on_stop;

	std::string  next_sequence_name;
	sprite::mode next_sequence_mode;
	bool         next_sequence_reverse;

	std::string      revert_name;
	sprite::sequence revert_sequence;
	int32_t          revert_index;
	float            revert_t;
	bool             revert_forward;

	bool do_loop() noexcept;
};

} // namespace crpg
