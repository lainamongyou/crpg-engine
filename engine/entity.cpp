#include "utility/serializer.hpp"
#include "utility/logging.hpp"
#include "entity.hpp"
#include "map.hpp"
#include "engine-opts.h"

#include <unordered_map>
#include <vector>

using namespace std;

namespace crpg {

/* ------------------------------------------------------------------------- */

#define ALIGN_BYTES 16
#define ENT_HEADER_SIZE 16
#define MAX_ENT_SIZE ((sizeof(entity) + ENT_HEADER_SIZE + EXTRA_ENT_BYTES + (ALIGN_BYTES - 1)) & ~(ALIGN_BYTES - 1))
static constexpr int max_entity_alloc_size = MAX_ENT_SIZE;

struct ent_header {
	ent_header  *next;
	ent_header **pprev_next;

	static inline ent_header *from_ent(entity *ent) noexcept
	{
		uint8_t *ptr = reinterpret_cast<uint8_t *>(ent);
		return reinterpret_cast<ent_header *>(ptr - ENT_HEADER_SIZE);
	}

	inline entity *ent() const noexcept
	{
		uint8_t *ptr = const_cast<uint8_t *>(reinterpret_cast<const uint8_t *>(this));
		return reinterpret_cast<entity *>(ptr + ENT_HEADER_SIZE);
	}
};

struct entity_alloc {
	char data[MAX_ENT_SIZE];

	inline ent_header *header() noexcept { return reinterpret_cast<ent_header *>(data); }
};

static vector<entity_alloc> entity_list;
ent_header                 *first_entity = nullptr;
ent_header                 *first_free   = nullptr;

static unordered_map<std::string, entity *> name_lookup;

static bool            cleanup                = false;
static bool            initialized            = false;
static bool            use_preallocated_array = true;
static type::entity_id entity_id_counter      = 1;

entity::entity() noexcept : id_(entity_id_counter++) {}

entity::~entity() noexcept
{
	if (!name_.empty())
		name_lookup.erase(name_);
	if (!cleanup)
		map::remove_entity(this);
}

void entity::destroy_later() noexcept
{
	destroy_later_ = true;
}

void entity::init() noexcept {}

void entity::tick(float) noexcept {}

void entity::render() noexcept {}

bool entity::savable() const noexcept
{
	return true;
}

bool entity::renderable() const noexcept
{
	return true;
}

void entity::on_frob() noexcept {}

entity *entity::find_by_id(type::entity_id id) noexcept
{
	ent_header *eh = first_entity;
	while (eh) {
		entity *ent = eh->ent();
		if (ent->id() == id)
			return ent;

		eh = eh->next;
	}

	return nullptr;
}

entity *entity::find_static_by_id(type::entity_id id) noexcept
{
	ent_header *eh = first_entity;
	while (eh) {
		entity *ent = eh->ent();
		if (ent->id() == id)
			return ent;

		eh = eh->next;
	}

	return nullptr;
}

void entity::set_name(const std::string &name) noexcept
{
	if (!name_.empty())
		name_lookup.erase(name_);

	auto it = name_lookup.find(name);
	if (it != name_lookup.end()) {
		warn("Name '%s' already exists on another entity! Overwriting.", name.c_str());
		entity *ent = it->second;
		ent->set_name("");
	}

	name_ = name;

	if (!name_.empty())
		name_lookup.emplace(name_, this);
}

entity *entity::find_by_name(const std::string &name) noexcept
{
	auto it = name_lookup.find(name);
	if (it == name_lookup.end())
		return nullptr;

	return it->second;
}

void entity::reset_id_counter() noexcept
{
	type::entity_id max_id = 0;

	ent_header *eh = first_entity;
	while (eh) {
		entity *ent = eh->ent();
		if (ent->id() > max_id)
			max_id = ent->id();
		eh = eh->next;
	}

	entity_id_counter = max_id + 1;
}

void entity::set_id(type::entity_id id) noexcept
{
	id_ = id;

	if (entity_id_counter <= id_)
		entity_id_counter = id_ + 1;
}

void entity::serialize(serializer &s) noexcept
{
	s >> name_ >> pos_ >> rot_;

	if (s.in())
		update();
}

void entity::tick_all(float seconds) noexcept
{
	ent_header *cur_eh = first_entity;
	while (cur_eh) {
		ent_header *next_eh = cur_eh->next;

		entity *ent = cur_eh->ent();
		if (ent->destroy_later_)
			delete ent;
		else
			ent->tick(seconds);

		cur_eh = next_eh;
	}
}

void entity::update() noexcept
{
	matrix_ = parent_ ? parent_->matrix_ : math::matrix::identity();
	if (rot_ != 0.0f)
		matrix_.rotate(0.0f, 0.0f, -1.0f, rot_);
	matrix_.translate(pos_.x, pos_.y, 0.0f);

	world_pos_.x = matrix_.t.x;
	world_pos_.y = matrix_.t.y;

	transformed_box_ = box_ + world_pos_;

	map::update_entity(this);
	on_update();

	entity *child = first_child_;
	while (child) {
		child->update();
		child = child->next_sibling_;
	}
}

void entity::set_layer_id(type::layer_id id) noexcept
{
	if (layer_id_ != 0)
		map::remove_entity(this);
	layer_id_ = id;
}

inline void entity::detach_internal() noexcept
{
	if (next_sibling_)
		next_sibling_->pprev_next_sibling = pprev_next_sibling;

	*pprev_next_sibling = next_sibling_;
	next_sibling_       = nullptr;
	parent_             = nullptr;
}

void entity::attach(entity *new_parent) noexcept
{
	if (new_parent == parent_)
		return;

	if (parent_)
		detach_internal();

	if (new_parent) {
		if (new_parent->first_child_) {
			new_parent->first_child_->pprev_next_sibling = &next_sibling_;
			next_sibling_                                = new_parent->first_child_;
		}

		pprev_next_sibling       = &new_parent->first_child_;
		new_parent->first_child_ = this;
	}

	update();
}

void entity::detach() noexcept
{
	detach_internal();
	update();
}

void entity::init_preallocation(size_t max_entities) noexcept
{
	if (initialized) {
		warn("%s: %s", __FUNCTION__, "Already initialized");
		return;
	}

	info("%s: %s", __FUNCTION__, "Using preallocated entity array allocation");

	entity_list.resize(max_entities);
	first_free = entity_list[0].header();

	ent_header **pprev_next = &first_free;
	for (entity_alloc &e : entity_list) {
		*pprev_next = e.header();
		pprev_next  = &e.header()->next;
	}

	initialized = true;
}

void entity::free_entities() noexcept
{
	ent_header *eh = first_entity;

	cleanup = true;

	while (eh) {
		ent_header *next = eh->next;
		delete eh->ent();
		eh = next;
	}

	cleanup = false;
}

static inline ent_header *prev_entity_alloc(ent_header *eh) noexcept
{
	return (reinterpret_cast<entity_alloc *>(eh) - 1)->header();
}

entity *entity::alloc(size_t size) noexcept
{
	if (!initialized) {
		info("%s: %s", __FUNCTION__, "Using malloc entity allocation");
		use_preallocated_array = false;
		initialized            = true;
	}

	size += ENT_HEADER_SIZE;

	if (use_preallocated_array) {
		if (size > max_entity_alloc_size) {
			error("%s: %s", __FUNCTION__, "Allocation size for entity exceeded");
			return nullptr;
		}

		/* ---------------------------------- *
		 * removed from freed pool            */

		ent_header *new_eh    = first_free;
		ent_header *next_free = first_free->next;
		first_free            = next_free;

		/* ---------------------------------- *
		 * insert in to allocated pool        */

		if (new_eh == entity_list[0].header()) {
			new_eh->next       = first_entity;
			new_eh->pprev_next = &first_entity;
			if (!first_entity)
				first_entity = new_eh;
		} else {
			ent_header *prev   = prev_entity_alloc(new_eh);
			new_eh->next       = prev->next;
			new_eh->pprev_next = &prev->next;
			prev->next         = new_eh;
		}

		if (new_eh->next)
			new_eh->next->pprev_next = &new_eh->next;

		return new_eh->ent();
	} else {
		ent_header *new_eh = reinterpret_cast<ent_header *>(malloc(size));
		if (!first_entity) {
			first_entity = new_eh;
			new_eh->next = nullptr;
		} else {
			first_entity->pprev_next = &new_eh->next;
			new_eh->next             = first_entity;
			first_entity             = new_eh;
		}
		new_eh->pprev_next = &first_entity;
		return new_eh->ent();
	}
}

void entity::free() noexcept
{
	auto *eh = ent_header::from_ent(this);

	if (use_preallocated_array) {
		/* ---------------------------------- *
		 * remove from allocated pool         */

		if (eh == first_entity)
			first_entity = eh->next;

		*eh->pprev_next = eh->next;
		if (eh->next)
			eh->next->pprev_next = eh->pprev_next;

		/* ---------------------------------- *
		 * insert in to freed pool            */

		if (first_free || first_free > eh) {
			ent_header *closest = first_free;
			if (closest < eh) {
				if (cleanup) {
					closest = prev_entity_alloc(eh);
				} else {
					while (closest->next && closest->next < eh)
						closest = closest->next;
				}
				eh->next      = closest->next;
				closest->next = eh;
			} else {
				eh->next   = first_free;
				first_free = eh;
			}

		} else {
			eh->next   = first_free;
			first_free = eh;
		}
	} else {
		*eh->pprev_next = eh->next;
		if (eh->next)
			eh->next->pprev_next = eh->pprev_next;
		::free(eh);
	}
}

const std::vector<entity *> entity::links() const
{
	std::vector<entity *> ents;
	ents.reserve(links_.size());

	for (type::entity_id id : links_) {
		entity *ent = find_by_id(id);
		if (ent)
			ents.push_back(ent);
	}

	return ents;
}

/* ------------------------------------------------------------------------- */
} // namespace crpg
