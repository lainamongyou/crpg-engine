#include "file-helpers.hpp"
#include "logging.hpp"

#include <cstdio>
#include <cstdlib>

#ifdef _WIN32
#include <windows.h>
#endif

namespace crpg {

static void default_callback(log_level level, const char *format, va_list args) noexcept
{
	char out[4096];
	vsnprintf(out, sizeof(out), format, args);

	switch (level) {
	case log_level::debug:
		fprintf(stdout, "debug: %s\n", out);
		break;
	case log_level::info:
		fprintf(stdout, "info: %s\n", out);
		break;
	case log_level::warning:
		fprintf(stdout, "warning: %s\n", out);
		break;
	case log_level::error:
		fprintf(stderr, "error: %s\n", out);
		break;
	}

#ifdef _WIN32
	OutputDebugStringW(os_utf8_to_wide(out).c_str());
#endif
}

static log_callback cur_log_callback = default_callback;

void set_log_callback(log_callback callback) noexcept
{
	cur_log_callback = callback;
}

void debug(const char *format, ...) noexcept
{
	va_list args;
	va_start(args, format);
	cur_log_callback(log_level::debug, format, args);
	va_end(args);
}

void info(const char *format, ...) noexcept
{
	va_list args;
	va_start(args, format);
	cur_log_callback(log_level::info, format, args);
	va_end(args);
}

void warn(const char *format, ...) noexcept
{
	va_list args;
	va_start(args, format);
	cur_log_callback(log_level::warning, format, args);
	va_end(args);
}

void error(const char *format, ...) noexcept
{
	va_list args;
	va_start(args, format);
	cur_log_callback(log_level::error, format, args);
	va_end(args);

	std::abort();
}
}; // namespace crpg
