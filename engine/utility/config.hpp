#pragma once

#include "../export.h"
#include <string>
#include <unordered_map>

namespace crpg {

class EXPORT config {
	std::string path_;

	typedef std::unordered_map<std::string, std::string> section_t;
	typedef std::unordered_map<std::string, section_t>   map_t;

	map_t sections;
	map_t defaults;
	bool  opened_ = false;

	std::string get_default(const char *section, const char *name) const noexcept;

public:
	enum open_type { existing, always };

	inline config() noexcept {}
	inline config(const char *path, open_type type = always) noexcept { open(path, type); }

	inline void create(const char *path__) noexcept
	{
		close();
		path_ = path__;
	}

	bool open(const char *path, open_type type = always) noexcept;
	bool save() noexcept;

	inline bool opened() const noexcept { return opened_; }

	bool load_defaults(const char *path) noexcept;

	inline void close() noexcept
	{
		path_.clear();
		sections.clear();
		defaults.clear();
		opened_ = false;
	}

	inline const std::string &path() const noexcept { return path_; }

	void        set(const char *section, const char *name, const std::string &str) noexcept;
	void        set_default(const char *section, const char *name, const std::string &str) noexcept;
	std::string get(const char *section, const char *name) const noexcept;

	inline void set(const char *section, const char *name, const char *v) noexcept
	{
		set(section, name, std::string(v));
	}
	inline void set(const char *section, const char *name, int v) noexcept
	{
		set(section, name, std::to_string(v));
	}
	inline void set(const char *section, const char *name, bool v) noexcept
	{
		set(section, name, v ? "true" : "false");
	}
	inline void set(const char *section, const char *name, float v) noexcept
	{
		set(section, name, std::to_string(v));
	}

	inline void set_default(const char *section, const char *name, const char *val) noexcept
	{
		set_default(section, name, std::string(val));
	}

	inline void set_default(const char *section, const char *name, int val) noexcept
	{
		set_default(section, name, std::to_string(val));
	}

	inline void set_default(const char *section, const char *name, bool val) noexcept
	{
		set_default(section, name, val ? "true" : "false");
	}

	inline void set_default(const char *section, const char *name, float val) noexcept
	{
		set_default(section, name, std::to_string(val));
	}

	int get_int(const char *section, const char *name) const noexcept
	{
		try {
			return std::stoi(get(section, name));
		} catch (...) {
			return 0;
		}
	}

	bool get_bool(const char *section, const char *name) const noexcept
	{
		std::string val = get(section, name);
		return val == "true" || val == "1";
	}

	float get_float(const char *section, const char *name) const noexcept
	{
		try {
			return stof(get(section, name));
		} catch (...) {
			return 0;
		}
	}

	bool exists(const char *section, const char *name) const noexcept;
	void remove(const char *section, const char *name) noexcept;
};
} // namespace crpg
