#pragma once

#include <stdarg.h>
#include <string>

#include "string-helpers.hpp"
#include "../export.h"

namespace crpg {

enum class log_level : int { error, warning, info, debug };

typedef void (*log_callback)(log_level, const char *, va_list) noexcept;
EXPORT void set_log_callback(log_callback callback) noexcept;

EXPORT void debug(const char *format, ...) noexcept;
EXPORT void warn(const char *format, ...) noexcept;
EXPORT void info(const char *format, ...) noexcept;
EXPORT void error(const char *format, ...) noexcept;

EXPORT std::string vstrprintf(const char *format, va_list args) noexcept;
EXPORT std::string strprintf(const char *format, ...) noexcept;

#define UNEXPECTED_EOF_EXCEPTION(path) strprintf("File '%s' unexpected EOF", path)
}; // namespace crpg
