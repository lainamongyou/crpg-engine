#include "fserializer.hpp"

using namespace std;

#define OUT_FLAGS (ios_base::binary | ios_base::out | ios_base::trunc)

namespace crpg {

ofserializer::ofserializer(const char *filename) noexcept : str(os_path(filename), OUT_FLAGS)
{
	mark_if_fail();
}

ofserializer::ofserializer(const string &filename) noexcept : str(os_path(filename), OUT_FLAGS)
{
	mark_if_fail();
}

void ofserializer::open(const std::string &filename) noexcept
{
	str.open(os_path(filename), OUT_FLAGS);
	mark_if_fail();
}

serializer &ofserializer::serialize(void *data, size_t size) noexcept
{
	str.write((const char *)data, size);
	if (str.fail())
		_state |= serializer::_fail;
	if (str.bad())
		_state |= serializer::_bad;
	if (!_ok())
		str.clear();
	return *this;
}

size_t ofserializer::tell() noexcept
{
	return str.tellp();
}

bool ofserializer::out() const noexcept
{
	return true;
}

/* ------------------------------------------------------------------------- */

serializer &ifserializer::serialize(void *data, size_t size) noexcept
{
	str.read((char *)data, size);
	if (str.fail())
		_state |= serializer::_fail;
	if (str.bad())
		_state |= serializer::_bad;
	if (str.eof())
		_state |= serializer::_eof;
	if (!_ok())
		str.clear();
	return *this;
}

size_t ifserializer::tell() noexcept
{
	return str.tellg();
}

bool ifserializer::out() const noexcept
{
	return false;
}

serializer &ifserializer::seek(int64_t pos, serializer::seek_type type) noexcept
{
	ios_base::seekdir dir = ios_base::beg;

	switch (type) {
	case serializer::seek_type::beg:
		dir = ios_base::beg;
		break;
	case serializer::seek_type::cur:
		dir = ios_base::cur;
		break;
	case serializer::seek_type::end:
		dir = ios_base::end;
		break;
	}

	str.seekg(pos, dir);
	if (str.fail())
		_state |= serializer::_fail;
	if (str.bad())
		_state |= serializer::_bad;
	if (!_ok())
		str.clear();
	return *this;
}

/* ------------------------------------------------------------------------- */
} // namespace crpg
