#include "stringserializer.hpp"

#include <algorithm>

namespace crpg {

/* ------------------------------------------------------------------------- */

serializer &ostringserializer::serialize(void *data, size_t size) noexcept
{
	if (_ok())
		str.append((const char *)data, size);
	return *this;
}

size_t ostringserializer::tell() noexcept
{
	return _ok() ? str.size() : (size_t)-1;
}

bool ostringserializer::out() const noexcept
{
	return true;
}

/* ------------------------------------------------------------------------- */

serializer &istringserializer::serialize(void *data, size_t size) noexcept
{
	if (!_ok())
		return *this;
	if (!str)
		return *this;

	if (pos >= str->size()) {
		_state |= serializer::_eof | serializer::_fail;
		return *this;
	}

	size_t total = std::min(size, str->size() - pos);

	memcpy(data, &(*str)[pos], total);
	pos += total;

	if (total > size)
		_state |= serializer::_eof | serializer::_fail;

	return *this;
}

serializer &istringserializer::seek(int64_t offset, serializer::seek_type type) noexcept
{
	int64_t abs_pos = 0;

	if (!_ok())
		return *this;

	switch (type) {
	case serializer::seek_type::beg:
		abs_pos = offset;
		break;
	case serializer::seek_type::cur:
		abs_pos = (int64_t)pos + offset;
		break;
	case serializer::seek_type::end:
		abs_pos = (int64_t)str->size() + offset;
		break;
	}

	if (abs_pos < 0 || abs_pos > (int64_t)str->size())
		_state |= serializer::_fail;
	else
		pos = (size_t)abs_pos;

	return *this;
}

size_t istringserializer::tell() noexcept
{
	return _ok() ? pos : (size_t)-1;
}

bool istringserializer::out() const noexcept
{
	return false;
}

/* ------------------------------------------------------------------------- */
} // namespace crpg
