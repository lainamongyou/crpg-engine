#include "serializer.hpp"

namespace crpg {

serializer::~serializer() noexcept {}

size_t serializer::tell() noexcept
{
	return (size_t)-1;
}

serializer &serializer::seek(int64_t pos, seek_type type) noexcept
{
	(void)pos;
	(void)type;
	_state |= serializer::_fail;
	return *this;
}

/* ------------------------------------------------------------------------- */
} // namespace crpg
