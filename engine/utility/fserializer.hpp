#pragma once

#include <fstream>
#include "serializer.hpp"
#include "file-helpers.hpp"

namespace crpg {

class EXPORT ofserializer : public serializer {
	std::ofstream str;

	inline void mark_if_fail() noexcept
	{
		if (str.fail()) {
			_state |= serializer::_fail;
			str.clear();
		}
	}

public:
	inline ofserializer() noexcept {}
	explicit ofserializer(const char *filename) noexcept;
	explicit ofserializer(const std::string &filename) noexcept;

	void        open(const std::string &filename) noexcept;
	inline void open(const char *filename) noexcept { open(std::string(filename)); }

	inline bool is_open() const noexcept { return str.is_open(); }

	inline void close() noexcept
	{
		str.close();
		mark_if_fail();
	}

	virtual serializer &serialize(void *data, size_t size) noexcept override;

	virtual size_t tell() noexcept override;
	virtual bool   out() const noexcept override;
};

class EXPORT ifserializer : public serializer {
	std::ifstream str;

	inline void mark_if_fail() noexcept
	{
		if (str.fail()) {
			_state |= serializer::_fail;
			str.clear();
		}
	}

public:
	inline ifserializer() noexcept {}
	inline ifserializer(const char *filename) noexcept
	        : str(os_path(filename), std::ios_base::binary | std::ios_base::in)
	{
		mark_if_fail();
	}
	inline ifserializer(const std::string &filename) noexcept
	        : str(os_path(filename), std::ios_base::binary | std::ios_base::in)
	{
		mark_if_fail();
	}

	inline void open(const std::string &filename) noexcept
	{
		str.open(os_path(filename), std::ios_base::binary | std::ios_base::in);
		mark_if_fail();
	}

	inline void open(const char *filename) noexcept { open(std::string(filename)); }

	inline bool is_open() const noexcept { return str.is_open(); }

	inline size_t gcount() const noexcept { return str.gcount(); }

	inline void close() noexcept
	{
		str.close();
		mark_if_fail();
	}

	virtual serializer &serialize(void *data, size_t size) noexcept override;

	virtual size_t tell() noexcept override;
	virtual bool   out() const noexcept override;

	virtual serializer &seek(int64_t pos, serializer::seek_type type) noexcept override;
};
} // namespace crpg
