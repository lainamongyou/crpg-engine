#include "config.hpp"
#include "file-helpers.hpp"
#include "logging.hpp"

#include <fstream>
#include <cstring>

using namespace std;

#ifdef _WIN32
#define NEWLINE "\r\n"
#define BOM "\xEF\xBB\xBF"
#else
#define NEWLINE "\n"
#endif

namespace crpg {

bool config::open(const char *path__, open_type type) noexcept
try {
	close();
	path_ = path__;

	ifstream f(os_path(path__), ios_base::binary | ios_base::in);
	if (f.fail()) {
		if (type == always)
			return true;
		throw strprintf("Failed to open file '%s'", path_.c_str());
	}

	f.seekg(0, ios_base::end);
	if (f.fail())
		throw strprintf("Failed to seek file '%s'", path_.c_str());

	size_t size = f.tellg();
	f.seekg(0);

#ifdef _WIN32
	if (size >= 3) {
		char bom[4];
		f.read(bom, 3);
		bom[3] = 0;
		if (strcmp(bom, BOM) != 0) {
			f.seekg(0);
		}
	}
#endif

	if (f.fail())
		throw strprintf("Failed to seek file '%s'", path_.c_str());

	std::string text;
	if (size) {
		text.resize(size + 1);
		f.read(&text[0], size);
		if (f.fail())
			throw strprintf("Failed to read file '%s'", path_.c_str());

		text += "\n";

		section_t cur_section;
		string    cur_section_name;

		const char *line_next = text.data();
		while (*line_next) {
			const char *line_start = line_next;
			const char *line_end   = strchr(line_start, '\n');
			if (!line_end)
				break;

			line_next = line_end + 1;
			if (line_end != line_start && line_end[-1] == '\r')
				--line_end;
			if ((line_end - line_start) == 0)
				continue;

			string line;
			line.append(line_start, line_end - line_start);

			if (line.front() == '[') {
				cur_section_name.clear();
				cur_section.clear();

				if (line.back() != ']')
					continue;

				cur_section_name = line.substr(1, line.size() - 2);
			} else if (cur_section_name.empty() || line.front() == '#' || line.front() == ';') {
				continue;
			}

			auto eq = line.find('=');
			if (eq == string::npos || eq == 0)
				continue;

			string name = line.substr(0, eq);
			string val  = line.substr(eq + 1, line.size() - eq);
			set(cur_section_name.c_str(), name.c_str(), val);
		}
	}

	opened_ = true;
	return true;

} catch (const std::string &text) {
	warn("%s: %s", __FUNCTION__, text.c_str());
	path_.clear();
	return false;
}

bool config::load_defaults(const char *path) noexcept
try {
	config f;
	bool   success = f.open(path, existing);
	if (!success)
		throw strprintf("Failed to load defaults from '%s'", path);
	defaults = std::move(f.sections);
	return true;

} catch (const std::string &text) {
	warn("%s: %s", __FUNCTION__, text.c_str());
	path_.clear();
	return false;
}

bool config::save() noexcept
try {
	std::string out;
	bool        first = true;

	for (auto &section : sections) {
		const string &section_name = section.first;
		section_t    &items        = section.second;

		if (!items.size())
			continue;

		if (!first) {
			out += NEWLINE;
			first = false;
		}

		out += "[" + section_name + "]" NEWLINE;
		for (auto &item : items)
			out += item.first + "=" + item.second + NEWLINE;
	}

	string temp_file = path_ + ".tmp";

	ofstream f(os_path(temp_file), ios_base::binary | ios_base::out | ios_base::trunc);
	if (f.fail())
		throw strprintf("Failed to create file '%s'", path_.c_str());

#ifdef _WIN32
	f.write(BOM, 3);
#endif
	f.write(out.data(), out.size());
	f.close();

	replace_file(temp_file, path_);
	return true;

} catch (const std::string &text) {
	warn("%s: %s", __FUNCTION__, text.c_str());
	return false;
}

void config::set(const char *section, const char *name, const std::string &str) noexcept
{
	sections[section][name] = str;
}

void config::set_default(const char *section, const char *name, const std::string &str) noexcept
{
	defaults[section][name] = str;
}

std::string config::get_default(const char *section, const char *name) const noexcept
{
	auto section_pair = defaults.find(section);
	if (section_pair == defaults.end())
		return string();

	const section_t &items     = section_pair->second;
	auto             item_pair = items.find(name);
	if (item_pair == items.end())
		return string();

	return item_pair->second;
}

std::string config::get(const char *section, const char *name) const noexcept
{
	auto section_pair = sections.find(section);
	if (section_pair == sections.end())
		return get_default(section, name);

	const section_t &items     = section_pair->second;
	auto             item_pair = items.find(name);
	if (item_pair == items.end())
		return get_default(section, name);

	return item_pair->second;
}

bool config::exists(const char *section, const char *name) const noexcept
{
	auto section_pair = sections.find(section);
	if (section_pair == sections.end())
		return false;

	const section_t &items     = section_pair->second;
	auto             item_pair = items.find(name);
	return item_pair != items.end();
}

void config::remove(const char *section, const char *name) noexcept
{
	auto section_pair = sections.find(section);
	if (section_pair == sections.end())
		return;

	section_t &items     = section_pair->second;
	auto       item_pair = items.find(name);
	if (item_pair == items.end())
		return;

	items.erase(item_pair);
}
} // namespace crpg
