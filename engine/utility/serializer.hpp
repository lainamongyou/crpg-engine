#pragma once

#include <map>
#include <vector>
#include <string>
#include <cstring>
#include <cassert>
#include <cstdint>
#include <type_traits>
#include <unordered_map>

#include "../export.h"

namespace crpg {

class EXPORT serializer {
protected:
	enum _state_flag { _fail = (1 << 0), _bad = (1 << 1), _eof = (1 << 2) };
	int _state = 0;

	inline bool _ok() const { return _state == 0; }

public:
	virtual ~serializer();

	enum seek_type { beg, cur, end };

	virtual serializer &serialize(void *data, size_t size) noexcept = 0;

	virtual bool   out() const noexcept = 0;
	inline bool    in() const noexcept { return !out(); }
	inline bool    loading() const noexcept { return in(); }
	inline bool    saving() const noexcept { return out(); }
	inline bool    eof() const noexcept { return (_state & _eof) != 0; }
	inline bool    fail() const noexcept { return (_state & _fail) != 0; }
	inline bool    bad() const noexcept { return (_state & _bad) != 0; }
	inline void    clear() noexcept { _state = 0; }
	virtual size_t tell() noexcept;

	virtual serializer &seek(int64_t pos, seek_type type) noexcept;
	inline serializer  &seek(size_t abs_pos) noexcept
	{
		seek((int64_t)abs_pos, seek_type::beg);
		return *this;
	}

	/* NOTE: lshift is write only, rshift is both read/write */

#define op_lshift(type)                                                                                                \
	friend inline serializer &operator<<(serializer &s, type val) noexcept                                         \
	{                                                                                                              \
		s.serialize(&val, sizeof(val));                                                                        \
		return s;                                                                                              \
	}

	op_lshift(bool);
	op_lshift(char);
	op_lshift(unsigned char);
	op_lshift(short);
	op_lshift(unsigned short);
	op_lshift(int);
	op_lshift(unsigned int);
	op_lshift(long);
	op_lshift(unsigned long);
	op_lshift(long long);
	op_lshift(unsigned long long);
#undef op_lshift

#define op_rshift(type)                                                                                                \
	friend inline serializer &operator>>(serializer &s, type &val) noexcept                                        \
	{                                                                                                              \
		s.serialize(&val, sizeof(val));                                                                        \
		return s;                                                                                              \
	}

	op_rshift(bool);
	op_rshift(char);
	op_rshift(unsigned char);
	op_rshift(short);
	op_rshift(unsigned short);
	op_rshift(int);
	op_rshift(unsigned int);
	op_rshift(long);
	op_rshift(unsigned long);
	op_rshift(long long);
	op_rshift(unsigned long long);
#undef op_rshift

	friend inline serializer &operator<<(serializer &s, const char *str) noexcept
	{
		assert(s.out());

		if (!str || !*str) {
			s << (size_t)0;
			return s;
		}

		size_t size = strlen(str);
		s << size;
		s.serialize((void *)str, (size_t)size);
		return s;
	}

	friend inline crpg::serializer &operator<<(crpg::serializer &s, const std::string &str) noexcept
	{
		assert(s.out());

		s << str.size();
		if (str.size())
			s.serialize((void *)&str[0], str.size());
		return s;
	}

	friend inline crpg::serializer &operator>>(crpg::serializer &s, std::string &str) noexcept
	{
		if (s.out()) {
			s << str.size();
		} else {
			size_t size;
			s >> size;
			str.resize(size);
		}

		if (str.size())
			s.serialize(&str[0], str.size());
		return s;
	}

	template<class T> friend inline crpg::serializer &operator<<(crpg::serializer &s, const T &v) noexcept
	{
		s.serialize((void *)&v, sizeof(v));
		return s;
	}

	template<class T> friend inline crpg::serializer &operator>>(crpg::serializer &s, T &v) noexcept
	{
		s.serialize(&v, sizeof(v));
		return s;
	}

	template<typename K, typename T>
	friend inline crpg::serializer &operator<<(crpg::serializer &s, const std::map<K, T> &m) noexcept
	{
		s << m.size();
		for (auto &pair : m)
			s << pair.first << pair.second;
		return s;
	}

	template<typename K, typename T>
	friend inline crpg::serializer &operator>>(crpg::serializer &s, std::map<K, T> &m) noexcept
	{
		if (s.out())
			return s << m;

		m.clear();

		size_t size;
		s >> size;
		for (size_t i = 0; i < size; i++) {
			K key;
			T data;

			s >> key >> data;
			m[key] = std::move(data);
		}
		return s;
	}

	template<typename K, typename T>
	friend inline crpg::serializer &operator<<(crpg::serializer &s, const std::unordered_map<K, T> &m) noexcept
	{
		s << m.size();
		for (auto &pair : m)
			s << pair.first << pair.second;
		return s;
	}

	template<typename K, typename T>
	friend inline crpg::serializer &operator>>(crpg::serializer &s, std::unordered_map<K, T> &m) noexcept
	{
		if (s.out())
			return s << m;

		m.clear();

		size_t size;
		s >> size;
		m.reserve(size);
		for (size_t i = 0; i < size; i++) {
			K key;
			T data;

			s >> key >> data;
			m[key] = std::move(data);
		}
		return s;
	}

	template<typename T>
	friend inline crpg::serializer &operator<<(crpg::serializer &s, const std::vector<T> &v) noexcept
	{
		s << v.size();
		if constexpr (std::is_trivially_copyable_v<T>) {
			if (v.size())
				return s.serialize((void *)&v[0], sizeof(T) * v.size());
			return s;
		} else {
			for (auto &val : v)
				s << val;
			return s;
		}
	}

	template<typename T> friend inline crpg::serializer &operator>>(crpg::serializer &s, std::vector<T> &v) noexcept
	{
		if (s.out())
			return s << v;

		size_t size;
		s >> size;
		if constexpr (std::is_trivially_copyable_v<T>) {
			v.resize(size);
			if (size)
				return s.serialize(&v[0], sizeof(T) * size);
			return s;
		} else {
			v.resize(0);
			v.reserve(size);
			for (size_t i = 0; i < size; i++) {
				T val;
				s >> val;
				v.push_back(std::move(val));
			}

			return s;
		}
	}

	struct ignore_type {
		int64_t size;
	};

	template<typename T> inline ignore_type ignore() noexcept { return {(int64_t)sizeof(T)}; }

	inline ignore_type ignore(size_t size) noexcept { return {(int64_t)size}; }

	template<typename T> inline ignore_type ignore_vector() noexcept
	{
		size_t vec_size;
		*this >> vec_size;
		return {(int64_t)(vec_size * sizeof(T))};
	}

	template<typename K, typename T> inline ignore_type ignore_map() noexcept
	{
		size_t map_size;
		*this >> map_size;
		return {(int64_t)(map_size * (sizeof(K) + sizeof(T)))};
	}

	inline ignore_type ignore_string() noexcept
	{
		size_t str_size;
		*this >> str_size;
		return {(int64_t)str_size};
	}

	friend inline serializer &operator>>(serializer &s, ignore_type ignore) noexcept
	{
		return s.seek(ignore.size, seek_type::cur);
	}
};
} // namespace crpg
