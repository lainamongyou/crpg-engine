#include <cstdio>

#include "string-helpers.hpp"

using namespace std::string_literals;

namespace crpg {

std::string vstrprintf(const char *format, va_list args) noexcept
{
	va_list args_copy;
	if (!format)
		return std::string();

	va_copy(args_copy, args);

	std::string str;
	int         size = (int)vsnprintf(nullptr, 0, format, args_copy);
	str.resize(size);
	vsnprintf(&str[0], size + 1, format, args);

	va_end(args_copy);

	return str;
}

std::string strprintf(const char *format, ...) noexcept
{
	va_list args;
	va_start(args, format);

	std::string str;
	str = vstrprintf(format, args);
	va_end(args);

	return str;
}

static void find_replace_param(std::string &out, const std::string &from, const std::string &to) noexcept
{
	size_t pos = 0;
	while ((pos = out.find('%', pos)) != std::string::npos) {
		if (out.compare(pos, 2, "%%") == 0) {
			pos += 2;
			continue;
		}
		if (out.compare(pos, from.size(), from) == 0) {
			out.replace(pos, from.size(), to);
			pos += to.size();
		}
	}
}

std::string strprint(const std::string &fmt, const std::string &p1) noexcept
{
	std::string out = fmt;
	if (!p1.empty())
		find_replace_param(out, "%1"s, p1);
	find_replace(out, "%%"s, "%"s);
	return out;
}

std::string strprint(const std::string &fmt, const std::string &p1, const std::string &p2) noexcept
{
	std::string out = fmt;
	if (!p1.empty())
		find_replace_param(out, "%1"s, p1);
	if (!p2.empty())
		find_replace_param(out, "%2"s, p2);
	find_replace(out, "%%"s, "%"s);
	return out;
}

std::string strprint(const std::string &fmt,
                     const std::string &p1,
                     const std::string &p2,
                     const std::string &p3) noexcept
{
	std::string out = fmt;
	if (!p1.empty())
		find_replace_param(out, "%1"s, p1);
	if (!p2.empty())
		find_replace_param(out, "%2"s, p2);
	if (!p3.empty())
		find_replace_param(out, "%3"s, p3);
	find_replace(out, "%%"s, "%"s);
	return out;
}

std::string strprint(const std::string &fmt,
                     const std::string &p1,
                     const std::string &p2,
                     const std::string &p3,
                     const std::string &p4) noexcept
{
	std::string out = fmt;
	if (!p1.empty())
		find_replace_param(out, "%1"s, p1);
	if (!p2.empty())
		find_replace_param(out, "%2"s, p2);
	if (!p3.empty())
		find_replace_param(out, "%3"s, p3);
	if (!p4.empty())
		find_replace_param(out, "%4"s, p4);
	find_replace(out, "%%"s, "%"s);
	return out;
}

std::string strprint(const std::string &fmt,
                     const std::string &p1,
                     const std::string &p2,
                     const std::string &p3,
                     const std::string &p4,
                     const std::string &p5) noexcept
{
	std::string out = fmt;
	if (!p1.empty())
		find_replace_param(out, "%1"s, p1);
	if (!p2.empty())
		find_replace_param(out, "%2"s, p2);
	if (!p3.empty())
		find_replace_param(out, "%3"s, p3);
	if (!p4.empty())
		find_replace_param(out, "%4"s, p4);
	if (!p5.empty())
		find_replace_param(out, "%5"s, p5);
	find_replace(out, "%%"s, "%"s);
	return out;
}

void find_replace(std::string &str, const std::string &from, const std::string &to) noexcept
{
	size_t pos = 0;
	while ((pos = str.find(from, pos)) != std::string::npos) {
		str.replace(pos, from.size(), to);
		pos += to.size();
	}
}

[[nodiscard]] std::string find_replace(const std::string &str, const std::string &from, const std::string &to) noexcept
{
	std::string out = str;
	find_replace(out, from, to);
	return out;
}

string_sequence split_string(const std::string &str, char ch) noexcept
{
	string_sequence seq;

	size_t pos  = 0;
	size_t last = 0;
	for (;;) {
		pos = str.find(ch, pos);
		if (pos == std::string::npos)
			pos = str.size();

		size_t count = pos - last;
		if (count)
			seq.emplace_back(str.substr(last, count));
		if (pos == str.size())
			break;

		last = ++pos;
	}

	return seq;
}

string_sequence split_string(const std::string &str, const std::string &split_text) noexcept
{
	string_sequence seq;

	size_t pos  = 0;
	size_t last = 0;
	for (;;) {
		pos = str.find(split_text, pos);
		if (pos == std::string::npos)
			pos = str.size();

		size_t count = pos - last;
		if (count)
			seq.emplace_back(str.substr(last, count));
		if (pos == str.size())
			break;

		pos += split_text.size();
		last = pos;
	}

	return seq;
}

} // namespace crpg
