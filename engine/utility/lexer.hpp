#pragma once

#include "../export.h"

#include <string>
#include <cstring>

namespace crpg {

EXPORT bool valid_int_str(const char *str, size_t n) noexcept;
EXPORT bool valid_float_str(const char *str, size_t n) noexcept;

static inline bool is_whitespace(char ch) noexcept
{
	return ch == ' ' || ch == '\r' || ch == '\t' || ch == '\n';
}

static inline bool is_newline(char ch) noexcept
{
	return ch == '\r' || ch == '\n';
}

static inline bool is_space_or_tab(const char ch) noexcept
{
	return ch == ' ' || ch == '\t';
}

static inline bool is_newline_pair(char ch1, char ch2) noexcept
{
	return (ch1 == '\r' && ch2 == '\n') || (ch1 == '\n' && ch2 == '\r');
}

static inline int newline_size(const char *array) noexcept
{
	if (strncmp(array, "\r\n", 2) == 0 || strncmp(array, "\n\r", 2) == 0)
		return 2;
	else if (*array == '\r' || *array == '\n')
		return 1;

	return 0;
}

/* ------------------------------------------------------------------------- */

struct EXPORT strref {
	const char *array = nullptr;
	size_t      len   = 0;

	inline strref &clear() noexcept
	{
		array = nullptr;
		len   = 0;
		return *this;
	}

	inline strref &set(const char *array, size_t size) noexcept
	{
		this->array = array;
		this->len   = size;
		return *this;
	}
	inline strref &set(const std::string &str, size_t pos, size_t size) noexcept
	{
		array = str.c_str() + pos;
		len   = size;
		return *this;
	}

	inline strref &operator+=(size_t size) noexcept
	{
		if (array)
			len += size;
		return *this;
	}
	inline strref &operator+=(const strref &ref) noexcept { return *this += ref.len; }
	inline strref  operator+(const strref &ref) const noexcept { return strref(*this) += ref; }
	inline strref  operator+(size_t size) const noexcept { return strref(*this) += size; }

	inline bool empty() const noexcept { return !array || !len; }

	int compare(const char *str) const noexcept;
	int compare(const strref &ref) const noexcept;
	int comparei(const char *str) const noexcept;
	int comparei(const strref &ref) const noexcept;

	inline int compare(const std::string &str) const noexcept { return compare(str.c_str()); }
	inline int comparei(const std::string &str) const noexcept { return comparei(str.c_str()); }

	inline bool operator==(const char *str) const noexcept { return compare(str) == 0; }
	inline bool operator==(const strref &ref) const noexcept { return compare(ref) == 0; }
	inline bool operator==(const std::string &str) const noexcept { return *this == str.c_str(); }

	inline bool valid_int() const noexcept { return valid_int_str(array, len); }
	inline bool valid_float() const noexcept { return valid_float_str(array, len); }
	inline bool is_whitespace() const noexcept { return crpg::is_whitespace(array[0]); }
	inline bool is_newline() const noexcept { return crpg::is_newline(array[0]); }
	inline bool is_space_or_tab() const noexcept { return crpg::is_space_or_tab(array[0]); }
	inline bool is_newline_pair() const noexcept { return crpg::is_newline_pair(array[0], array[1]); }

	inline char operator[](size_t idx) const noexcept { return array[idx]; }

	void append_stringified_sequence_to_string(std::string &out) noexcept;
};

/* ------------------------------------------------------------------------- */

/*
 * A "base" token is one of four things:
 *   1.) A sequence of alpha characters
 *   2.) A sequence of numeric characters
 *   3.) A single whitespace character if whitespace is not ignored
 *   4.) A single character that does not fall into the above 3 categories
 */

enum class token_type {
	none,
	alpha,
	digit,
	whitespace,
	other,
};

struct token {
	using enum token_type;

	strref     text;
	token_type type = token_type::none;

	inline bool is_alnum() const noexcept { return type == token_type::digit || type == token_type::alpha; }
	inline bool is_alpha() const noexcept { return type == token_type::alpha; }
	inline bool is_digit() const noexcept { return type == token_type::digit; }
	inline bool is_whitespace() const noexcept { return type == token_type::whitespace; }
	inline bool is_space_or_tab() const noexcept
	{
		return type == token_type::whitespace && text.is_space_or_tab();
	}
	inline bool is_newline() const noexcept { return type == token_type::whitespace && text.is_newline(); }
};

/* ------------------------------------------------------------------------- */

enum class error_type {
	error,
	warning,
};

struct error_item {
	std::string text;
	const char *file = nullptr;
	int         row  = 0;
	int         col  = 0;
	error_type  type = error_type::error;
};

/* ------------------------------------------------------------------------- */

struct EXPORT error_data {
	std::vector<error_item> errors;

	std::string make_string() noexcept;

	inline void add(const char *file, int row, int col, const char *message, error_type type) noexcept
	{
		errors.emplace_back(message, file, row, col, type);
	}

	inline bool success() const noexcept
	{
		for (auto &error : errors) {
			if (error.type == error_type::error) {
				return false;
			}
		}
		return true;
	}
	inline bool failed() const noexcept { return !success(); }
};

/* ------------------------------------------------------------------------- */

enum ignore_whitespace : bool {
	no,
	yes,
};

struct EXPORT lexer {
	std::string text;
	const char *offset = nullptr;

	inline lexer() noexcept = default;
	inline lexer(std::string &&str) noexcept
	{
		text   = std::move(str);
		offset = text.c_str();
	}

	inline void start(std::string &&str) noexcept
	{
		text   = std::move(str);
		offset = text.c_str();
	}

	inline void reset() noexcept { offset = text.c_str(); }

	bool get_token(token &token, ignore_whitespace iws = ignore_whitespace::no) noexcept;
	void get_string_offset(const char *str, int &row, int &col) noexcept;

	inline bool peek_token(token &token, ignore_whitespace iws = ignore_whitespace::no) noexcept
	{
		const char *prev_offset = offset;
		bool        success     = get_token(token, iws);
		offset                  = prev_offset;
		return success;
	}

	inline bool valid() const noexcept { return !text.empty(); }
};

struct lexer_sequence {
	crpg::lexer &lexer;
	crpg::token  orig_token;
	crpg::token &token;
	const char  *last_offset;

public:
	explicit inline lexer_sequence(crpg::lexer &lexer, crpg::token &token) noexcept
	        : lexer(lexer), orig_token(token), token(token), last_offset(lexer.offset)
	{
	}
	inline ~lexer_sequence() noexcept
	{
		if (last_offset) {
			token          = orig_token;
			token.text.len = last_offset - token.text.array;
			lexer.offset   = last_offset;
		}
	}

	inline void allow() noexcept { last_offset = lexer.offset; }
};

} // namespace crpg
