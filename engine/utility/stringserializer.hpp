#pragma once

#include "serializer.hpp"

namespace crpg {

class EXPORT ostringserializer : public serializer {
	std::string &str;

public:
	inline ostringserializer(std::string &str_) noexcept : str(str_) {}

	virtual serializer &serialize(void *data, size_t size) noexcept override;

	virtual size_t tell() noexcept override;
	virtual bool   out() const noexcept override;
};

class EXPORT istringserializer : public serializer {
	const std::string *str = nullptr;
	size_t             pos = 0;

public:
	inline istringserializer() noexcept { _state |= serializer::_eof | serializer::_fail; }
	inline istringserializer(const std::string &str_) noexcept : str(&str_) {}

	virtual serializer &serialize(void *data, size_t size) noexcept override;

	virtual size_t tell() noexcept override;
	virtual bool   out() const noexcept override;

	virtual serializer &seek(int64_t pos, serializer::seek_type type) noexcept override;
};

} // namespace crpg
