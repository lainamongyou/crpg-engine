#include "time.hpp"

#ifdef _WIN32
#include <windows.h>
#else
#include <time.h>
#endif

namespace crpg {

#ifdef _WIN32
static bool          have_clockfreq = false;
static LARGE_INTEGER clock_freq;

static inline uint64_t get_clockfreq(void) noexcept
{
	if (!have_clockfreq)
		QueryPerformanceFrequency(&clock_freq);
	return (uint64_t)clock_freq.QuadPart;
}

int64_t get_time_ms() noexcept
{
	LARGE_INTEGER      current_time;
	constexpr uint64_t msec_to_sec = 1000ULL;

	QueryPerformanceCounter(&current_time);
	return (int64_t)mul_div_u64(current_time.QuadPart, msec_to_sec, get_clockfreq());
}

int64_t get_time_ns() noexcept
{
	LARGE_INTEGER      current_time;
	constexpr uint64_t nsec_to_sec = 1000000000ULL;

	QueryPerformanceCounter(&current_time);
	return (int64_t)mul_div_u64(current_time.QuadPart, nsec_to_sec, get_clockfreq());
}

#else
int64_t get_time_ms() noexcept
{
	struct timespec ts;
	clock_gettime(CLOCK_MONOTONIC, &ts);
	return ((uint64_t)ts.tv_sec * 1000ULL + (uint64_t)(ts.tv_nsec / 1000000));
}

int64_t get_time_ns() noexcept
{
	struct timespec ts;
	clock_gettime(CLOCK_MONOTONIC, &ts);
	return ((uint64_t)ts.tv_sec * 1000000000ULL + (uint64_t)ts.tv_nsec);
}
#endif
} // namespace crpg
