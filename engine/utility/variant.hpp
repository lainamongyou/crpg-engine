#pragma once

#include "stringserializer.hpp"
namespace crpg {

namespace variant {
template<typename T> static inline std::string create(const T &val) noexcept
{
	std::string       data;
	ostringserializer s(data);
	s << val;
	return data;
}

template<> inline std::string create<std::string>(const std::string &val) noexcept
{
	return val;
}

template<typename T> static inline T get(const std::string &data) noexcept
{
	T                 ret;
	istringserializer s(data);
	s >> ret;
	return ret;
}

template<typename T> static inline void get(const std::string &data, T &val) noexcept
{
	istringserializer s(data);
	s >> val;
}

static inline void get(const std::string &data, std::string &val) noexcept
{
	val = data;
}

template<> inline std::string get(const std::string &data) noexcept
{
	return data;
}
} // namespace variant

} // namespace crpg
