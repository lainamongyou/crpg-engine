#pragma once

#include <unordered_map>
#include <functional>
#include <memory>
#include <vector>
#include <string>

#include "../export.h"

namespace crpg {

class EXPORT translator {
	translator() = default;

	std::unordered_map<std::string, std::string> entries;
	std::function<size_t(unsigned long)>         get_plural_idx;
	std::string                                  key_buffer;

public:
	static std::unique_ptr<translator> open(const std::string &path) noexcept;

	const char *get(const char *key) noexcept;
	const char *get(const char *ctx, const char *key) noexcept;
	const char *plural(const char *key, const char *key_plural, unsigned long count) noexcept;
	const char *plural(const char *ctx, const char *key, const char *key_plural, unsigned long count) noexcept;
};

} // namespace crpg
