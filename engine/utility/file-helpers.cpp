#include "file-helpers.hpp"
#include "logging.hpp"

#include <fstream>

#if defined(_WIN32)
#include <windows.h>
#elif defined(__APPLE__)
#warning "not yet implemented"
#else
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>
#endif

namespace crpg {

#ifdef _WIN32
std::wstring os_utf8_to_wide(const std::string &utf8) noexcept
{
	if (utf8.empty() || !utf8[0])
		return std::wstring();

	int new_size = MultiByteToWideChar(CP_UTF8, 0, utf8.c_str(), (int)utf8.size(), nullptr, 0);

	std::wstring wide;
	wide.resize((size_t)new_size);
	MultiByteToWideChar(CP_UTF8, 0, utf8.c_str(), (int)utf8.size(), &wide[0], new_size);
	return wide;
}

std::string os_wide_to_utf8(const std::wstring &wide) noexcept
{
	if (wide.empty() || !wide[0])
		return std::string();

	int new_size = WideCharToMultiByte(CP_UTF8, 0, wide.c_str(), (int)wide.size(), nullptr, 0, nullptr, nullptr);

	std::string utf8;
	utf8.resize((size_t)new_size);
	WideCharToMultiByte(CP_UTF8, 0, wide.c_str(), (int)wide.size(), &utf8[0], new_size, nullptr, nullptr);
	return utf8;
}

bool file_exists(const char *path) noexcept
{
	WIN32_FIND_DATAW wfd;
	HANDLE           handle;

	std::wstring wpath = os_utf8_to_wide(path);

	handle = FindFirstFileW(wpath.c_str(), &wfd);
	if (handle != INVALID_HANDLE_VALUE)
		FindClose(handle);

	return handle != INVALID_HANDLE_VALUE;
}

bool replace_file(const std::string &from, const std::string &to) noexcept
{
	std::wstring wfrom   = os_utf8_to_wide(from);
	std::wstring wto     = os_utf8_to_wide(to);
	bool         success = false;

	if (ReplaceFileW(wto.c_str(), wfrom.c_str(), nullptr, 0, nullptr, nullptr)) {
		success = true;
	} else if (GetLastError() == ERROR_FILE_NOT_FOUND) {
		success = !!MoveFileExW(wfrom.c_str(), wto.c_str(), MOVEFILE_REPLACE_EXISTING);
	}

	return success;
}

class os_file_win32 : public os_file_entry {
	HANDLE handle = INVALID_HANDLE_VALUE;

	inline void skip_over_dots() noexcept
	{
		if (name_ == ".." || name_ == ".")
			next();
	}

	inline void convert_wfd(const WIN32_FIND_DATA &wfd) noexcept
	{
		directory_ = !!(wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY);
		name_      = os_wide_to_utf8(wfd.cFileName);
		valid_     = true;
	}

public:
	inline os_file_win32(const std::string &path)
	{
		std::wstring    w_path = os_utf8_to_wide(path);
		WIN32_FIND_DATA wfd;

		w_path += L"/*.*";
		handle = FindFirstFile(w_path.c_str(), &wfd);
		if (handle == INVALID_HANDLE_VALUE)
			throw strprintf("Failed to open directory: '%s'", path.c_str());

		convert_wfd(wfd);
		skip_over_dots();
	}

	~os_file_win32() noexcept
	{
		if (handle != INVALID_HANDLE_VALUE)
			FindClose(handle);
	}

	bool next() noexcept override
	{
		if (!valid_)
			return false;

		WIN32_FIND_DATA wfd;
		if (!FindNextFile(handle, &wfd)) {
			valid_     = false;
			name_      = "";
			directory_ = false;
			return false;
		}

		convert_wfd(wfd);
		skip_over_dots();
		return valid_;
	}
};

os_file_entry_t os_open_dir(const std::string &path) noexcept
try {
	os_file_entry_t val(new os_file_win32(path));
	return val;

} catch (const std::string &text) {
	error("%s: %s", __FUNCTION__, text.c_str());
	return os_file_entry_t();
}

#else
bool file_exists(const char *path) noexcept
{
	return access(path, F_OK) == 0;
}

bool replace_file(const std::string &from, const std::string &to) noexcept
{
	std::string backup = to + ".bak";

	if (file_exists(to.c_str())) {
		unlink(backup.c_str());
		if (rename(to.c_str(), backup.c_str()) != 0) {
			return false;
		}
	}

	if (rename(from.c_str(), to.c_str()) != 0) {
		return false;
	}
	unlink(backup.c_str());
	return true;
}

class os_file_nix : public os_file_entry {
	DIR           *dir;
	std::string    base_path;
	std::string    cur_path;
	struct dirent *ent;

	inline void skip_over_dots()
	{
		if (name_ == ".." || name_ == ".")
			next();
	}

public:
	inline os_file_nix(const std::string &path) : base_path(path)
	{
		dir = opendir(path.c_str());
		if (!dir) {
			throw strprintf("Failed to open directory: '%s'", path.c_str());
		}

		valid_ = true;
		next();
	}

	~os_file_nix() noexcept
	{
		if (dir) {
			closedir(dir);
		}
	}

	bool next() noexcept override
	{
		if (!valid_)
			return false;

		ent = readdir(dir);
		if (!ent) {
			valid_     = false;
			name_      = "";
			directory_ = false;
			return false;
		}

		name_ = ent->d_name;

		cur_path = base_path;
		cur_path += "/";
		cur_path += name_;

		struct stat s;
		if (stat(cur_path.c_str(), &s) != 0) {
			valid_     = false;
			name_      = "";
			directory_ = false;
			return false;
		}

		directory_ = S_ISDIR(s.st_mode) != 0;

		skip_over_dots();
		return valid_;
	}
};

os_file_entry_t os_open_dir(const std::string &path) noexcept
try {
	os_file_entry_t val(new os_file_nix(path));
	return val;

} catch (const std::string &text) {
	error("%s: %s", __FUNCTION__, text.c_str());
	return os_file_entry_t();
}

#endif

std::string load_memory_from_file(const char *path) noexcept
try {
	std::ifstream f(os_path(path), std::ios_base::binary | std::ios_base::in);
	if (f.fail()) {
		throw strprintf("Failed to open file '%s'", path);
	}

	f.seekg(0, std::ios_base::end);
	if (f.fail())
		throw strprintf("Failed to seek file '%s'", path);

	size_t size = f.tellg();
	f.seekg(0);
	std::string text;

	if (size) {
		text.resize(size);
		f.read(&text[0], size);
	}

	return text;

} catch (const std::string &text) {
	error("%s: %s", __FUNCTION__, text.c_str());
	return std::string();
}

bool save_memory_to_file(const char *path, const void *data, size_t size) noexcept
try {
	std::string temp_path = path;
	temp_path += ".tmp";

	std::ofstream f(os_path(temp_path), std::ios_base::binary | std::ios_base::out | std::ios_base::trunc);
	if (f.fail())
		throw strprintf("Failed to open file '%s'", path);

	f.write((const char *)data, size);
	if (f.bad())
		throw strprintf("Failed to write file '%s'", temp_path.c_str());

	f.flush();
	f.close();

	if (!replace_file(temp_path, path))
		throw strprintf("Failed to replace file '%s'", path);

	return true;

} catch (const std::string &text) {
	error("%s: %s", __FUNCTION__, text.c_str());
	return false;
}

} // namespace crpg
