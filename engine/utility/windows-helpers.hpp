#pragma once

namespace crpg {

void error_hr(const char *text, unsigned long error) noexcept;
void warn_hr(const char *text, unsigned long error) noexcept;
void error_lasterror(const char *text) noexcept;
void warn_lasterror(const char *text) noexcept;
}; // namespace crpg
