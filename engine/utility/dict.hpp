#pragma once

#include <cassert>

#include "stringserializer.hpp"
#include "logging.hpp"
#include "../types.hpp"

namespace crpg {

class dict {
#ifdef CRPG_EDITOR
	std::map<std::string, std::string> data;
#else
	std::unordered_map<std::string, std::string> data;
#endif

public:
	inline dict() noexcept {}
	explicit inline dict(serializer &s) noexcept { s >> data; }

	template<typename T> void get(const char *name, T &val) const noexcept
	{
		auto pair = data.find(name);
		if (pair != data.end()) {
			istringserializer s(pair->second);
			s >> val;
		}
#ifdef _DEBUG
		if (pair == data.end()) {
			debug("Failed to load the '%s' key", name);
		}
#endif
	}
	template<typename T> void set(const char *name, const T &val) noexcept
	{
		std::string       out;
		ostringserializer s(out);
		s << val;

		data[name] = out;
	}

	void get_asset_id(const char *name, type::asset_id &asset_id) const noexcept
	{
		auto pair = data.find(name);
		if (pair != data.end()) {
			istringserializer s(pair->second);
			s >> s.ignore<type::asset_id>() >> asset_id;
		}
		assert(pair != data.end());
	}

	istringserializer get_serializer(const char *name) const noexcept
	{
		auto pair = data.find(name);
		if (pair != data.end())
			return istringserializer(pair->second);
		assert(pair != data.end());
		return istringserializer();
	}

	friend inline serializer &operator<<(serializer &s, const dict &d) noexcept { return s << d.data; }
	friend inline serializer &operator>>(serializer &s, dict &d) noexcept { return s >> d.data; }
};

} // namespace crpg
