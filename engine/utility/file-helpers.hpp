#pragma once

#include "../export.h"
#include <string>
#include <memory>

namespace crpg {

/* used for opening/creating files with utf8 paths */
#ifdef _WIN32
EXPORT std::wstring os_utf8_to_wide(const std::string &path) noexcept;
EXPORT std::string os_wide_to_utf8(const std::wstring &path) noexcept;
#define os_path(x) os_utf8_to_wide(x).c_str()
#else
#define os_path(x) std::string(x).c_str()
#endif

class os_file_entry {
protected:
	std::string name_;
	bool        directory_ = false;
	bool        valid_     = false;

public:
	virtual ~os_file_entry() = default;

	virtual bool next() noexcept = 0;

	inline const std::string &name() const noexcept { return name_; }
	inline bool               directory() const noexcept { return directory_; }
	inline bool               valid() const noexcept { return valid_; }
};

using os_file_entry_t = std::unique_ptr<os_file_entry>;
EXPORT os_file_entry_t os_open_dir(const std::string &path) noexcept;

EXPORT bool        file_exists(const char *path) noexcept;
static inline bool file_exists(const std::string &path) noexcept
{
	return file_exists(path.c_str());
}

/* used for safely replacing and backing up files */
EXPORT bool replace_file(const std::string &from, const std::string &to) noexcept;

EXPORT std::string        load_memory_from_file(const char *path) noexcept;
static inline std::string load_memory_from_file(const std::string &path) noexcept
{
	return load_memory_from_file(path.c_str());
}

EXPORT bool        save_memory_to_file(const char *path, const void *data, size_t size) noexcept;
static inline bool save_memory_to_file(const std::string &path, const void *data, size_t size) noexcept
{
	return save_memory_to_file(path.c_str(), data, size);
}
static inline bool save_memory_to_file(const char *path, const std::string &str) noexcept
{
	return save_memory_to_file(path, str.data(), str.size());
}
static inline bool save_memory_to_file(const std::string &path, const std::string &str) noexcept
{
	return save_memory_to_file(path.c_str(), str.data(), str.size());
}

} // namespace crpg
