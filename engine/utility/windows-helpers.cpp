#include <windows.h>
#include "windows-helpers.hpp"
#include "logging.hpp"

namespace crpg {

#define BUF_SIZE 1024

static inline void get_last_error_reason(unsigned long error, char *buffer) noexcept
{
	DWORD ret = FormatMessageA(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
	                           nullptr,
	                           error,
	                           MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US),
	                           buffer,
	                           BUF_SIZE,
	                           nullptr);
	if (!ret) {
		*buffer = 0;
	} else {
		buffer[ret - 2] = 0;
	}
}

void error_hr(const char *text, unsigned long error_val) noexcept
{
	char buf[BUF_SIZE];
	get_last_error_reason(error_val, buf);
	error("%s: %s (%08lX)", text, buf, error_val);
}

void warn_hr(const char *text, unsigned long error_val) noexcept
{
	char buf[BUF_SIZE];
	get_last_error_reason(error_val, buf);
	warn("%s: %s (%08lX)", text, buf, error_val);
}

void error_lasterror(const char *text) noexcept
{
	char  buf[BUF_SIZE];
	DWORD error_val = GetLastError();
	get_last_error_reason(error_val, buf);
	error("%s: %s (%ld)", text, buf, error_val);
}

void warn_lasterror(const char *text) noexcept
{
	char  buf[BUF_SIZE];
	DWORD error_val = GetLastError();
	get_last_error_reason(error_val, buf);
	warn("%s: %s (%ld)", text, buf, error_val);
}
}; // namespace crpg
