#pragma once

#include <cstdint>
#include "../export.h"

#if defined(_MSC_VER) && defined(_M_X64)
#include <intrin.h>
#endif

namespace crpg {

static inline uint64_t mul_div_u64(uint64_t num, uint64_t mul, uint64_t div) noexcept
{
#if defined(_MSC_VER) && defined(_M_X64)
	unsigned __int64       high;
	const unsigned __int64 low = _umul128(num, mul, &high);
	unsigned __int64       rem;
	return _udiv128(high, low, div, &rem);
#else
	const uint64_t rem = num % div;
	return (num / div) * mul + (rem * mul) / div;
#endif
}

static inline int64_t mul_div_64(int64_t num, int64_t mul, int64_t div) noexcept
{
#if defined(_MSC_VER) && defined(_M_X64)
	__int64       high;
	const __int64 low = _mul128(num, mul, &high);
	__int64       rem;
	return _div128(high, low, div, &rem);
#else
	const int64_t rem = num % div;
	return (num / div) * mul + (rem * mul) / div;
#endif
}

EXPORT int64_t get_time_ms() noexcept;
EXPORT int64_t get_time_ns() noexcept;
} // namespace crpg
