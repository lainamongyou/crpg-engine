#pragma once

#include <cstdarg>
#include <vector>
#include <string>

#include "../export.h"

namespace crpg {

EXPORT std::string vstrprintf(const char *format, va_list args) noexcept;
EXPORT std::string strprintf(const char *format, ...) noexcept;

static constexpr const std::string &strprint(const std::string &fmt) noexcept
{
	return fmt;
}

EXPORT std::string strprint(const std::string &fmt, const std::string &p1) noexcept;
EXPORT std::string strprint(const std::string &fmt, const std::string &p1, const std::string &p2) noexcept;
EXPORT std::string strprint(const std::string &fmt,
                            const std::string &p1,
                            const std::string &p2,
                            const std::string &p3) noexcept;
EXPORT std::string strprint(const std::string &fmt,
                            const std::string &p1,
                            const std::string &p2,
                            const std::string &p3,
                            const std::string &p4) noexcept;
EXPORT std::string strprint(const std::string &fmt,
                            const std::string &p1,
                            const std::string &p2,
                            const std::string &p3,
                            const std::string &p4,
                            const std::string &p5) noexcept;

EXPORT void find_replace(std::string &str, const std::string &from, const std::string &to) noexcept;
EXPORT [[nodiscard]] std::string find_replace(const std::string &str,
                                              const std::string &from,
                                              const std::string &to) noexcept;

using string_sequence = std::vector<std::string>;
EXPORT string_sequence split_string(const std::string &str, char ch) noexcept;
EXPORT string_sequence split_string(const std::string &str, const std::string &split_text) noexcept;

class string_reverter {
	std::string &str;
	size_t       last_size;

public:
	inline string_reverter(std::string &str) noexcept : str(str), last_size(str.size()) {}
	inline ~string_reverter() noexcept { str.resize(last_size); }
};

}; // namespace crpg
