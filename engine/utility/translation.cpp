#include "file-helpers.hpp"
#include "translation.hpp"
#include "logging.hpp"
#include "lexer.hpp"

namespace crpg {

static inline bool get_full_token(crpg::lexer &lexer, crpg::token &token) noexcept
{
	if (!lexer.get_token(token))
		return false;

	if (!token.is_alpha())
		return true;

	lexer_sequence seq(lexer, token);
	while (lexer.get_token(token) && (token.is_alnum() || token.text[0] == '_'))
		seq.allow();
	return true;
}

static inline bool get_string_sequence(crpg::lexer &lexer, crpg::token &token) noexcept
{
	lexer_sequence seq(lexer, token);
	while (lexer.get_token(token)) {
		if (token.text[0] == '\\') {
			crpg::token new_token = token;

			lexer_sequence new_seq(lexer, new_token);
			if (!lexer.get_token(new_token))
				return false;
			if (new_token.text[0] == '"' || new_token.text[0] == '\\')
				new_seq.allow();

		} else if (token.text[0] == '"') {
			seq.allow();
			return true;
		}
	}

	return false;
}

struct entry {
	std::string              msgctxt;
	std::string              msgid;
	std::string              msgid_plural;
	std::vector<std::string> msgstr;

	inline void reset() noexcept
	{
		msgctxt.resize(0);
		msgid.resize(0);
		msgid_plural.resize(0);
		for (auto &str : msgstr)
			str.resize(0);
	}

	inline bool valid() noexcept { return !msgid.empty() && msgstr.size() && !msgstr[0].empty(); }
};

enum class po_specifier : int {
	invalid,
	msgstr,
	msgid_plural,
	msgid,
	msgctxt,
};

std::unique_ptr<translator> translator::open(const std::string &path) noexcept
{
	crpg::lexer lexer;
	crpg::token token;

	if (!path.empty())
		lexer = load_memory_from_file(path);

	crpg::translator *translator = new crpg::translator;

	/* return an empty object (will just use keys directly instead of file
	 * data) */
	if (!lexer.valid())
		return std::unique_ptr<crpg::translator>(translator);

	auto next_line = [&]() {
		while (lexer.get_token(token)) {
			if (token.is_newline()) {
				return true;
			}
		}
		return false;
	};

	entry        cur_entry;
	po_specifier cur_type   = po_specifier::invalid;
	int          msgstr_idx = 0;
	std::string  id;
	std::string  val;

	auto get_msgstr_idx = [&]() {
		msgstr_idx = 0;

		lexer_sequence seq(lexer, token);
		if (!lexer.get_token(token))
			return;
		if (token.text[0] != '[')
			return;
		if (!lexer.get_token(token))
			return;
		if (!token.is_digit() || token.text.len > 2)
			return;

		std::string idx_str;
		idx_str.append(token.text.array, token.text.len);

		if (!lexer.get_token(token))
			return;
		if (token.text[0] != ']')
			return;

		msgstr_idx = std::stoi(idx_str);
		seq.allow();
	};

	auto add_entry = [&]() {
		if (!cur_entry.valid()) {
			cur_entry.reset();
			return;
		}

		id.resize(0);

		if (!cur_entry.msgctxt.empty()) {
			id = cur_entry.msgctxt;
			id += ':';
		}

		if (cur_entry.msgid_plural.empty()) {
			id += cur_entry.msgid;
			translator->entries.emplace(id, cur_entry.msgstr[0]);

		} else {
			id += cur_entry.msgid;
			id += ':';
			id += cur_entry.msgid_plural;
			id += ':';

			for (size_t idx = 1; idx < cur_entry.msgstr.size(); idx++) {
				const std::string &val = cur_entry.msgstr[idx];
				if (!val.empty()) {
					string_reverter r(id);
					id += std::to_string(idx);

					translator->entries.emplace(id, cur_entry.msgstr[idx]);
				}
			}
		}

		cur_entry.reset();
	};

	while (get_full_token(lexer, token)) {
		if (token.text[0] == '#' && !next_line())
			break;
		if (token.is_whitespace())
			continue;
		if (token.is_alpha()) {
			po_specifier type;

			if (token.text == "msgctxt") {
				type = po_specifier::msgctxt;
			} else if (token.text == "msgid") {
				type = po_specifier::msgid;
			} else if (token.text == "msgid_plural") {
				type = po_specifier::msgid_plural;
			} else if (token.text == "msgstr") {
				type = po_specifier::msgstr;
			} else if (!next_line()) {
				break;
			} else {
				continue;
			}

			if (cur_type == po_specifier::msgstr) {
				if (cur_entry.msgstr.size() <= msgstr_idx)
					cur_entry.msgstr.resize(msgstr_idx + 1);
				cur_entry.msgstr[msgstr_idx] = val;
			} else {
				/* clang-format off */
				switch ((int)cur_type) {
				case (int)po_specifier::msgctxt:      cur_entry.msgctxt      = val; break;
				case (int)po_specifier::msgid:        cur_entry.msgid        = val; break;
				case (int)po_specifier::msgid_plural: cur_entry.msgid_plural = val; break;
				}
				/* clang-format on */
			}

			if (type > cur_type) {
				/* this is a new entry, so save existing entry */
				add_entry();
			}
			if (type == po_specifier::msgstr) {
				get_msgstr_idx();
			}

			val.resize(0);
			cur_type = type;

		} else if (token.text[0] == '"') {
			if (!get_string_sequence(lexer, token))
				break;

			token.text.append_stringified_sequence_to_string(val);

		} else if (!next_line()) {
			break;
		}
	}

	return std::unique_ptr<crpg::translator>(translator);
}

const char *translator::get(const char *key) noexcept
{
	key_buffer = key;

	auto it = entries.find(key_buffer);
	if (it == entries.end())
		return key;

	return it->second.c_str();
}

const char *translator::get(const char *ctx, const char *key) noexcept
{
	key_buffer = ctx;
	key_buffer += ':';
	key_buffer += key;

	auto it = entries.find(key_buffer);
	if (it == entries.end())
		return key;

	return it->second.c_str();
}

static inline const char *default_plural(const char *key, const char *key_plural, unsigned long count) noexcept
{
	return count == 1 ? key : key_plural;
}

const char *translator::plural(const char *key, const char *key_plural, unsigned long count) noexcept
{
	if (!get_plural_idx)
		return default_plural(key, key_plural, count);

	key_buffer = key;
	key_buffer += ':';
	key_buffer += key_plural;
	key_buffer += ':';
	key_buffer += std::to_string(get_plural_idx(count));

	auto it = entries.find(key_buffer);
	if (it == entries.end())
		return default_plural(key, key_plural, count);

	return it->second.c_str();
}

const char *translator::plural(const char *ctx, const char *key, const char *key_plural, unsigned long count) noexcept
{
	if (!get_plural_idx)
		return default_plural(key, key_plural, count);

	key_buffer = ctx;
	key_buffer += ':';
	key_buffer += key;
	key_buffer += ':';
	key_buffer += key_plural;
	key_buffer += ':';
	key_buffer += std::to_string(get_plural_idx(count));

	auto it = entries.find(key_buffer);
	if (it == entries.end())
		return default_plural(key, key_plural, count);

	return it->second.c_str();
}

} // namespace crpg
