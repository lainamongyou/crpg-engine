#include "string-helpers.hpp"
#include "lexer.hpp"

namespace crpg {

static const char *astrblank = "";

int strref::compare(const char *str) const noexcept
{
	size_t i = 0;

	if (empty())
		return (!str || !*str) ? 0 : -1;
	if (!str)
		str = astrblank;

	do {
		char ch1, ch2;

		ch1 = (i < len) ? array[i] : 0;
		ch2 = *str;

		if (ch1 < ch2)
			return -1;
		else if (ch1 > ch2)
			return 1;
	} while (i++ < len && *str++);

	return 0;
}

int strref::compare(const strref &ref) const noexcept
{
	size_t i = 0;

	if (empty())
		return ref.empty() ? 0 : -1;
	if (ref.empty())
		return -1;

	do {
		char ch1, ch2;

		ch1 = (i < len) ? array[i] : 0;
		ch2 = (i < ref.len) ? ref.array[i] : 0;

		if (ch1 < ch2)
			return -1;
		else if (ch1 > ch2)
			return 1;

		i++;
	} while (i <= len && i <= ref.len);

	return 0;
}

int strref::comparei(const char *str) const noexcept
{
	size_t i = 0;

	if (empty())
		return (!str || !*str) ? 0 : -1;
	if (!str)
		str = astrblank;

	do {
		char ch1, ch2;

		ch1 = (i < len) ? (char)toupper(array[i]) : 0;
		ch2 = (char)toupper(*str);

		if (ch1 < ch2)
			return -1;
		else if (ch1 > ch2)
			return 1;
	} while (i++ < len && *str++);

	return 0;
}

int strref::comparei(const strref &ref) const noexcept
{
	size_t i = 0;

	if (empty())
		return ref.empty() ? 0 : -1;
	if (ref.empty())
		return -1;

	do {
		char ch1, ch2;

		ch1 = (i < len) ? (char)toupper(array[i]) : 0;
		ch2 = (i < ref.len) ? (char)toupper(ref.array[i]) : 0;

		if (ch1 < ch2)
			return -1;
		else if (ch1 > ch2)
			return 1;

		i++;
	} while (i <= len && i <= ref.len);

	return 0;
}

void strref::append_stringified_sequence_to_string(std::string &out) noexcept
{
	for (size_t i = 1; i < (len - 1); i++) {
		if (array[i] == '\\') {
			char next_ch = array[++i];
			if (next_ch == 'n')
				out.push_back('\n');
			else if (next_ch == 'r')
				out.push_back('\r');
			else if (next_ch == 't')
				out.push_back('\t');
			else if (next_ch == '"')
				out.push_back('"');
			else if (next_ch == '\\')
				out.push_back('\\');
			continue;
		}

		out.push_back(array[i]);
	}
}
/* ------------------------------------------------------------------------- */

bool valid_int_str(const char *str, size_t n) noexcept
{
	bool found_num = false;

	if (!str)
		return false;
	if (!*str)
		return false;

	if (!n)
		n = strlen(str);
	if (*str == '-' || *str == '+')
		++str;

	do {
		if (*str > '9' || *str < '0')
			return false;

		found_num = true;
	} while (*++str && --n);

	return found_num;
}

bool valid_float_str(const char *str, size_t n) noexcept
{
	bool found_num = false;
	bool found_exp = false;
	bool found_dec = false;

	if (!str)
		return false;
	if (!*str)
		return false;

	if (!n)
		n = strlen(str);
	if (*str == '-' || *str == '+')
		++str;

	do {
		if (*str == '.') {
			if (found_dec || found_exp || !found_num)
				return false;

			found_dec = true;

		} else if (*str == 'e') {
			if (found_exp || !found_num)
				return false;

			found_exp = true;
			found_num = false;

		} else if (*str == '-' || *str == '+') {
			if (!found_exp || !found_num)
				return false;

		} else if (*str > '9' || *str < '0') {
			return false;
		} else {
			found_num = true;
		}
	} while (*++str && --n);

	return found_num;
}

/* ------------------------------------------------------------------------- */

std::string error_data::make_string() noexcept
{
	std::string out;
	for (auto &error : errors)
		out += strprintf("%s (%d, %d): %s\n", error.file, error.row, error.col, error.text.c_str());
	return out;
}

/* ------------------------------------------------------------------------- */

static inline token_type get_char_token_type(const char ch) noexcept
{
	if (is_whitespace(ch))
		return token::whitespace;
	else if (ch >= '0' && ch <= '9')
		return token::digit;
	else if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z'))
		return token::alpha;

	return token::other;
}

bool lexer::get_token(token &token, ignore_whitespace iws) noexcept
{
	const char     *offset            = this->offset;
	const char     *token_start       = nullptr;
	enum token_type type              = token::none;
	bool            ignore_whitespace = static_cast<bool>(iws);

	if (!offset)
		return false;

	while (*offset != 0) {
		char            ch       = *(offset++);
		enum token_type new_type = get_char_token_type(ch);

		if (type == token::none) {
			if (new_type == token::whitespace && ignore_whitespace)
				continue;

			token_start = offset - 1;
			type        = new_type;

			if (type != token::digit && type != token::alpha) {
				if (is_newline(ch) && is_newline_pair(ch, *offset))
					offset++;
				break;
			}
		} else if (type != new_type) {
			offset--;
			break;
		}
	}

	this->offset = offset;

	if (token_start && offset > token_start) {
		token.text.set(token_start, offset - token_start);
		token.type = type;
		return true;
	}

	return false;
}

void lexer::get_string_offset(const char *str, int &row, int &col) noexcept
{
	if (!str)
		return;

	int         cur_col = 1;
	int         cur_row = 1;
	const char *text    = this->text.c_str();

	while (text < str) {
		if (is_newline(*text)) {
			text += newline_size(text) - 1;
			cur_col = 1;
			cur_row++;
		} else {
			cur_col++;
		}

		text++;
	}

	row = cur_row;
	col = cur_col;
}

} // namespace crpg
