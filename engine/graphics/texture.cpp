#include "../utility/stringserializer.hpp"
#include "../utility/fserializer.hpp"
#include "../utility/logging.hpp"
#include "graphics.hpp"

#include <deque>

using namespace std;

namespace crpg {
namespace graphics {

int get_mipmap_levels(int width, int height) noexcept
{
	uint32_t size       = width > height ? width : height;
	uint32_t num_levels = 1;

	while (size > 1) {
		size /= 2;
		num_levels++;
	}

	return num_levels;
}

/* ------------------------------------------------------------------------- */

texture_ref create_texture_from_png(serializer &file) noexcept;
texture_ref create_texture_from_mask(serializer &file) noexcept;

texture_ref texture::create_from_file(const char *path) noexcept
try {
	if (!path || !*path)
		throw std::string("Null file path");

	size_t len = strlen(path);
	if (len <= 4)
		throw strprintf("Invalid texture file: '%s'", path);

	const char *ext = strrchr(path, '.');
	if (!ext++)
		throw strprintf("Invalid or unsupported file type: '%s'", path);

	ifserializer file(path);
	if (!file.is_open())
		throw strprintf("Failed to open: %s", path);

	texture_ref tex;

	if (strcmp(ext, "png") == 0) {
		tex = create_texture_from_png(file);
	} else if (strcmp(ext, "mask") == 0) {
		tex = create_texture_from_mask(file);
	} else {
		throw strprintf("Invalid or unsupported file type: '%s'", path);
	}

	if (!tex)
		throw strprintf("Failed to create texture from file: '%s'", path);

	return tex;

} catch (const std::string &text) {
	warn("%s: %s", __FUNCTION__, text.c_str());
	return texture_ref();
}

texture_ref texture::create_from_memory(const char *ext, const std::string &data) noexcept
try {
	istringserializer s(data);

	if (strcmp(ext, "png") == 0)
		return create_texture_from_png(s);
	else if (strcmp(ext, "mask") == 0)
		return create_texture_from_mask(s);

	throw strprintf("Invalid or unsupported file type: '%s'", ext);

} catch (const std::string &text) {
	warn("%s: %s", __FUNCTION__, text.c_str());
	return texture_ref();
}

/* ------------------------------------------------------------------------- */
} // namespace graphics
} // namespace crpg
