#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#include "engine-opts.h"
#include "vulkan-graphics.hpp"
#include "../../utility/logging.hpp"
#include "../../utility/windows-helpers.hpp"

#include <vulkan/vulkan_win32.h>
#include <assert.h>

using namespace std;

namespace crpg {
namespace graphics {

/* ========================================================================= */

class vulkan_renderer_windows : public vulkan_renderer {
public:
	const char *platform_surface_extension() const noexcept override { return VK_KHR_WIN32_SURFACE_EXTENSION_NAME; }

	inline vulkan_renderer_windows() noexcept : vulkan_renderer() {}
};

renderer_ref create_system_renderer()
{
	auto ref = make_shared<vulkan_renderer_windows>();
	ref->init();
	return ref;
}

/* ========================================================================= */

class vulkan_display_windows : public vulkan_display {
public:
	inline vulkan_display_windows(const info &info__, uint32_t cx, uint32_t cy) : vulkan_display(info__, cx, cy) {}

	void init_surface() override;
};

display_ref display::create(const info &info_, uint32_t cx, uint32_t cy) noexcept
try {
	auto ref  = make_shared<vulkan_display_windows>(info_, cx, cy);
	ref->weak = ref;
	ref->init();
	return ref;

} catch (const vk::Error &vkerr) {
	error("%s: %s", __func__, vkerr.what());
	return display_ref();
}

void vulkan_display_windows::init_surface()
{
	auto *r = vulkan_renderer::current;

	HINSTANCE hinst = (HINSTANCE)GetWindowLongPtr((HWND)info_.hwnd, GWLP_HINSTANCE);

	vk::Win32SurfaceCreateInfoKHR surf_info(vk::Win32SurfaceCreateFlagsKHR(), hinst, (HWND)info_.hwnd);
	surface = r->inst.createWin32SurfaceKHR(surf_info);
}

} // namespace graphics
} // namespace crpg
