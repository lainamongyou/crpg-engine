#include "../../utility/stringserializer.hpp"
#include "vulkan-graphics.hpp"

namespace crpg {
namespace graphics {

static inline bool is_pow2(int size)
{
	return (size & (size - 1)) == 0;
}

struct mip_copy_info {
	const void *data;
	size_t      offset;
	size_t      size;
};

static uint64_t texture_id_counter = 0;

inline vulkan_texture::vulkan_texture(int             cx,
                                      int             cy,
                                      int             levels,
                                      texture_format  format,
                                      const uint8_t **data,
                                      texture::type   texture_type)
{
	auto        *r      = vulkan_renderer::current;
	vkr::Device &device = r->device;

	size_t   pixel_size = get_pixel_size(format);
	size_t   mem_size   = 0;
	uint32_t type_idx;

	cx_        = cx;
	cy_        = cy;
	id         = ++texture_id_counter;
	renderable = texture_type == texture::type::renderable;

	/* -------------------------------------------------- */
	/* get mip levels                                     */

	if (levels != 1 && (!is_pow2(cx) || !is_pow2(cy))) {
		warn("%s: %s", __func__, "Texture is not power of two, cannot have more than one level");
		levels = 1;
	}

	if (levels == 0) {
		int dimension = cx > cy ? cx : cy;
		while (dimension > 1) {
			levels++;
			dimension >>= 1;
		}
	}

	this->levels_ = levels;

	/* -------------------------------------------------- */
	/* create texture                                     */

	vk::ImageUsageFlags flags = vk::ImageUsageFlagBits::eSampled;

	switch (texture_type) {
	case texture::type::static_:
		flags |= vk::ImageUsageFlagBits::eTransferDst;
		break;
	case texture::type::renderable:
		flags |= vk::ImageUsageFlagBits::eColorAttachment;
		if (levels > 1)
			flags |= vk::ImageUsageFlagBits::eTransferSrc | vk::ImageUsageFlagBits::eTransferDst;
		break;
	}

	vk::ImageCreateInfo ici;
	ici.imageType     = vk::ImageType::e2D;
	ici.format        = get_vk_format(format);
	ici.extent        = vk::Extent3D((uint32_t)cx, (uint32_t)cy, 1);
	ici.mipLevels     = (uint32_t)levels;
	ici.arrayLayers   = 1;
	ici.usage         = flags;
	ici.initialLayout = renderable ? vk::ImageLayout::eUndefined : vk::ImageLayout::ePreinitialized;

	image = device.createImage(ici);

	/* -------------------------------------------------- */
	/* create/bind texture memory                         */

	vk::MemoryRequirements reqs = image.getMemoryRequirements();
	type_idx = r->get_memory_type_index(reqs.memoryTypeBits, vk::MemoryPropertyFlagBits::eDeviceLocal);
	mem      = device.allocateMemory({reqs.size, type_idx});

	image.bindMemory(*mem, 0);

	/* -------------------------------------------------- */
	/* create image view                                  */

	vk::ImageViewCreateInfo ivci;
	ivci.image            = *image;
	ivci.viewType         = vk::ImageViewType::e2D;
	ivci.format           = ici.format;
	ivci.subresourceRange = {vk::ImageAspectFlagBits::eColor, 0, (uint32_t)levels, 0, 1};

	if (format == texture_format::a8) {
		ivci.components.r = vk::ComponentSwizzle::eOne;
		ivci.components.g = vk::ComponentSwizzle::eOne;
		ivci.components.b = vk::ComponentSwizzle::eOne;
		ivci.components.a = vk::ComponentSwizzle::eR;

	} else if (format == texture_format::l8) {
		ivci.components.r = vk::ComponentSwizzle::eR;
		ivci.components.g = vk::ComponentSwizzle::eR;
		ivci.components.b = vk::ComponentSwizzle::eR;
		ivci.components.a = vk::ComponentSwizzle::eOne;
	}

	view = device.createImageView(ivci);

	if (renderable) {
		/* -------------------------------------------------- */
		/* create render view                                 */

		if (levels_ > 1) {
			ivci.subresourceRange.levelCount = 1;

			view2       = device.createImageView(ivci);
			render_view = *view2;
		} else {
			render_view = *view;
		}

		/* -------------------------------------------------- */
		/* create render pass                                 */

		vk::AttachmentDescription color_attachment;
		color_attachment.format         = ici.format;
		color_attachment.loadOp         = vk::AttachmentLoadOp::eClear;
		color_attachment.stencilLoadOp  = vk::AttachmentLoadOp::eDontCare;
		color_attachment.stencilStoreOp = vk::AttachmentStoreOp::eDontCare;
		color_attachment.finalLayout    = vk::ImageLayout::eColorAttachmentOptimal;

		vk::AttachmentReference color_ref(0, vk::ImageLayout::eColorAttachmentOptimal);

		vk::SubpassDescription subpass_desc;
		subpass_desc.pipelineBindPoint    = vk::PipelineBindPoint::eGraphics;
		subpass_desc.colorAttachmentCount = 1;
		subpass_desc.pColorAttachments    = &color_ref;

		vk::AccessFlags dst_flags =
		        vk::AccessFlagBits::eColorAttachmentWrite | vk::AccessFlagBits::eColorAttachmentRead;

		vk::SubpassDependency color_dependency;
		color_dependency.srcSubpass      = VK_SUBPASS_EXTERNAL;
		color_dependency.dstSubpass      = 0;
		color_dependency.srcStageMask    = vk::PipelineStageFlagBits::eColorAttachmentOutput;
		color_dependency.dstStageMask    = vk::PipelineStageFlagBits::eColorAttachmentOutput;
		color_dependency.srcAccessMask   = vk::AccessFlagBits();
		color_dependency.dstAccessMask   = dst_flags;
		color_dependency.dependencyFlags = vk::DependencyFlags();

		renderpass =
		        device.createRenderPass({{}, 1, &color_attachment, 1, &subpass_desc, 1, &color_dependency});

		/* -------------------------------------------------- */
		/* create frame buffer                                */

		std::array<vk::ImageView, 1> fb_attachments = {render_view};
		framebuffer =
		        device.createFramebuffer({{}, *renderpass, fb_attachments, (uint32_t)cx, (uint32_t)cy, 1});

		/* -------------------------------------------------- */
		/* transition layout                                  */

		vkr::CommandBuffer &cmdbuf = r->get_flush_cmdbuf();

		std::array<vk::ImageMemoryBarrier, 1> imb = {
		        vk::ImageMemoryBarrier(vk::AccessFlagBits(),
		                               vk::AccessFlagBits::eShaderRead,
		                               vk::ImageLayout::eUndefined,
		                               vk::ImageLayout::eShaderReadOnlyOptimal,
		                               0,
		                               0,
		                               *image,
		                               {vk::ImageAspectFlagBits::eColor, 0, (uint32_t)levels, 0, 1})};
		cmdbuf.pipelineBarrier(vk::PipelineStageFlagBits::eTopOfPipe,
		                       vk::PipelineStageFlagBits::eFragmentShader,
		                       {},
		                       {},
		                       {},
		                       imb);

		if (r->multi_swapchain)
			r->submit_frameless_cmd();
	} else {
		/* -------------------------------------------------- */
		/* create and flush staging buffer memory             */

		std::vector<vk::BufferImageCopy> regions;
		std::vector<mip_copy_info>       mips;
		regions.reserve(levels);
		mips.reserve(levels);

		for (int i = 0, cur_cx = cx, cur_cy = cy; i < levels; i++) {
			size_t cur_size = cur_cx * cur_cy * pixel_size;

			regions.push_back({mem_size,
			                   0,
			                   0,
			                   {vk::ImageAspectFlagBits::eColor, (uint32_t)i, 0, 1},
			                   {},
			                   {(uint32_t)cur_cx, (uint32_t)cur_cy, 1}});

			mips.push_back({data[i], mem_size, cur_size});

			mem_size += cur_size;

			if (cur_cx > 1)
				cur_cx /= 2;
			if (cur_cy > 1)
				cur_cy /= 2;
		}

		vk_buffer staging;
		staging.init(vk::BufferUsageFlagBits::eTransferSrc, vk::MemoryPropertyFlagBits::eHostVisible, mem_size);

		void *ptr = staging.map(0, staging.mem_size);
		for (mip_copy_info mip : mips)
			memcpy((uint8_t *)ptr + mip.offset, mip.data, mip.size);
		staging.flush(0, staging.mem_size);
		staging.unmap();

		/* -------------------------------------------------- */
		/* copy staging buffer to texture                     */

		vkr::CommandBuffer &cmdbuf = r->get_flush_cmdbuf();

		std::array<vk::ImageMemoryBarrier, 1> imb = {
		        vk::ImageMemoryBarrier(vk::AccessFlagBits(),
		                               vk::AccessFlagBits::eTransferWrite,
		                               vk::ImageLayout::ePreinitialized,
		                               vk::ImageLayout::eTransferDstOptimal,
		                               0,
		                               0,
		                               *image,
		                               {vk::ImageAspectFlagBits::eColor, 0, (uint32_t)levels, 0, 1})};
		cmdbuf.pipelineBarrier(
		        vk::PipelineStageFlagBits::eTopOfPipe, vk::PipelineStageFlagBits::eTransfer, {}, {}, {}, imb);

		cmdbuf.copyBufferToImage(*staging.buffer, *image, vk::ImageLayout::eTransferDstOptimal, regions);

		imb[0].srcAccessMask = vk::AccessFlagBits::eTransferWrite;
		imb[0].dstAccessMask = vk::AccessFlagBits::eShaderRead;
		imb[0].oldLayout     = vk::ImageLayout::eTransferDstOptimal;
		imb[0].newLayout     = vk::ImageLayout::eShaderReadOnlyOptimal;

		cmdbuf.pipelineBarrier(vk::PipelineStageFlagBits::eTransfer,
		                       vk::PipelineStageFlagBits::eFragmentShader,
		                       {},
		                       {},
		                       {},
		                       imb);

		if (r->multi_swapchain)
			r->submit_frameless_cmd();
	}
}

vulkan_texture::~vulkan_texture()
{
	auto *r                 = vulkan_renderer::current;
	auto &pipelines         = r->pipelines;
	auto &program_lookup    = r->program_lookup;
	auto *cur_display_image = r->cur_display_image;

	if (!cur_display_image)
		r->wait_for_idle();

	for (const std::string &id : pipeline_ids) {
		auto it = pipelines.find(id);
		if (it != pipelines.end()) {
			if (cur_display_image)
				cur_display_image->cleanup.pipelines.push_back(std::move(it->second));

			pipelines.erase(it);
		}
	}

	for (const std::string &id : descriptor_set_ids) {
		istringserializer s(id);

		uint64_t program_id;
		s >> program_id;

		auto it = program_lookup.find(program_id);
		if (it != program_lookup.end()) {
			auto &program = it->second;
			program->descriptor_sets.erase(id);
		}
	}
}

extern vulkan_texture *cur_textures[8];

void vulkan_texture::load(int unit) noexcept
{
	if (cur_textures[unit] == this)
		return;

	cur_textures[unit] = this;
	loaded_unit        = unit;
}

void vulkan_texture::unload() noexcept
{
	if (loaded_unit != -1) {
		cur_textures[loaded_unit] = nullptr;
		loaded_unit               = -1;
	}
}

void vulkan_texture::transition(vkr::CommandBuffer       &cmd,
                                vk::PipelineStageFlagBits stage_before,
                                vk::PipelineStageFlagBits stage_after,
                                vk::AccessFlagBits        af_before,
                                vk::AccessFlagBits        af_after,
                                vk::ImageLayout           lo_before,
                                vk::ImageLayout           lo_after,
                                uint32_t                  level)
{
	std::array<vk::ImageMemoryBarrier, 1> imb = {
	        vk::ImageMemoryBarrier(af_before,
	                               af_after,
	                               lo_before,
	                               lo_after,
	                               0,
	                               0,
	                               *image,
	                               {vk::ImageAspectFlagBits::eColor, level, 1, 0, 1})};
	cmd.pipelineBarrier(stage_before, stage_after, {}, {}, {}, imb);
}

void vulkan_texture::gen_mipmap(vkr::CommandBuffer       &cmd,
                                uint32_t                  level,
                                vk::PipelineStageFlagBits stage_before,
                                vk::AccessFlagBits        af_before,
                                vk::ImageLayout           lo_before,
                                int32_t                   cx,
                                int32_t                   cy)
{
	uint32_t mip_level = level + 1;
	int32_t  mip_cx    = cx == 1 ? 1 : cx / 2;
	int32_t  mip_cy    = cy == 1 ? 1 : cy / 2;

	transition(cmd,
	           stage_before,
	           vk::PipelineStageFlagBits::eTransfer,
	           af_before,
	           vk::AccessFlagBits::eTransferRead,
	           lo_before,
	           vk::ImageLayout::eTransferSrcOptimal,
	           level);
	transition(cmd,
	           vk::PipelineStageFlagBits::eFragmentShader,
	           vk::PipelineStageFlagBits::eTransfer,
	           vk::AccessFlagBits::eShaderRead,
	           vk::AccessFlagBits::eTransferWrite,
	           vk::ImageLayout::eShaderReadOnlyOptimal,
	           vk::ImageLayout::eTransferDstOptimal,
	           mip_level);

	std::array<vk::ImageBlit, 1> blit = {vk::ImageBlit({vk::ImageAspectFlagBits::eColor, level, 0, 1},
	                                                   {vk::Offset3D(0, 0, 0), vk::Offset3D(cx, cy, 1)},
	                                                   {vk::ImageAspectFlagBits::eColor, mip_level, 0, 1},
	                                                   {vk::Offset3D(0, 0, 0), vk::Offset3D(mip_cx, mip_cy, 1)})};
	cmd.blitImage(*image,
	              vk::ImageLayout::eTransferSrcOptimal,
	              *image,
	              vk::ImageLayout::eTransferDstOptimal,
	              blit,
	              vk::Filter::eLinear);

	if (mip_level == (uint32_t)(levels_ - 1)) {
		transition(cmd,
		           vk::PipelineStageFlagBits::eTransfer,
		           vk::PipelineStageFlagBits::eFragmentShader,
		           vk::AccessFlagBits::eTransferWrite,
		           vk::AccessFlagBits::eShaderRead,
		           vk::ImageLayout::eTransferDstOptimal,
		           vk::ImageLayout::eShaderReadOnlyOptimal,
		           mip_level);
	} else {
		gen_mipmap(cmd,
		           mip_level,
		           vk::PipelineStageFlagBits::eTransfer,
		           vk::AccessFlagBits::eTransferWrite,
		           vk::ImageLayout::eTransferDstOptimal,
		           mip_cx,
		           mip_cy);
	}

	transition(cmd,
	           vk::PipelineStageFlagBits::eTransfer,
	           vk::PipelineStageFlagBits::eFragmentShader,
	           vk::AccessFlagBits::eTransferRead,
	           vk::AccessFlagBits::eShaderRead,
	           vk::ImageLayout::eTransferSrcOptimal,
	           vk::ImageLayout::eShaderReadOnlyOptimal,
	           level);
}

void vulkan_texture::generate_mipmaps() noexcept
try {
	if (levels_ == 1)
		return;

	auto *r = vulkan_renderer::current;

	r->check_start_rendering();

	vkr::CommandBuffer &cmd = r->get_prerender_cmdbuf();

	gen_mipmap(cmd,
	           0,
	           vk::PipelineStageFlagBits::eFragmentShader,
	           vk::AccessFlagBits::eShaderRead,
	           vk::ImageLayout::eShaderReadOnlyOptimal,
	           (uint32_t)cx_,
	           (uint32_t)cy_);

} catch (const vk::Error &err) {
	error("%s: %s", __func__, err.what());
}

void vulkan_texture::set_filter(texture_filter min_filter, texture_filter mag_filter) noexcept
{
	this->min_filter = min_filter;
	this->mag_filter = mag_filter;
}

void vulkan_texture::set_wrap(address_mode mode_s, address_mode mode_t) noexcept
{
	this->mode_s = mode_s;
	this->mode_t = mode_t;
}

void vulkan_texture::swap(texture_ref &tex) noexcept
{
	auto *swap_tex = static_cast<vulkan_texture *>(tex.get());

	unload();
	swap_tex->unload();

	vulkan_texture temp = std::move(*this);
	*this               = std::move(*swap_tex);
	*swap_tex           = std::move(temp);
}

static vk::Filter             def_min_filter = vk::Filter::eNearest;
static vk::Filter             def_mag_filter = vk::Filter::eNearest;
static vk::SamplerMipmapMode  def_mmm        = vk::SamplerMipmapMode::eNearest;
static vk::SamplerAddressMode def_mode_s     = vk::SamplerAddressMode::eRepeat;
static vk::SamplerAddressMode def_mode_t     = vk::SamplerAddressMode::eRepeat;

static vk::Filter vk_filter(texture_filter filter)
{
	switch (filter) {
	case texture_filter::default_:
	case texture_filter::point:
	case texture_filter::nearest_mipmap_nearest:
	case texture_filter::nearest_mipmap_linear:
		return vk::Filter::eNearest;
	case texture_filter::linear:
	case texture_filter::linear_mipmap_nearest:
	case texture_filter::linear_mipmap_linear:
		return vk::Filter::eLinear;
	}

	return vk::Filter::eNearest;
}

static vk::SamplerMipmapMode vk_mipmap_mode(texture_filter filter)
{
	switch (filter) {
	case texture_filter::default_:
	case texture_filter::point:
	case texture_filter::linear:
	case texture_filter::nearest_mipmap_nearest:
	case texture_filter::linear_mipmap_nearest:
		return vk::SamplerMipmapMode::eNearest;
	case texture_filter::nearest_mipmap_linear:
	case texture_filter::linear_mipmap_linear:
		return vk::SamplerMipmapMode::eLinear;
	}

	return vk::SamplerMipmapMode::eNearest;
}

static vk::SamplerAddressMode vk_address_mode(address_mode mode)
{
	switch (mode) {
	case address_mode::default_:
	case address_mode::wrap:
		return vk::SamplerAddressMode::eRepeat;
	case address_mode::clamp:
		return vk::SamplerAddressMode::eClampToEdge;
	case address_mode::mirror:
		return vk::SamplerAddressMode::eMirroredRepeat;
	}

	return vk::SamplerAddressMode::eRepeat;
}

void set_default_texture_filter(texture_filter min_filter, texture_filter mag_filter) noexcept
{
	def_min_filter = vk_filter(min_filter);
	def_mag_filter = vk_filter(mag_filter);
	def_mmm        = vk_mipmap_mode(min_filter);
}

void set_default_texture_wrap(address_mode mode_s, address_mode mode_t) noexcept
{
	def_mode_s = vk_address_mode(mode_s);
	def_mode_t = vk_address_mode(mode_t);
}

vk::Sampler vulkan_texture::get_sampler()
{
	std::string       id;
	ostringserializer s(id);

	vk::Filter cur_minf = min_filter == texture_filter::default_ ? def_min_filter : vk_filter(min_filter);
	vk::Filter cur_magf = mag_filter == texture_filter::default_ ? def_mag_filter : vk_filter(mag_filter);
	vk::SamplerMipmapMode  cur_mmm = min_filter == texture_filter::default_ ? def_mmm : vk_mipmap_mode(min_filter);
	vk::SamplerAddressMode cur_ms  = mode_s == address_mode::default_ ? def_mode_s : vk_address_mode(mode_s);
	vk::SamplerAddressMode cur_mt  = mode_t == address_mode::default_ ? def_mode_t : vk_address_mode(mode_t);

	s << cur_minf << cur_magf << cur_mmm << cur_ms << cur_mt;

	auto *r        = vulkan_renderer::current;
	auto &samplers = r->samplers;

	auto it = samplers.find(id);
	if (it != samplers.end())
		return *it->second;

	vkr::Sampler sampler = r->device.createSampler(
	        {{}, cur_magf, cur_minf, cur_mmm, cur_ms, cur_mt, {}, {}, {}, {}, {}, {}, {}, VK_LOD_CLAMP_NONE});

	samplers.emplace(id, std::move(sampler));
	it = samplers.find(id);
	return *it->second;
}

texture_ref texture::create(int             cx,
                            int             cy,
                            int             levels,
                            texture_format  format,
                            const uint8_t **data,
                            texture::type   texture_type) noexcept
try {
	return std::make_shared<vulkan_texture>(cx, cy, levels, format, data, texture_type);

} catch (const vk::Error &vkerr) {
	error("%s: %s", __func__, vkerr.what());
	return texture_ref();
}

} // namespace graphics
} // namespace crpg
