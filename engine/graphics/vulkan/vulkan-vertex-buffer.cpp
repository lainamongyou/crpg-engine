#include "vulkan-graphics.hpp"

namespace crpg {
namespace graphics {

extern vertex_buffer *cur_vertex_buffer;

void vk_buffer_base::flush(uint32_t offset, size_t size)
{
	std::array<vk::MappedMemoryRange, 1> range = {vk::MappedMemoryRange(*mem, offset, size)};
	vulkan_renderer::current->device.flushMappedMemoryRanges(range);
}

void vk_buffer::init(vk::BufferUsageFlags usage, vk::MemoryPropertyFlags properties, size_t size)
{
	auto                  *r      = vulkan_renderer::current;
	vkr::Device           &device = r->device;
	vk::MemoryRequirements reqs;
	uint32_t               type_idx;

	this->size = size;

	buffer   = device.createBuffer({{}, size, usage, vk::SharingMode::eExclusive, 0, nullptr});
	reqs     = buffer.getMemoryRequirements();
	mem_size = reqs.size;
	type_idx = r->get_memory_type_index(reqs.memoryTypeBits, properties);
	mem      = device.allocateMemory({reqs.size, type_idx});
	buffer.bindMemory(*mem, 0);
}

void vk_buffer::stage(const vk_buffer &staging, vk::AccessFlags access_flags, vk::PipelineStageFlags stages)
{
	auto *r = vulkan_renderer::current;

	std::array<vk::BufferMemoryBarrier, 1> bmb = {
	        vk::BufferMemoryBarrier(vk::AccessFlagBits::eTransferWrite, access_flags, 0, 0, *buffer, 0, size)};
	std::array<vk::BufferCopy, 1> bc = {vk::BufferCopy(0, 0, size)};

	vkr::CommandBuffer &cmdbuf = r->get_flush_cmdbuf();
	cmdbuf.copyBuffer(*staging.buffer, *buffer, bc);
	cmdbuf.pipelineBarrier(vk::PipelineStageFlagBits::eTransfer, stages, {}, {}, bmb, {});

	if (r->multi_swapchain)
		r->submit_frameless_cmd();
}

void vulkan_buffer::init(vk::BufferUsageFlags usage, void *data, size_t size)
{
	bool      is_index_buffer = usage == vk::BufferUsageFlagBits::eIndexBuffer;
	vk_buffer staging;

	vk_buffer::init(vk::BufferUsageFlagBits::eTransferDst | usage, vk::MemoryPropertyFlagBits::eDeviceLocal, size);
	staging.init(vk::BufferUsageFlagBits::eTransferSrc, vk::MemoryPropertyFlagBits::eHostVisible, size);

	void *ptr = staging.map(0, mem_size);
	memcpy(ptr, data, size);
	staging.flush(0, mem_size);
	staging.unmap();

	stage(staging,
	      is_index_buffer ? vk::AccessFlagBits::eIndexRead : vk::AccessFlagBits::eVertexAttributeRead,
	      vk::PipelineStageFlagBits::eVertexInput);
}

inline vulkan_vertex_buffer::vulkan_vertex_buffer(const data &data)
{
	vert_count_  = data.points.size();
	index_count_ = data.indices.size();

	points.init(vk::BufferUsageFlagBits::eVertexBuffer,
	            (void *)data.points.data(),
	            data.points.size() * sizeof(math::vec3));
	if (data.coords.size())
		coords.init(vk::BufferUsageFlagBits::eVertexBuffer,
		            (void *)data.coords.data(),
		            data.coords.size() * sizeof(math::vec2));
	if (data.indices.size())
		indices.init(vk::BufferUsageFlagBits::eIndexBuffer,
		             (void *)data.indices.data(),
		             data.indices.size() * sizeof(unsigned long));
}

void vulkan_vertex_buffer::load() noexcept
{
	cur_vertex_buffer = this;
}

void vulkan_vertex_buffer::bind()
{
	auto *r = vulkan_renderer::current;
	if (r->cur_bound_vb == this) {
		return;
	}

	r->cur_bound_vb = this;

	bool shader_has_coords = static_cast<vulkan_shader *>(cur_vertex_shader)->has_coords;

	std::vector<vk::Buffer>     buffers;
	std::vector<vk::DeviceSize> offsets;
	buffers.reserve(2);
	offsets.reserve(2);
	buffers.push_back(*points.buffer);
	offsets.push_back(0);

	if (shader_has_coords && coords) {
		buffers.push_back(*coords.buffer);
		offsets.push_back(0);
	}

	r->cur_cmd->bindVertexBuffers(0, buffers, offsets);
	if (indices)
		r->cur_cmd->bindIndexBuffer(*indices.buffer, 0, vk::IndexType::eUint32);
}

void vulkan_vertex_buffer::unload() noexcept
{
	if (cur_vertex_buffer == this)
		cur_vertex_buffer = nullptr;
}

void vulkan_vertex_buffer::swap(vertex_buffer_ref &vb) noexcept
{
	auto *swap_vb = static_cast<vulkan_vertex_buffer *>(vb.get());

	unload();
	swap_vb->unload();

	vulkan_vertex_buffer temp = std::move(*this);
	*this                     = std::move(*swap_vb);
	*swap_vb                  = std::move(temp);
}

vertex_buffer_ref vertex_buffer::create(const data &data) noexcept
try {
	return std::make_shared<vulkan_vertex_buffer>(data);

} catch (const vk::Error &vkerr) {
	error("%s: %s", __func__, vkerr.what());
	return vertex_buffer_ref();
}

} // namespace graphics
} // namespace crpg
