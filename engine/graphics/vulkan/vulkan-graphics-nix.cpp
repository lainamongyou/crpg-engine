#include "engine-opts.h"
#include "vulkan-graphics.hpp"
#include "../../utility/logging.hpp"

using namespace std;

namespace crpg {
namespace graphics {

/* ========================================================================= */

class vulkan_renderer_xlib : public vulkan_renderer {
public:
	const char *platform_surface_extension() const noexcept override { return VK_KHR_XLIB_SURFACE_EXTENSION_NAME; }

	inline vulkan_renderer_xlib() noexcept : vulkan_renderer() {}
};

class vulkan_renderer_wayland : public vulkan_renderer {
public:
	const char *platform_surface_extension() const noexcept override
	{
		return VK_KHR_WAYLAND_SURFACE_EXTENSION_NAME;
	}

	inline vulkan_renderer_wayland() noexcept : vulkan_renderer() {}
};

renderer_ref create_system_renderer() noexcept
{
	char *type = getenv("XDG_SESSION_TYPE");
	if (type) {
		if (strcmp(type, "x11") == 0) {
			auto ref = make_shared<vulkan_renderer_xlib>();
			ref->init();
			return ref;

		} else if (strcmp(type, "wayland") == 0) {
			auto ref = make_shared<vulkan_renderer_wayland>();
			ref->init();
			return ref;
		}
	}

	return renderer_ref();
}

/* ========================================================================= */

class vulkan_display_xlib : public vulkan_display {
public:
	inline vulkan_display_xlib(const info &info__, uint32_t cx, uint32_t cy) : vulkan_display(info__, cx, cy) {}

	void init_surface() override
	{
		auto *r = vulkan_renderer::current;
		surface = r->inst.createXlibSurfaceKHR({{}, (Display *)info_.x11_display, (Window)info_.x11_id});
	}
};

class vulkan_display_wayland : public vulkan_display {
public:
	inline vulkan_display_wayland(const info &info__, uint32_t cx, uint32_t cy) : vulkan_display(info__, cx, cy) {}

	void init_surface() override
	{
		auto *r = vulkan_renderer::current;
		surface = r->inst.createWaylandSurfaceKHR(
		        {{}, (wl_display *)info_.wayland_display, (wl_surface *)info_.wayland_surf});
	}
};

display_ref display::create(const info &info_, uint32_t cx, uint32_t cy) noexcept
try {
	char *type = getenv("XDG_SESSION_TYPE");
	if (type) {
		if (strcmp(type, "x11") == 0) {
			auto ref  = make_shared<vulkan_display_xlib>(info_, cx, cy);
			ref->weak = ref;
			ref->init();
			return ref;

		} else if (strcmp(type, "wayland") == 0) {
			auto ref  = make_shared<vulkan_display_wayland>(info_, cx, cy);
			ref->weak = ref;
			ref->init();
			return ref;
		}
	}
	return display_ref();

} catch (const vk::Error &vkerr) {
	error("%s: %s", __func__, vkerr.what());
	return display_ref();
}

} // namespace graphics
} // namespace crpg
