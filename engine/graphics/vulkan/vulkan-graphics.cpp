#include "../../utility/stringserializer.hpp"
#include "vulkan-graphics.hpp"

#include <deque>

namespace crpg {
namespace graphics {

extern weak_renderer_ref cur_renderer;
static const char       *debug_layer_name       = "VK_LAYER_KHRONOS_validation";
static const char       *portability_device_ext = "VK_KHR_portability_subset";
bool                     pipeline_changed       = true;

/* ========================================================================= */

vk_buffer::~vk_buffer()
{
	if (*buffer) {
		auto *cur_display_image = vulkan_renderer::current->cur_display_image;
		if (cur_display_image)
			cur_display_image->cleanup.buffers.push_back(std::move(*(vk_buffer_base *)this));
	}
}

vk_shader::~vk_shader()
{
	if (*module) {
		auto *cur_display_image = vulkan_renderer::current->cur_display_image;
		if (cur_display_image)
			cur_display_image->cleanup.shaders.push_back(std::move(*(vk_shader_base *)this));
	}
}

vk_texture::~vk_texture()
{
	if (*image) {
		auto *cur_display_image = vulkan_renderer::current->cur_display_image;
		if (cur_display_image)
			cur_display_image->cleanup.textures.push_back(std::move(*(vk_texture_base *)this));
	}
}

vk_program::~vk_program()
{
	if (*pipeline_layout) {
		auto *cur_display_image = vulkan_renderer::current->cur_display_image;
		if (cur_display_image)
			cur_display_image->cleanup.programs.push_back(std::move(*(vk_program_base *)this));
	}
}

vk_descriptor_set::~vk_descriptor_set()
{
	if (*set) {
		auto *cur_display_image = vulkan_renderer::current->cur_display_image;
		if (cur_display_image)
			cur_display_image->cleanup.sets.push_back(std::move(*(vk_descriptor_set_base *)this));
	}
}

/* ========================================================================= */

vulkan_renderer *vulkan_renderer::current = nullptr;

#if ENABLE_VULKAN_VALIDATION
static VkBool32 VKAPI_CALL debug_callback(VkDebugUtilsMessageSeverityFlagBitsEXT      vk_severity,
                                          VkDebugUtilsMessageTypeFlagsEXT             vk_type,
                                          const VkDebugUtilsMessengerCallbackDataEXT *data,
                                          void *) noexcept
{
	std::ostringstream msg;
	auto               severity = vk::DebugUtilsMessageSeverityFlagBitsEXT(vk_severity);
	auto               type     = vk::DebugUtilsMessageTypeFlagsEXT(vk_type);

	msg << vk::to_string(severity) << " : " << vk::to_string(type);

	msg << " - Msg Id Number: " << std::to_string(data->messageIdNumber);
	msg << " | Message Id Name: " << data->pMessageIdName << "\n\t" << data->pMessage << "\n";

	if (data->objectCount > 0) {
		msg << "\n\tObjects - " << data->objectCount << "\n";

		for (uint32_t i = 0; i < data->objectCount; i++) {
			auto       &object = data->pObjects[i];
			const char *name   = object.pObjectName;

			msg << "\t\tObject[" << i << "] - " << vk::to_string(vk::ObjectType(object.objectType))
			    << ", Handle " << object.objectHandle;
			if (name && *name) {
				msg << ", Name \"" << name << "\"\n";
			} else {
				msg << "\n";
			}
		}
	}

	if (data->cmdBufLabelCount > 0) {
		msg << "\n\tCommand Buffer Labels - " << data->cmdBufLabelCount << "\n";

		for (uint32_t i = 0; i < data->cmdBufLabelCount; i++) {
			auto &label = data->pCmdBufLabels[i];

			msg << "\t\tLabel[" << i << "] - " << label.pLabelName << " { " << label.color[0] << ", "
			    << label.color[1] << ", " << label.color[2] << ", " << label.color[3] << "}\n";
		}
	}

	debug("%s", msg.str().c_str());
	return false;
}
#endif

void vulkan_renderer::init()
{
	/* ------------------------------------------------------------ */
	/* get validation layer (if enabled)                            */

	void *p_next = nullptr;

#if ENABLE_VULKAN_VALIDATION
	const std::vector<vk::LayerProperties> instance_layers = context.enumerateInstanceLayerProperties();
	for (auto &layer : instance_layers) {
		if (strcmp(layer.layerName, debug_layer_name) == 0) {
			layers.push_back(debug_layer_name);
			break;
		}
	}

	vk::DebugUtilsMessageSeverityFlagsEXT severities =
	        vk::DebugUtilsMessageSeverityFlagBitsEXT::eWarning | vk::DebugUtilsMessageSeverityFlagBitsEXT::eError;
	vk::DebugUtilsMessageTypeFlagsEXT message_types = vk::DebugUtilsMessageTypeFlagBitsEXT::eGeneral |
	                                                  vk::DebugUtilsMessageTypeFlagBitsEXT::ePerformance |
	                                                  vk::DebugUtilsMessageTypeFlagBitsEXT::eValidation;
	vk::DebugUtilsMessengerCreateInfoEXT dumci({}, severities, message_types, debug_callback, nullptr);

	p_next = &dumci;
#endif

	/* ------------------------------------------------------------ */
	/* get instance extensions                                      */

	bool surf_ext_found          = false;
	bool platform_surf_ext_found = false;

	const char *platform_surf_ext = platform_surface_extension();

	std::vector<vk::ExtensionProperties> exts = context.enumerateInstanceExtensionProperties();
	for (const auto &ext : exts) {
		if (strcmp(ext.extensionName, VK_KHR_SURFACE_EXTENSION_NAME) == 0) {
			surf_ext_found = true;
			instance_extensions.push_back(VK_KHR_SURFACE_EXTENSION_NAME);

#if ENABLE_VULKAN_VALIDATION
		} else if (strcmp(ext.extensionName, VK_EXT_DEBUG_UTILS_EXTENSION_NAME) == 0) {
			instance_extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
#endif
		} else if (strcmp(ext.extensionName, platform_surf_ext) == 0) {
			platform_surf_ext_found = true;
			instance_extensions.push_back(platform_surf_ext);
		}
	}

	if (!surf_ext_found)
		throw vk::LogicError("Could not find required surface extension");
	if (!platform_surf_ext_found)
		throw vk::LogicError("Could not find required platform surface extension");

	/* ------------------------------------------------------------ */
	/* get instance                                                 */

	vk::ApplicationInfo app_info(GAME_NAME, GAME_VERSION, "crpg-engine", CRPG_ENGINE_VERSION, VK_API_VERSION_1_1);

	vk::InstanceCreateInfo ici({},
	                           &app_info,
	                           (uint32_t)layers.size(),
	                           layers.data(),
	                           (uint32_t)instance_extensions.size(),
	                           instance_extensions.data());
	ici.setPNext(p_next);

	inst = context.createInstance(ici);

	/* ------------------------------------------------------------ */
	/* enable debug messenger                                       */

#if ENABLE_VULKAN_VALIDATION
	debug_messenger = inst.createDebugUtilsMessengerEXT(dumci);
#endif

	/* ------------------------------------------------------------ */
	/* get gpu                                                      */

	int gpu_idx = -1;

	std::vector<vkr::PhysicalDevice> gpus = inst.enumeratePhysicalDevices();

	if (gpu_idx == -1) {
		constexpr size_t device_type_count = (size_t)vk::PhysicalDeviceType::eCpu + 1;

		int device_prioritization[device_type_count];
		device_prioritization[(int)vk::PhysicalDeviceType::eDiscreteGpu]   = 4;
		device_prioritization[(int)vk::PhysicalDeviceType::eIntegratedGpu] = 3;
		device_prioritization[(int)vk::PhysicalDeviceType::eVirtualGpu]    = 2;
		device_prioritization[(int)vk::PhysicalDeviceType::eCpu]           = 1;
		device_prioritization[(int)vk::PhysicalDeviceType::eOther]         = 0;

		int best_priority = -1;

		for (size_t i = 0; i < gpus.size(); i++) {
			const vkr::PhysicalDevice         &gpu            = gpus[i];
			const vk::PhysicalDeviceProperties gpu_properties = gpu.getProperties();

			int priority = device_prioritization[(int)gpu_properties.deviceType];
			if (priority > best_priority) {
				gpu_idx       = (int)i;
				best_priority = priority;
			}
		}
	}

	if (gpu_idx == -1)
		throw vk::LogicError("Could not find a GPU");

	gpu               = std::move(gpus[gpu_idx]);
	memory_properties = gpu.getMemoryProperties();

	/* ------------------------------------------------------------ */
	/* get device extensions                                        */

	bool swapchain_ext_found = false;

	exts = gpu.enumerateDeviceExtensionProperties();
	for (const auto &ext : exts) {
		if (strcmp(ext.extensionName, VK_KHR_SWAPCHAIN_EXTENSION_NAME) == 0) {
			swapchain_ext_found = true;
			device_extensions.push_back(VK_KHR_SWAPCHAIN_EXTENSION_NAME);

		} else if (strcmp(ext.extensionName, portability_device_ext) == 0) {
			device_extensions.push_back(portability_device_ext);
		}
	}

	if (!swapchain_ext_found)
		throw vk::LogicError("Could not find required swapchain extension");
}

uint32_t vulkan_renderer::get_memory_type_index(uint32_t type_bits, vk::MemoryPropertyFlags mem_flags)
{
	for (size_t i = 0; i < memory_properties.memoryTypeCount; i++) {
		if ((type_bits & 1) == 1) {
			vk::MemoryType type = memory_properties.memoryTypes[i];
			if ((type.propertyFlags & mem_flags) == mem_flags) {
				return (uint32_t)i;
			}
		}

		type_bits >>= 1;
	}

	throw vk::LogicError("Could not get memory heap index, bad memory flags?");
}

void vulkan_renderer::init_device(vk::SurfaceKHR surface)
{
	/* ------------------------------------------------------------ */
	/* get queue family index                                       */

	std::vector<vk::QueueFamilyProperties> families = gpu.getQueueFamilyProperties();
	int                                    idx      = -1;
	float                                  priority = 0.0f;

	for (size_t i = 0; i < families.size(); i++) {
		auto &family = families[i];
		if (family.queueFlags & vk::QueueFlagBits::eGraphics) {
			if (!surface || gpu.getSurfaceSupportKHR((uint32_t)i, surface)) {
				idx = (int)i;
				break;
			}
		}
	}

	if (idx == -1)
		throw vk::LogicError("Failed to find appropriate queue family");

	queue_idx = (uint32_t)idx;

	/* ------------------------------------------------------------ */
	/* create device and queues                                     */

	vk::DeviceQueueCreateInfo queue_create_info({}, idx, 1, &priority);

	device = gpu.createDevice({{},
	                           1,
	                           &queue_create_info,
	                           (uint32_t)layers.size(),
	                           layers.data(),
	                           (uint32_t)device_extensions.size(),
	                           device_extensions.data(),
	                           nullptr});

	queue       = device.getQueue(queue_idx, 0);
	initialized = true;

	if (multi_swapchain)
		init_multi_swapchain();

	/* ------------------------------------------------------------ */
	/* create pipeline cache                                        */

	pipeline_cache = device.createPipelineCache({{}, 0, nullptr});
}

/* ------------------------------------------------------------------------- */

renderer_ref create_system_renderer();

renderer_ref renderer::create(renderer::error *error_out, bool multi_swap) noexcept
try {
	if (cur_renderer.lock().get())
		throw error::already_exists;

	renderer_ref new_renderer                 = create_system_renderer();
	cur_renderer                              = new_renderer;
	vulkan_renderer::current->multi_swapchain = multi_swap;

	return new_renderer;

} catch (renderer::error err) {
	if (error_out)
		*error_out = err;
	return renderer_ref();
}

/* ========================================================================= */

static uint64_t display_id_counter = 0;

vulkan_display::vulkan_display(const info &info__, uint32_t cx, uint32_t cy)
        : display(info__, cx, cy), id(++display_id_counter)
{
	if (id > 1) {
		auto *r = vulkan_renderer::current;
		if (!r->multi_swapchain) {
			r->init_multi_swapchain();
		}
	}
}

vulkan_display::~vulkan_display()
{
	auto *r         = vulkan_renderer::current;
	auto &pipelines = r->pipelines;

	r->wait_for_idle();

	if (r->cur_display == this) {
		r->cur_display       = nullptr;
		r->cur_display_image = nullptr;
		r->cur_frame_id      = 0;
	}

	for (const std::string &id : pipeline_ids)
		pipelines.erase(id);
}

void display_image::init(vkr::Device    &device,
                         vulkan_display &display,
                         uint32_t        queue_idx,
                         vk::Format      format,
                         vk::Image       image_)
{
	static uint64_t display_image_id_counter = 0;

	image = image_;
	id    = ++display_image_id_counter;

	vk::ImageViewCreateInfo ivci;
	ivci.image            = image;
	ivci.viewType         = vk::ImageViewType::e2D;
	ivci.format           = format;
	ivci.subresourceRange = {vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1};

	view = device.createImageView(ivci);

	std::vector<vk::ImageView> fb_attachments;
	fb_attachments.reserve(2);
	fb_attachments.push_back(*view);

	uint32_t cx = display.cx();
	uint32_t cy = display.cy();
	framebuffer = device.createFramebuffer({{}, *display.d.renderpass, fb_attachments, cx, cy, 1});

	cmdpool = device.createCommandPool({{}, queue_idx});

	fence = device.createFence({});

	draw_sem = device.createSemaphore({});
}

vkr::CommandBuffer &display_image::get_cmdbuf()
{
	if (cmdbufs_used < cmdbufs.size()) {
		vkr::CommandBuffer cmdbuf = std::move(cmdbufs.front());
		cmdbufs.pop_front();
		cmdbufs.push_back(std::move(cmdbuf));
	} else {
		auto *r    = vulkan_renderer::current;
		auto  cmds = r->device.allocateCommandBuffers({*cmdpool, vk::CommandBufferLevel::ePrimary, 1});
		cmdbufs.push_back(std::move(cmds[0]));
	}

	cmdbufs_used++;
	return cmdbufs.back();
}

void display_image::reset()
{
	if (!first_use) {
		auto *r = vulkan_renderer::current;

		/* wait for any remaining commands to complete */
		r->wait_for_fence(fence);

		/* reset command pool */
		cmdpool.reset();
		cmdbufs_used = 0;

		/* clean up objects from last use of this frame */
		cleanup.clear();
	}
}

void vulkan_display::init()
{
	auto *r = vulkan_renderer::current;

	init_surface();
	if (!r->initialized)
		r->init_device(*surface);

	reset(false);
}

static inline vk::Format get_backbuffer_format(texture_format format)
{
	switch (format) {
	case texture_format::rgba:
		return vk::Format::eB8G8R8A8Unorm;
	case texture_format::rgba32f:
		return vk::Format::eR32G32B32A32Sfloat;
	default:
		throw vk::LogicError("Unsupported format");
	}
}

static inline vk::ColorSpaceKHR get_color_space(texture_format format)
{
	switch (format) {
	case texture_format::rgba:
		return vk::ColorSpaceKHR::eSrgbNonlinear;
	case texture_format::rgba32f:
		return vk::ColorSpaceKHR::eExtendedSrgbLinearEXT;
	default:
		throw vk::LogicError("Unsupported color space");
	}
}

void vulkan_display::reset(bool resizing)
{
	auto *r = vulkan_renderer::current;

	vkr::PhysicalDevice &gpu    = r->gpu;
	vkr::Device         &device = r->device;

	/* ------------------------------------------------------------ */
	/* get new size                                                 */

	vk::SurfaceCapabilitiesKHR surface_caps = gpu.getSurfaceCapabilitiesKHR(*surface);

	uint32_t new_cx;
	uint32_t new_cy;

	if (surface_caps.currentExtent.width >= surface_caps.maxImageExtent.width &&
	    surface_caps.currentExtent.height >= surface_caps.minImageExtent.height &&
	    surface_caps.currentExtent.width <= surface_caps.maxImageExtent.width &&
	    surface_caps.currentExtent.height <= surface_caps.maxImageExtent.height) {
		new_cx = surface_caps.currentExtent.width;
		new_cy = surface_caps.currentExtent.height;

	} else if (resizing) {
		new_cx = resize_cx;
		new_cy = resize_cy;

	} else {
		new_cx = cx_;
		new_cy = cy_;
	}

	if (resizing && new_cx == cx_ && new_cy == cy_)
		return;

	/* ------------------------------------------------------------ */
	/* remove any old data and pass it to the new swapchain         */

	vk_display prev = std::move(d);

	/* ------------------------------------------------------------ */
	/* get render pass                                              */
	/*                                                              */
	/* defines swap chain components and how they're used           */

	vk::Format format = get_backbuffer_format(info_.format);

	vk::AttachmentDescription color_attachment;
	color_attachment.format         = format;
	color_attachment.loadOp         = vk::AttachmentLoadOp::eClear;
	color_attachment.stencilLoadOp  = vk::AttachmentLoadOp::eDontCare;
	color_attachment.stencilStoreOp = vk::AttachmentStoreOp::eDontCare;
	color_attachment.finalLayout    = vk::ImageLayout::ePresentSrcKHR;

	vk::AttachmentReference color_ref(0, vk::ImageLayout::eColorAttachmentOptimal);

	vk::SubpassDescription subpass_desc;
	subpass_desc.pipelineBindPoint    = vk::PipelineBindPoint::eGraphics;
	subpass_desc.colorAttachmentCount = 1;
	subpass_desc.pColorAttachments    = &color_ref;

	vk::AccessFlags dst_flags =
	        vk::AccessFlagBits::eColorAttachmentWrite | vk::AccessFlagBits::eColorAttachmentRead;

	vk::SubpassDependency color_dependency;
	color_dependency.srcSubpass      = VK_SUBPASS_EXTERNAL;
	color_dependency.dstSubpass      = 0;
	color_dependency.srcStageMask    = vk::PipelineStageFlagBits::eColorAttachmentOutput;
	color_dependency.dstStageMask    = vk::PipelineStageFlagBits::eColorAttachmentOutput;
	color_dependency.srcAccessMask   = vk::AccessFlagBits();
	color_dependency.dstAccessMask   = dst_flags;
	color_dependency.dependencyFlags = vk::DependencyFlags();

	d.renderpass = device.createRenderPass({{}, 1, &color_attachment, 1, &subpass_desc, 1, &color_dependency});

	/* ------------------------------------------------------------ */
	/* get swapchain                                                */

	r->target_cx = cx_ = new_cx;
	r->target_cy = cy_ = new_cy;

	uint32_t backbuffer_count = 2;
	if (backbuffer_count < surface_caps.minImageCount)
		backbuffer_count = surface_caps.minImageCount;
	if (surface_caps.maxImageCount > 0 && backbuffer_count > surface_caps.maxImageCount)
		backbuffer_count = surface_caps.maxImageCount;

	vk::SurfaceTransformFlagBitsKHR transform = surface_caps.currentTransform;
	if (surface_caps.supportedTransforms & vk::SurfaceTransformFlagBitsKHR::eIdentity)
		transform = vk::SurfaceTransformFlagBitsKHR::eIdentity;

	vk::CompositeAlphaFlagBitsKHR alpha_flag     = vk::CompositeAlphaFlagBitsKHR::eOpaque;
	vk::CompositeAlphaFlagBitsKHR alpha_flags[4] = {
	        vk::CompositeAlphaFlagBitsKHR::eOpaque,
	        vk::CompositeAlphaFlagBitsKHR::ePreMultiplied,
	        vk::CompositeAlphaFlagBitsKHR::ePostMultiplied,
	        vk::CompositeAlphaFlagBitsKHR::eInherit,
	};

	for (auto flag : alpha_flags) {
		if (surface_caps.supportedCompositeAlpha & flag) {
			alpha_flag = flag;
			break;
		}
	}

	vk::SwapchainCreateInfoKHR sci;
	sci.surface          = *surface;
	sci.minImageCount    = backbuffer_count;
	sci.imageFormat      = format;
	sci.imageColorSpace  = get_color_space(info_.format);
	sci.imageExtent      = vk::Extent2D(cx_, cy_);
	sci.imageArrayLayers = 1;
	sci.imageUsage       = vk::ImageUsageFlagBits::eColorAttachment;
	sci.imageSharingMode = vk::SharingMode::eExclusive;
	sci.preTransform     = transform;
	sci.compositeAlpha   = alpha_flag;
	sci.presentMode      = vk::PresentModeKHR::eFifo;
	sci.clipped          = true;
	sci.oldSwapchain     = *prev.swapchain;

	d.swapchain = device.createSwapchainKHR(sci);

	/* ------------------------------------------------------------ */
	/* initialize backbuffers                                       */

	std::vector<vk::Image> images = d.swapchain.getImages();
	d.backbuffers.resize(images.size());

	for (size_t i = 0; i < images.size(); i++)
		d.backbuffers[i].init(device, *this, r->queue_idx, format, images[i]);
	for (size_t i = 0; i < images.size(); i++)
		d.acquire_sems.emplace_back(device.createSemaphore({}));

	d.cur_backbuffer_idx = 0;
	d.first_frame_id     = d.backbuffers[0].id;
	first_image          = true;

	if (r->cur_display == this)
		r->cur_display_image = nullptr;
}

void vulkan_display::acquire_next_image()
{
	auto *r = vulkan_renderer::current;

	if (do_resize) {
		r->wait_for_idle();
		reset(true);
		do_resize = false;
	}

	for (;;) {
		try {
			auto ret = d.swapchain.acquireNextImage(UINT64_MAX, *d.acquire_sems.front());
			if (ret.first == vk::Result::eSuccess || ret.first == vk::Result::eSuboptimalKHR) {
				vkr::Semaphore acquire_sem        = std::move(d.acquire_sems.front());
				d.cur_backbuffer_idx              = ret.second;
				display_image *next_display_image = &d.backbuffers[d.cur_backbuffer_idx];

				d.acquire_sems.pop_front();
				d.recycle_acquire_sem(next_display_image);

				next_display_image->acquire_sem = std::move(acquire_sem);
			} else {
				vk::detail::throwResultException(ret.first, "acquireNextImage");
			}
			break;

		} catch (const vk::OutOfDateKHRError &) {
			r->wait_for_idle();
			reset(false);

		} catch (const vk::SurfaceLostKHRError &) {
			r->wait_for_idle();
			{
				vkr::SurfaceKHR old_surface = std::move(surface);
				vk_display      old_d       = std::move(d);
			}
			init();
		}
	}

	first_image = false;
}

void vulkan_display::swap() noexcept
try {
	auto           *r                 = vulkan_renderer::current;
	display_image *&cur_display_image = r->cur_display_image;
	vkr::Queue     &queue             = r->queue;

	r->check_start_rendering();

	if (r->cur_frame_cmd) {
		/* ------------------------------------------------------------ */
		/* build command buffer array                                   */

		r->cmds_to_submit.reserve(r->rt_cmds.size() + 2);

		/* resource initialization and flush/copy commands */
		if (r->cur_flush_cmd) {
			r->cur_flush_cmd->end();
			r->cmds_to_submit.push_back(**r->cur_flush_cmd);
		}

		/* render target command buffers (in order of first to last used) */
		for (auto &[_, image, cmd] : r->rt_cmds) {
			std::array<vk::ImageMemoryBarrier, 1> imb = {
			        vk::ImageMemoryBarrier(vk::AccessFlagBits::eColorAttachmentWrite,
			                               vk::AccessFlagBits::eShaderRead,
			                               vk::ImageLayout::eColorAttachmentOptimal,
			                               vk::ImageLayout::eShaderReadOnlyOptimal,
			                               0,
			                               0,
			                               image,
			                               {vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1}),
			};

			cmd.endRenderPass();
			cmd.pipelineBarrier(vk::PipelineStageFlagBits::eColorAttachmentOutput,
			                    vk::PipelineStageFlagBits::eFragmentShader,
			                    {},
			                    {},
			                    {},
			                    imb);
			cmd.end();

			r->cmds_to_submit.push_back(cmd);
		}

		/* prerender command buffer (usually mipmap generation) */
		if (r->cur_prerender_cmd) {
			r->cur_prerender_cmd->end();
			r->cmds_to_submit.push_back(**r->cur_prerender_cmd);
		}

		/* main swapchain frame command buffer goes last */
		r->cur_frame_cmd->endRenderPass();
		r->cur_frame_cmd->end();
		r->cmds_to_submit.push_back(**r->cur_frame_cmd);

		/* ------------------------------------------------------------ */
		/* submit queued commands                                       */

		vk::Semaphore acquire_sem = *cur_display_image->acquire_sem;
		vk::Semaphore draw_sem    = *cur_display_image->draw_sem;

		if (r->cmds_to_submit.size()) {
			vk::PipelineStageFlags acquire_wait_pipeline =
			        vk::PipelineStageFlagBits::eColorAttachmentOutput;

			std::array<vk::SubmitInfo, 1> submission = {vk::SubmitInfo(1,
			                                                           &acquire_sem,
			                                                           &acquire_wait_pipeline,
			                                                           (uint32_t)r->cmds_to_submit.size(),
			                                                           r->cmds_to_submit.data(),
			                                                           1,
			                                                           &draw_sem)};

			queue.submit(submission, *cur_display_image->fence);
			r->idle = false;
		}

		/* ------------------------------------------------------------ */
		/* reset all command buffer queues                              */

		r->rt_cmds.resize(0);
		r->cmds_to_submit.resize(0);
		r->cur_flush_cmd = nullptr;

		/* ------------------------------------------------------------ */
		/* queue frame presentation and acquire new frame               */

		vk::SwapchainKHR swap = *d.swapchain;
		try {
			vk::Result res = queue.presentKHR({1, &draw_sem, 1, &swap, &d.cur_backbuffer_idx});
			if (res != vk::Result::eSuccess && res != vk::Result::eSuboptimalKHR)
				vk::detail::throwResultException(res, "presentKHR");

		} catch (const vk::OutOfDateKHRError &) {
			r->wait_for_idle();
			if (r->cur_display == this)
				cur_display_image = nullptr;
			reset(false);

		} catch (const vk::SurfaceLostKHRError &) {
			{
				vkr::SurfaceKHR old_surface = std::move(surface);
				vk_display      old_d       = std::move(d);
			}

			r->wait_for_idle();
			if (r->cur_display == this)
				cur_display_image = nullptr;
			init();
		}

		if (cur_display_image) {
			cur_display_image->first_use = false;
			d.recycle_acquire_sem(cur_display_image);
		}

		acquire_next_image();

		/* turn off pushing vk_* objects to cleanup to ensure immediate
		 * free of objects related to shaders */
		r->cur_display_image = nullptr;

		display_image *next_display_image = &d.backbuffers[d.cur_backbuffer_idx];
		next_display_image->reset();

		r->cur_display_image = next_display_image;
		r->cur_frame_id      = next_display_image->id;
		r->cur_rt            = nullptr;
		r->last_rt           = nullptr;
		r->cur_frame_cmd     = nullptr;
		r->cur_flush_cmd     = nullptr;
		r->cur_prerender_cmd = nullptr;
		r->cur_cmd           = nullptr;
		r->viewport_changed  = true;
		r->last_pipeline     = nullptr;
		pipeline_changed     = true;
	}

} catch (const vk::Error &vkerr) {
	error("%s: %s", __func__, vkerr.what());
}

void vulkan_display::resize(uint32_t cx, uint32_t cy) noexcept
{
	resize_cx = cx;
	resize_cy = cy;
	do_resize = true;
}

void vulkan_display::load_internal()
{
	if (first_image)
		acquire_next_image();

	auto *r              = vulkan_renderer::current;
	r->cur_display       = this;
	r->cur_display_image = &d.backbuffers[d.cur_backbuffer_idx];
	r->cur_rt            = nullptr;
	r->last_rt           = nullptr;
	r->cur_frame_cmd     = nullptr;
	r->cur_flush_cmd     = nullptr;
	r->cur_prerender_cmd = nullptr;
	r->cur_cmd           = nullptr;
	r->cur_frame_id      = r->cur_display_image->id;
	r->viewport_changed  = true;

	r->rt_cmds.resize(0);
	r->cmds_to_submit.resize(0);
}

/* ------------------------------------------------------------------------- */

struct blend_val {
	vk::BlendFactor src;
	vk::BlendFactor dst;
	vk::BlendFactor src_alpha;
	vk::BlendFactor dst_alpha;
	vk::BlendOp     eq_rgb;
	vk::BlendOp     eq_alpha;

	inline bool func_equal(const blend_val &val) const noexcept
	{
		return src == val.src && dst == val.dst && src_alpha == val.src_alpha && dst_alpha == val.dst_alpha;
	}

	inline bool eq_equal(const blend_val &val) const noexcept
	{
		return eq_rgb == val.eq_rgb && eq_alpha == val.eq_alpha;
	}
};

static blend_val cur_blend{vk::BlendFactor::eSrcAlpha,
                           vk::BlendFactor::eOneMinusSrcAlpha,
                           vk::BlendFactor::eOne,
                           vk::BlendFactor::eZero,
                           vk::BlendOp::eAdd,
                           vk::BlendOp::eAdd};

static std::deque<blend_val> blend_vals{cur_blend};

void push_blend() noexcept
{
	blend_val val = blend_vals[0];
	blend_vals.push_front(val);
	blend_equation(equation::add, equation::add);
}

void pop_blend() noexcept
{
	if (blend_vals.size() > 1) {
		blend_vals.pop_front();

		const blend_val &val = blend_vals[0];
		pipeline_changed     = true;
		cur_blend            = val;
	}
}

static inline vk::BlendFactor get_vk_blend_val(blend val)
{
	/* clang-format off */
	switch (val) {
	case blend::zero:            return vk::BlendFactor::eZero;
	case blend::one:             return vk::BlendFactor::eOne;
	case blend::src_color:       return vk::BlendFactor::eSrcColor;
	case blend::inv_src_color:   return vk::BlendFactor::eOneMinusSrcColor;
	case blend::dst_color:       return vk::BlendFactor::eDstColor;
	case blend::inv_dst_color:   return vk::BlendFactor::eOneMinusDstColor;
	case blend::src_alpha:       return vk::BlendFactor::eSrcAlpha;
	case blend::inv_src_alpha:   return vk::BlendFactor::eOneMinusSrcAlpha;
	case blend::dst_alpha:       return vk::BlendFactor::eDstAlpha;
	case blend::inv_dst_alpha:   return vk::BlendFactor::eOneMinusDstAlpha;
	case blend::const_color:     return vk::BlendFactor::eConstantColor;
	case blend::inv_const_color: return vk::BlendFactor::eOneMinusConstantColor;
	case blend::const_alpha:     return vk::BlendFactor::eConstantAlpha;
	case blend::inv_const_alpha: return vk::BlendFactor::eOneMinusConstantAlpha;
	}
	/* clang-format on */

	return vk::BlendFactor::eZero;
}

void blend_func(blend src, blend dst, blend src_alpha, blend dst_alpha) noexcept
{
	blend_val &val = blend_vals[0];
	val.src        = get_vk_blend_val(src);
	val.dst        = get_vk_blend_val(dst);
	val.src_alpha  = get_vk_blend_val(src_alpha);
	val.dst_alpha  = get_vk_blend_val(dst_alpha);

	if (!val.func_equal(cur_blend)) {
		pipeline_changed = true;
		cur_blend        = val;
	}
}

static vk::BlendOp get_vk_equation_val(equation val) noexcept
{
	/* clang-format off */
	switch (val) {
	case equation::add:              return vk::BlendOp::eAdd;
	case equation::subtract:         return vk::BlendOp::eSubtract;
	case equation::reverse_subtract: return vk::BlendOp::eReverseSubtract;
	case equation::min:              return vk::BlendOp::eMin;
	case equation::max:              return vk::BlendOp::eMax;
	}
	/* clang-format on */

	return vk::BlendOp::eAdd;
}

void blend_equation(equation rgb, equation alpha) noexcept
{
	blend_val &val = blend_vals[0];
	val.eq_rgb     = get_vk_equation_val(rgb);
	val.eq_alpha   = get_vk_equation_val(alpha);

	if (!val.eq_equal(cur_blend)) {
		pipeline_changed = true;
		cur_blend        = val;
	}
}

/* ------------------------------------------------------------------------- */

struct viewport_rect {
	int x;
	int y;
	int cx;
	int cy;

	inline bool operator==(const viewport_rect &vp) const
	{
		return x == vp.x && y == vp.y && cx == vp.cx && cy == vp.cy;
	}
};

static std::deque<viewport_rect> viewports{{0, 0, 0, 0}};

void push_viewport() noexcept
{
	viewport_rect vp = viewports[0];
	viewports.push_front(vp);
}

void pop_viewport() noexcept
{
	if (viewports.size() > 1) {
		viewport_rect vp = viewports[0];
		viewports.pop_front();

		if (vp != viewports[0]) {
			auto *r             = vulkan_renderer::current;
			r->viewport_changed = true;
		}
	}
}

void set_viewport(int x, int y, int cx, int cy) noexcept
{
	viewport_rect vp;
	vp.x  = x;
	vp.y  = y;
	vp.cx = cx;
	vp.cy = cy;

	viewport_rect &cur_vp = viewports[0];
	if (cur_vp != vp) {
		auto *r             = vulkan_renderer::current;
		cur_vp              = vp;
		r->viewport_changed = true;
	}
}

/* ------------------------------------------------------------------------- */

extern texture *cur_render_target;

void set_render_target(texture *tex_in) noexcept
try {
	auto *tex = static_cast<vulkan_texture *>(tex_in);

	if (cur_render_target == tex_in)
		return;
	if (tex && !tex->renderable)
		throw vk::LogicError("Texture is not renderable");

	auto *r = vulkan_renderer::current;

	r->check_start_rendering();

	cur_render_target = tex;
	r->cur_rt         = tex;

} catch (const vk::Error &vkerr) {
	warn("%s: %s", __func__, vkerr.what());
}

/* ------------------------------------------------------------------------- */

void vulkan_renderer::load_display(display *d) noexcept
try {
	if (cur_display == d)
		return;

	cur_display = static_cast<vulkan_display *>(d);
	cur_display->load_internal();

} catch (const vk::Error &vkerr) {
	crpg::error("%s: %s", __FUNCTION__, vkerr.what());
}

display *vulkan_renderer::current_display() const noexcept
{
	return cur_display;
}

/* ------------------------------------------------------------------------- */

void clear(float r_, float g, float b, float a) noexcept
try {
	auto                *r    = vulkan_renderer::current;
	std::array<float, 4> rgba = {r_, g, b, a};

	r->cur_clear_color.setFloat32(rgba);

} catch (const vk::Error &vkerr) {
	warn("%s: %s", __func__, vkerr.what());
}

/* ------------------------------------------------------------------------- */

extern std::deque<math::matrix> proj_matrices;
extern std::deque<math::matrix> view_matrices;

static inline vk::PrimitiveTopology get_topology(primitive topology) noexcept
{
	/* clang-format off */
	switch (topology) {
	case primitive::points:    return vk::PrimitiveTopology::ePointList;
	case primitive::lines:     return vk::PrimitiveTopology::eLineList;
	case primitive::linestrip: return vk::PrimitiveTopology::eLineStrip;
	case primitive::triangles: return vk::PrimitiveTopology::eTriangleList;
	case primitive::tristrip:  return vk::PrimitiveTopology::eTriangleStrip;
	}
	/* clang-format on */

	assert(0);
	return vk::PrimitiveTopology::ePointList;
}

vertex_buffer  *cur_vertex_buffer = nullptr;
vulkan_texture *cur_textures[8]   = {};

vkr::Pipeline vulkan_renderer::create_pipeline(primitive topology)
{
	auto *cur_vs = static_cast<vulkan_shader *>(cur_vertex_shader);
	auto *cur_rt = static_cast<vulkan_texture *>(cur_render_target);
	auto *cur_vb = static_cast<vulkan_vertex_buffer *>(cur_vertex_buffer);

	std::vector<vk::VertexInputBindingDescription>   vertex_bindings;
	std::vector<vk::VertexInputAttributeDescription> vertex_attributes;

	vertex_bindings.emplace_back(0, (uint32_t)sizeof(math::vec3), vk::VertexInputRate::eVertex);
	vertex_attributes.emplace_back(0, 0, vk::Format::eR32G32B32A32Sfloat);
	if (cur_vs->has_coords && cur_vb->coords.size) {
		vertex_bindings.emplace_back(1, (uint32_t)sizeof(math::vec2), vk::VertexInputRate::eVertex);
		vertex_attributes.emplace_back(1, 1, vk::Format::eR32G32Sfloat);
	}

	vk::PipelineVertexInputStateCreateInfo visci = {{}, vertex_bindings, vertex_attributes};

	vk::PipelineInputAssemblyStateCreateInfo iasci({}, get_topology(topology));

	vk::PipelineTessellationStateCreateInfo tsci;
	vk::PipelineViewportStateCreateInfo     vsci({}, 1, nullptr, 1, nullptr);

	vk::FrontFace front = cur_render_target ? vk::FrontFace::eClockwise : vk::FrontFace::eCounterClockwise;
	vk::PipelineRasterizationStateCreateInfo rsci = {
	        {}, false, false, vk::PolygonMode::eFill, {}, front, {}, {}, {}, {}, 1.0f};

	vk::PipelineMultisampleStateCreateInfo  msci;
	vk::PipelineDepthStencilStateCreateInfo dssci;

	constexpr vk::ColorComponentFlags color_flags = vk::ColorComponentFlagBits::eR |
	                                                vk::ColorComponentFlagBits::eG |
	                                                vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA;

	blend_val                             blend       = blend_vals[0];
	vk::PipelineColorBlendAttachmentState blend_state = {true,
	                                                     cur_blend.src,
	                                                     cur_blend.dst,
	                                                     cur_blend.eq_rgb,
	                                                     cur_blend.src_alpha,
	                                                     cur_blend.dst_alpha,
	                                                     cur_blend.eq_alpha,
	                                                     color_flags};

	vk::PipelineColorBlendStateCreateInfo cbsci = {{}, false, {}, 1, &blend_state};

	std::array<vk::DynamicState, 2>    dynamic_state = {vk::DynamicState::eViewport, vk::DynamicState::eScissor};
	vk::PipelineDynamicStateCreateInfo dsci          = {{}, dynamic_state};

	vkr::RenderPass &renderpass = cur_render_target ? cur_rt->renderpass : cur_display->d.renderpass;

	vk::GraphicsPipelineCreateInfo gpci({},
	                                    2,
	                                    cur_program->stages,
	                                    &visci,
	                                    &iasci,
	                                    &tsci,
	                                    &vsci,
	                                    &rsci,
	                                    &msci,
	                                    &dssci,
	                                    &cbsci,
	                                    &dsci,
	                                    *cur_program->pipeline_layout,
	                                    *renderpass,
	                                    0);

	return device.createGraphicsPipeline(pipeline_cache, gpci);
}

inline bool vulkan_renderer::can_update_pipeline(primitive topology)
{
	return !last_pipeline || pipeline_changed || last_topology != topology;
}

inline void vulkan_renderer::update_pipeline(primitive topology)
{
	if (!can_update_pipeline(topology))
		return;

	pipeline_lookup.resize(0);
	ostringserializer s(pipeline_lookup);

	auto    *cur_vb  = static_cast<vulkan_vertex_buffer *>(cur_vertex_buffer);
	auto    *cur_rt  = static_cast<vulkan_texture *>(cur_render_target);
	uint64_t rt_id   = cur_rt ? cur_rt->id : cur_display->id;
	bool     has_rt  = !!cur_rt;
	size_t   buffers = cur_vb->coords.size ? 2 : 1;

	s << has_rt << rt_id << cur_program->key << buffers << blend_vals[0] << topology;

	auto it = pipelines.find(pipeline_lookup);
	if (it == pipelines.end()) {
		pipelines.emplace(pipeline_lookup, create_pipeline(topology));
		it = pipelines.find(pipeline_lookup);

		if (cur_rt)
			cur_rt->pipeline_ids.push_back(pipeline_lookup);
		else
			cur_display->pipeline_ids.push_back(pipeline_lookup);
	}

	vkr::Pipeline &new_pipeline = it->second;

	if (last_pipeline != &new_pipeline)
		cur_cmd->bindPipeline(vk::PipelineBindPoint::eGraphics, *new_pipeline);

	last_pipeline    = &new_pipeline;
	last_topology    = topology;
	pipeline_changed = false;
}

vkr::CommandBuffer *vulkan_renderer::get_cmdbuf()
{
	vkr::CommandBuffer &cmdbuf = cur_display_image->get_cmdbuf();
	return &cmdbuf;
}

void vulkan_renderer::init_multi_swapchain()
{
	multi_swapchain = true;
	frameless_pool  = device.createCommandPool({vk::CommandPoolCreateFlagBits::eResetCommandBuffer, queue_idx});
	auto cmds       = device.allocateCommandBuffers({*frameless_pool, vk::CommandBufferLevel::ePrimary, 1});
	frameless_cmd   = std::move(cmds[0]);
	frameless_fence = device.createFence({});
}

void vulkan_renderer::submit_frameless_cmd()
{
	frameless_cmd.end();

	std::array<vk::SubmitInfo, 1> submission = {vk::SubmitInfo(0, nullptr, nullptr, 1, &*frameless_cmd)};
	queue.submit(submission, *frameless_fence);

	vk::Result res = wait_for_fence(frameless_fence);
	if (res != vk::Result::eSuccess)
		vk::detail::throwResultException(res, "wait_for_fence");

	frameless_cmd.reset();
}

void vulkan_renderer::update_render_target()
{
	vkr::RenderPass              &renderpass  = cur_rt ? cur_rt->renderpass : cur_display->d.renderpass;
	vkr::Framebuffer             &framebuffer = cur_rt ? cur_rt->framebuffer : cur_display_image->framebuffer;
	std::array<vk::ClearValue, 1> clear       = {cur_clear_color};

	target_cx = (uint32_t)(cur_rt ? cur_rt->cx() : cur_display->cx());
	target_cy = (uint32_t)(cur_rt ? cur_rt->cy() : cur_display->cy());

	if (cur_cmd && last_rt && last_rt != cur_rt)
		rt_cmds.emplace_back(last_rt->id, *last_rt->image, **cur_cmd);

	if (cur_rt && last_rt != cur_rt) {
		bool found = false;

		for (auto it = rt_cmds.begin(); it != rt_cmds.end(); ++it) {
			if (std::get<0>(*it) == cur_rt->id) {
				rt_cmds.erase(it);
				found = true;
				break;
			}
		}

		if (!found) {
			std::array<vk::ImageMemoryBarrier, 1> imb = {
			        vk::ImageMemoryBarrier(vk::AccessFlagBits::eShaderRead,
			                               vk::AccessFlagBits::eColorAttachmentWrite,
			                               vk::ImageLayout::eShaderReadOnlyOptimal,
			                               vk::ImageLayout::eColorAttachmentOptimal,
			                               0,
			                               0,
			                               *cur_rt->image,
			                               {vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1}),
			};

			cur_cmd = get_cmdbuf();
			cur_cmd->begin({});
			cur_cmd->pipelineBarrier(vk::PipelineStageFlagBits::eFragmentShader,
			                         vk::PipelineStageFlagBits::eColorAttachmentOutput,
			                         {},
			                         {},
			                         {},
			                         imb);
			cur_cmd->beginRenderPass({*renderpass, *framebuffer, {{0, 0}, {target_cx, target_cy}}, clear},
			                         vk::SubpassContents::eInline);
		}
	}

	if (!cur_rt) {
		if (!cur_frame_cmd) {
			cur_frame_cmd = get_cmdbuf();
			cur_frame_cmd->begin({});
			cur_frame_cmd->beginRenderPass(
			        {*renderpass, *framebuffer, {{0, 0}, {target_cx, target_cy}}, clear},
			        vk::SubpassContents::eInline);
		}
		cur_cmd = cur_frame_cmd;
	}

	last_rt          = cur_rt;
	viewport_changed = true;
	pipeline_changed = true;
}

extern bool check_load_program();

inline void vulkan_renderer::draw(primitive topology, uint32_t start, uint32_t count) noexcept
try {

	if (!cur_vertex_buffer)
		throw vk::LogicError("No vertex buffer loaded");
	if (!cur_vertex_shader)
		throw vk::LogicError("No vertex shader loaded");
	if (!cur_fragment_shader)
		throw vk::LogicError("No fragment shader loaded");
	if (!check_load_program())
		throw vk::LogicError("Failed to load program");

	check_start_rendering();

	/* ----------------------------------------- */
	/* Update viewproj matrix                    */

	math::matrix proj = proj_matrices[0];

	proj.x.y = -proj.x.y;
	proj.y.y = -proj.y.y;
	proj.z.y = -proj.z.y;
	proj.t.y = -proj.t.y;

	math::matrix   viewproj       = (view_matrices[0] * proj);
	shader::param *viewproj_param = cur_vertex_shader->get_viewproj();

	if (viewproj_param)
		viewproj_param->set(viewproj);

	/* ----------------------------------------- */
	/* set viewport/scissor                      */

	if (viewport_changed) {
		viewport_rect               vp  = viewports[0];
		std::array<vk::Viewport, 1> vps = {
		        vk::Viewport((float)vp.x, (float)vp.y, (float)vp.cx, (float)vp.cy, 0.0f, 1.0f)};
		cur_cmd->setViewport(0, vps);

		std::array<vk::Rect2D, 1> scissor = {vk::Rect2D({0, 0}, {target_cx, target_cy})};
		cur_cmd->setScissor(0, scissor);

		viewport_changed = false;
	}

	/* ----------------------------------------- */
	/* If pipeline changed, rebind all           */

	if (can_update_pipeline(topology)) {
		cur_bound_vb = nullptr;
		cur_bound_descriptor_hash.clear();
	}

	/* ----------------------------------------- */
	/* Load shader data                          */

	cur_program->load();

	/* ----------------------------------------- */
	/* bind vertex/index buffers                 */

	static_cast<vulkan_vertex_buffer *>(cur_vertex_buffer)->bind();

	/* ----------------------------------------- */
	/* Draw                                      */

	update_pipeline(topology);

	auto *cur_vb = static_cast<vulkan_vertex_buffer *>(cur_vertex_buffer);
	if (cur_vb->indices) {
		if (count == 0)
			count = (uint32_t)cur_vb->index_count();

		cur_cmd->drawIndexed(count, 1, start, 0, 0);
	} else {
		if (count == 0)
			count = (uint32_t)cur_vertex_buffer->vert_count();

		cur_cmd->draw(count, 1, start, 0);
	}

} // namespace graphics
catch (const vk::Error &vkerr) {
	warn("%s: %s", __func__, vkerr.what());
}

void draw(primitive topology, int start, int count) noexcept
{
	auto *r = vulkan_renderer::current;
	r->draw(topology, (uint32_t)start, (uint32_t)count);
}

} // namespace graphics
} // namespace crpg
