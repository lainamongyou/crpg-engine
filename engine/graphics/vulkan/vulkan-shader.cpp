#include "../../utility/stringserializer.hpp"
#include "vulkan-graphics.hpp"

extern "C" {
#include "spirv-reflect/spirv_reflect.h"
}

namespace crpg {
namespace graphics {

vk_program_base *cur_program         = nullptr;
static bool      program_load_failed = false;

bool load_program()
try {
	auto        *r      = vulkan_renderer::current;
	vkr::Device &device = r->device;

	if (!cur_vertex_shader)
		throw vk::LogicError("No vertex shader loaded");
	if (!cur_fragment_shader)
		throw vk::LogicError("No fragment shader loaded");

	uint64_t key  = (uint64_t)cur_vertex_shader->id() | ((uint64_t)cur_fragment_shader->id() << 32ULL);
	auto     pair = r->program_lookup.find(key);
	if (pair != r->program_lookup.end()) {
		cur_program = &*pair->second;
	} else {
		vk_program new_program(device, key);
		new_program.link();

		r->programs.push_front(std::move(new_program));

		auto it                = r->programs.begin();
		r->program_lookup[key] = it;
		cur_program            = &*it;

		cur_program->init();
	}

	return true;

} catch (const vk::Error &vkerr) {
	program_load_failed = true;
	error("%s: %s", __func__, vkerr.what());
	return false;
}

bool check_load_program()
{
	if (cur_program)
		return true;
	else if (program_load_failed)
		return false;
	return load_program();
}

vk_program_base::vk_program_base(vkr::Device &device, uint64_t key)
        : device(device),
          key(key),
          vs(static_cast<vulkan_shader *>(cur_vertex_shader)),
          fs(static_cast<vulkan_shader *>(cur_fragment_shader))
{
}

vk::DescriptorSet vk_program_base::get_descriptor_set(std::string &descriptor_set_id)
{
	auto              *r      = vulkan_renderer::current;
	vkr::Device       &device = r->device;
	vkr::DescriptorSet set    = nullptr;

	for (size_t i = 0; i < pools.size(); i++) {
		vk::DescriptorPool pool = *pools.front();
		try {
			auto sets = device.allocateDescriptorSets({pool, 1, &*descriptor_layout});
			set       = std::move(sets[0]);

		} catch (const vk::OutOfPoolMemoryError &) {
			vkr::DescriptorPool front = std::move(pools.front());
			pools.pop_front();
			pools.push_back(std::move(front));
		}
	}

	if (!*set) {
		vkr::DescriptorPool pool = device.createDescriptorPool(
		        {vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet, max_pool_sets, descriptor_sizes});
		auto sets = device.allocateDescriptorSets({*pool, 1, &*descriptor_layout});
		set       = std::move(sets[0]);
		pools.push_front(std::move(pool));
	}

	vk::DescriptorSet ret = *set;

	std::vector<vk::WriteDescriptorSet>  writes;
	std::vector<vk::DescriptorImageInfo> write_images;
	vk::DescriptorBufferInfo             write_buffer;

	write_images.reserve(descriptors.size());

	for (auto &descriptor : descriptors) {
		vk::WriteDescriptorSet write;

		write.dstSet          = ret;
		write.dstBinding      = descriptor.binding;
		write.descriptorCount = 1;

		texture_ref     ref     = descriptor.texture_param->tex.lock();
		vulkan_texture *tex     = static_cast<vulkan_texture *>(ref.get());
		vk::Sampler     sampler = tex->get_sampler();

		tex->descriptor_set_ids.push_back(descriptor_set_id);

		write_images.emplace_back(sampler, *tex->view, vk::ImageLayout::eShaderReadOnlyOptimal);

		write.descriptorType = vk::DescriptorType::eCombinedImageSampler;
		write.pImageInfo     = &write_images.back();

		writes.push_back(write);
	}

	device.updateDescriptorSets(writes, {});

	vk_descriptor_set ds;
	ds.set = std::move(set);
	descriptor_sets.emplace(std::move(descriptor_set_id), std::move(ds));
	return ret;
}

void vk_program_base::load()
{
	auto *r = vulkan_renderer::current;

	auto copy_param = [this](vulkan_shader_param &param, uint8_t *dst) {
		param.changed = false;

		if (param.type_ == shader::param::type::t_texture)
			return;

		auto &data = param.data;
		if (&param == viewproj && world->is_set()) {
			math::matrix  mat       = *reinterpret_cast<math::matrix *>(data.data());
			math::matrix &world_mat = *reinterpret_cast<math::matrix *>(world_param.data.data());
			mat *= world_mat;

			memcpy(dst + param.offset, &mat, sizeof(mat));
		} else {
			memcpy(dst + param.offset, data.data(), data.size());
		}
	};

	/* ----------------------------------------- */
	/* check params                              */

	for (auto &param : cur_program->param_array) {
		if (!param.is_set())
			throw vk::LogicError(strprintf("Program param '%s' not set", param.name.c_str()));
		if (param.type_ == shader::param::type::t_texture)
			continue;

		auto &data = param.data;

		if (data.size() < param.size || data.size() > param.padded_size)
			throw vk::LogicError(strprintf("Program param '%s' has bad size", param.name.c_str()));
	}

	/* ----------------------------------------- */
	/* bind descriptor set                       */

	if (descriptors.size()) {
		descriptor_set_lookup.resize(0);
		ostringserializer s(descriptor_set_lookup);

		s << key;

		for (auto &descriptor : descriptors) {
			auto       *param = descriptor.texture_param;
			texture_ref ref   = param->tex.lock();
			if (!ref)
				throw vk::LogicError(strprintf("Program param '%s': no texture", param->name.c_str()));

			vulkan_texture *tex = static_cast<vulkan_texture *>(ref.get());
			s << tex->id;
		}

		if (descriptor_set_lookup != r->cur_bound_descriptor_hash) {
			r->cur_bound_descriptor_hash = descriptor_set_lookup;

			vk::DescriptorSet set;

			auto ds_it = descriptor_sets.find(descriptor_set_lookup);
			if (ds_it != descriptor_sets.end()) {
				set = *ds_it->second.set;
			} else {
				set = get_descriptor_set(descriptor_set_lookup);
			}

			r->cur_cmd->bindDescriptorSets(
			        vk::PipelineBindPoint::eGraphics, *pipeline_layout, 0, {set}, {});
		}
	}

	/* ----------------------------------------- */
	/* update push constants                     */

	if (push_constant.size()) {
		uint8_t *dst = push_constant.data();

		for (auto &param : cur_program->param_array)
			copy_param(param, dst);

		r->cur_cmd->pushConstants<uint8_t>(*pipeline_layout, push_constant_stages, 0, push_constant);
	}
}

static inline bool layouts_match(const vk::DescriptorSetLayoutBinding &a, const vk::DescriptorSetLayoutBinding &b)
{
	return a.descriptorType == b.descriptorType && a.descriptorCount == b.descriptorCount &&
	       a.stageFlags != b.stageFlags;
}

void vk_program_base::link()
{
	/* ------------------------------------------------------------ */
	/* build shader parameters                                      */

	param_array.reserve(vs->estimated_params + fs->estimated_params);

	std::vector<vk::DescriptorSetLayoutBinding> layout_bindings;

	auto add_descriptor_set_layout_binding = [&](const vulkan_binding &binding) {
		auto &layout = binding.layout;

		auto it = std::find_if(layout_bindings.begin(), layout_bindings.end(), [&](auto &val) {
			return val.binding >= layout.binding;
		});

		if (it != layout_bindings.end() && it->binding == layout.binding) {
			if (!layouts_match(*it, layout))
				throw vk::LogicError("Mismatching descriptor layouts");

			it->stageFlags |= layout.stageFlags;
		} else {
			layout_bindings.insert(it, layout);

			descriptor desc;
			desc.binding = layout.binding;

			if (binding.members.size()) {
				throw vk::LogicError("Uniform buffers not supported (yet)");
			} else {
				param_array.emplace_back(
				        binding.name, layout.binding, binding.offset, binding.size, binding.size);

				auto *param          = &param_array.back();
				params[binding.name] = param;
				desc.texture_param   = param;
			}

			auto desc_it = std::find_if(descriptors.begin(), descriptors.end(), [&](auto val) {
				return val.binding > layout.binding;
			});

			descriptors.insert(desc_it, desc);
		}
	};

	auto add_push_constant = [&](const vulkan_binding &binding) {
		for (auto &member : binding.members) {

			auto it = params.find(member.name);
			if (it != params.end()) {
				vulkan_shader_param *param = it->second;

				if (param->offset != member.offset || param->size != member.size ||
				    param->padded_size != member.padded_size)
					throw vk::LogicError("Mismatching push constants");
				continue;
			}

			param_array.emplace_back(member.name, 0, member.offset, member.size, member.padded_size);
			params[member.name] = &param_array.back();

			if (member.name == "viewproj")
				viewproj = &param_array.back();
			else if (member.name == "world")
				throw vk::LogicError("Don't use a 'world' constant");
		}
	};

	for (const auto &binding : vs->bindings) {
		if (binding.type == param_type::uniform)
			add_descriptor_set_layout_binding(binding);
		else
			add_push_constant(binding);
	}
	for (const auto &binding : fs->bindings) {
		if (binding.type == param_type::uniform)
			add_descriptor_set_layout_binding(binding);
		else
			add_push_constant(binding);
	}

	/* ------------------------------------------------------------ */
	/* build push constant information                              */

	std::vector<vk::PushConstantRange> ranges;

	if (vs->push_constant_size || fs->push_constant_size) {
		uint32_t push_constant_size = vs->push_constant_size ? vs->push_constant_size : fs->push_constant_size;

		if (vs->push_constant_size)
			push_constant_stages |= vk::ShaderStageFlagBits::eVertex;
		if (fs->push_constant_size)
			push_constant_stages |= vk::ShaderStageFlagBits::eFragment;

		if (vs->push_constant_size && fs->push_constant_size &&
		    vs->push_constant_size != fs->push_constant_size)
			throw vk::LogicError("Push constants don't match in the shaders");

		push_constant.resize(push_constant_size);
		ranges.emplace_back(push_constant_stages, 0, push_constant_size);
	}

	/* ------------------------------------------------------------ */
	/* build descriptor information                                 */

	for (const auto &layout : layout_bindings) {
		auto it = std::find_if(descriptor_sizes.begin(), descriptor_sizes.end(), [&](auto &val) {
			return val.type == layout.descriptorType;
		});

		if (it == descriptor_sizes.end()) {
			vk::DescriptorPoolSize dps(layout.descriptorType, layout.descriptorCount);
			descriptor_sizes.push_back(dps);
		} else {
			it->descriptorCount += layout.descriptorCount;
		}
	}

	for (auto &dps : descriptor_sizes)
		dps.descriptorCount *= max_pool_sets;

	/* ------------------------------------------------------------ */
	/* create descriptor layout                                     */

	descriptor_layout =
	        device.createDescriptorSetLayout({{}, (uint32_t)layout_bindings.size(), layout_bindings.data()});

	vk::DescriptorSetLayout dsl = *descriptor_layout;
	pipeline_layout = device.createPipelineLayout({{}, 1, &dsl, (uint32_t)ranges.size(), ranges.data()});

	stages[0] = {{}, vk::ShaderStageFlagBits::eVertex, *vs->module, "main", {}};
	stages[1] = {{}, vk::ShaderStageFlagBits::eFragment, *fs->module, "main", {}};
}

void vk_program_base::init()
{
	world           = &world_param;
	params["world"] = world;
}

/* --------------------------------------------------- */

vulkan_shader::vulkan_shader(shader_type type, const std::string &spirv) : type(type)
{
	SpvReflectShaderModule refl_module;
	SpvReflectResult       res;
	uint32_t               count;

	res = spvReflectCreateShaderModule(spirv.size(), (uint32_t *)spirv.data(), &refl_module);
	if (res != SPV_REFLECT_RESULT_SUCCESS)
		throw vk::LogicError(strprintf("spvReflectCreateShaderModule failed: %d", (int)res));

	/* ------------------------------------------------------------ */
	/* get shader layout bindings                                   */

	res = spvReflectEnumerateDescriptorSets(&refl_module, &count, nullptr);
	if (res != SPV_REFLECT_RESULT_SUCCESS)
		throw vk::LogicError(strprintf("spvReflectEnumerateDescriptorSets failed: %d", (int)res));

	std::vector<SpvReflectDescriptorSet *> sets(count);
	if (count) {
		res = spvReflectEnumerateDescriptorSets(&refl_module, &count, sets.data());
		if (res != SPV_REFLECT_RESULT_SUCCESS)
			throw vk::LogicError(strprintf("spvReflectEnumerateDescriptorSets failed: %d", (int)res));
	}

	for (const auto *set : sets) {
		for (uint32_t i = 0; i < set->binding_count; i++) {
			SpvReflectDescriptorBinding *refl_binding = set->bindings[i];

			vulkan_binding binding;
			binding.name                   = refl_binding->name;
			binding.type                   = param_type::uniform;
			binding.layout.binding         = refl_binding->binding;
			binding.layout.descriptorType  = static_cast<vk::DescriptorType>(refl_binding->descriptor_type);
			binding.layout.descriptorCount = 1;
			binding.layout.stageFlags      = static_cast<vk::ShaderStageFlagBits>(refl_module.shader_stage);
			binding.offset                 = refl_binding->block.offset;
			binding.size                   = refl_binding->block.size;

			if (refl_binding->descriptor_type == SPV_REFLECT_DESCRIPTOR_TYPE_UNIFORM_BUFFER) {
				uint32_t offset_size = binding.offset + binding.size;

				if (uniform_offset > binding.offset)
					uniform_offset = binding.offset;
				if (uniform_size < offset_size)
					uniform_size = offset_size;
			}

			for (uint32_t j = 0; j < refl_binding->array.dims_count; j++)
				binding.layout.descriptorCount *= refl_binding->array.dims[j];

			for (uint32_t j = 0; j < refl_binding->block.member_count; j++) {
				auto &var = refl_binding->block.members[j];

				member_variable member = {var.name, var.offset, var.size, var.padded_size};
				binding.members.push_back(std::move(member));
				estimated_params++;
			}

			bindings.push_back(std::move(binding));
			estimated_params++;
		}
	}

	/* ------------------------------------------------------------ */
	/* get shader push constants                                    */

	res = spvReflectEnumeratePushConstantBlocks(&refl_module, &count, nullptr);
	if (res != SPV_REFLECT_RESULT_SUCCESS)
		throw vk::LogicError(strprintf("spvReflectEnumeratePushConstantBlocks failed: %d", (int)res));

	std::vector<SpvReflectBlockVariable *> push_constants(count);
	if (count) {
		res = spvReflectEnumeratePushConstantBlocks(&refl_module, &count, push_constants.data());
		if (res != SPV_REFLECT_RESULT_SUCCESS)
			throw vk::LogicError(strprintf("spvReflectEnumeratePushConstantBlocks failed: %d", (int)res));
	}

	for (const auto *pc : push_constants) {
		vulkan_binding binding;
		binding.name   = pc->name;
		binding.type   = param_type::push_constant;
		binding.offset = pc->offset;
		binding.size   = pc->size;

		push_constant_size += pc->padded_size;

		for (uint32_t i = 0; i < pc->member_count; i++) {
			auto &var = pc->members[i];

			member_variable member = {var.name, var.offset, var.size, var.padded_size};
			binding.members.push_back(std::move(member));
			estimated_params++;
		}

		bindings.push_back(std::move(binding));
		estimated_params++;
	}

	if (uniform_offset == 0xFFFFFFFF)
		uniform_offset = 0;

	/* ------------------------------------------------------------ */
	/* get shader input variables (if vertex shader)                */

	if (type == shader_type::vertex) {
		res = spvReflectEnumerateInputVariables(&refl_module, &count, nullptr);
		if (res != SPV_REFLECT_RESULT_SUCCESS)
			throw vk::LogicError(strprintf("spvReflectEnumerateInputVariables failed: %d", (int)res));

		/* cheap hack, expects that things are in the right position
		 * and all that stuff */
		has_coords = count > 1;
	}

	/* ------------------------------------------------------------ */
	/* create shader                                                */

	auto        *r      = vulkan_renderer::current;
	vkr::Device &device = r->device;

	module = device.createShaderModule({{}, spirv.size(), (uint32_t *)spirv.data()});
}

vulkan_shader::~vulkan_shader()
{
	auto *r                 = vulkan_renderer::current;
	auto *cur_display_image = r->cur_display_image;

	if (!cur_display_image)
		r->wait_for_idle();

	uint64_t andval;
	uint64_t cmpval;

	if (type == shader_type::vertex) {
		andval = 0xFFFFFFFFULL;
		cmpval = id();
	} else {
		andval = ~0xFFFFFFFFULL;
		cmpval = (uint64_t)id() << 32ULL;
	}

	std::vector<uint64_t> delete_keys;
	delete_keys.reserve(8);
	for (auto &pair : r->program_lookup) {
		if ((pair.first & andval) == cmpval) {
			if (cur_display_image)
				cur_display_image->cleanup.programs.push_back(std::move(*pair.second));
			r->programs.erase(pair.second);
			delete_keys.push_back(pair.first);
		}
	}

	for (uint64_t key : delete_keys)
		r->program_lookup.erase(key);

	if (cur_vertex_shader == this)
		cur_vertex_shader = nullptr;
	else if (cur_fragment_shader == this)
		cur_fragment_shader = nullptr;
}

extern bool pipeline_changed;

void vulkan_shader::load() noexcept
{
	if (cur_program) {
		for (auto &param : cur_program->param_array)
			param.unset();
		cur_program->world_param.unset();
	}

	cur_program         = nullptr;
	program_load_failed = false;
	pipeline_changed    = true;

	if (type == shader_type::vertex) {
		if (cur_vertex_shader == this)
			return;
		cur_vertex_shader = this;

	} else if (type == shader_type::fragment) {
		if (cur_fragment_shader == this)
			return;
		cur_fragment_shader = this;
	}
}

/* --------------------------------------------------- */

shader_ref shader::create(shader_type type, const std::string &spirv) noexcept
try {
	shader_ref ref = std::make_shared<vulkan_shader>(type, spirv);
	ref->weak      = ref;
	return ref;

} catch (const vk::Error &vkerr) {
	error("%s: %s", __func__, vkerr.what());
	return shader_ref();
}

shader::param *shader::get_param(const char *name) noexcept
{
	if (check_load_program())
		return cur_program->get_param(name);
	return nullptr;
}

shader::param *shader::get_viewproj() const noexcept
{
	if (check_load_program())
		return cur_program->viewproj;
	return nullptr;
}

shader::param *shader::get_world() const noexcept
{
	if (check_load_program())
		return cur_program->world;
	return nullptr;
}

} // namespace graphics
} // namespace crpg
