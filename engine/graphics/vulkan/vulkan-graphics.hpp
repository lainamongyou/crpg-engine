#pragma once

#include <atomic>
#include <vector>
#include <string>
#include <deque>
#include <list>
#include <map>

#include "../../utility/logging.hpp"
#include "../graphics.hpp"
#include "engine-opts.h"

#include <vulkan/vulkan_raii.hpp>

namespace crpg {
namespace graphics {

namespace vkr = vk::raii;

class vulkan_renderer;
class vulkan_display;

/* --------------------------------------------------- */

struct vk_buffer_base {
	vkr::Buffer       buffer   = nullptr;
	vkr::DeviceMemory mem      = nullptr;
	size_t            mem_size = 0;
	size_t            size     = 0;

	inline uint8_t *map(uint32_t offset, size_t size) { return (uint8_t *)mem.mapMemory(offset, size); }

	inline void unmap() { mem.unmapMemory(); }

	void flush(uint32_t offset, size_t size);
};

struct vk_buffer : vk_buffer_base {
	~vk_buffer();

	vk_buffer()                             = default;
	vk_buffer(const vk_buffer &)            = delete;
	vk_buffer(vk_buffer &&)                 = default;
	vk_buffer &operator=(const vk_buffer &) = delete;
	vk_buffer &operator=(vk_buffer &&)      = default;

	void init(vk::BufferUsageFlags usage, vk::MemoryPropertyFlags properties, size_t size);
	void stage(const vk_buffer &staging, vk::AccessFlags access_flags, vk::PipelineStageFlags stages);
};

struct vulkan_buffer : vk_buffer {
	void init(vk::BufferUsageFlags usage, void *data, size_t size);

	inline operator bool() const noexcept { return !!size; }
};

class vulkan_vertex_buffer : public vertex_buffer {
public:
	vulkan_buffer points;
	vulkan_buffer coords;
	vulkan_buffer indices;

	vulkan_vertex_buffer(vulkan_vertex_buffer &&)                 = default;
	vulkan_vertex_buffer(const vulkan_vertex_buffer &)            = delete;
	vulkan_vertex_buffer &operator=(vulkan_vertex_buffer &&)      = default;
	vulkan_vertex_buffer &operator=(const vulkan_vertex_buffer &) = delete;

	inline vulkan_vertex_buffer(const data &data);

	void bind();

	void load() noexcept override;
	void unload() noexcept override;

	void swap(vertex_buffer_ref &vb) noexcept override;
};

/* --------------------------------------------------- */

struct vk_shader_base {
	vkr::ShaderModule module = nullptr;
};

struct vk_shader : vk_shader_base {
	~vk_shader();

	vk_shader()                             = default;
	vk_shader(const vk_shader &)            = delete;
	vk_shader(vk_shader &&)                 = default;
	vk_shader &operator=(const vk_shader &) = delete;
	vk_shader &operator=(vk_shader &&)      = default;
};

struct member_variable {
	std::string name;
	uint32_t    offset;
	uint32_t    size;
	uint32_t    padded_size;
};

enum class param_type {
	uniform,
	push_constant,
};

struct vulkan_binding {
	std::string                    name;
	param_type                     type;
	vk::DescriptorSetLayoutBinding layout;
	uint32_t                       offset;
	uint32_t                       size;

	std::vector<member_variable> members;
};

class vulkan_shader : public shader, public vk_shader {
public:
	shader_type type;

	std::vector<vulkan_binding> bindings;
	size_t                      estimated_params   = 0;
	uint32_t                    uniform_offset     = 0xFFFFFFFF;
	uint32_t                    uniform_size       = 0;
	uint32_t                    push_constant_size = 0;
	bool                        has_coords         = false;

	vulkan_shader(shader_type type, const std::string &spirv);
	~vulkan_shader();

	void load() noexcept override;
};

/* --------------------------------------------------- */

class vulkan_shader_param : public shader::param {
public:
	std::string name;
	uint32_t    binding;
	uint32_t    offset;
	uint32_t    size;
	uint32_t    padded_size;

	vulkan_shader_param(std::nullptr_t) {}
	vulkan_shader_param() = delete;
	inline vulkan_shader_param(std::string name,
	                           uint32_t    binding,
	                           uint32_t    offset,
	                           uint32_t    size,
	                           uint32_t    padded_size) noexcept
	        : name(name), binding(binding), offset(offset), size(size), padded_size(padded_size)
	{
	}

	inline void unset() noexcept { val_set = false; }
	inline bool is_set() const noexcept { return val_set && type_ != param::type::t_invalid; }
};

/* --------------------------------------------------- */

struct vk_descriptor_set_base {
	vkr::DescriptorSet set = nullptr;
};

struct vk_descriptor_set : vk_descriptor_set_base {
	~vk_descriptor_set();

	vk_descriptor_set()                                     = default;
	vk_descriptor_set(const vk_descriptor_set &)            = delete;
	vk_descriptor_set(vk_descriptor_set &&)                 = default;
	vk_descriptor_set &operator=(const vk_descriptor_set &) = delete;
	vk_descriptor_set &operator=(vk_descriptor_set &&)      = default;
};

struct descriptor {
	uint32_t             binding;
	vulkan_shader_param *texture_param;
};

extern shader *cur_vertex_shader;
extern shader *cur_fragment_shader;

struct vk_program_base {
	vkr::Device &device;

	uint64_t key;

	std::vector<vulkan_shader_param>                       param_array;
	std::unordered_map<std::string, vulkan_shader_param *> params;
	std::vector<descriptor>                                descriptors;

	vk::ShaderStageFlags push_constant_stages = {};
	std::vector<uint8_t> push_constant;

	vulkan_shader_param world_param = nullptr;

	vkr::PipelineLayout pipeline_layout = nullptr;

	vk::PipelineShaderStageCreateInfo stages[2];

	static constexpr uint32_t                          max_pool_sets     = 20;
	vkr::DescriptorSetLayout                           descriptor_layout = nullptr;
	std::vector<vk::DescriptorPoolSize>                descriptor_sizes;
	std::deque<vkr::DescriptorPool>                    pools;
	std::unordered_map<std::string, vk_descriptor_set> descriptor_sets;
	std::string                                        descriptor_set_lookup;

	vulkan_shader_param *viewproj = nullptr;
	vulkan_shader_param *world    = nullptr;

	vulkan_shader *vs;
	vulkan_shader *fs;

	vk_program_base(const vk_program_base &)            = delete;
	vk_program_base(vk_program_base &&)                 = default;
	vk_program_base &operator=(const vk_program_base &) = delete;
	vk_program_base &operator=(vk_program_base &&)      = default;

	vk_program_base(vkr::Device &device, uint64_t key);

	inline vulkan_shader_param *get_param(const char *name) noexcept
	{
		auto pair = params.find(name);
		if (pair == params.end())
			return nullptr;

		return pair->second;
	}

	vk::DescriptorSet get_descriptor_set(std::string &descriptor_set_id);

	void init();
	void load();
	void link();
};

struct vk_program : vk_program_base {
	inline vk_program(vkr::Device &device, uint64_t key) : vk_program_base(device, key) {}
	~vk_program();

	vk_program()                              = default;
	vk_program(const vk_program &)            = delete;
	vk_program(vk_program &&)                 = default;
	vk_program &operator=(const vk_program &) = delete;
	vk_program &operator=(vk_program &&)      = default;
};

using program_list = std::list<vk_program>;
extern vk_program_base *cur_program;

/* --------------------------------------------------- */

struct vk_texture_base {
	vkr::Image        image       = nullptr;
	vkr::DeviceMemory mem         = nullptr;
	vkr::ImageView    view        = nullptr;
	vkr::ImageView    view2       = nullptr;
	vk::ImageView     render_view = nullptr;
	vkr::Framebuffer  framebuffer = nullptr;
	vkr::RenderPass   renderpass  = nullptr;
};

struct vk_texture : vk_texture_base {
	~vk_texture();

	vk_texture()                              = default;
	vk_texture(const vk_texture &)            = delete;
	vk_texture(vk_texture &&)                 = default;
	vk_texture &operator=(const vk_texture &) = delete;
	vk_texture &operator=(vk_texture &&)      = default;
};

class vulkan_texture : public texture, public vk_texture {
public:
	uint64_t id = 0;
	bool     renderable;

	std::vector<std::string> pipeline_ids;
	std::vector<std::string> descriptor_set_ids;

	address_mode   mode_s     = address_mode::default_;
	address_mode   mode_t     = address_mode::default_;
	texture_filter min_filter = texture_filter::default_;
	texture_filter mag_filter = texture_filter::default_;

	vulkan_texture() = default;
	inline vulkan_texture(int             cx,
	                      int             cy,
	                      int             levels,
	                      texture_format  format,
	                      const uint8_t **data,
	                      texture::type   texture_type);
	~vulkan_texture();

	vulkan_texture(vulkan_texture &&)                 = default;
	vulkan_texture(const vulkan_texture &)            = delete;
	vulkan_texture &operator=(vulkan_texture &&)      = default;
	vulkan_texture &operator=(const vulkan_texture &) = delete;

	void load(int unit) noexcept override;
	void unload() noexcept override;
	void generate_mipmaps() noexcept override;

	void transition(vkr::CommandBuffer       &cmd,
	                vk::PipelineStageFlagBits stage_before,
	                vk::PipelineStageFlagBits stage_after,
	                vk::AccessFlagBits        af_before,
	                vk::AccessFlagBits        af_after,
	                vk::ImageLayout           lo_before,
	                vk::ImageLayout           lo_after,
	                uint32_t                  level);
	void gen_mipmap(vkr::CommandBuffer       &cmd,
	                uint32_t                  level,
	                vk::PipelineStageFlagBits stage_before,
	                vk::AccessFlagBits        af_before,
	                vk::ImageLayout           lo_before,
	                int32_t                   cx,
	                int32_t                   cy);

	void set_filter(texture_filter min_filter, texture_filter mag_filter) noexcept override;
	void set_wrap(address_mode mode_s, address_mode mode_t) noexcept override;

	void swap(texture_ref &tex) noexcept override;

	vk::Sampler get_sampler();
};

/* --------------------------------------------------- */

struct cleanup_data {
	std::vector<vk_texture_base>        textures;
	std::vector<vk_buffer_base>         buffers;
	std::vector<vk_shader_base>         shaders;
	std::vector<vk_program_base>        programs;
	std::vector<vk_descriptor_set_base> sets;
	std::vector<vkr::Pipeline>          pipelines;

	inline void clear()
	{
		pipelines.clear();
		sets.clear();
		programs.clear();
		shaders.clear();
		buffers.clear();
		textures.clear();
	}
};

struct display_image {
	uint64_t         id          = 0;
	vk::Image        image       = nullptr;
	vkr::ImageView   view        = nullptr;
	vkr::Framebuffer framebuffer = nullptr;
	vkr::Fence       fence       = nullptr;
	vkr::Semaphore   draw_sem    = nullptr;
	vkr::Semaphore   acquire_sem = nullptr;

	vkr::CommandPool              cmdpool = nullptr;
	std::list<vkr::CommandBuffer> cmdbufs;
	size_t                        cmdbufs_used = 0;
	bool                          first_use    = true;

	cleanup_data cleanup;

	inline vkr::CommandBuffer &get_cmdbuf();

	void init(vkr::Device &device, vulkan_display &display, uint32_t queue_idx, vk::Format format, vk::Image image);
	void reset();
};

struct vk_display {
	vkr::SwapchainKHR swapchain  = nullptr;
	vkr::RenderPass   renderpass = nullptr;

	std::vector<display_image> backbuffers;
	std::deque<vkr::Semaphore> acquire_sems;
	uint64_t                   first_frame_id     = 0;
	uint32_t                   cur_backbuffer_idx = 0;

	inline void recycle_acquire_sem(display_image *image) noexcept
	{
		vkr::Semaphore sem = std::move(image->acquire_sem);
		if (*sem) {
			if (acquire_sems.size() == 2) {
				int test = 0;
				test     = 1;
			}
			acquire_sems.push_back(std::move(sem));
		}
	}
};

class vulkan_display : public display {
public:
	vulkan_display(const info &info__, uint32_t cx, uint32_t cy);
	~vulkan_display();

	uint64_t id;

	vkr::SurfaceKHR          surface = nullptr;
	std::vector<std::string> pipeline_ids;
	vk_display               d;
	weak_display_ref         weak;
	bool                     first_image = true;
	std::atomic<uint32_t>    resize_cx   = 0;
	std::atomic<uint32_t>    resize_cy   = 0;
	std::atomic_bool         do_resize   = false;

	void load_internal();

	void init();
	void reset(bool resizing);
	void acquire_next_image();
	void swap() noexcept override;
	void resize(uint32_t cx, uint32_t cy) noexcept override;

	virtual void init_surface() = 0;
};

/* --------------------------------------------------- */

class vulkan_renderer : public renderer {
public:
	/* -------------------------------- */
	/* base stuff                       */

	static vulkan_renderer *current;

	std::vector<const char *> instance_extensions;
	std::vector<const char *> device_extensions;
	std::vector<const char *> layers;

	vkr::Context        context;
	vkr::Instance       inst      = nullptr;
	vkr::PhysicalDevice gpu       = nullptr;
	uint32_t            queue_idx = 0;

#ifdef ENABLE_VULKAN_VALIDATION
	vkr::DebugUtilsMessengerEXT debug_messenger = nullptr;
#endif

	vk::PhysicalDeviceMemoryProperties memory_properties;

	/* -------------------------------- */
	/* lazy initialization              */

	bool               initialized    = false;
	vkr::Device        device         = nullptr;
	vkr::Queue         queue          = nullptr;
	vkr::PipelineCache pipeline_cache = nullptr;

	/* -------------------------------- */
	/* current rendering data           */

	std::list<std::tuple<uint64_t, vk::Image, vk::CommandBuffer>> rt_cmds;
	std::vector<vk::CommandBuffer>                                cmds_to_submit;

	vulkan_display     *cur_display       = nullptr;
	display_image      *cur_display_image = nullptr;
	vulkan_texture     *cur_rt            = nullptr;
	vulkan_texture     *last_rt           = nullptr;
	vertex_buffer      *cur_bound_vb      = nullptr;
	vkr::CommandBuffer *cur_frame_cmd     = nullptr;
	vkr::CommandBuffer *cur_flush_cmd     = nullptr;
	vkr::CommandBuffer *cur_prerender_cmd = nullptr;
	vkr::CommandBuffer *cur_cmd           = nullptr;
	vkr::Pipeline      *last_pipeline     = nullptr;
	primitive           last_topology     = primitive::points;
	vk::ClearColorValue cur_clear_color   = {};
	uint64_t            cur_frame_id      = 0;
	uint32_t            target_cx         = 0;
	uint32_t            target_cy         = 0;
	bool                multi_swapchain   = false;
	bool                viewport_changed  = false;
	bool                idle              = true;

	std::string pipeline_lookup;

	/* -------------------------------- */
	/* multi-swapchain stuff            */

	vkr::CommandPool   frameless_pool  = nullptr;
	vkr::CommandBuffer frameless_cmd   = nullptr;
	vkr::Fence         frameless_fence = nullptr;

	void submit_frameless_cmd();

	/* -------------------------------- */
	/* cached shader programs           */

	program_list                                         programs;
	std::unordered_map<uint64_t, program_list::iterator> program_lookup;
	std::string                                          cur_bound_descriptor_hash;

	/* -------------------------------- */
	/* cached pipelines                 */

	std::unordered_map<std::string, vkr::Pipeline> pipelines;

	/* -------------------------------- */
	/* cached samplers                  */

	std::unordered_map<std::string, vkr::Sampler> samplers;

	/* -------------------------------- */
	/* funcs                            */

	inline vulkan_renderer() noexcept { current = this; }
	inline ~vulkan_renderer()
	{
		if (initialized)
			wait_for_idle();

		current = nullptr;
	}

	void     init();
	void     init_device(vk::SurfaceKHR surface);
	void     load_display(display *d) noexcept override;
	void     init_multi_swapchain();
	display *current_display() const noexcept override;

	virtual const char *platform_surface_extension() const noexcept = 0;

	uint32_t get_memory_type_index(uint32_t type_bits, vk::MemoryPropertyFlags mem_flags);

	vkr::CommandBuffer *get_cmdbuf();

	inline vkr::CommandBuffer &get_flush_cmdbuf()
	{
		if (*frameless_cmd) {
			frameless_cmd.begin({});
			return frameless_cmd;
		}
		if (cur_flush_cmd)
			return *cur_flush_cmd;

		cur_flush_cmd = get_cmdbuf();
		cur_flush_cmd->begin({});
		return *cur_flush_cmd;
	}

	inline vkr::CommandBuffer &get_prerender_cmdbuf()
	{
		if (cur_prerender_cmd)
			return *cur_prerender_cmd;

		cur_prerender_cmd = get_cmdbuf();
		cur_prerender_cmd->begin({});
		return *cur_prerender_cmd;
	}

	inline vk::Result wait_for_fence(vkr::Fence &fence, uint64_t duration = UINT64_MAX)
	{
		std::array<vk::Fence, 1> fences = {*fence};
		vk::Result               res    = device.waitForFences(fences, true, duration);
		device.resetFences(fences);
		return res;
	}

	inline bool   can_update_pipeline(primitive topology);
	inline void   update_pipeline(primitive topology);
	vkr::Pipeline create_pipeline(primitive topology);

	inline void wait_for_idle()
	{
		if (!idle) {
			device.waitIdle();
			idle = true;
		}
	}

	inline void draw(primitive topology, uint32_t start, uint32_t count) noexcept;
	void        update_render_target();

	inline void check_start_rendering()
	{
		if (cur_rt != last_rt || !cur_cmd)
			update_render_target();
	}
};

/* --------------------------------------------------- */

static inline vk::Format get_vk_format(texture_format format) noexcept
{
	/* clang-format off */
	switch (format) {
	case texture_format::r8:
	case texture_format::a8:
	case texture_format::l8:      return vk::Format::eR8Unorm;
	case texture_format::rg:      return vk::Format::eR8G8Unorm;
	case texture_format::rgba:    return vk::Format::eR8G8B8A8Unorm;

	case texture_format::r16f:    return vk::Format::eR16Sfloat;
	case texture_format::rg16f:   return vk::Format::eR16G16Sfloat;
	case texture_format::rgba16f: return vk::Format::eR16G16B16A16Sfloat;

	case texture_format::r32f:    return vk::Format::eR32Sfloat;
	case texture_format::rg32f:   return vk::Format::eR32G32Sfloat;
	case texture_format::rgba32f: return vk::Format::eR32G32B32A32Sfloat;
	}
	/* clang-format on */

	return vk::Format::eUndefined;
}

static inline size_t get_pixel_size(texture_format format) noexcept
{
	/* clang-format off */
	switch (format) {
	case texture_format::r8:
	case texture_format::a8:      return 1;
	case texture_format::l8:      return 1;
	case texture_format::rg:      return 2;
	case texture_format::rgba:    return 4;

	case texture_format::r16f:    return 2;
	case texture_format::rg16f:   return 4;
	case texture_format::rgba16f: return 8;

	case texture_format::r32f:    return 4;
	case texture_format::rg32f:   return 8;
	case texture_format::rgba32f: return 16;
	}
	/* clang-format on */

	return 0;
}

} // namespace graphics
} // namespace crpg
