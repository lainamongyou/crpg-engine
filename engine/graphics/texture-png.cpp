#include "../utility/fserializer.hpp"
#include "../utility/logging.hpp"
#include "graphics.hpp"

#include <png.h>
#include <cstdio>
#include <string>

namespace crpg {
namespace graphics {

class png_obj {
	png_structp ptr_       = nullptr;
	png_infop   info_      = nullptr;
	bool        valid_     = false;
	const char *last_error = nullptr;

	static void handle_png_error(png_structp priv, png_const_charp str) noexcept
	{
		png_obj *png    = (png_obj *)priv;
		png->last_error = str;
	}
	static void handle_png_warn(png_structp, png_const_charp str) noexcept { debug("PNG warning: %s", str); }

public:
	inline png_obj() noexcept
	{
		ptr_ = png_create_read_struct(PNG_LIBPNG_VER_STRING, this, handle_png_error, handle_png_warn);
		if (!ptr_)
			return;
		info_ = png_create_info_struct(ptr_);
		if (!info_) {
			png_destroy_read_struct(&ptr_, nullptr, nullptr);
			ptr_ = nullptr;
			return;
		}
		valid_ = true;
	}

	inline ~png_obj() noexcept
	{
		if (ptr_)
			png_destroy_read_struct(&ptr_, &info_, nullptr);
	}

	inline bool valid() const noexcept { return valid_; }

	inline png_structp ptr() const noexcept { return ptr_; }
	inline png_infop   info() const noexcept { return info_; }

	inline const char *error() const { return last_error; }
};

static void read_png(png_structp png, png_bytep data, png_size_t size) noexcept
{
	ifserializer &file = *reinterpret_cast<ifserializer *>(png_get_io_ptr(png));
	file.serialize(data, size);
}

texture_ref create_texture_from_png(serializer &file) noexcept
try {
	uint8_t header[8];

	file.serialize(header, sizeof(header));
	if (png_sig_cmp(header, 0, 8) != 0)
		throw std::string("Invalid PNG file");

	png_obj png;
	if (!png.valid())
		throw std::string("Failed to initialize libpng");

	std::vector<uint8_t> img;
	size_t               linesize;
	uint32_t             cx;
	uint32_t             cy;
	uint8_t              type;
	uint8_t              bpp;

	png_set_read_fn(png.ptr(), &file, read_png);
	png_set_sig_bytes(png.ptr(), 8);
	png_read_info(png.ptr(), png.info());
	if (png.error())
		throw strprintf("PNG read error: %s", png.error());

	cx   = png_get_image_width(png.ptr(), png.info());
	cy   = png_get_image_height(png.ptr(), png.info());
	type = png_get_color_type(png.ptr(), png.info());
	bpp  = png_get_bit_depth(png.ptr(), png.info());

	if (bpp == 16)
		png_set_strip_16(png.ptr());
	if ((type & PNG_COLOR_MASK_PALETTE) != 0)
		png_set_palette_to_rgb(png.ptr());
	if (type != PNG_COLOR_TYPE_GRAY) {
		if (png_get_valid(png.ptr(), png.info(), PNG_INFO_tRNS))
			png_set_tRNS_to_alpha(png.ptr());
		else if ((type & PNG_COLOR_MASK_ALPHA) == 0)
			png_set_add_alpha(png.ptr(), 0xFFFF, PNG_FILLER_AFTER);
	}

	png_read_update_info(png.ptr(), png.info());
	if (png.error())
		throw strprintf("PNG read error: %s", png.error());

	cx   = png_get_image_width(png.ptr(), png.info());
	cy   = png_get_image_height(png.ptr(), png.info());
	type = png_get_color_type(png.ptr(), png.info());
	bpp  = png_get_bit_depth(png.ptr(), png.info());

	linesize = png_get_rowbytes(png.ptr(), png.info());

	std::vector<uint8_t *> rows;
	rows.resize(cy);
	img.resize(linesize * cy);

	uint8_t *out = img.data();
	for (uint32_t y = 0; y < cy; y++) {
		rows[y] = out;
		out += linesize;
	}

	png_read_image(png.ptr(), rows.data());
	if (png.error())
		throw strprintf("PNG read error: %s", png.error());

	texture_format format;

	if (type == PNG_COLOR_TYPE_RGB_ALPHA) {
		format = texture_format::rgba;
	} else if (type == PNG_COLOR_TYPE_GRAY) {
		format = texture_format::l8;
	} else if (type == PNG_COLOR_TYPE_GRAY_ALPHA) {
		format = texture_format::a8;
	} else {
		throw strprintf("Invalid PNG type: %d", (int)type);
	}

	const uint8_t *data = img.data();
	texture_ref    ref  = texture::create(cx, cy, 1, format, &data);
	return ref;

} catch (const std::string &text) {
	error("%s: %s", __FUNCTION__, text.c_str());
	return texture_ref();
}

} // namespace graphics
} // namespace crpg
