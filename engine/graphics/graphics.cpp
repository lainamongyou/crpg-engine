#include "../utility/logging.hpp"
#include "../resources.hpp"
#include "graphics.hpp"

#include <deque>

namespace crpg {
namespace graphics {

/* ------------------------------------------------------------------------- */

weak_renderer_ref cur_renderer;

renderer::~renderer() noexcept
{
	cur_renderer.reset();
}

renderer_ref renderer::get() noexcept
{
	return cur_renderer.lock();
}

/* --------------------------------------------------- */

shader *cur_vertex_shader   = nullptr;
shader *cur_fragment_shader = nullptr;

shader *shader::current_fs() noexcept
{
	return cur_fragment_shader;
}

shader *shader::current_vs() noexcept
{
	return cur_vertex_shader;
}

/* --------------------------------------------------- */

struct shader_backup {
	weak_shader_ref vs;
	weak_shader_ref ps;
};

std::deque<math::matrix>         proj_matrices{math::matrix::identity()};
std::deque<math::matrix>         view_matrices{math::matrix::identity()};
static std::deque<shader_backup> shader_stack;

void push_projection_matrix() noexcept
{
	math::matrix cur = proj_matrices[0];
	proj_matrices.push_front(cur);
}

void pop_projection_matrix() noexcept
{
	if (proj_matrices.size() > 1)
		proj_matrices.pop_front();
}

math::matrix current_projection_matrix() noexcept
{
	return proj_matrices[0];
}

void set_projection_matrix(const math::matrix &m) noexcept
{
	proj_matrices[0] = m;
}

void push_shaders() noexcept
{
	shader_backup shaders;

	if (cur_vertex_shader)
		shaders.vs = cur_vertex_shader->weak;
	if (cur_fragment_shader)
		shaders.ps = cur_fragment_shader->weak;

	shader_stack.push_back(std::move(shaders));
}

void pop_shaders() noexcept
{
	if (!shader_stack.size())
		return;

	shader_backup shaders = std::move(shader_stack[0]);
	shader_stack.pop_front();

	shader_ref vs = shaders.vs.lock();
	shader_ref ps = shaders.ps.lock();

	if (vs)
		vs->load();
	else
		cur_vertex_shader = nullptr;

	if (ps)
		ps->load();
	else
		cur_fragment_shader = nullptr;
}

texture *cur_render_target = nullptr;

extern void set_render_target(texture *tex) noexcept;

render_target::render_target() noexcept : last(cur_render_target)
{
	set_render_target(nullptr);
}

render_target::render_target(texture_ref &tex) noexcept : last(cur_render_target), ref(tex)
{
	set_render_target(tex.get());
}

render_target::~render_target() noexcept
{
	set_render_target(last);
}

void render_target::set(texture_ref &tex) noexcept
{
	set_render_target(tex.get());
	ref = tex;
}

void render_target::clear() noexcept
{
	set_render_target(nullptr);
	ref.reset();
}

void ortho(float left, float right, float top, float bottom, float znear, float zfar) noexcept
{
	proj_matrices[0].set_ortho(left, right, top, bottom, znear, zfar);
}

void frustum(float left, float right, float top, float bottom, float znear, float zfar) noexcept
{
	proj_matrices[0].set_frustum(left, right, top, bottom, znear, zfar);
}

void perspective(float fov, float aspect, float znear, float zfar) noexcept
{
	float ymax = znear * tanf(math::to_rad(fov) * 0.5f);
	float ymin = -ymax;

	float xmin = ymin * aspect;
	float xmax = ymax * aspect;

	frustum(xmin, xmax, ymin, ymax, znear, zfar);
}

void matrix_push() noexcept
{
	math::matrix cur = view_matrices[0];
	view_matrices.push_front(cur);
}

void matrix_pop() noexcept
{
	if (view_matrices.size() > 1)
		view_matrices.pop_front();
}

void matrix_identity() noexcept
{
	view_matrices[0].set_identity();
}

void matrix_set(const math::matrix &matrix) noexcept
{
	view_matrices[0] = matrix;
}

math::matrix matrix_get() noexcept
{
	return view_matrices[0];
}

void matrix_multiply(const math::matrix &matrix) noexcept
{
	view_matrices[0] *= matrix;
}

void matrix_translate(float x, float y, float z) noexcept
{
	view_matrices[0].translate(x, y, z);
}

void matrix_translate(const math::vec3 &v) noexcept
{
	view_matrices[0].translate(v);
}

void matrix_translate(const math::vec2 &v) noexcept
{
	view_matrices[0].translate(v.x, v.y);
}

void matrix_rotate(float x, float y, float z, float rot) noexcept
{
	view_matrices[0].rotate(x, y, z, rot);
}

void matrix_rotate(const math::quat &quat) noexcept
{
	view_matrices[0].rotate(quat);
}

void matrix_rotate(const math::axisang &aa) noexcept
{
	view_matrices[0].rotate(aa);
}

void matrix_scale(float x, float y, float z) noexcept
{
	view_matrices[0].scale(x, y, z);
}

void matrix_scale(const math::vec3 &v) noexcept
{
	view_matrices[0].scale(v);
}

void matrix_scale(const math::vec2 &v) noexcept
{
	view_matrices[0].scale(v.x, v.y, 1.0f);
}

void draw() noexcept
{
	vertex_buffer_ref sprite_vb = resources::sprite_vb();
	sprite_vb->load();
	draw(primitive::tristrip);
}

void draw(float x, float y, float cx, float cy) noexcept
try {
	if (!cur_vertex_shader)
		throw "No vertex shader set";

	shader::param       *param = cur_vertex_shader->get_world();
	std::vector<uint8_t> old   = param->vec();

	math::matrix m;
	m.set_identity();
	m.scale(cx, cy, 1.0f);
	m.translate(x, y, 0.0f);

	cur_vertex_shader->set_world(m);
	draw();

	if (old.size())
		cur_vertex_shader->set_world(*reinterpret_cast<math::matrix *>(old.data()));

} catch (const char *text) {
	warn("%s: %s", __FUNCTION__, text);
}

void draw(texture_ref &tex, float x, float y, float cx, float cy) noexcept
try {
	if (!cur_fragment_shader)
		throw "No fragment shader set";

	if (cx == 0.0f)
		cx = (float)tex->cx();
	if (cy == 0.0f)
		cy = (float)tex->cy();

	cur_fragment_shader->set("image", tex);
	draw(x, y, cx, cy);

} catch (const char *text) {
	warn("%s: %s", __FUNCTION__, text);
}

/* ------------------------------------------------------------------------- */
} // namespace graphics
} // namespace crpg
