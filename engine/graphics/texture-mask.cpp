#include "../utility/fserializer.hpp"
#include "../utility/logging.hpp"
#include "../mask.hpp"
#include "graphics.hpp"

#include <string>

/* This is usually only ever used for debugging purposes */

namespace crpg {
namespace graphics {

texture_ref create_texture_from_mask(serializer &s) noexcept
{
	uint32_t cx;
	uint32_t cy;

	std::vector<mask::block> blocks;

	s >> cx >> cy;

	uint32_t block_cx = cx / mask::block::size;
	uint32_t block_cy = cy / mask::block::size;

	size_t count = block_cx * block_cy;
	blocks.resize(count);
	s.serialize(&blocks[0], sizeof(mask::block) * count);

	std::vector<uint32_t> image;
	image.resize(cx * cy);

	for (uint32_t block_y = 0; block_y < block_cy; block_y++) {
		uint32_t base_y = block_y * mask::block::size;

		for (uint32_t block_x = 0; block_x < block_cx; block_x++) {
			uint32_t     base_x = block_x * mask::block::size;
			mask::block &block  = blocks[block_y * block_cx + block_x];

			for (uint16_t sub_y = 0; sub_y < mask::block::size; sub_y++) {
				uint32_t y_offset = (base_y + sub_y) * cx;
				uint16_t row      = block.rows[sub_y];

				for (uint16_t sub_x = 0; sub_x < mask::block::size; sub_x++) {
					uint32_t x          = base_x + sub_x;
					image[y_offset + x] = ((row >> sub_x) & 1) ? 0xFFFFFFFF : 0x00000000;
				}
			}
		}
	}

	const uint8_t *data = (const uint8_t *)image.data();
	texture_ref    ref  = texture::create(cx, cy, 1, texture_format::rgba, &data);
	return ref;
}

} // namespace graphics
} // namespace crpg
