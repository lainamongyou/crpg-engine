#pragma once

#include <unordered_map>
#include <functional>
#include <cstdint>
#include <string>
#include <memory>
#include <vector>

#include "../math/math-defs.hpp"
#include "../math/matrix.hpp"
#include "../math/vec2.hpp"
#include "../math/vec3.hpp"
#include "../math/vec4.hpp"

namespace crpg {
namespace graphics {

/* --------------------------------------------------- */

class renderer;
class texture;
class shader;
class vertex_buffer;
class display;

using renderer_ref      = std::shared_ptr<renderer>;
using texture_ref       = std::shared_ptr<texture>;
using shader_ref        = std::shared_ptr<shader>;
using vertex_buffer_ref = std::shared_ptr<vertex_buffer>;
using display_ref       = std::shared_ptr<display>;

using weak_renderer_ref      = std::weak_ptr<renderer>;
using weak_texture_ref       = std::weak_ptr<texture>;
using weak_shader_ref        = std::weak_ptr<shader>;
using weak_vertex_buffer_ref = std::weak_ptr<vertex_buffer>;
using weak_display_ref       = std::weak_ptr<display>;

/* --------------------------------------------------- */

constexpr int max_textures{8};

/* ------------------------------------------------------------------------- */

enum class blend : int {
	zero,
	one,
	src_color,
	inv_src_color,
	dst_color,
	inv_dst_color,
	src_alpha,
	inv_src_alpha,
	dst_alpha,
	inv_dst_alpha,
	const_color,
	inv_const_color,
	const_alpha,
	inv_const_alpha,
};

enum class equation : int {
	add,
	subtract,
	reverse_subtract,
	min,
	max,
};

/* ------------------------------------------------------------------------- */

class EXPORT renderer {
public:
	enum class error { success, generic_fail, already_exists, unsupported };

	virtual ~renderer() noexcept;

	static renderer_ref create(error *error = nullptr, bool multi_swap = false) noexcept;
	static renderer_ref get() noexcept;

	virtual void     load_display(display *dis) noexcept = 0;
	virtual display *current_display() const noexcept    = 0;
};

/* --------------------------------------------------- */

class EXPORT render_obj {
	mutable renderer_ref r;

protected:
	inline renderer *parent() noexcept { return r.get(); }
	inline renderer *parent() const noexcept { return r.get(); }

public:
	inline render_obj() noexcept : r(renderer::get()) {}
	virtual ~render_obj() = default;
};

/* --------------------------------------------------- */

enum class texture_format : int {
	r8,
	a8,
	l8,
	rg,
	rgba,

	/* 16bit float formats */
	r16f = 100,
	rg16f,
	rgba16f,

	/* 32bit float formats */
	r32f = 200,
	rg32f,
	rgba32f,
};

enum class texture_filter {
	default_,
	point,
	linear,
	nearest_mipmap_nearest,
	linear_mipmap_nearest,
	nearest_mipmap_linear,
	linear_mipmap_linear,
};

enum class address_mode {
	default_,
	clamp,
	wrap,
	mirror,
};

class EXPORT texture : public render_obj {
	friend class shader;

protected:
	virtual void load(int unit) noexcept = 0;
	virtual void unload() noexcept       = 0;

	int loaded_unit = -1;
	int cx_         = 0;
	int cy_         = 0;
	int levels_     = 0;

public:
	enum class type {
		static_,
		renderable,
	};

	static texture_ref create(int             cx,
	                          int             cy,
	                          int             levels,
	                          texture_format  format,
	                          const uint8_t **data  = nullptr,
	                          texture::type   type_ = texture::type::static_) noexcept;

	static texture_ref create_from_file(const char *path) noexcept;
	static texture_ref create_from_memory(const char *ext, const std::string &data) noexcept;

	static inline texture_ref create_from_file(const std::string &path) noexcept
	{
		return create_from_file(path.c_str());
	}

	virtual void set_filter(texture_filter min_filter, texture_filter mag_filter) noexcept = 0;
	virtual void set_wrap(address_mode mode_s, address_mode mode_t) noexcept               = 0;

	virtual void swap(texture_ref &tex) noexcept = 0;

	inline int cx() const noexcept { return cx_; }
	inline int cy() const noexcept { return cy_; }
	inline int levels() const noexcept { return levels_; }

	virtual void generate_mipmaps() noexcept = 0;
};

/* --------------------------------------------------- */

EXPORT void push_shaders() noexcept;
EXPORT void pop_shaders() noexcept;

enum class shader_type {
	vertex,
	pixel,
	fragment = pixel,
};

class EXPORT shader : public render_obj {
	uint32_t id_;

public:
	class EXPORT param {
		friend struct vk_program_base;
		friend class shader;

	protected:
		enum class type { t_invalid, t_bool, t_float, t_int, t_matrix, t_vec2, t_vec4, t_texture };

		std::vector<uint8_t> data;
		weak_texture_ref     tex;
		type                 type_   = type::t_invalid;
		bool                 changed = false;
		bool                 val_set = false;

		inline void set_base(const void *ptr, size_t size, type type__) noexcept;

	public:
		void set(bool val) noexcept;
		void set(float val) noexcept;
		void set(int val) noexcept;
		void set(const math::matrix &val) noexcept;
		void set(const math::vec2 &val) noexcept;
		void set(const math::vec3 &val) noexcept;
		void set(const math::vec4 &val) noexcept;

		inline void set(texture_ref &t) noexcept
		{
			if (!changed) {
				texture_ref ref = tex.lock();
				changed         = (!!data.size() || ref.get() != t.get());
			}
			data.clear();
			tex     = t;
			type_   = type::t_texture;
			val_set = true;
		}

		inline const std::vector<uint8_t> &vec() const noexcept { return data; }

		inline bool is_set() const noexcept { return val_set; }
	};

	shader() noexcept;

	inline const char *name() const noexcept { return name_.c_str(); }
	inline uint32_t    id() const noexcept { return id_; }

	param *get_param(const char *name) noexcept;

	inline void set(const char *name, bool val) noexcept
	{
		param *p = get_param(name);
		if (p)
			p->set(val);
	}
	inline void set(const char *name, float val) noexcept
	{
		param *p = get_param(name);
		if (p)
			p->set(val);
	}
	inline void set(const char *name, int val) noexcept
	{
		param *p = get_param(name);
		if (p)
			p->set(val);
	}
	inline void set(const char *name, const math::matrix &val) noexcept
	{
		param *p = get_param(name);
		if (p)
			p->set(val);
	}
	inline void set(const char *name, const math::vec2 &val) noexcept
	{
		param *p = get_param(name);
		if (p)
			p->set(val);
	}
	inline void set(const char *name, const math::vec3 &val) noexcept
	{
		param *p = get_param(name);
		if (p)
			p->set(val);
	}
	inline void set(const char *name, const math::vec4 &val) noexcept
	{
		param *p = get_param(name);
		if (p)
			p->set(val);
	}
	inline void set(const char *name, texture_ref &t) noexcept
	{
		param *p = get_param(name);
		if (p)
			p->set(t);
	}

	param *get_viewproj() const noexcept;
	param *get_world() const noexcept;

	inline void set_viewproj(const math::matrix &matrix) noexcept { get_viewproj()->set(matrix); }
	inline void set_world(const math::matrix &matrix) noexcept { get_world()->set(matrix); }

	static shader_ref create(shader_type type, const std::string &spirv) noexcept;
	static shader_ref create_from_file(std::string filename) noexcept;

	virtual void load() noexcept = 0;

	static shader        *current_fs() noexcept;
	static inline shader *current_ps() noexcept { return current_fs(); }
	static shader        *current_vs() noexcept;

protected:
	std::string name_;

private:
	weak_shader_ref weak;
	friend void     crpg::graphics::push_shaders() noexcept;
};

/* --------------------------------------------------- */

class EXPORT vertex_buffer : public render_obj {
	virtual void unload() = 0;

public:
	struct data {
		std::vector<math::vec3> points;
		std::vector<math::vec2> coords;
		std::vector<uint32_t>   indices;

		inline data() noexcept {}

		inline bool valid() const noexcept
		{
			return points.size() && (!coords.size() || coords.size() == points.size());
		}

		inline void clear() noexcept
		{
			points.clear();
			coords.clear();
			indices.clear();
		}
	};

	inline size_t vert_count() const noexcept { return vert_count_; }
	inline size_t index_count() const noexcept { return index_count_; }

	static vertex_buffer_ref create(const data &data) noexcept;

	virtual void load() noexcept = 0;

	virtual void swap(vertex_buffer_ref &vb) noexcept = 0;

protected:
	size_t vert_count_  = 0;
	size_t index_count_ = 0;
};

/* --------------------------------------------------- */

class EXPORT display : public render_obj {
public:
	struct info {
#if defined(_WIN32)
		void *hwnd;
#elif defined(__APPLE__)
		__unsafe_unretained id view;
#elif defined(__linux__) || defined(__FreeBSD__)
		uint32_t x11_id;
		void    *x11_display;
		void    *wayland_surf;
		void    *wayland_display;
#endif
		texture_format format;
	};

	static display_ref create(const info &info, uint32_t cx, uint32_t cy) noexcept;

	inline void  load() noexcept { renderer::get()->load_display(this); }
	virtual void swap() noexcept                           = 0;
	virtual void resize(uint32_t cx, uint32_t cy) noexcept = 0;

	inline uint32_t cx() const noexcept { return cx_; }
	inline uint32_t cy() const noexcept { return cy_; }

protected:
	inline display(const info &info__, uint32_t cx, uint32_t cy) noexcept : info_(info__), cx_(cx), cy_(cy) {}

	info     info_;
	uint32_t cx_;
	uint32_t cy_;
};

/* --------------------------------------------------- */

EXPORT void set_default_texture_filter(texture_filter min_filter, texture_filter mag_filter) noexcept;
EXPORT void set_default_texture_wrap(address_mode mode_s, address_mode mode_t) noexcept;

EXPORT void push_projection_matrix() noexcept;
EXPORT void pop_projection_matrix() noexcept;
EXPORT void set_projection_matrix(const math::matrix &m) noexcept;
EXPORT math::matrix current_projection_matrix() noexcept;
static inline void  push_projection_matrix(const math::matrix &m) noexcept
{
	push_projection_matrix();
	set_projection_matrix(m);
}

EXPORT void        push_viewport() noexcept;
EXPORT void        pop_viewport() noexcept;
EXPORT void        set_viewport(int x, int y, int cx, int cy) noexcept;
static inline void push_viewport(int x, int y, int cx, int cy) noexcept
{
	push_viewport();
	set_viewport(x, y, cx, cy);
}

EXPORT void push_blend() noexcept;
EXPORT void pop_blend() noexcept;
EXPORT void blend_func(blend src, blend dst, blend src_alpha = blend::one, blend dst_alpha = blend::zero) noexcept;
EXPORT void blend_equation(equation rgb, equation alpha = equation::add) noexcept;
static inline void push_blend(blend src,
                              blend dst,
                              blend src_alpha = blend::one,
                              blend dst_alpha = blend::zero) noexcept
{
	push_blend();
	blend_func(src, dst, src_alpha, dst_alpha);
}

EXPORT void clear(float r, float g, float b, float a) noexcept;

EXPORT void ortho(float left,
                  float right,
                  float top,
                  float bottom,
                  float znear = -100.0f,
                  float zfar  = 100.0f) noexcept;
EXPORT void frustum(float left, float right, float top, float bottom, float znear, float zfar) noexcept;
EXPORT void perspective(float fov, float aspect, float znear, float zfar) noexcept;

/* Applies a very minor offset to prevent nearest-neighbor precision issues when sampling */
inline void ortho_pixel(float left,
                        float right,
                        float top,
                        float bottom,
                        float znear = -100.0f,
                        float zfar  = 100.0f) noexcept
{
	ortho(left + math::epsilon, right + math::epsilon, top + math::epsilon, bottom + math::epsilon, znear, zfar);
}

EXPORT void matrix_push() noexcept;
EXPORT void matrix_pop() noexcept;
EXPORT void matrix_identity() noexcept;
EXPORT void matrix_set(const math::matrix &matrix) noexcept;
EXPORT void matrix_multiply(const math::matrix &matrix) noexcept;
EXPORT void matrix_translate(float x, float y, float z = 0.0f) noexcept;
EXPORT void matrix_translate(const math::vec3 &v) noexcept;
EXPORT void matrix_translate(const math::vec2 &v) noexcept;
EXPORT void matrix_rotate(float x, float y, float z, float rot) noexcept;
EXPORT void matrix_rotate(const math::quat &quat) noexcept;
EXPORT void matrix_rotate(const math::axisang &aa) noexcept;
EXPORT void matrix_scale(float x, float y, float z = 1.0f) noexcept;
EXPORT void matrix_scale(const math::vec3 &v) noexcept;
EXPORT void matrix_scale(const math::vec2 &v) noexcept;
EXPORT math::matrix matrix_get() noexcept;

enum class primitive {
	points,
	lines,
	linestrip,
	triangles,
	tristrip,
};

EXPORT void draw(primitive topology, int start = 0, int count = 0) noexcept;
EXPORT void draw() noexcept;
EXPORT void draw(float x, float y, float cx, float cy) noexcept;
EXPORT void draw(texture_ref &tex, float x = 0.0f, float y = 0.0f, float cx = 0.0f, float cy = 0.0f) noexcept;

/* ------------------------------------------------------------------------- */

class matrix_guard {
public:
	inline matrix_guard() noexcept { matrix_push(); }
	inline ~matrix_guard() noexcept { matrix_pop(); }

	inline void reset() noexcept
	{
		matrix_pop();
		matrix_push();
	}
};

class projection_matrix_guard {
public:
	inline projection_matrix_guard() noexcept { push_projection_matrix(); }
	inline projection_matrix_guard(const math::matrix &m) noexcept { push_projection_matrix(m); }
	inline ~projection_matrix_guard() noexcept { pop_projection_matrix(); }
};

class ortho_guard : public projection_matrix_guard {
public:
	inline ortho_guard(float left,
	                   float right,
	                   float top,
	                   float bottom,
	                   float znear = -100.0f,
	                   float zfar  = 100.0f) noexcept
	{
		ortho(left, right, top, bottom, znear, zfar);
	}
};

class viewport_guard {
public:
	inline viewport_guard() noexcept { push_viewport(); }
	inline viewport_guard(int x, int y, int cx, int cy) noexcept { push_viewport(x, y, cx, cy); }
	inline ~viewport_guard() noexcept { pop_viewport(); }
};

class blend_guard {
public:
	inline blend_guard() noexcept { push_blend(); }
	inline blend_guard(blend src, blend dst, blend src_alpha = blend::one, blend dst_alpha = blend::zero) noexcept
	{
		push_blend(src, dst, src_alpha, dst_alpha);
	}
	inline ~blend_guard() noexcept { pop_blend(); }
};

class shader_guard {
public:
	inline shader_guard() noexcept { push_shaders(); }
	inline ~shader_guard() noexcept { pop_shaders(); }
};

class EXPORT render_target {
	texture    *last;
	texture_ref ref;

public:
	render_target() noexcept;
	render_target(texture_ref &tex) noexcept;
	~render_target() noexcept;

	void set(texture_ref &ref) noexcept;
	void clear() noexcept;
};

} // namespace graphics
} // namespace crpg
