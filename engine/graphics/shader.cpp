#include "../utility/file-helpers.hpp"
#include "../utility/logging.hpp"
#include "graphics.hpp"

#include <cstring>

using namespace std;

namespace crpg {
namespace graphics {

extern shader *cur_vertex_shader;
extern shader *cur_fragment_shader;

static uint32_t shader_counter = 1;

/* ------------------------------------------------------------------------- */

class gl_shader : public shader {
public:
};

shader::shader() noexcept : id_(shader_counter++) {}

shader_ref shader::create_from_file(std::string filename) noexcept
try {
	shader_type type;

	if (filename.ends_with(".vert")) {
		filename += ".spv";
		type = shader_type::vertex;

	} else if (filename.ends_with(".vert.spv")) {
		type = shader_type::vertex;

	} else if (filename.ends_with(".frag")) {
		filename += ".spv";
		type = shader_type::fragment;

	} else if (filename.ends_with(".frag.spv")) {
		type = shader_type::fragment;

	} else {
		throw strprintf("Invalid file extension: %s", filename.c_str());
	}

	string shader = load_memory_from_file(filename);
	return shader::create(type, shader);

} catch (const std::string &text) {
	error("%s: %s", __func__, text.c_str());
	return shader_ref();
}

/* --------------------------------------------------- */

inline void shader::param::set_base(const void *ptr, size_t size, type type__) noexcept
{
	if (!changed)
		changed = (size != data.size() || (size && memcmp(ptr, data.data(), size) != 0)) || type_ != type__;
	if (changed) {
		data.resize(size);
		if (size)
			memcpy(&data[0], ptr, size);
		type_ = type__;
	}

	val_set = true;
}

void shader::param::set(bool val) noexcept
{
	set_base((const void *)&val, sizeof(val), type::t_bool);
}

void shader::param::set(float val) noexcept
{
	set_base((const void *)&val, sizeof(val), type::t_float);
}

void shader::param::set(int val) noexcept
{
	set_base((const void *)&val, sizeof(val), type::t_int);
}

void shader::param::set(const math::matrix &val_in) noexcept
{
	math::matrix val = val_in.transposed();
	set_base((const void *)&val, sizeof(val), type::t_matrix);
}

void shader::param::set(const math::vec2 &val) noexcept
{
	set_base((const void *)&val, sizeof(val), type::t_vec2);
}

void shader::param::set(const math::vec3 &val) noexcept
{
	set_base((const void *)&val, sizeof(val), type::t_vec4);
}

void shader::param::set(const math::vec4 &val) noexcept
{
	set_base((const void *)&val, sizeof(val), type::t_vec4);
}

/* ------------------------------------------------------------------------- */
} // namespace graphics
} // namespace crpg
