#pragma once

/* clang-format off */

#if defined(_MSC_VER) && !defined(STATIC_ONLY)
# ifdef EXPORT_DLL
#  define EXPORT __declspec(dllexport)
# else
#  define EXPORT __declspec(dllimport)
# endif
# define EXPORT_GAME extern "C" __declspec(dllexport)
#else
# define EXPORT
# define EXPORT_GAME
#endif

/* clang-format on */
