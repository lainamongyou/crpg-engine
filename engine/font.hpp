#pragma once

#include "math/box.hpp"
#include "export.h"
#include <cstdint>
#include <memory>
#include <string>

namespace crpg {

namespace graphics {
class texture;
using texture_ref = std::shared_ptr<texture>;
} // namespace graphics

class font;

using font_ref      = std::shared_ptr<font>;
using weak_font_ref = std::weak_ptr<font>;

class EXPORT font {

	int size_;
	int outline_size_;

protected:
	float height_;
	float ascender_;
	float descender_;

	inline font(int size, int outline_size) noexcept : size_(size), outline_size_(outline_size) {}

public:
	struct glyph {
		graphics::texture_ref texture;
		float                 bearing_x;
		float                 bearing_y;
		float                 advance_x;
		int                   ch;

		graphics::texture_ref outline;
		float                 outline_bearing_x;
		float                 outline_bearing_y;
	};

	virtual ~font() noexcept;

	static font_ref create_from_file(const char *file, int size, int outline_size = 0) noexcept;

	virtual void draw(const char *text,
	                  float       x,
	                  float       y,
	                  uint32_t    color         = 0xFFFFFFFF,
	                  uint32_t    outline_color = 0xFF000000) noexcept = 0;

	inline void draw(const std::string &text,
	                 float              x,
	                 float              y,
	                 uint32_t           color         = 0xFFFFFFFF,
	                 uint32_t           outline_color = 0xFF000000) noexcept
	{
		draw(text.c_str(), x, y, color, outline_color);
	}

	virtual void draw_char_init() noexcept = 0;

	virtual void draw_char(int      ch,
	                       int      prev,
	                       float    orig_x,
	                       float   &x,
	                       float   &y,
	                       uint32_t color,
	                       uint32_t outline_color) noexcept = 0;

	static bool    next_utf32(const char *&text, int &ch, int &prev) noexcept;
	virtual glyph *advance_char(int ch, int prev, float origin_x, float &x, float &y) noexcept = 0;

	virtual math::box   text_metrics(const char *text) noexcept        = 0;
	virtual std::string word_wrap(const char *text, float cx) noexcept = 0;

	inline int   size() const noexcept { return size_; }
	inline bool  outline() const noexcept { return !!outline_size_; }
	inline int   outline_size() const noexcept { return outline_size_; }
	inline float height() const noexcept { return height_; }
	inline float ascender() const noexcept { return ascender_; }
	inline float descender() const noexcept { return descender_; }

	static bool ft2_init() noexcept;
	static void ft2_free() noexcept;
};

} // namespace crpg
