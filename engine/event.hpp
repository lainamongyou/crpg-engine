#pragma once

#include "export.h"

#include <functional>

namespace crpg {

class EXPORT event {
	friend class event_manager;

public:
	enum class type {
		control,
		concurrent,
	};

	using pos_t     = int32_t;
	using action_cb = std::function<void()>;

	inline event(crpg::event::type type) noexcept : type_(type) {}
	virtual ~event() noexcept;

	inline void add() noexcept;
	void        remove() noexcept;

protected:
	virtual bool can_activate() noexcept = 0;
	inline bool  active() noexcept { return event_is_active; };
	inline void  deactivate() noexcept { event_is_active = false; }
	inline void  step() noexcept { on_event_step(++pos_); }
	inline void  reset() noexcept
	{
		deactivate();
		pos_ = -1;
	}

	void wait(int ms) noexcept;

	virtual void on_event_tick(float seconds) noexcept;
	virtual void on_event_step(pos_t pos) noexcept;
	void         set_event_pos(pos_t pos) noexcept;
	inline pos_t pos() const noexcept { return pos_; }

	inline void set_actions(std::vector<action_cb> &&new_actions) noexcept { actions = std::move(new_actions); }

	inline action_cb make_step() noexcept
	{
		return [this]() { step(); };
	}

	inline void set_next_step_count(size_t count) noexcept { next_step_count = count; }

private:
	event                **pprev_next;
	event                 *next;
	type                   type_;
	int64_t                wait_start      = 0;
	int64_t                wait_duration   = 0;
	pos_t                  pos_            = -1;
	size_t                 next_step_count = 0;
	size_t                 step_count      = 0;
	bool                   event_is_active = false;
	std::vector<action_cb> actions;

	inline void check_wait() noexcept;
};

class control_event : public event {
public:
	inline control_event() noexcept;
};

class concurrent_event : public event {
public:
	inline concurrent_event() noexcept;
};

/* ========================================================================= */

class EXPORT event_manager {
	friend class event;

	event *first            = nullptr;
	event *first_concurrent = nullptr;
	event *current_event    = nullptr;

	static event_manager *current;

	void add(crpg::event::type type, crpg::event *event) noexcept;

public:
	event_manager() noexcept;
	virtual ~event_manager() noexcept;

	void tick(float seconds) noexcept;

	inline bool active() const noexcept { return !!current_event; }

	static inline event_manager *get() noexcept { return current; }

	virtual void on_event_start() noexcept;
	virtual void on_event_stop() noexcept;
	virtual void on_event_activate(crpg::event *event) noexcept;
	virtual void on_event_deactivate(crpg::event *event) noexcept;
};

inline void event::add() noexcept
{
	event_manager::get()->add(type_, this);
}

inline control_event::control_event() noexcept : event(crpg::event::type::control)
{
	add();
}

inline concurrent_event::concurrent_event() noexcept : event(crpg::event::type::concurrent)
{
	add();
}

} // namespace crpg
