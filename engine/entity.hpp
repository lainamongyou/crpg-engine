#pragma once

#include <memory>
#include <string>

#include "math/matrix.hpp"
#include "math/box.hpp"
#include "math/vec3.hpp"
#include "types.hpp"
#include "export.h"

namespace crpg {

class serializer;
struct segment;

/* ------------------------------------------------------------------------- */

/* NOTE: derived classes must not contain too much extra data! */

class EXPORT entity {
	friend struct collision_layer;
	friend class map_internal;
	friend class map;

	std::string     name_;
	type::entity_id id_;

	/* local data */
	math::vec2 pos_ = {};
	float      rot_ = 0.0f;
	math::box  box_ = math::box(math::vec2(0.0f, 0.0f), math::vec2(1.0f, 1.0f));

	/* world data */
	type::layer_id layer_id_        = 0;
	math::vec2     world_pos_       = {};
	math::box      transformed_box_ = {};
	math::matrix   matrix_          = math::matrix::identity();

	/* relational data */
	entity  *parent_            = nullptr;
	entity  *first_child_       = nullptr;
	entity  *next_sibling_      = nullptr;
	entity **pprev_next_sibling = nullptr;

	std::vector<type::entity_id> links_;

	bool destroy_later_ = false;

	static entity *alloc(size_t size) noexcept;
	void           free() noexcept;

	static void free_entities() noexcept;

	inline void detach_internal() noexcept;

	void set_id(type::entity_id id) noexcept;

protected:
	virtual void on_update() {}

public:
	entity() noexcept;
	virtual ~entity() noexcept;

	void destroy_later() noexcept;

	virtual void init() noexcept;

	virtual void tick(float seconds) noexcept;
	virtual void render() noexcept;

	virtual bool savable() const noexcept;
	virtual bool renderable() const noexcept;

	virtual void on_frob() noexcept;

	virtual void serialize(serializer &s) noexcept;

	static void tick_all(float seconds) noexcept;

	void           set_name(const std::string &name) noexcept;
	static entity *find_by_id(type::entity_id id) noexcept;
	static entity *find_static_by_id(type::entity_id id) noexcept;
	static entity *find_by_name(const std::string &name) noexcept;
	static void    reset_id_counter() noexcept;

	inline const std::string &name() const noexcept { return name_; }
	inline type::entity_id    id() const noexcept { return id_; }
	inline const math::vec2  &pos() const noexcept { return pos_; }
	inline float              rot() const noexcept { return rot_; }

	inline const math::vec2   &world_pos() const noexcept { return world_pos_; }
	inline const math::box    &transformed_box() const noexcept { return transformed_box_; }
	inline const math::matrix &matrix() const noexcept { return matrix_; }

	inline void set_pos(const math::vec2 &pos) noexcept { pos_ = pos; }
	inline void set_rot(float rot) noexcept { rot_ = rot; }
	inline void set_box(const math::box &box) noexcept { box_ = box; }

	void update() noexcept;

	inline type::layer_id layer_id() const noexcept { return layer_id_; }
	void                  set_layer_id(type::layer_id id) noexcept;

	inline entity *parent() const noexcept { return parent_; }
	inline entity *first_child() const noexcept { return first_child_; }
	inline entity *next_sibling() const noexcept { return next_sibling_; }
	void           attach(entity *new_parent) noexcept;
	void           detach() noexcept;

	const std::vector<entity *> links() const;

	static void init_preallocation(size_t max_entities) noexcept;

	inline void *operator new(size_t size) noexcept { return alloc(size); }
	inline void  operator delete(void *ptr) noexcept { reinterpret_cast<entity *>(ptr)->free(); }
};

/* ------------------------------------------------------------------------- */
} // namespace crpg
