#include "class.hpp"
#include "utility/logging.hpp"

#include <map>
#include <string>
#include <unordered_map>

using namespace std;

namespace crpg {

/* ------------------------------------------------------------------------- */

static std::unordered_map<std::string, class_info *> classes;

class_info::class_info(const char *path, class_info::create_cb create_) noexcept : construct(create_)
{
	std::string full_name = std::string("object:") + path;
	classes[full_name]    = this;
}

entity *factory_create(const std::string &asset_path) noexcept
try {
	auto it = classes.find(asset_path);
	if (it == classes.end())
		throw strprintf("Could not find class '%s'", asset_path.c_str());

	class_info *cur_class = it->second;
	return cur_class->create();

} catch (const string &text) {
	warn("%s: %s", __FUNCTION__, text.c_str());
	return nullptr;
}

/* ------------------------------------------------------------------------- */
} // namespace crpg
