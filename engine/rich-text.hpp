#pragma once

#include <functional>
#include <deque>

#include "font.hpp"

namespace crpg {

class EXPORT rich_text {
public:
	using char_cb = std::function<bool(rich_text &)>;

	rich_text(const char_cb &cb,
	          const char    *def_face_resource,
	          int            def_size,
	          uint32_t       def_rgb          = 0xFFFFFF,
	          uint8_t        def_alpha        = 0xFF,
	          bool           def_bold         = false,
	          bool           def_italic       = false,
	          int            def_outline_size = 0,
	          uint32_t       def_outline_rgb  = 0x000000) noexcept;

	inline void draw(const char *text, float x, float y, int count = -1) noexcept
	{
		draw(text, x, y, count, false);
	}
	inline void draw_from_top(const char *text, float x, float y, int count = -1) noexcept
	{
		draw(text, x, y, count, true);
	}
	inline void draw(const std::string &text, float x, float y, int count = -1) noexcept
	{
		draw(text.c_str(), x, y, count, false);
	}
	inline void draw_from_top(const std::string &text, float x, float y, int count = -1) noexcept
	{
		draw(text.c_str(), x, y, count, true);
	}

	int        get_total_drawn_characters(const char *text) noexcept;
	inline int get_total_drawn_characters(const std::string &text) noexcept
	{
		return get_total_drawn_characters(text.c_str());
	}

	std::string        word_wrap(const char *text, float wrap_width) noexcept;
	inline std::string word_wrap(const std::string &text, float wrap_width) noexcept
	{
		return word_wrap(text.c_str(), wrap_width);
	}

	inline void set_color_override(bool enable, uint32_t argb = 0) noexcept
	{
		color_override_enabled = enable;
		color_override         = argb;
	}

	struct special_command {
		int         pos;
		std::string command;
		std::string params;
	};

	std::deque<special_command>        &&get_special_commands(const char *text) noexcept;
	inline std::deque<special_command> &&get_special_commands(const std::string &text)
	{
		return std::move(get_special_commands(text.c_str()));
	}

	inline bool valid() const noexcept { return valid_; }

	/* -------------------------------------------------------------------- *
	 * the following functions are meant to be called by the callback       */

	inline int size() const noexcept { return cur.size; }
	inline int outline_size() const noexcept { return cur.outline_size; }

	void set_face(const char *face_resource) noexcept;
	void set_size(int size) noexcept;
	void set_color(uint32_t rgb) noexcept;
	void set_alpha(uint8_t alpha) noexcept;
	void set_outline_size(int outline_size) noexcept;
	void set_outline_color(uint32_t rgb) noexcept;
	void toggle_bold() noexcept;
	void toggle_italic() noexcept;

	inline void set_face(const std::string &face_resource) noexcept { set_face(face_resource.c_str()); }

	void reset() noexcept;
	void reset_face() noexcept;
	void reset_size() noexcept;
	void reset_alpha() noexcept;
	void reset_color() noexcept;
	void reset_style() noexcept;
	void reset_outline_size() noexcept;
	void reset_outline_color() noexcept;

	void        add_special_command(const std::string &command, const std::string &params = std::string()) noexcept;
	inline bool building_special_commands() const noexcept { return building_special_commands_; }

	inline const char *text() const noexcept { return next_text; }
	inline int         ch() const noexcept { return cur_ch; }
	int                next_ch() noexcept;
	int                peek_ch() noexcept;

	/* -------------------------------------------------------------------- */

private:
	char_cb cb;

	struct font_state {
		font_ref    font;
		std::string face;
		int         size;
		bool        bold;
		bool        italic;
		uint32_t    rgb;
		uint8_t     alpha;
		int         outline_size;
		uint32_t    outline_rgb;

		inline font_state(const std::string &face,
		                  int                size,
		                  bool               bold,
		                  bool               italic,
		                  uint32_t           rgb,
		                  uint8_t            alpha,
		                  int                outline_size,
		                  uint32_t           outline_rgb) noexcept
		        : face(face),
		          size(size),
		          bold(bold),
		          italic(italic),
		          rgb(rgb),
		          alpha(alpha),
		          outline_size(outline_size),
		          outline_rgb(outline_rgb)
		{
		}
	};

	font_state cur;
	font_state def;

	bool     color_override_enabled = false;
	uint32_t color_override         = 0;

	bool font_changed = false;
	bool valid_       = false;

	int         cur_ch    = 0;
	int         prev      = 0;
	const char *next_text = nullptr;
	int         cur_pos   = 0;

	bool                        building_special_commands_ = false;
	std::deque<special_command> special_commands;

	inline font_ref get_font() noexcept;

	void draw(const char *text, float x, float y, int count, bool with_ascender) noexcept;

	float get_line_height() noexcept;
	float get_line_ascender() noexcept;
};

} // namespace crpg
