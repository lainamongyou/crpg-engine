#include "utility/fserializer.hpp"
#include "utility/logging.hpp"
#include "math/math-extra.hpp"
#include "utility/dict.hpp"
#include "pixel-math.hpp"
#include "collidable.hpp"
#include "map-tools.hpp"
#include "resources.hpp"
#include "tileset.hpp"
#include "sprite.hpp"
#include "assets.hpp"
#include "class.hpp"
#include "map.hpp"

#include <unordered_map>
#include <functional>
#include <vector>
#include <memory>

using namespace std;

namespace crpg {

/* ======================================================================== */
/* actual map class                                                         */

struct collision_test_info {
	pixel_vec pos;
	bool      result;
};

class map_internal : public map {
public:
	std::vector<tileset_ref>  tilesets;
	std::vector<sprite_state> sprites;

	std::unordered_map<type::asset_id, type::index> sprite_asset_to_idx;

	map_layer                                                     *root;
	std::unordered_map<std::string, type::layer_id>                layer_id_by_name;
	std::unordered_map<type::layer_id, std::unique_ptr<map_layer>> layers;

	graphics::shader_ref vs_unlit;
	graphics::shader_ref ps_unlit;
	graphics::shader_ref vs_unlit_color;
	graphics::shader_ref ps_unlit_color;

	map_tools::seg_pos tl;
	map_tools::seg_pos br;
	math::box          view;

	collision_layer                 *last_collision_layer = nullptr;
	std::vector<collidable *>        collided_entities;
	std::vector<collidable *>        prev_triggers;
	std::vector<collidable *>        next_triggers;
	std::vector<collision_test_info> saved_collision_tests;

	std::vector<type::sprite_id> cur_instances;
	std::vector<entity *>        cur_entities;

	map_internal() noexcept
	        : vs_unlit(resources::shader("engine:unlit.vert")),
	          ps_unlit(resources::shader("engine:unlit.frag")),
	          vs_unlit_color(resources::shader("engine:unlit-color.vert")),
	          ps_unlit_color(resources::shader("engine:unlit-color.frag"))
	{
	}

	map_layer *load_layer(serializer &s);
	void       load_folder_layer(map_layer *layer, serializer &s);
	void       load_tileset_layer(map_layer *layer, serializer &s);
	void       load_sprite_layer(map_layer *layer, serializer &s);
	void       load_object_layer(map_layer *layer, serializer &s);
	void       load_collision_layer(map_layer *layer, serializer &s);

	void        load_collision_data(serializer &s) noexcept;
	inline void load(serializer &s);

	map_layer *create_layer(map_layer_type type);

	type::layer_id get_layer_id_internal(const std::string &name) noexcept override
	{
		auto it = layer_id_by_name.find(name);
		if (it != layer_id_by_name.end())
			return it->second;
		return 0;
	}

	void update_entity_internal(entity *ent) noexcept override;
	void remove_entity_internal(entity *ent) noexcept override;
	void tick(float seconds) noexcept override;
	void render(const math::box &view) noexcept override;

	void        render_tileset_layer(tileset_layer *layer) noexcept;
	void        render_sprite_layer(sprite_layer *layer) noexcept;
	void        render_object_layer(object_layer *layer) noexcept;
	inline void render_layer(map_layer *layer) noexcept;

	inline object_layer *get_object_layer(type::layer_id id) noexcept
	{
		auto it = layers.find(id);
		if (it == layers.end())
			return nullptr;

		map_layer *layer = it->second.get();
		if (layer->type != map_layer_type::object)
			return nullptr;

		return static_cast<object_layer *>(layer);
	}

	bool blocked(collidable *ent, const pixel_box &box, const pixel_vec &pos) noexcept;

	bool check_for_corridor_path(collidable      *ent,
	                             const pixel_box &box,
	                             const pixel_vec &check_pos,
	                             const pixel_vec &pixel_p2) noexcept;

	static inline math::box get_move_box(collidable *ent, const math::vec2 &p1, const math::vec2 &p2) noexcept;

	bool actually_move_collidable(collidable       *ent,
	                              const math::box  &ent_box,
	                              const math::vec2 &world_p1,
	                              math::vec2       &world_p2) noexcept;
	bool move_collidable_internal(collidable *ent, const math::vec2 &p2) noexcept override;

	void get_objects_internal(type::layer_id         id,
	                          const math::vec2      &pos,
	                          std::vector<entity *> &objects) noexcept override;
	void get_objects_internal(type::layer_id         id,
	                          const math::box       &box,
	                          std::vector<entity *> &objects) noexcept override;
	void frob_internal(type::layer_id id, const math::vec2 &pos) noexcept override;

	void set_layer_visible_internal(type::layer_id id, bool visible) noexcept override
	{
		auto it = layers.find(id);
		if (it == layers.end())
			return;

		map_layer *layer = it->second.get();
		layer->visible   = visible;
	}

	void set_layer_opacity_internal(type::layer_id id, float opacity) noexcept override
	{
		auto it = layers.find(id);
		if (it == layers.end())
			return;

		map_layer *layer = it->second.get();
		layer->opacity   = opacity;
	}
};

/* ------------------------------------------------------------------------ */
/* loading                                                                  */

static inline std::string corruption(const char *text) noexcept
{
	return strprintf("File corrupt: %s", text);
}

map_layer *map_internal::create_layer(map_layer_type type)
{
	switch (type) {
	case crpg::map_layer_type::folder:
		return new folder_layer;
	case crpg::map_layer_type::tileset:
		return new tileset_layer;
	case crpg::map_layer_type::sprite:
		return new sprite_layer;
	case crpg::map_layer_type::object:
		return new object_layer;
	case crpg::map_layer_type::collision:
		return new collision_layer;
	case crpg::map_layer_type::custom:
		return new programmable_layer;
	}

	throw corruption("bad layer type");
}

void map_internal::load_folder_layer(map_layer *base, serializer &s)
{
	auto            *layer                = static_cast<folder_layer *>(base);
	collision_layer *prev_collision_layer = last_collision_layer;

	size_t size;
	s >> size;

	layer->children.reserve(size);
	for (size_t i = 0; i < size; i++)
		layer->children.emplace(layer->children.begin(), load_layer(s));

	last_collision_layer = prev_collision_layer;
}

void map_internal::load_tileset_layer(map_layer *base, serializer &s)
{
	auto  *layer = static_cast<tileset_layer *>(base);
	size_t size;

	std::vector<std::pair<type::tileset_id, tileset *>> id_to_tileset;

	crpg::dict dict(s);

	/* ---------------------------------------------- */
	/* tilesets                                       */

	auto s_tilesets = dict.get_serializer("tilesets");

	s_tilesets >> size;
	for (size_t i = 0; i < size; i++) {
		type::tileset_id id;
		type::asset_id   asset_id;

		s_tilesets >> id;

		crpg::dict asset_dict(s_tilesets);
		asset_dict.get_asset_id("asset", asset_id);

		tileset_ref tileset = assets::tileset(asset_id);
		if (!tileset)
			throw corruption("bad tileset id");

		tilesets.push_back(tileset);
		id_to_tileset.emplace_back(id, tileset.get());
	}

	/* ---------------------------------------------- */
	/* segments                                       */

	auto s_segments = dict.get_serializer("segments");

	s_segments >> size;
	for (size_t i = 0; i < size; i++) {
		tileset_segment  seg;
		type::segment_id seg_id;
		segment          seg_def;

		s_segments >> seg_id >> seg_def;

		for (auto [id, tileset] : id_to_tileset) {
			graphics::vertex_buffer::data vbd = map_tools::generate_tileset_seg(
			        seg_def, tileset->cx(), tileset->cy(), tileset->tile_width(), id);

			if (vbd.points.size()) {
				tileset_render_data render_data;
				render_data.vb      = graphics::vertex_buffer::create(std::move(vbd));
				render_data.texture = tileset->texture();
				seg.render_data.push_back(std::move(render_data));
			}
		}

		if (seg.render_data.size()) {
			layer->segments.emplace(seg_id, std::move(seg));
		}
	}
}

void map_internal::load_sprite_layer(map_layer *base, serializer &s)
{
	auto  *layer = static_cast<sprite_layer *>(base);
	size_t size;

	std::unordered_map<type::sprite_id, type::index> idx_remap;

	crpg::dict dict(s);

	/* ---------------------------------------------- */
	/* sprites                                        */

	auto s_sprites = dict.get_serializer("sprites");

	s_sprites >> size;
	for (size_t i = 0; i < size; i++) {
		type::sprite_id id;
		type::asset_id  asset_id;
		math::vec2      pos;

		s_sprites >> id;

		crpg::dict sprite_dict(s_sprites);
		sprite_dict.get_asset_id("asset", asset_id);
		sprite_dict.get("pos", pos);

		type::index idx;

		auto it = sprite_asset_to_idx.find(asset_id);
		if (it == sprite_asset_to_idx.end()) {
			sprites.emplace_back(assets::sprite(asset_id));
			idx                           = (type::index)sprites.size() - 1;
			sprite_asset_to_idx[asset_id] = idx;
		} else {
			idx = it->second;
		}

		idx_remap[id] = (type::index)layer->instances.size();

		sprite_instance instance = {idx, pos};
		layer->instances.push_back(instance);
	}

	/* ---------------------------------------------- */
	/* segments                                       */

	auto s_segments = dict.get_serializer("segments");

	s_segments >> size;
	for (size_t i = 0; i < size; i++) {
		type::segment_id             id;
		std::vector<type::sprite_id> sprite_ids;

		s_segments >> id;

		crpg::dict segment_dict(s_segments);
		segment_dict.get("sprites", sprite_ids);

		sprite_segment &seg = layer->segments[id];
		for (auto sprite_id : sprite_ids)
			seg.instances.push_back(idx_remap[sprite_id]);
	}
}

void map_internal::load_object_layer(map_layer *base, serializer &s)
{
	auto  *layer = static_cast<object_layer *>(base);
	size_t size;

	layer->collision = last_collision_layer;

	crpg::dict dict(s);

	/* ---------------------------------------------- */
	/* objects                                        */

	auto s_objects = dict.get_serializer("objects");

	std::unordered_map<type::object_id, entity *>            obj_id_to_entity;
	std::vector<entity *>                                    obj_entities;
	std::vector<crpg::type::object_id>                       obj_links;
	std::vector<std::pair<type::object_id, type::object_id>> all_links;

	s_objects >> size;

	obj_entities.reserve(size);

	for (size_t i = 0; i < size; i++) {
		type::object_id          id;
		type::asset_id           asset_id;
		math::vec2               pos;
		math::vec2               custom_size;
		std::vector<std::string> properties;

		s_objects >> id;

		crpg::dict object_dict(s_objects);
		object_dict.get_asset_id("asset", asset_id);
		object_dict.get("pos", pos);
		object_dict.get("custom_size", custom_size);
		object_dict.get("properties", properties);
		object_dict.get("links", obj_links);

		for (type::object_id linked_id : obj_links)
			all_links.push_back(std::pair(id, linked_id));
		obj_links.clear();

		object_ref ref = assets::object(asset_id);
		auto      *obj = ref.get();

		entity *ent = factory_create(obj->name());
		if (ent) {
			ent->layer_id_ = layer->id;
			ent->set_pos(pos);

			obj_entities.push_back(ent);
			obj_id_to_entity[id] = ent;

			if (!custom_size.close(math::vec2::zero())) {
				math::box box(math::vec2::zero(), custom_size);

				if (obj->type() == object_type::box) {
					auto *collidable = dynamic_cast<crpg::collidable *>(ent);
					if (collidable)
						collidable->set_box_collision(box);
					ent->set_box(box);
				}
			}

			/* TODO: properties */
		}
	}

	/* ---------------------------------------------- */
	/* link objects together                          */

	for (auto link : all_links) {
		entity *ent1 = obj_id_to_entity[link.first];
		entity *ent2 = obj_id_to_entity[link.second];

		if (ent1 && ent2)
			ent1->links_.push_back(ent2->id());
		else
			warn("Could not add object link due to linked "
			     "objects failing to be created");
	}

	for (entity *ent : obj_entities) {
		ent->update();
		ent->init();
	}
}

struct internal_tileset_def {
	type::asset_id   asset_owner_id;
	type::asset_id   asset_id;
	type::tileset_id tileset_id;
	uint32_t         refs;
};

void map_internal::load_collision_layer(map_layer *base, serializer &s)
{
	auto *layer = static_cast<collision_layer *>(base);

	/* ignore collision data here, load from engine.data instead */
	crpg::dict dict(s);
	(void)dict;

	last_collision_layer = layer;
}

map_layer *map_internal::load_layer(serializer &s)
{
	map_layer_type type;
	type::asset_id map_id;
	type::layer_id parent_id;
	std::string    name;

	crpg::dict dict(s);
	dict.get("map_id", map_id);
	dict.get("type", type);
	dict.get("parent_id", parent_id);

	map_layer *layer = create_layer(type);
	layer->type      = type;

	dict.get("name", layer->name);
	dict.get("id", layer->id);
	dict.get("blend", layer->blend);
	dict.get("opacity", layer->opacity);
	dict.get("visible", layer->visible);

	layers.emplace(layer->id, layer);
	layer_id_by_name.emplace(layer->name, layer->id);

	switch (type) {
	case map_layer_type::folder:
		load_folder_layer(layer, s);
		break;
	case map_layer_type::tileset:
		load_tileset_layer(layer, s);
		break;
	case map_layer_type::sprite:
		load_sprite_layer(layer, s);
		break;
	case map_layer_type::object:
		load_object_layer(layer, s);
		break;
	case map_layer_type::collision:
		load_collision_layer(layer, s);
		break;
	}

	return layer;
}

inline void map_internal::load(serializer &s)
{
	root = load_layer(s);
}

/* ------------------------------------------------------------------------ */
/* rendering                                                                */

void map_internal::render_tileset_layer(tileset_layer *layer) noexcept
{
	apply_layer_opacity(ps_unlit_color, layer->blend, layer->opacity);

	for (int16_t y = tl.y; y <= br.y; y++) {
		for (int16_t x = tl.x; x <= br.x; x++) {
			map_tools::seg_pos pos(x, y);

			auto it = layer->segments.find(pos.hash);
			if (it == layer->segments.end())
				continue;

			tileset_segment &seg = it->second;
			for (tileset_render_data &data : seg.render_data) {
				ps_unlit_color->set("image", data.texture);
				data.vb->load();

				graphics::draw(graphics::primitive::triangles);
			}
		}
	}
}

void map_internal::render_sprite_layer(sprite_layer *layer) noexcept
{
	auto &instances = layer->instances;

	apply_layer_opacity(ps_unlit_color, layer->blend, layer->opacity);

	cur_instances.resize(0);

	for (int16_t y = tl.y; y <= br.y; y++) {
		for (int16_t x = tl.x; x <= br.x; x++) {
			map_tools::seg_pos sp(x, y);

			auto seg_it = layer->segments.find(sp.hash);
			if (seg_it == layer->segments.end())
				continue;

			sprite_segment &seg = seg_it->second;
			for (type::sprite_id id : seg.instances) {
				auto it = std::find(cur_instances.begin(), cur_instances.end(), id);
				if (it == cur_instances.end())
					cur_instances.push_back(id);
			}
		}
	}

	for (type::sprite_id id : cur_instances) {
		sprite_instance &instance = instances[id];
		sprite_state    &sprite   = sprites[instance.idx];

		math::matrix world;
		world.set_identity();
		world.t.x = instance.pos.x;
		world.t.y = instance.pos.y;
		ps_unlit_color->set_world(world);

		sprite.render();
	}

	vs_unlit_color->set_world(math::matrix::identity());
}

static inline void insert_unique_y_sorted_entity(std::vector<entity *> &entities, entity *ent) noexcept
{
	for (auto it = entities.begin(); it != entities.end(); ++it) {
		entity *cmp_ent = *it;

		if (cmp_ent == ent) {
			return;
		}
		if (cmp_ent->pos().y > ent->pos().y) {
			entities.insert(it, ent);
			return;
		}
	}

	entities.push_back(ent);
}

void map_internal::render_object_layer(object_layer *layer) noexcept
{
	graphics::shader_guard sg;

	cur_entities.resize(0);

	for (int16_t y = tl.y; y <= br.y; y++) {
		for (int16_t x = tl.x; x <= br.x; x++) {
			map_tools::seg_pos sp(x, y);

			auto seg_it = layer->segments.find(sp.hash);
			if (seg_it == layer->segments.end())
				continue;

			object_segment &seg = seg_it->second;
			for (entity *ent : seg.entities)
				insert_unique_y_sorted_entity(cur_entities, ent);
		}
	}

	for (entity *ent : cur_entities)
		ent->render();
}

inline void map_internal::render_layer(map_layer *layer) noexcept
{
	if (!layer->visible || layer->opacity == 0.0f)
		return;

	if (layer->type == map_layer_type::folder) {
		auto *folder = static_cast<folder_layer *>(layer);

		for (map_layer *child_layer : folder->children)
			render_layer(child_layer);
		return;
	}

	graphics::blend_guard bg;
	apply_layer_blend(layer->blend);

	switch (layer->type) {
	case map_layer_type::tileset:
		render_tileset_layer(static_cast<tileset_layer *>(layer));
		break;
	case map_layer_type::sprite:
		render_sprite_layer(static_cast<sprite_layer *>(layer));
		break;
	case map_layer_type::object:
		render_object_layer(static_cast<object_layer *>(layer));
		break;
	}
}

void map_internal::render(const math::box &view_) noexcept
{
	view = view_;
	tl   = get_map_segment_position(view.min);
	br   = get_map_segment_position(view.max);

	vs_unlit_color->load();
	ps_unlit_color->load();

	render_layer(root);
}

/* ------------------------------------------------------------------------ */
/* collision                                                                */

inline math::box map_internal::get_move_box(collidable *ent, const math::vec2 &p1, const math::vec2 &p2) noexcept
{
	math::box box(p1, p1);
	box.merge(p2);
	box.max += ent->collision_box().max;
	box.min += ent->collision_box().min;
	return box;
}

bool collision_segment::blocked(const std::vector<mask_ref> &tilesets, const pixel_box &world_box) const noexcept
{
	constexpr int64_t pixel_multiplier = (int64_t)mask::block::size;
	constexpr int64_t seg_pixel_size   = (int64_t)collision_segment::width * pixel_multiplier;

	pixel_vec pixel_offset((int64_t)x * pixel_multiplier, (int64_t)y * pixel_multiplier);
	pixel_box box = world_box;
	box -= pixel_offset;

	box.min = pixel_vec::max(box.min, 0);
	box.max = pixel_vec::min(box.max, seg_pixel_size);

	if (!box.max.x || !box.max.y)
		return false;

	int64_t min_x_offset = box.min.x % mask::block::size;
	int64_t min_y_offset = box.min.y % mask::block::size;
	int64_t max_x_offset = mask::block::size - (box.max.x % mask::block::size);
	int64_t max_y_offset = box.max.y % mask::block::size;

	if (max_x_offset == mask::block::size)
		max_x_offset = 0;
	if (max_y_offset == 0)
		max_y_offset = mask::block::size;

	box.min /= mask::block::size;
	box.max = (box.max + (mask::block::size - 1)) / mask::block::size;

	for (int64_t tile_y = box.min.y; tile_y < box.max.y; tile_y++) {
		int64_t top_y = tile_y == box.min.y ? min_y_offset : 0;
		int64_t bot_y = tile_y == (box.max.y - 1) ? max_y_offset : mask::block::size;

		for (int64_t tile_x = box.min.x; tile_x < box.max.x; tile_x++) {
			auto tile = tiles[tile_y][tile_x];
			if (!tile.tileset_id)
				continue;

			crpg::mask *mask = tilesets[tile.tileset_id - 1].get();
			if (!mask)
				continue;

			auto &block = mask->get(tile.val.x, tile.val.y);

			int64_t rshift = tile_x == box.min.x ? min_x_offset : 0;
			int64_t lshift = tile_x == (box.max.x - 1) ? max_x_offset : 0;

			for (int64_t sub_y = top_y; sub_y < bot_y; sub_y++) {
				uint16_t row = block.rows[sub_y];

				row >>= rshift;
				row <<= (rshift + lshift);
				row >>= lshift;
				if (row != 0)
					return true;
			}
		}
	}

	return false;
}

static inline const map_tools::seg_pos get_seg_pos(int64_t x, int64_t y) noexcept
{
	constexpr int64_t seg_size        = (int64_t)segment::width;
	constexpr int64_t mask_size       = (int64_t)mask::block::size;
	constexpr int64_t negative_offset = seg_size * mask_size - 1;
	if (x < 0)
		x -= negative_offset;
	if (y < 0)
		y -= negative_offset;
	return map_tools::seg_pos((int16_t)(x / mask_size / seg_size), (int16_t)(y / mask_size / seg_size));
}

static inline const map_tools::seg_pos get_seg_pos(const pixel_vec &p) noexcept
{
	return get_seg_pos(p.x, p.y);
}

bool collision_layer::blocked(const pixel_box &box) const noexcept
{
	const map_tools::seg_pos sp_min = get_seg_pos(box.min);
	const map_tools::seg_pos sp_max = get_seg_pos(box.max);

	for (int16_t y = sp_min.y; y <= sp_max.y; y++) {
		for (int16_t x = sp_min.x; x <= sp_max.x; x++) {
			map_tools::seg_pos sp(x, y);

			auto it = segments.find(sp.hash);
			if (it == segments.end())
				continue;

			const collision_segment &seg = it->second;
			if (seg.blocked(tilesets, box))
				return true;
		}
	}

	return false;
}

static inline void add_collided_entity(std::vector<collidable *> &entities, collidable *ent) noexcept
{
	auto ent_it = std::find(entities.begin(), entities.end(), ent);
	if (ent_it == entities.end())
		entities.push_back(ent);
}

static math::box get_ent_box(collidable *ent)
{
	return ent->collision_box();
}

bool object_layer::blocked(collidable *ent, const pixel_box &box, std::vector<collidable *> &collided_entities) noexcept
{
	cur_objects.resize(0);

	const map_tools::seg_pos sp_min = get_seg_pos(box.min);
	const map_tools::seg_pos sp_max = get_seg_pos(box.max);

	for (int16_t y = sp_min.y; y <= sp_max.y; y++) {
		for (int16_t x = sp_min.x; x <= sp_max.x; x++) {
			map_tools::seg_pos sp(x, y);

			auto it = segments.find(sp.hash);
			if (it == segments.end())
				continue;

			object_segment &seg = it->second;
			for (entity *seg_ent : seg.entities) {
				collidable *col = dynamic_cast<collidable *>(seg_ent);
				if (col == ent || !col)
					continue;

				auto obj_it = std::find(cur_objects.begin(), cur_objects.end(), col);
				if (obj_it == cur_objects.end())
					cur_objects.push_back(col);
			}
		}
	}

	bool intersected = false;

	for (collidable *col : cur_objects) {
		bool is_trigger = !!dynamic_cast<crpg::trigger *>(col);
		if (!is_trigger && col->can_block()) {

			math::box col_box = get_ent_box(col);
			if (box.intersects(col_box + col->world_pos())) {
				add_collided_entity(collided_entities, col);
				intersected = true;
			}
		}
	}

	return intersected;
}

void object_layer::get_triggers(collidable *ent, const math::box &box, std::vector<collidable *> &triggers) noexcept
{
	cur_objects.resize(0);

	map_tools::seg_pos sp_min = get_map_segment_position(box.min);
	map_tools::seg_pos sp_max = get_map_segment_position(box.max);

	for (int16_t y = sp_min.y; y <= sp_max.y; y++) {
		for (int16_t x = sp_min.x; x <= sp_max.x; x++) {
			map_tools::seg_pos sp(x, y);

			auto it = segments.find(sp.hash);
			if (it == segments.end())
				continue;

			object_segment &seg = it->second;
			for (entity *seg_ent : seg.entities) {
				collidable *col = dynamic_cast<collidable *>(seg_ent);
				if (col == ent || !col)
					continue;

				auto obj_it = std::find(cur_objects.begin(), cur_objects.end(), col);
				if (obj_it == cur_objects.end())
					cur_objects.push_back(col);
			}
		}
	}

	for (collidable *col : cur_objects) {
		bool is_trigger = !!dynamic_cast<crpg::trigger *>(col);
		if (is_trigger) {
			math::box col_box = get_ent_box(col);
			if (box.intersects(col_box + col->world_pos())) {
				add_collided_entity(triggers, col);
			}
		}
	}
}

bool map_internal::blocked(collidable *ent, const pixel_box &box, const pixel_vec &pos) noexcept
{
	auto *object_layer = map_internal::get_object_layer(ent->layer_id());
	if (!object_layer)
		return false;

	const pixel_box world_box = box + pos;

	for (const auto &collision_info : saved_collision_tests) {
		if (collision_info.pos == pos) {
			return collision_info.result;
		}
	}

	auto *collision_layer = object_layer->collision;
	bool  hit             = false;

	if (collision_layer)
		hit = collision_layer->blocked(world_box);
	hit |= object_layer->blocked(ent, world_box, collided_entities);

	collision_test_info collision_test = {pos, hit};
	saved_collision_tests.push_back(collision_test);
	return hit;
}

static void interpolate_pixel_vec(const pixel_vec &vec, const std::function<bool(pixel_vec)> &cb) noexcept
{
	if (!vec) {
		cb(vec);
		return;
	}

	int64_t x_mul;
	int64_t x_div;
	int64_t y_mul;
	int64_t y_div;
	int64_t count;

	if (llabs(vec.x) > llabs(vec.y)) {
		x_mul = 1;
		x_div = vec.x < 0 ? -1 : 1;
		y_mul = vec.y;
		count = y_div = llabs(vec.x);
	} else {
		y_mul = 1;
		y_div = vec.y < 0 ? -1 : 1;
		x_mul = vec.x;
		count = x_div = llabs(vec.y);
	}

	for (int64_t i = 0; i <= count; i++) {
		const pixel_vec v(i * x_mul / x_div, i * y_mul / y_div);

		if (!cb(v))
			return;
	}
}

static inline void interpolate_pixel_line_from_center(const pixel_vec                      &p1,
                                                      const pixel_vec                      &p2,
                                                      const std::function<bool(pixel_vec)> &cb) noexcept
{
	const pixel_vec vec = p2 - p1;

	if (!vec) {
		cb(p1);
		return;
	}

	int64_t x_mul;
	int64_t x_div;
	int64_t y_mul;
	int64_t y_div;
	int64_t count;

	if (llabs(vec.x) > llabs(vec.y)) {
		x_mul = 1;
		x_div = vec.x < 0 ? -1 : 1;
		y_mul = vec.y;
		count = y_div = llabs(vec.x);
	} else {
		y_mul = 1;
		y_div = vec.y < 0 ? -1 : 1;
		x_mul = vec.x;
		count = x_div = llabs(vec.y);
	}

	/* count / 2 to get the center, then move outward */
	for (int64_t i = count / 2; i >= 0; i--) {
		const pixel_vec p_r(p1.x + i * x_mul / x_div, p1.y + i * y_mul / y_div);

		int64_t         inv_i = count - i;
		const pixel_vec p_l(p1.x + inv_i * x_mul / x_div, p1.y + inv_i * y_mul / y_div);

		if (!cb(p_r))
			return;
		if (p_r != p_l && !cb(p_l))
			return;
	}
}

static inline void interpolate_cone(bool                                  forward,
                                    const pixel_vec                      &p1,
                                    const pixel_vec                      &p2,
                                    const std::function<bool(pixel_vec)> &cb) noexcept
{
	const pixel_vec vec = p2 - p1;

	if (!vec) {
		cb(p1);
		return;
	}

	/* interpolate cone edge */
	interpolate_pixel_vec(vec, [&](pixel_vec edge) {
		if (!forward)
			edge = vec - edge;

		/* get opposite cone edge */
		const pixel_vec opp_edge  = edge.cross_cw();
		const pixel_vec edge1_pos = p1 + edge;
		const pixel_vec edge2_pos = p1 + opp_edge;

		bool ret   = true;
		auto on_cb = [&](pixel_vec p) {
			if (!cb(p)) {
				ret = false;
				return false;
			}
			return true;
		};

		/* interpolate from center outward to each cone edge to get the
		 * best collision position */
		interpolate_pixel_line_from_center(edge1_pos, edge2_pos, on_cb);
		return ret;
	});
}

bool map_internal::check_for_corridor_path(collidable      *ent,
                                           const pixel_box &box,
                                           const pixel_vec &check_pos,
                                           const pixel_vec &pixel_p2) noexcept
{
	const pixel_vec vec = pixel_p2 - check_pos;

	bool was_solid      = true;
	bool found_corridor = false;

	interpolate_pixel_vec(vec, [&](pixel_vec v) {
		bool solid = blocked(ent, box, check_pos + v);
		if (!was_solid) {
			if (solid) {
				found_corridor = true;
				return false;
			}
		}

		was_solid = solid;
		return true;
	});

	return found_corridor;
}

bool map_internal::actually_move_collidable(collidable       *ent,
                                            const math::box  &ent_box,
                                            const math::vec2 &world_p1,
                                            math::vec2       &world_p2) noexcept
{
	if (ent->get_collision_mode() == collision_mode::none)
		return false;

	const pixel_box box = ent_box;

	collided_entities.resize(0);
	saved_collision_tests.resize(0);

	const pixel_vec pixel_p1 = world_p1;
	pixel_vec       pixel_p2 = world_p2;
	pixel_vec       vec      = pixel_p2 - pixel_p1;

	if (!blocked(ent, box, pixel_p2)) {
		return false;
	}

	/* -------------------------- */
	/* check for corridor path    */

	if (ent->get_collision_mode() == collision_mode::character) {
		constexpr float pixel_size = 1.0f / (float)mask::block::size;

		const math::vec2 move_vec      = world_p2 - world_p1;
		const float      move_vec_len  = move_vec.len();
		const math::vec2 move_vec_norm = move_vec / move_vec_len;

		/* only check for corridor path on angles close to 90 degrees */
		const float angle = fabsf(asinf(move_vec_norm.y));
		if (angle < math::to_rad(10.0f) || angle > math::to_rad(80.0f)) {

			const float test_dist = collided_entities.size() > 0 ? 10.0f : 16.0f;

			const math::vec2 adjusted_vec = move_vec_norm * pixel_size * test_dist;
			const math::vec2 center_vec   = move_vec_norm * pixel_size * 1.2f;
			const math::vec2 center_pos   = world_p1 + center_vec;
			math::vec2       test_vec     = center_vec + adjusted_vec.cross_cw();
			math::vec2       corridor_vec = move_vec_norm + move_vec_norm.cross_cw();

			bool corridor = check_for_corridor_path(ent, box, world_p1 + test_vec, center_pos);
			if (!corridor) {
				test_vec     = center_vec + adjusted_vec.cross_ccw();
				corridor_vec = move_vec_norm + move_vec_norm.cross_ccw();
				corridor     = check_for_corridor_path(ent, box, world_p1 + test_vec, center_pos);
			}

			if (corridor) {
				world_p2 = world_p1 + corridor_vec.norm() * move_vec_len;
				pixel_p2 = world_p2;
				vec      = pixel_p2 - pixel_p1;

				if (!blocked(ent, box, pixel_p2)) {
					return true;
				}
			}
		}
	}

	/* -------------------------- */
	/* get sliding path           */

	if (ent->get_collision_mode() >= collision_mode::slide) {
		const pixel_vec cw_vec   = vec.cross_cw();
		const pixel_vec half_vec = (vec - cw_vec) / 2;

		if (!half_vec) {
			world_p2 = pixel_p1.to_vec2();
			return true;
		}

		const pixel_vec right_vec = cw_vec + half_vec;

		bool      found = false;
		pixel_vec new_pos;

		auto on_test = [&](pixel_vec p) {
			new_pos = p;

			if (!blocked(ent, box, p)) {
				found = true;
				return false;
			}
			return true;
		};

		interpolate_cone(true, pixel_p2, pixel_p1 + right_vec, on_test);
		if (!found)
			interpolate_cone(false, pixel_p1, pixel_p1 + right_vec.cross_ccw(), on_test);

		if (found) {
			world_p2 = new_pos.to_vec2();
			return true;
		}
	}

	/* -------------------------- */
	/* fully blocked              */

	world_p2 = world_p1;
	return true;
}

static inline void set_ent_world_pos(collidable *ent, const math::vec2 &world_pos) noexcept
{
	if (ent->parent()) {
		const math::vec2 local_pos = math::vec2::transform(world_pos, ent->parent()->matrix().transposed());
		ent->set_pos(local_pos);
	} else {
		ent->set_pos(world_pos);
	}
}

bool map_internal::move_collidable_internal(collidable *ent, const math::vec2 &p2) noexcept
{
	const math::vec2 world_p1 = ent->world_pos();
	math::vec2       world_p2 = ent->parent() ? math::vec2::transform(p2, ent->parent()->matrix()) : p2;
	const math::box  box      = get_ent_box(ent);
	bool             result   = actually_move_collidable(ent, box, world_p1, world_p2);

	set_ent_world_pos(ent, world_p2);

	if (world_p1.close(world_p2))
		return result;

	/* -------------------------- */
	/* process triggers           */

	prev_triggers.resize(0);
	next_triggers.resize(0);

	auto *object_layer = map_internal::get_object_layer(ent->layer_id());
	if (!object_layer)
		return result;

	object_layer->get_triggers(ent, box + world_p1, prev_triggers);
	object_layer->get_triggers(ent, box + world_p2, next_triggers);

	for (size_t i = prev_triggers.size(); i > 0; i--) {
		size_t idx = i - 1;
		auto   it  = std::find(next_triggers.begin(), next_triggers.end(), prev_triggers[idx]);
		if (it != next_triggers.end()) {
			prev_triggers.erase(prev_triggers.begin() + idx);
			next_triggers.erase(it);
		}
	}

	for (collidable *leave_trigger : prev_triggers) {
		leave_trigger->on_leave(ent);
		ent->on_leave(leave_trigger);
	}
	for (collidable *enter_trigger : next_triggers) {
		enter_trigger->on_enter(ent);
		ent->on_enter(enter_trigger);
	}

	/* -------------------------- */
	/* process entity collisions  */

	for (collidable *collision_ent : collided_entities) {
		collision_ent->on_collision(ent);
		ent->on_collision(collision_ent);
	}

	return result;
}

void map_internal::get_objects_internal(type::layer_id         id,
                                        const math::vec2      &pos,
                                        std::vector<entity *> &objects) noexcept
try {
	objects.resize(0);

	auto layer_it = layers.find(id);
	if (layer_it == layers.end())
		throw strprintf("Layer id %u does not exist", id);

	object_layer *layer = dynamic_cast<object_layer *>(layer_it->second.get());
	if (!layer)
		throw strprintf("Layer %u ('%s') is not an object layer", id, layer_it->second->name.c_str());

	const map_tools::seg_pos sp     = get_map_segment_position(pos);
	auto                     seg_it = layer->segments.find(sp.hash);
	if (seg_it == layer->segments.end())
		return;

	object_segment &seg = seg_it->second;
	for (entity *ent : seg.entities) {
		const math::box &ent_box = ent->transformed_box();
		if (ent_box.inside(pos))
			objects.push_back(ent);
	}

} catch (const std::string &text) {
	warn("%s: %s", __FUNCTION__, text.c_str());
}

void map_internal::get_objects_internal(type::layer_id         id,
                                        const math::box       &box,
                                        std::vector<entity *> &objects) noexcept
try {
	objects.resize(0);

	auto layer_it = layers.find(id);
	if (layer_it == layers.end())
		throw strprintf("Layer id %u does not exist", id);

	object_layer *layer = dynamic_cast<object_layer *>(layer_it->second.get());
	if (!layer)
		throw strprintf("Layer %u ('%s') is not an object layer", id, layer_it->second->name.c_str());

	objects.resize(0);

	map_tools::seg_pos sp_min = get_map_segment_position(box.min);
	map_tools::seg_pos sp_max = get_map_segment_position(box.max);

	for (int16_t y = sp_min.y; y <= sp_max.y; y++) {
		for (int16_t x = sp_min.x; x <= sp_max.x; x++) {
			map_tools::seg_pos sp(x, y);

			auto it = layer->segments.find(sp.hash);
			if (it == layer->segments.end())
				continue;

			object_segment &seg = it->second;
			for (entity *ent : seg.entities) {
				if (ent->transformed_box().intersects(box)) {
					auto obj_it = std::find(objects.begin(), objects.end(), ent);
					if (obj_it == objects.end())
						objects.push_back(ent);
				}
			}
		}
	}

} catch (const std::string &text) {
	warn("%s: %s", __FUNCTION__, text.c_str());
}

void map_internal::frob_internal(type::layer_id id, const math::vec2 &pos) noexcept
{
	get_objects_internal(id, pos, cur_entities);
	for (entity *ent : cur_entities)
		ent->on_frob();
}

void map_internal::load_collision_data(serializer &s) noexcept
{
	size_t num_collision_layers;

	s >> num_collision_layers;
	for (size_t i = 0; i < num_collision_layers; i++) {
		type::layer_id layer_id;

		s >> layer_id;

		auto *layer = static_cast<collision_layer *>(layers[layer_id].get());

		size_t num_tilesets;
		s >> num_tilesets;

		layer->tilesets.reserve(num_tilesets);

		for (size_t j = 0; j < num_tilesets; j++) {
			type::asset_id tileset_id;
			s >> tileset_id;

			layer->tilesets.push_back(assets::tileset_mask(tileset_id));
		}

		s >> layer->segments;
	}
}

/* ------------------------------------------------------------------------ */
/* general map stuff                                                        */

void map_internal::update_entity_internal(entity *ent) noexcept
try {
	remove_entity_internal(ent);

	type::layer_id id = ent->layer_id();
	if (id == 0)
		return;

	auto it = layers.find(id);
	if (it == layers.end())
		throw std::string("Invalid layer id assigned to entity");

	auto *layer = dynamic_cast<object_layer *>(it->second.get());
	if (!layer)
		throw std::string("Layer assigned to entity is not an object layer");

	math::box box = ent->transformed_box();
	box.min -= 0.1f;
	box.max += 0.1f;

	map_tools::seg_pos sp_min = get_map_segment_position(box.min);
	map_tools::seg_pos sp_max = get_map_segment_position(box.max);

	std::vector<type::segment_id> segments;

	for (int16_t y = sp_min.y; y <= sp_max.y; y++) {
		for (int16_t x = sp_min.x; x <= sp_max.x; x++) {
			map_tools::seg_pos sp(x, y);

			segments.push_back(sp.hash);

			auto &seg = layer->segments[sp.hash];
			seg.entities.push_back(ent);
		}
	}

	layer->entities[ent->id()].segments = std::move(segments);

} catch (const std::string &text) {
	warn("%s: %s", __FUNCTION__, text.c_str());
}

void map_internal::remove_entity_internal(entity *ent) noexcept
try {
	type::layer_id layer_id = ent->layer_id();
	if (layer_id == 0)
		return;

	auto it = layers.find(layer_id);
	if (it == layers.end())
		throw std::string("Invalid layer id assigned to entity");

	auto *layer = dynamic_cast<object_layer *>(it->second.get());
	if (!layer)
		throw std::string("Layer assigned to entity is not an object layer");

	type::entity_id               id       = ent->id();
	std::vector<type::segment_id> segments = std::move(layer->entities[id].segments);
	layer->entities.erase(id);

	for (auto seg_id : segments) {
		auto &ents   = layer->segments[seg_id].entities;
		auto  ent_it = std::find(ents.begin(), ents.end(), ent);
		if (ent_it != ents.end())
			ents.erase(ent_it);
	}

} catch (const std::string &text) {
	warn("%s: %s", __FUNCTION__, text.c_str());
}

void map_internal::tick(float seconds) noexcept
{
	/* TODO */
}

/* ======================================================================== */
/* map base class                                                           */

map *map::current = nullptr;

map::map() noexcept
{
	current = this;
}

map::~map() noexcept
{
	if (current == this) {
		entity::free_entities();
		current = nullptr;
	}
}

map *map::load(const std::string &path) noexcept
try {
	/* -------------------------------------------------- */
	/* main map data                                      */

	std::string  filename = path + "/asset.data";
	ifserializer s(filename);
	if (s.fail())
		throw strprintf("Failed to load %s", filename.c_str());

	entity::free_entities();

	map_internal *map = new map_internal;
	map->load(s);

	/* -------------------------------------------------- */
	/* precalculated engine data                          */

	filename = path + "/engine.data";

	s.close();
	s.open(filename);
	if (s.fail())
		throw strprintf("Failed to load %s", filename.c_str());

	map->load_collision_data(s);

	/* -------------------------------------------------- */

	return map;

} catch (const std::string &text) {
	error("%s: %s", __FUNCTION__, text.c_str());
	return nullptr;
}

/* ======================================================================== */
/* map utils                                                                */

namespace map_tools {
void add_tile_sprite(graphics::vertex_buffer::data &vbd,
                     const crpg::math::vec3        &p1,
                     const crpg::math::vec3        &p4,
                     const crpg::math::vec2        &uv1,
                     const crpg::math::vec2        &uv4) noexcept
{
	unsigned long start_idx = (unsigned long)vbd.points.size();
	vbd.points.emplace_back(p1);
	vbd.points.emplace_back(p4.x, p1.y);
	vbd.points.emplace_back(p1.x, p4.y);
	vbd.points.emplace_back(p4);
	vbd.coords.emplace_back(uv1);
	vbd.coords.emplace_back(uv4.x, uv1.y);
	vbd.coords.emplace_back(uv1.x, uv4.y);
	vbd.coords.emplace_back(uv4);
	vbd.indices.push_back(start_idx);
	vbd.indices.push_back(start_idx + 2);
	vbd.indices.push_back(start_idx + 1);
	vbd.indices.push_back(start_idx + 1);
	vbd.indices.push_back(start_idx + 2);
	vbd.indices.push_back(start_idx + 3);
}

graphics::vertex_buffer::data generate_tileset_seg(const crpg::segment &seg,
                                                   uint32_t             cx,
                                                   uint32_t             cy,
                                                   uint32_t             tile_width,
                                                   type::tileset_id     tileset_id) noexcept
{
	float cx_mul = 1.0f / (float)(cx / tile_width);
	float cy_mul = 1.0f / (float)(cy / tile_width);

	using namespace crpg;

	graphics::vertex_buffer::data vbd;

	for (int y = 0; y < (int)segment::width; y++) {
		for (int x = 0; x < (int)segment::width; x++) {
			const segment::tile &tile = seg.tiles[y][x];
			if (tile.tileset_id != tileset_id)
				continue;

			if (tile.type == tile_type::normal) {
				math::vec2 uv1((float)tile.vals[0].x * cx_mul, (float)tile.vals[0].y * cy_mul);
				math::vec2 uv4((float)(tile.vals[0].x + 1) * cx_mul,
				               (float)(tile.vals[0].y + 1) * cy_mul);
				math::vec3 p1((float)(x + seg.x), (float)(y + seg.y), 0.0f);
				math::vec3 p4((float)(x + 1 + seg.x), (float)(y + 1 + seg.y), 0.0f);
				uv1 += math::tiny_epsilon;
				uv4 -= math::tiny_epsilon;

				add_tile_sprite(vbd, p1, p4, uv1, uv4);
			}

			/* TODO: autotile/etc stuff */
		}
	}

	return vbd;
}
} // namespace map_tools

map_tools::seg_pos get_map_segment_position(const math::vec2 &pos) noexcept
{
	return map_tools::seg_pos((int16_t)floorf(pos.x / (float)segment::width),
	                          (int16_t)floorf(pos.y / (float)segment::width));
}

} // namespace crpg
