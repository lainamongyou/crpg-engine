#include "resources.hpp"
#include "entity.hpp"
#include "widget.hpp"
#include "assets.hpp"
#include "font.hpp"
#include "app.hpp"

namespace crpg {

app *app::current = nullptr;

app::app() noexcept : rm(resource_manager::create())
{
	current = this;
}

app::~app() noexcept
{
	widget::destroy_all();
	current = nullptr;
}

void app::tick_all(float seconds) noexcept
{
	if (next_map) {
		bool success = assets::load_map_now(next_map);
		if (map_callback)
			map_callback(success);

		map_callback = std::function<void(bool)>();
		next_map     = 0;
	}

	tick(seconds);
	widget::tick_all(seconds);
	entity::tick_all(seconds);
	post_tick(seconds);
}

void app::init() noexcept {}
void app::tick(float) noexcept {}
void app::post_tick(float) noexcept {}
void app::prerender(int, int) noexcept {}
void app::render() noexcept {}

void app::quit_event() noexcept
{
	quit();
}

} // namespace crpg
