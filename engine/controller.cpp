#include "controller.hpp"
#include "pawn.hpp"

namespace crpg {

controller::~controller() noexcept
{
	if (target_) {
		target_->on_disconnect(this);
		on_disconnect(target_);
		target_->cur_controller = nullptr;
	}
}

void controller::on_connect(pawn *) noexcept {}
void controller::on_disconnect(pawn *) noexcept {}

} // namespace crpg
