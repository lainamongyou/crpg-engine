#include "collidable.hpp"
#include "map.hpp"

namespace crpg {

void collidable::on_collision(collidable *) noexcept {}
void collidable::on_enter(collidable *) noexcept {}
void collidable::on_leave(collidable *) noexcept {}

bool collidable::can_block() const noexcept
{
	return collision_mode_ != collision_mode::none;
}

void collidable::move(const math::vec2 &new_pos) noexcept
{
	map::move_collidable(this, new_pos);
}

void collidable::set_box_collision(const math::box &box) noexcept
{
	collision_box_ = box;
}

bool trigger::can_block() const noexcept
{
	return false;
}

} // namespace crpg
