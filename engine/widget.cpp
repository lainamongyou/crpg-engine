#include "graphics/graphics.hpp"
#include "widget.hpp"

#include <functional>

namespace crpg {

widget         *widget::first_         = nullptr;
widget         *widget::last_          = nullptr;
control_widget *widget::focused_widget = nullptr;

static SDL_MouseMotionEvent last_mouse_move_event = {};
static bool                 cleanup               = false;

/* ----------------------------------------------------------------------- */

inline void widget::push_back(widget *&first__, widget *&last__) noexcept
{
	if (!first__) {
		first__ = last__ = this;
	} else {
		prev_         = last__;
		last__->next_ = this;
		last__        = this;
	}
}

widget::widget(widget *parent__) noexcept : parent_(parent__)
{
	if (parent_)
		push_back(parent_->first_child_, parent_->last_child_);
	else
		push_back(first_, last_);
}

inline void widget::detach() noexcept
{
	if (prev_) {
		prev_->next_ = next_;
	} else {
		if (parent_)
			parent_->first_child_ = next_;
		else
			first_ = next_;
	}

	if (next_) {
		next_->prev_ = prev_;
	} else {
		if (parent_)
			parent_->last_child_ = prev_;
		else
			last_ = prev_;
	}

	parent_ = nullptr;
}

widget::~widget() noexcept
{
	if (owner)
		owner->w = nullptr;

	if (!cleanup && this == focused_widget) {
		focused_widget = nullptr;
		if (last_mouse_move_event.type != SDL_MOUSEMOTION)
			widget::sdl_mouse_move_event(last_mouse_move_event);
	}

	widget *child = first_child_;
	while (child) {
		widget *next_child = child->next_;
		delete child;
		child = next_child;
	}

	detach();
}

void widget::init() noexcept {}
void widget::tick(float) noexcept {}
void widget::render() noexcept {}

void widget::destroy_later() noexcept
{
	if (owner)
		owner->w = nullptr;
	destroy_later_ = true;
}

void widget::destroy_all() noexcept
{
	cleanup = true;

	widget *w = first_;
	while (w) {
		widget *next_w = w->next_;
		delete w;
		w = next_w;
	}

	cleanup = false;
}

static constexpr int x_center = 0b0001;
static constexpr int x_right  = 0b0010;
static constexpr int y_center = 0b0100;
static constexpr int y_bottom = 0b1000;

inline math::vec2 widget::get_actual_pos(const math::vec2 &parent_size) noexcept
{
	math::vec2 final_pos = pos_;

	if (((int)origin_ & x_center) != 0) {
		final_pos.x -= size_.x * 0.5f;
	} else if (((int)origin_ & x_right) != 0) {
		final_pos.x -= size_.x;
	}

	if (((int)origin_ & y_center) != 0) {
		final_pos.y -= size_.y * 0.5f;
	} else if (((int)origin_ & y_bottom) != 0) {
		final_pos.y -= size_.y;
	}

	if ((int)alignment_ == 0)
		return final_pos;

	if (((int)alignment_ & x_center) != 0) {
		final_pos.x += parent_size.x * 0.5f;
	} else if (((int)alignment_ & x_right) != 0) {
		final_pos.x += parent_size.x;
	}

	if (((int)alignment_ & y_center) != 0) {
		final_pos.y += parent_size.y * 0.5f;
	} else if (((int)alignment_ & y_bottom) != 0) {
		final_pos.y += parent_size.y;
	}

	return final_pos;
}

void widget::render_list(widget *start, const math::vec2 &size) noexcept
{
	widget *w = start;
	while (w) {
		widget *next_w = w->next_;
		if (w->showing_) {
			graphics::matrix_push();
			graphics::matrix_translate(w->get_actual_pos(size));

			w->render();
			render_list(w->first_child(), w->size_);

			graphics::matrix_pop();
		}
		w = next_w;
	}
}

void widget::tick_list(widget *start, float seconds) noexcept
{
	widget *w = start;
	while (w) {
		widget *next_w = w->next_;
		if (w->destroy_later_) {
			delete w;
		} else {
			w->tick(seconds);
			tick_list(w->first_child(), seconds);
		}
		w = next_w;
	}
}

typedef std::function<bool(widget *)> recurse_cb;

static inline bool recurse_reverse(widget *end, recurse_cb cb) noexcept
{
	widget *w = end;
	while (w) {
		widget *prev_w = w->prev();
		if (!cb(w))
			return false;
		w = prev_w;
	}

	return true;
}

static bool recurse_key_event(widget *end, std::function<bool(widget *w)> func) noexcept
{
	return recurse_reverse(end, [&](widget *w) {
		if (w->last_child() && !recurse_key_event(w->last_child(), func))
			return false;
		return func(w);
	});
}

void widget::sdl_key_event(SDL_KeyboardEvent &event) noexcept
{
	recurse_key_event(last_, [&](widget *w) {
		auto *cw = dynamic_cast<control_widget *>(w);
		return cw ? !cw->key_event(event) : true;
	});
}

static bool mouse_event(const math::vec2              &pos,
                        const math::vec2              &cur_tl,
                        widget                        *end,
                        std::function<void(widget *w)> func) noexcept
{
	return recurse_reverse(end, [&](widget *w) {
		math::vec2 tl = cur_tl + w->pos();
		math::vec2 br = tl + w->size();

		if (pos.x > tl.x && pos.y > tl.y && pos.x < br.x && pos.y < br.y) {
			if (mouse_event(pos, tl, w->last_child(), func))
				func(w);
			return false;
		}
		return true;
	});
}

inline void widget::set_focus() noexcept
{
	if (this == focused_widget)
		return;

	auto *cw = static_cast<control_widget *>(this);

	if (focused_widget)
		focused_widget->lose_focus();
	cw->gain_focus();
	focused_widget = cw;
}

void widget::sdl_mouse_move_event(SDL_MouseMotionEvent &event) noexcept
{
	recurse_cb unset_mouse_over;
	unset_mouse_over = [&](widget *w) {
		recurse_reverse(w->last_child_, unset_mouse_over);

		auto *cw = dynamic_cast<control_widget *>(w);
		if (cw)
			cw->mouse_over = false;
		return true;
	};

	recurse_reverse(last_, unset_mouse_over);

	math::vec2 pos((float)event.x, (float)event.y);
	mouse_event(pos, math::vec2::zero(), last_, [&](widget *w) {
		auto *cw = dynamic_cast<control_widget *>(w);
		if (!cw)
			return;

		cw->mouse_over = true;

		if (!focused_widget || !focused_widget->keeping_focus())
			cw->set_focus();
		if (cw == focused_widget)
			cw->mouse_move_event(event);
	});

	recurse_cb check_enter_leave;
	check_enter_leave = [&](widget *w) {
		recurse_reverse(w->last_child_, check_enter_leave);

		auto *cw = dynamic_cast<control_widget *>(w);
		if (cw && cw->mouse_over != cw->mouse_was_over) {
			if (cw->mouse_over)
				cw->mouse_enter();
			else
				cw->mouse_leave();
			cw->mouse_was_over = cw->mouse_over;
		}
		return true;
	};

	recurse_reverse(last_, check_enter_leave);
}

void widget::sdl_mouse_wheel_event(SDL_MouseWheelEvent &event) noexcept
{
	math::vec2 pos((float)event.x, (float)event.y);
	mouse_event(pos, math::vec2::zero(), last_, [&](widget *w) {
		auto *cw = dynamic_cast<control_widget *>(w);

		if (cw && cw == focused_widget)
			cw->mouse_wheel_event(event);
	});
}

void widget::sdl_mouse_button_event(SDL_MouseButtonEvent &event) noexcept
{
	math::vec2 pos((float)event.x, (float)event.y);
	mouse_event(pos, math::vec2::zero(), last_, [&](widget *w) {
		auto *cw = dynamic_cast<control_widget *>(w);

		if (cw && cw == focused_widget)
			cw->mouse_button_event(event);
	});
}

bool control_widget::key_event(SDL_KeyboardEvent &) noexcept
{
	return false;
}
void control_widget::mouse_move_event(SDL_MouseMotionEvent &) noexcept {}
void control_widget::mouse_wheel_event(SDL_MouseWheelEvent &) noexcept {}
void control_widget::mouse_button_event(SDL_MouseButtonEvent &) noexcept {}

void control_widget::gain_focus() noexcept {}
void control_widget::lose_focus() noexcept {}
void control_widget::mouse_enter() noexcept {}
void control_widget::mouse_leave() noexcept {}

void control_widget::keep_focus(bool keep) noexcept
{
	if (keep == keep_focus_)
		return;

	keep_focus_ = keep;

	if (keep)
		set_focus();
	else if (last_mouse_move_event.type != SDL_MOUSEMOTION)
		widget::sdl_mouse_move_event(last_mouse_move_event);
}

/* ----------------------------------------------------------------------- */
} // namespace crpg
