#pragma once

#include "math/vec2.hpp"
#include "math/box.hpp"
#include "mask.hpp"

#include <cstdint>

namespace crpg {

struct pixel_vec {
	int64_t x;
	int64_t y;

	static constexpr float pixel_multiplier = (float)mask::block::size;

	inline pixel_vec() noexcept = default;
	inline pixel_vec(int64_t x, int64_t y) noexcept : x(x), y(y) {}
	inline pixel_vec(const math::vec2 &v) noexcept
	{
		x = (int64_t)roundf(v.x * pixel_multiplier);
		y = (int64_t)roundf(v.y * pixel_multiplier);
	}

	inline pixel_vec &operator+=(int64_t val) noexcept
	{
		x += val;
		y += val;
		return *this;
	}
	inline pixel_vec &operator-=(int64_t val) noexcept
	{
		x -= val;
		y -= val;
		return *this;
	}
	inline pixel_vec &operator*=(int64_t val) noexcept
	{
		x *= val;
		y *= val;
		return *this;
	}
	inline pixel_vec &operator/=(int64_t val) noexcept
	{
		x /= val;
		y /= val;
		return *this;
	}
	inline pixel_vec operator+(int64_t val) const noexcept { return pixel_vec(*this) += val; }
	inline pixel_vec operator-(int64_t val) const noexcept { return pixel_vec(*this) -= val; }
	inline pixel_vec operator*(int64_t val) const noexcept { return pixel_vec(*this) *= val; }
	inline pixel_vec operator/(int64_t val) const noexcept { return pixel_vec(*this) /= val; }

	inline pixel_vec &operator+=(const pixel_vec &v) noexcept
	{
		x += v.x;
		y += v.y;
		return *this;
	}
	inline pixel_vec &operator-=(const pixel_vec &v) noexcept
	{
		x -= v.x;
		y -= v.y;
		return *this;
	}
	inline pixel_vec &operator*=(const pixel_vec &v) noexcept
	{
		x *= v.x;
		y *= v.y;
		return *this;
	}
	inline pixel_vec &operator/=(const pixel_vec &v) noexcept
	{
		x /= v.x;
		y /= v.y;
		return *this;
	}
	inline pixel_vec operator+(const pixel_vec &v) const noexcept { return pixel_vec(*this) += v; }
	inline pixel_vec operator-(const pixel_vec &v) const noexcept { return pixel_vec(*this) -= v; }
	inline pixel_vec operator*(const pixel_vec &v) const noexcept { return pixel_vec(*this) *= v; }
	inline pixel_vec operator/(const pixel_vec &v) const noexcept { return pixel_vec(*this) /= v; }

	inline pixel_vec cross() const noexcept { return pixel_vec(y, -x); }
	inline pixel_vec cross_ccw() const noexcept { return cross(); }
	inline pixel_vec cross_cw() const noexcept { return pixel_vec(-y, x); }

	inline math::vec2 to_vec2() const noexcept
	{
		return math::vec2((float)x / pixel_multiplier, (float)y / pixel_multiplier);
	}

	static inline pixel_vec min(const pixel_vec &v, int64_t val) noexcept
	{
		return pixel_vec(v.x < val ? v.x : val, v.y < val ? v.y : val);
	}
	static inline pixel_vec max(const pixel_vec &v, int64_t val) noexcept
	{
		return pixel_vec(v.x > val ? v.x : val, v.y > val ? v.y : val);
	}

	inline bool operator!() const noexcept { return x == 0 && y == 0; }

	inline bool operator==(const pixel_vec &v) const noexcept { return x == v.x && y == v.y; }
};

struct pixel_box {
	pixel_vec min;
	pixel_vec max;

	inline pixel_box() noexcept = default;
	inline pixel_box(const math::box &box) noexcept : min(box.min)
	{
		min.x = (int64_t)floorf(box.min.x * pixel_vec::pixel_multiplier);
		min.y = (int64_t)floorf(box.min.y * pixel_vec::pixel_multiplier);
		max.x = (int64_t)ceilf(box.max.x * pixel_vec::pixel_multiplier);
		max.y = (int64_t)ceilf(box.max.y * pixel_vec::pixel_multiplier);
	}

	inline bool intersects(const pixel_box &test) const noexcept
	{
		return ((min.x - test.max.x) <= 0) && ((test.min.x - max.x) <= 0) && ((min.y - test.max.y) <= 0) &&
		       ((test.min.y - max.y) <= 0);
	}

	inline pixel_box &operator+=(const pixel_vec &v) noexcept
	{
		min += v;
		max += v;
		return *this;
	}
	inline pixel_box &operator-=(const pixel_vec &v) noexcept
	{
		min -= v;
		max -= v;
		return *this;
	}
	inline pixel_box operator+(const pixel_vec &v) const noexcept { return pixel_box(*this) += v; }
	inline pixel_box operator-(const pixel_vec &v) const noexcept { return pixel_box(*this) -= v; }
};

} // namespace crpg
