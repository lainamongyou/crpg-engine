#include "utility/fserializer.hpp"
#include "graphics/graphics.hpp"
#include "utility/logging.hpp"
#include "utility/dict.hpp"
#include "tileset.hpp"

namespace crpg {

tileset_ref tileset::create(const std::string &path) noexcept
try {
	ifserializer s(path + "/asset.data");
	if (s.fail())
		throw strprintf("Failed to open '%s'", path.c_str());

	crpg::tileset *tileset = new crpg::tileset;
	std::string    ext;

	crpg::dict dict(s);
	dict.get("ext", ext);

	std::string image_path = path + "/image." + ext;
	tileset->texture_      = graphics::texture::create_from_file(image_path);
	if (!tileset->texture_)
		throw strprintf("Failed to open '%s'", image_path.c_str());

	dict.get("cx", tileset->cx_);
	dict.get("cy", tileset->cy_);
	dict.get("tile_width", tileset->tile_width_);

	return std::shared_ptr<crpg::tileset>(tileset);

} catch (const std::string &text) {
	error("%s: %s", __FUNCTION__, text.c_str());
	return tileset_ref();
}

} // namespace crpg
