#pragma once

#include "export.h"

namespace crpg {

class pawn;

class EXPORT controller {
	friend class pawn;

	pawn *target_ = nullptr;

public:
	virtual ~controller() noexcept;
	virtual void on_connect(pawn *ent) noexcept;
	virtual void on_disconnect(pawn *ent) noexcept;

	inline pawn *target() const noexcept { return target_; }
};

} // namespace crpg
