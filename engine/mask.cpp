#include "utility/fserializer.hpp"
#include "utility/logging.hpp"
#include "mask.hpp"

namespace crpg {

mask_ref mask::create(const std::string &path) noexcept
try {
	ifserializer s(path);
	if (s.fail())
		throw strprintf("Failed to open '%s'", path.c_str());

	uint32_t cx;
	uint32_t cy;

	s >> cx >> cy;

	uint32_t block_cx = cx / mask::block::size;
	uint32_t block_cy = cy / mask::block::size;

	crpg::mask *mask = new crpg::mask;
	mask->cx_        = block_cx;
	mask->cy_        = block_cy;

	mask->blocks.resize(block_cx * block_cy);
	s.serialize(&mask->blocks[0], mask->blocks.size() * sizeof(mask::block));

	return mask_ref(mask);

} catch (const std::string &text) {
	error("%s: %s", __FUNCTION__, text.c_str());
	return mask_ref();
}

} // namespace crpg
