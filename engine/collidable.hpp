#pragma once

#include "entity.hpp"

namespace crpg {

enum class collision_mode {
	none,
	block_only,
	slide,
	character,
};

class EXPORT collidable : public entity {
	math::box      collision_box_  = math::box(math::vec2::zero(), math::vec2::zero());
	collision_mode collision_mode_ = collision_mode::none;

public:
	virtual void on_collision(collidable *ent) noexcept;
	virtual void on_enter(collidable *ent) noexcept;
	virtual void on_leave(collidable *ent) noexcept;
	virtual bool can_block() const noexcept;

	void move(const math::vec2 &pos) noexcept;

	inline const math::box     &collision_box() const noexcept { return collision_box_; }
	inline crpg::collision_mode get_collision_mode() const noexcept { return collision_mode_; }
	inline void                 set_collision_mode(crpg::collision_mode mode) noexcept { collision_mode_ = mode; }

	void set_box_collision(const math::box &box) noexcept;
};

class EXPORT trigger : public collidable {
public:
	bool can_block() const noexcept override;
};

} // namespace crpg
