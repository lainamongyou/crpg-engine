#pragma once

#include <functional>
#include <vector>
#include <string>
#include <memory>

#include "export.h"
#include "types.hpp"

namespace crpg {

class sprite;
class tileset;
class mask;

using sprite_ref  = std::shared_ptr<sprite>;
using tileset_ref = std::shared_ptr<tileset>;

using mask_ref      = std::shared_ptr<mask>;
using weak_mask_ref = std::weak_ptr<mask>;

enum class object_type : uint32_t {
	box,
	point,
	sprite,
	unused,
};

class object_info {
	friend class asset_manager_internal;

public:
	struct property {
		std::string          name;
		object_property_type type;
	};

	inline const std::string            name() const noexcept { return name_; }
	inline const std::vector<property> &properties() const noexcept { return properties_; }
	inline object_type                  type() const noexcept { return type_; }

private:
	std::string           name_;
	std::vector<property> properties_;
	object_type           type_;
};

using object_ref      = std::shared_ptr<object_info>;
using weak_object_ref = std::weak_ptr<object_info>;

class EXPORT asset_manager {
protected:
	static asset_manager *current;

public:
	virtual ~asset_manager() noexcept;

	static std::unique_ptr<asset_manager> create() noexcept;
	static inline asset_manager          *get() noexcept { return current; }

	virtual type::asset_id get_id(const std::string &name) noexcept     = 0;
	virtual bool           load_map_now(type::asset_id id) noexcept     = 0;
	virtual sprite_ref     get_sprite(type::asset_id id) noexcept       = 0;
	virtual tileset_ref    get_tileset(type::asset_id id) noexcept      = 0;
	virtual mask_ref       get_tileset_mask(type::asset_id id) noexcept = 0;
	virtual object_ref     get_object(type::asset_id id) noexcept       = 0;

	using load_callback_t = std::function<void(bool)>;

	void load_map(type::asset_id id, load_callback_t callback = load_callback_t()) noexcept;
};

namespace assets {

EXPORT bool load_map_now(const std::string &name) noexcept;
inline bool load_map_now(type::asset_id id) noexcept
{
	return asset_manager::get()->load_map_now(id);
}

EXPORT void load_map(const std::string &name) noexcept;
inline void load_map(type::asset_id id) noexcept
{
	asset_manager::get()->load_map(id);
}

EXPORT sprite_ref sprite(const std::string &name, bool fail_if_not_found = true) noexcept;
inline sprite_ref sprite(type::asset_id id) noexcept
{
	return asset_manager::get()->get_sprite(id);
}

inline tileset_ref tileset(const std::string &name) noexcept
{
	auto *am = asset_manager::get();
	return am->get_tileset(am->get_id(name));
}
inline tileset_ref tileset(type::asset_id id) noexcept
{
	return asset_manager::get()->get_tileset(id);
}

inline mask_ref tileset_mask(const std::string &name) noexcept
{
	auto *am = asset_manager::get();
	return am->get_tileset_mask(am->get_id(name));
}
inline mask_ref tileset_mask(type::asset_id id) noexcept
{
	return asset_manager::get()->get_tileset_mask(id);
}

inline object_ref object(const std::string &name) noexcept
{
	auto *am = asset_manager::get();
	return am->get_object(am->get_id(name));
}
inline object_ref object(type::asset_id id) noexcept
{
	return asset_manager::get()->get_object(id);
}

} // namespace assets

} // namespace crpg
