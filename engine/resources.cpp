#include <unordered_map>
#include <sstream>
#include <cstring>

#include "utility/logging.hpp"
#include "resources.hpp"

using namespace std;

namespace crpg {

/* ----------------------------------------------------------------------- */

class resource_manager_internal : public resource_manager {
	unordered_map<std::string, graphics::texture_ref> textures;
	unordered_map<std::string, graphics::shader_ref>  shaders;
	graphics::vertex_buffer_ref                       sprite_vb_;
	unordered_map<std::string, font_ref>              fonts;

public:
	inline resource_manager_internal() noexcept;

	graphics::texture_ref       get_texture(const std::string &name) noexcept override;
	graphics::shader_ref        get_shader(const char *name) noexcept override;
	graphics::vertex_buffer_ref sprite_vb() noexcept override;

	font_ref get_font(const std::string &name, int size, int outline_size) noexcept override;

	void reset() noexcept override;
};

/* ----------------------------------------------------------------------- */

string get_module_file(const char *type, const char *name) noexcept
try {
	if (!name || !*name)
		throw string("Null name");
	if (!type || !*type)
		throw string("Null type");

	const char *colen = strchr(name, ':');
	if (!colen)
		throw strprintf("Invalid name: '%s'", name);

	const char *file = colen + 1;
	if (!*file)
		throw strprintf("Invalid name: '%s'", name);

	string module;
	module.append(name, colen - name);
	if (!module.size())
		throw strprintf("Invalid name: '%s'", name);

	string path;
	path += "resources/";
	path += module;
	path += "/";
	path += type;
	path += "/";
	path += file;
	return path;

} catch (const string &text) {
	warn("%s: %s", __FUNCTION__, text.c_str());
	return string();
}

/* ----------------------------------------------------------------------- */

resource_manager *resource_manager::current = nullptr;

inline resource_manager_internal::resource_manager_internal() noexcept
{
	current = this;
}

resource_manager::~resource_manager() noexcept
{
	current = nullptr;
}

unique_ptr<resource_manager> resource_manager::create() noexcept
try {
	if (current)
		throw "A resource manager already exists";

	return unique_ptr<resource_manager>(new resource_manager_internal());

} catch (const char *str) {
	error("%s: %s", __FUNCTION__, str);
	return nullptr;
}

graphics::texture_ref resource_manager_internal::get_texture(const std::string &name) noexcept
try {
	graphics::texture_ref ref = textures[name];
	if (ref)
		return ref;

	string filename = get_module_file("textures", name.c_str());
	if (filename.empty())
		throw strprintf("Invalid name '%s'", name.c_str());

	ref = graphics::texture::create_from_file(filename.c_str());
	if (ref) {
		textures[name] = ref;
		return ref;
	}

	throw strprintf("Failed to create texture '%s'", name.c_str());

} catch (const string &text) {
	warn("%s: %s", __FUNCTION__, text.c_str());
	return graphics::texture_ref();
}

graphics::shader_ref resource_manager_internal::get_shader(const char *name) noexcept
try {
	graphics::shader_ref ref = shaders[name];
	if (ref)
		return ref;

	string filename = get_module_file("shaders", name);
	if (filename.empty())
		throw strprintf("Invalid name '%s'", name);

	ref = graphics::shader::create_from_file(filename.c_str());
	if (ref) {
		shaders[name] = ref;
		return ref;
	}

	throw strprintf("Failed to create shader '%s'", name);

} catch (const string &text) {
	warn("%s: %s", __FUNCTION__, text.c_str());
	return graphics::shader_ref();
}

graphics::vertex_buffer_ref resource_manager_internal::sprite_vb() noexcept
try {
	if (!sprite_vb_) {
		graphics::vertex_buffer::data vbd;
		vbd.points.resize(4);
		vbd.coords.resize(4);

		vbd.points[0].set_zero();
		vbd.points[1].set(0.0f, 1.0f, 0.0f);
		vbd.points[2].set(1.0f, 0.0f, 0.0f);
		vbd.points[3].set(1.0f, 1.0f, 0.0f);
		vbd.coords[0].set_zero();
		vbd.coords[1].set(0.0f, 1.0f);
		vbd.coords[2].set(1.0f, 0.0f);
		vbd.coords[3].set(1.0f, 1.0f);

		sprite_vb_ = graphics::vertex_buffer::create(std::move(vbd));
		if (!sprite_vb_)
			throw string("Failed to create vertex buffer");
	}

	return sprite_vb_;

} catch (const string &text) {
	warn("%s: %s", __FUNCTION__, text.c_str());
	return graphics::vertex_buffer_ref();
}

font_ref resource_manager_internal::get_font(const std::string &name_base, int size, int outline_size) noexcept
try {
	stringstream str;
	str << name_base;
	str << ":";
	str << to_string(size);
	str << ":";
	str << to_string(outline_size);
	string name = std::move(str.str());

	font_ref ref = fonts[name];
	if (ref)
		return ref;

	string filename = get_module_file("fonts", name_base);
	if (filename.empty())
		throw strprintf("Invalid name '%s'", name_base.c_str());

	ref = font::create_from_file(filename.c_str(), size, outline_size);
	if (ref) {
		fonts[name] = ref;
		return ref;
	}

	throw strprintf("Failed to create font '%s'", name_base.c_str());

} catch (const string &text) {
	warn("%s: %s", __FUNCTION__, text.c_str());
	return font_ref();
}

void resource_manager_internal::reset() noexcept
{
	textures.clear();
	fonts.clear();
}

/* ----------------------------------------------------------------------- */
} // namespace crpg
