#pragma once

#include <memory>
#include "graphics/graphics.hpp"
#include "font.hpp"
#include "export.h"

namespace crpg {

/* ------------------------------------- */

EXPORT std::string        get_module_file(const char *type, const char *name) noexcept;
static inline std::string get_module_file(const char *type, const std::string &name) noexcept
{
	return get_module_file(type, name.c_str());
}

/* ------------------------------------- */

class EXPORT resource_manager {
protected:
	static resource_manager *current;

public:
	virtual ~resource_manager();

	static std::unique_ptr<resource_manager> create() noexcept;
	static inline resource_manager          *get() noexcept { return current; }

	virtual graphics::texture_ref       get_texture(const std::string &name) noexcept = 0;
	virtual graphics::shader_ref        get_shader(const char *name) noexcept         = 0;
	virtual graphics::vertex_buffer_ref sprite_vb() noexcept                          = 0;

	virtual font_ref get_font(const std::string &name, int size, int outline_size) noexcept = 0;

	virtual void reset() noexcept = 0;
};

namespace resources {

inline void free() noexcept
{
	return resource_manager::get()->reset();
}

inline graphics::texture_ref texture(const std::string &name) noexcept
{
	return resource_manager::get()->get_texture(name);
}

inline graphics::shader_ref shader(const char *name) noexcept
{
	return resource_manager::get()->get_shader(name);
}

inline graphics::vertex_buffer_ref sprite_vb() noexcept
{
	return resource_manager::get()->sprite_vb();
}

inline font_ref font(const std::string &name, int size, int outline_size = 0) noexcept
{
	return resource_manager::get()->get_font(name, size, outline_size);
}

} // namespace resources

/* ------------------------------------- */
} // namespace crpg
