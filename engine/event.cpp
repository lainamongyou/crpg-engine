#include "utility/time.hpp"
#include "event.hpp"

namespace crpg {

event::~event() noexcept
{
	auto *em = event_manager::get();
	if (em && em->current_event == this) {
		em->on_event_deactivate(this);
		em->current_event = nullptr;
		em->on_event_stop();
	}

	remove();
}

void event::remove() noexcept
{
	event_is_active = false;

	if (pprev_next) {
		if (next)
			next->pprev_next = pprev_next;
		*pprev_next = next;
	}

	pprev_next = nullptr;
	next       = nullptr;
}

void event::set_event_pos(pos_t pos) noexcept
{
	pos_ = pos;
	on_event_step(pos);
}

void event::on_event_tick(float) noexcept {}
void event::on_event_step(pos_t pos) noexcept
{
	if (!next_step_count || (++step_count == next_step_count)) {
		next_step_count = 0;
		step_count      = 0;

		actions[pos]();
	} else {
		--pos_;
	}
}

void event::wait(int ms) noexcept
{
	wait_duration = ms;
	wait_start    = get_time_ms();
}

inline void event::check_wait() noexcept
{
	if (wait_duration) {
		int64_t elapsed = get_time_ms() - wait_start;

		if (elapsed >= wait_duration) {
			wait_duration = 0;
			wait_start    = 0;
			step();
		}
	}
}

/* ========================================================================= */

event_manager *event_manager::current = nullptr;

event_manager::event_manager() noexcept
{
	current = this;
}

event_manager::~event_manager() noexcept
{
	if (current_event)
		current_event->event_is_active = false;

	while (first) {
		event *next       = first->next;
		first->pprev_next = nullptr;
		first->next       = nullptr;
		first             = next;
	}
	while (first_concurrent) {
		event *next                       = first_concurrent->next;
		first_concurrent->event_is_active = false;
		first_concurrent->pprev_next      = nullptr;
		first_concurrent->next            = nullptr;
		first_concurrent                  = next;
	}

	current = nullptr;
}

void event_manager::tick(float seconds) noexcept
{
	bool event_was_active = !!current_event;

	if (current_event && !current_event->active()) {
		on_event_deactivate(current_event);
		current_event = nullptr;
	}

	if (current_event) {
		current_event->on_event_tick(seconds);
		current_event->check_wait();
	} else {
		crpg::event *event = first;
		while (event) {
			if (event->can_activate()) {
				current_event                  = event;
				current_event->event_is_active = true;
				if (!event_was_active)
					on_event_start();
				on_event_activate(current_event);
				event->step();
				break;
			}
			event = event->next;
		}
	}

	crpg::event *event = first_concurrent;
	while (event) {
		crpg::event *next = event->next;

		if (event->active()) {
			event->on_event_tick(seconds);
			event->check_wait();

		} else if (event->can_activate()) {
			event->event_is_active = true;
			event->step();
		}

		event = next;
	}

	if (event_was_active && !current_event)
		on_event_stop();
}

void event_manager::add(crpg::event::type type, crpg::event *event) noexcept
{
	if (type == crpg::event::type::control) {
		event->pprev_next = &first;
		event->next       = first;
		if (first)
			first->pprev_next = &event->next;
		first = event;
	} else {
		event->pprev_next = &first_concurrent;
		event->next       = first_concurrent;
		if (first_concurrent)
			first_concurrent->pprev_next = &event->next;
		first_concurrent = event;
	}
}

void event_manager::on_event_start() noexcept {}
void event_manager::on_event_stop() noexcept {}
void event_manager::on_event_activate(crpg::event *) noexcept {}
void event_manager::on_event_deactivate(crpg::event *) noexcept {}

} // namespace crpg
