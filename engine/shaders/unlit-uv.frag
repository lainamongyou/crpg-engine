#version 450

layout(binding = 0) uniform sampler2D image;

layout(location = 0) in vec2 uv;
layout(location = 0) out vec4 color_out;

void main(void)
{
	color_out = texture(image, uv);
}
