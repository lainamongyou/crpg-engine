#version 450

layout(push_constant) uniform constants
{
	mat4x4 viewproj;
	vec4   color;
};

layout(location = 0) out vec4 color_out;

void main(void)
{
	color_out = color;
}
