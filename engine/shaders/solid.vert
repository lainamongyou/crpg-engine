#version 450

layout(push_constant) uniform constants
{
	mat4x4 viewproj;
	vec4   color;
};

layout(location = 0) in vec4 pos_in;

void main(void)
{
	gl_Position = vec4(pos_in.xyz, 1.0) * viewproj;
}
