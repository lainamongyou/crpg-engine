#pragma once

#include "collidable.hpp"

namespace crpg {

class controller;

class EXPORT pawn : public collidable {
	friend class controller;

	controller *cur_controller = nullptr;

public:
	~pawn() noexcept;

	void set_controller(controller *c) noexcept;

	virtual void on_connect(controller *c) noexcept;
	virtual void on_disconnect(controller *c) noexcept;
};

} // namespace crpg
