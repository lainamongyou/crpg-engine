#pragma once

#include <vector>
#include <string>

#include "math/box.hpp"
#include "types.hpp"
#include "export.h"

namespace crpg {

class entity;
class collidable;

class EXPORT map {
	friend class entity;
	friend class asset_manager_internal;

	static map *current;

	static map *load(const std::string &path) noexcept;

	virtual void update_entity_internal(entity *ent) noexcept = 0;
	virtual void remove_entity_internal(entity *ent) noexcept = 0;

	virtual type::layer_id get_layer_id_internal(const std::string &name) noexcept = 0;

	virtual bool move_collidable_internal(collidable *ent, const math::vec2 &p2) noexcept = 0;
	virtual void get_objects_internal(type::layer_id         id,
	                                  const math::vec2      &pos,
	                                  std::vector<entity *> &objects) noexcept            = 0;
	virtual void get_objects_internal(type::layer_id         id,
	                                  const math::box       &box,
	                                  std::vector<entity *> &objects) noexcept            = 0;
	virtual void frob_internal(type::layer_id id, const math::vec2 &pos) noexcept         = 0;
	virtual void set_layer_visible_internal(type::layer_id id, bool visible) noexcept     = 0;
	virtual void set_layer_opacity_internal(type::layer_id id, float opacity) noexcept    = 0;

	static inline void update_entity(entity *ent) noexcept { get()->update_entity_internal(ent); }
	static inline void remove_entity(entity *ent) noexcept { get()->remove_entity_internal(ent); }

public:
	map() noexcept;
	virtual ~map() noexcept;

	virtual void tick(float seconds) noexcept           = 0;
	virtual void render(const math::box &view) noexcept = 0;

	static inline map *get() noexcept { return current; }

	static inline type::layer_id get_layer_id(const std::string &name) noexcept
	{
		return current->get_layer_id_internal(name);
	}

	static inline bool move_collidable(collidable *ent, const math::vec2 &p2) noexcept
	{
		return current->move_collidable_internal(ent, p2);
	}

	static inline void get_objects_at_pos(type::layer_id         id,
	                                      const math::vec2      &pos,
	                                      std::vector<entity *> &objects) noexcept
	{
		current->get_objects_internal(id, pos, objects);
	}

	static inline void get_objects_in_box(type::layer_id         id,
	                                      const math::box       &box,
	                                      std::vector<entity *> &objects) noexcept
	{
		current->get_objects_internal(id, box, objects);
	}

	static inline void frob(type::layer_id id, const math::vec2 &pos) noexcept { current->frob_internal(id, pos); }

	static inline void set_layer_visible(const std::string &layer, bool visible) noexcept
	{
		current->set_layer_visible_internal(current->get_layer_id_internal(layer), visible);
	}

	static inline void set_layer_visible(type::layer_id id, bool visible) noexcept
	{
		current->set_layer_visible_internal(id, visible);
	}

	static inline void set_layer_opacity(const std::string &layer, float opacity) noexcept
	{
		current->set_layer_opacity_internal(current->get_layer_id_internal(layer), opacity);
	}

	static inline void set_layer_opacity(type::layer_id id, float opacity) noexcept
	{
		current->set_layer_opacity_internal(id, opacity);
	}
};

} // namespace crpg
