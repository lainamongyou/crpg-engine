#pragma once

#include <functional>
#include <memory>
#include <SDL_events.h>
#include "export.h"

namespace crpg {

namespace math {
struct vec2;
};

class resource_manager;

class EXPORT app_base {
public:
	virtual ~app_base() = default;
};

class EXPORT app : public app_base {
	friend struct game_manager;
	friend class asset_manager;

	void tick_all(float seconds) noexcept;

	virtual const char *name() const noexcept              = 0;
	virtual void        startup(int &cx, int &cy) noexcept = 0;

	virtual void init() noexcept;
	virtual void prerender(int cx, int cy) noexcept;
	virtual void tick(float seconds) noexcept;
	virtual void post_tick(float seconds) noexcept;
	virtual void render() noexcept;

	virtual void quit_event() noexcept;

	static app *current;
	bool        quitting_ = false;

	std::function<void(bool)> map_callback;
	uint64_t                  next_map = 0;

	std::unique_ptr<crpg::resource_manager> rm;

protected:
	bool center_view = false;
	int  center_cx   = 0;
	int  center_cy   = 0;

public:
	app() noexcept;
	~app() noexcept;

	inline bool        quitting() const noexcept { return quitting_; }
	static inline void quit() noexcept
	{
		if (current)
			current->quitting_ = true;
	}

	static inline app *get() noexcept { return current; }
};

typedef app *(*create_app_cb)();
} // namespace crpg

#if !defined(NOT_GAME_MODULE) && !defined(STATIC_ONLY)
extern "C"
#ifdef _WIN32
        __declspec(dllexport)
#endif
                crpg::app *create_app() noexcept;
#endif
