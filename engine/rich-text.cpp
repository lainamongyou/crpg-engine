#include "utility/logging.hpp"
#include "rich-text.hpp"
#include "resources.hpp"

namespace crpg {

inline font_ref rich_text::get_font() noexcept
{
	std::string name = cur.face;

	font_changed = false;

	if (cur.bold && !cur.italic) {
		name += "-Bold";
	} else if (!cur.bold && cur.italic) {
		name += "-Italic";
	} else if (cur.bold && cur.italic) {
		name += "-BoldItalic";
	} else {
		name += "-Regular";
	}

	font_ref ref = resources::font(name + ".ttf", cur.size, cur.outline_size);
	if (ref)
		return ref;

	ref = resources::font(name + ".otf", cur.size, cur.outline_size);
	if (!ref) {
		error("%s: %s", __func__, strprintf("Invalid font resource: %s", name.c_str()).c_str());
		ref = cur.font;
	}

	return ref;
}

rich_text::rich_text(const char_cb &cb,
                     const char    *def_face_resource,
                     int            def_size,
                     uint32_t       def_rgb,
                     uint8_t        def_alpha,
                     bool           def_bold,
                     bool           def_italic,
                     int            def_outline_size,
                     uint32_t       def_outline_rgb) noexcept
        : cb(cb),
          cur(def_face_resource, def_size, def_bold, def_italic, def_rgb, def_alpha, def_outline_size, def_outline_rgb),
          def(def_face_resource, def_size, def_bold, def_italic, def_rgb, def_alpha, def_outline_size, def_outline_rgb)
{
	def.font = cur.font = get_font();
	valid_              = !!def.font;
}

float rich_text::get_line_height() noexcept
{
	const char *last_text  = next_text;
	int         last_ch    = cur_ch;
	int         last_prev  = prev;
	font_state  last_state = cur;

	float height = cur.font->height();

	while (font::next_utf32(next_text, cur_ch, prev)) {
		if (cur_ch == '\n' || cur_ch == 0)
			break;

		if (cb(*this)) {
			if (font_changed) {
				cur.font = get_font();

				float new_height = cur.font->height();
				if (new_height > height)
					height = new_height;
			}
		}
	}

	next_text = last_text;
	cur_ch    = last_ch;
	prev      = last_prev;
	cur       = last_state;

	return height;
}

float rich_text::get_line_ascender() noexcept
{
	const char *last_text  = next_text;
	int         last_ch    = cur_ch;
	int         last_prev  = prev;
	font_state  last_state = cur;

	float ascender = cur.font->ascender();

	while (font::next_utf32(next_text, cur_ch, prev)) {
		if (cur_ch == '\n' || cur_ch == 0)
			break;

		if (cb(*this)) {
			if (font_changed) {
				cur.font = get_font();

				float new_ascender = cur.font->ascender();
				if (new_ascender > ascender)
					ascender = new_ascender;
			}
		}
	}

	next_text = last_text;
	cur_ch    = last_ch;
	prev      = last_prev;
	cur       = last_state;

	return ascender;
}

int rich_text::get_total_drawn_characters(const char *text) noexcept
{
	if (!valid_ || !text || !*text)
		return 0;

	reset();
	prev      = 0;
	next_text = text;

	int total = 0;

	while (font::next_utf32(next_text, cur_ch, prev)) {
		if (cb(*this)) {
			total++;
		}
	}

	reset();
	return total;
}

void rich_text::draw(const char *text, float x, float y, int count, bool with_ascender) noexcept
{
	if (!valid_ || !text || !*text || count == 0)
		return;

	reset();
	prev      = 0;
	next_text = text;

	cur.font->draw_char_init();

	float new_x = x;
	float new_y = y;
	int   drawn = 0;

	if (with_ascender)
		new_y += get_line_ascender();

	while (font::next_utf32(next_text, cur_ch, prev)) {
		bool advance = cb(*this);

		if (advance && cur_ch) {
			if (font_changed)
				cur.font = get_font();

			if (cur_ch == '\n') {
				new_y += get_line_height();
				new_x = x;
			} else {
				uint32_t alpha         = (uint32_t)cur.alpha << 24;
				uint32_t color         = alpha | (cur.rgb & 0xFFFFFF);
				uint32_t outline_color = alpha | (cur.outline_rgb & 0xFFFFFF);

				if (color_override_enabled) {
					color         = color_override;
					outline_color = color_override;
				}

				cur.font->draw_char(cur_ch, prev, x, new_x, new_y, color, outline_color);
			}

			if (count != -1 && ++drawn == count)
				break;
		} else {
			prev = 0;
		}
	}
}

static inline bool is_actual_wspace(int ch) noexcept
{
	return ch != '\n' && iswspace((wint_t)ch);
}

std::string rich_text::word_wrap(const char *text, float wrap_width) noexcept
{
	if (!valid_ || !text || !*text)
		return std::string();

	reset();
	prev      = 0;
	next_text = text;

	std::string out;

	const char *start_text     = text;
	const char *last_space     = nullptr;
	float       x              = 0.0f;
	float       y              = 0.0f;
	bool        prev_was_space = false;

#define write_text()                                                                                                   \
	do {                                                                                                           \
		while (start_text < next_text) {                                                                       \
			char new_ch = *(start_text++);                                                                 \
			if (!new_ch)                                                                                   \
				break;                                                                                 \
			out.push_back(new_ch);                                                                         \
		}                                                                                                      \
	} while (false)

#define reset_pos()                                                                                                    \
	do {                                                                                                           \
		last_space     = nullptr;                                                                              \
		prev_was_space = false;                                                                                \
		x              = 0.0f;                                                                                 \
		prev           = 0;                                                                                    \
	} while (false)

	while (font::next_utf32(next_text, cur_ch, prev)) {
		bool advance = cb(*this);

		if (advance && cur_ch) {
			if (font_changed)
				cur.font = get_font();

			font::glyph *g = cur.font->advance_char(cur_ch, prev, 0.0f, x, y);
			if (g)
				x += g->advance_x;

			bool ignore_space = prev_was_space;
			prev_was_space    = false;

			if (text != start_text) {
				if (is_actual_wspace(cur_ch)) {
					if (!ignore_space)
						last_space = text;
					prev_was_space = true;
				}
			}

			if (cur_ch == '\n') {
				write_text();
				reset_pos();
			} else if (x > wrap_width) {
				next_text = last_space ? last_space : text;

				write_text();
				out.push_back('\n');

				while (font::next_utf32(next_text, cur_ch, prev)) {
					start_text = text;

					if (!is_actual_wspace(cur_ch)) {
						/* we want the next primary
						 * while loop to start at this
						 * character. */
						next_text--;
						prev = 0;
						break;
					}

					text = next_text;
				}

				if (cur_ch == 0)
					break;

				reset_pos();
			}
		} else {
			prev = 0;
		}

		text = next_text;
	}

	write_text();
#undef reset_pos
#undef write_text

	return out;
}

std::deque<rich_text::special_command> &&rich_text::get_special_commands(const char *text) noexcept
{
	if (!valid_ || !text || !*text)
		return std::move(special_commands);

	reset();
	prev      = 0;
	cur_pos   = 0;
	next_text = text;

	building_special_commands_ = true;

	while (font::next_utf32(next_text, cur_ch, prev)) {
		if (cb(*this)) {
			cur_pos++;
		}
	}

	building_special_commands_ = false;

	reset();

	return std::move(special_commands);
}

void rich_text::set_face(const char *face_resource) noexcept
{
	if (cur.face != face_resource) {
		cur.face     = face_resource;
		font_changed = true;
	}
}

void rich_text::set_size(int size) noexcept
{
	if (cur.size != size) {
		cur.size     = size;
		font_changed = true;
	}
}

void rich_text::set_alpha(uint8_t alpha) noexcept
{
	cur.alpha = alpha;
}

void rich_text::set_color(uint32_t rgb) noexcept
{
	cur.rgb = rgb;
}

void rich_text::set_outline_size(int outline_size) noexcept
{
	if (cur.outline_size != outline_size) {
		cur.outline_size = outline_size;
		font_changed     = true;
	}
}

void rich_text::set_outline_color(uint32_t rgb) noexcept
{
	cur.outline_rgb = rgb;
}

void rich_text::toggle_bold() noexcept
{
	cur.bold     = !cur.bold;
	font_changed = true;
}

void rich_text::toggle_italic() noexcept
{
	cur.italic   = !cur.italic;
	font_changed = true;
}

int rich_text::next_ch() noexcept
{
	int prev = 0;

	font::next_utf32(next_text, cur_ch, prev);
	return cur_ch;
}

int rich_text::peek_ch() noexcept
{
	int         prev = 0;
	const char *text = next_text;

	font::next_utf32(text, cur_ch, prev);
	return cur_ch;
}

void rich_text::reset() noexcept
{
	cur          = def;
	font_changed = false;
}

void rich_text::reset_face() noexcept
{
	if (cur.face != def.face) {
		cur.face     = def.face;
		font_changed = true;
	}
}

void rich_text::reset_size() noexcept
{
	if (cur.size != def.size) {
		cur.size     = def.size;
		font_changed = true;
	}
}

void rich_text::reset_alpha() noexcept
{
	cur.alpha = def.alpha;
}

void rich_text::reset_color() noexcept
{
	cur.rgb = def.rgb;
}

void rich_text::reset_style() noexcept
{
	if (cur.bold != def.bold || cur.italic != def.italic) {
		cur.bold     = def.bold;
		cur.italic   = def.italic;
		font_changed = true;
	}
}

void rich_text::reset_outline_size() noexcept
{
	if (cur.outline_size != def.outline_size) {
		cur.outline_size = def.outline_size;
		font_changed     = true;
	}
}

void rich_text::reset_outline_color() noexcept
{
	cur.outline_rgb = def.outline_rgb;
}

void rich_text::add_special_command(const std::string &command, const std::string &params) noexcept
{
	if (!building_special_commands_)
		return;

	special_command sc;
	sc.command = command;
	sc.params  = params;
	sc.pos     = cur_pos;

	special_commands.push_back(std::move(sc));
}

} // namespace crpg
