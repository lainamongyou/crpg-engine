cmake_minimum_required(VERSION 3.20)

find_package(Vulkan REQUIRED COMPONENTS glslc)

if(NOT MSVC)
	add_compile_options("$<$<CONFIG:DEBUG>:-DDEBUG=1;-D_DEBUG=1>")
endif()

if(MSVC)
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /std:c++latest /wd4458 /wd4459 /wd5054")
endif()

set(BUILD_STATIC FALSE CACHE BOOL "Builds all-static version")
if(BUILD_STATIC)
	set(SHARED_LIB STATIC)
	set(MODULE_LIB STATIC)
else()
	set(SHARED_LIB SHARED)
	set(MODULE_LIB MODULE)
endif()

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_CURRENT_SOURCE_DIR}/crpg-engine/cmake/")
include(helpers)

add_subdirectory(crpg-engine)
